#include <iostream>
#include <getopt.h>
#include <iostream>
#include <cctype>
#include <string>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <sstream>

#define no_argument 0
#define required_argument 1
#define optional_argument 2

using namespace std;

int dlzkaSpravy( bool argLogin, bool argUid, bool argL, bool argU, bool argG, bool argN, bool argH, bool argS, int indexLogins, int indexUids);
//string posielanaSprava( bool argLogin, bool argUid, bool argL, bool argU, bool argG, bool argN, bool argH, bool argS, int indexLogins, int indexUids, string logins[], string uids[]);

int main(int argc, char * argv[])
{
    const struct option longopts[] =
    {
        {"hostname",   no_argument,        0, 'h'},
        {"port",      no_argument,        0, 'p'},
        {"login",     required_argument,  0, 'l'},
        {"uid",     required_argument,  0, 'u'},
        {"name",     required_argument,  0, 'L'},
        {"UID",     required_argument,  0, 'U'},
        {"GID",     required_argument,  0, 'G'},
        {"gecos",     required_argument,  0, 'N'},
        {"home_dir",     required_argument,  0, 'H'},
        {"shell",     required_argument,  0, 'H'},
        {0,0,0,0,},
    };

    int index;
    int iarg=0;

    string hostname;
    string port;
    string logins [100];//= new string [100];
    string uids [100];//= new string [100];

    bool argHost = false;
    bool argPort = false;
    bool argLogin = false;
    bool argUid = false;
    bool argLogin2 = false;
    bool argUid2 = false;
    bool argL = false;
    bool argU = false;
    bool argG = false;
    bool argN = false;
    bool argH = false;
    bool argS = false;
    string sprava = "";

    int indexLogins=0 , indexUids=0;

    //turn off getopt error message
    opterr=1;

    while (iarg != -1)
    {
        iarg = getopt_long(argc, argv, "-h:-p:-l:-u:LUGNHS", longopts, &index);

        switch (iarg)
        {
            case 'h':
                //cout << "-h" << endl;
                if(optarg)
                {
                    if(argHost)
                    {
                        fprintf(stderr, "CHYBA");
                    }
                    else
                    {
                        //cout << optarg << endl;
                        hostname=optarg;
                        argHost=true;
                    }
                }
                else
                {
                    fprintf(stderr, "Chybajuci argument v -h");
                    return(-1);
                }
                break;

            case 'p':
                //cout << "-p" << endl;
                if(optarg)
                {
                    if(argPort)
                    {
                        fprintf(stderr, "CHYBA");
                    }
                    else
                    {
                        //cout << optarg << endl;
                        if ((atoi(optarg) != 0 ) || strcmp(optarg,"0") ==0)
                        {
                            port= optarg;
                        }
                        else
                        {
                            fprintf(stderr, "Pri -p NEZADALI ste cislo\n");
                            return(-1);
                        }
                        argPort =true;
                    }
                }
                else
                {
                    fprintf(stderr, "Chybajuci argument v -p");
                    return(-1);
                }
                break;

            case 'l':
                //cout << "-l" << endl;
                if(optarg)
                {
                    if(argLogin || argLogin2)
                    {
                        fprintf(stderr, "CHYBA");
                        return(-1);
                    }
                    else
                    {
                        logins[indexLogins] = optarg;
                        //cout << indexLogins << ". login :" << logins [indexLogins] << endl;
                        indexLogins++;
                        if(argUid)
                        {
                            argUid2 = true;
                        }
                        argLogin = true;
                        argUid = false;
                    }
                }
                else
                {
                    fprintf(stderr, "Chybajuci argument v -l");
                    return(-1);
                }
                break;
            case 'u':
                //cout << "-u" << endl;
                if(optarg)
                {
                    if(argUid || argUid2)
                    {
                        fprintf(stderr, "CHYBA");
                        return(-1);
                    }
                    else
                    {
                        //if ((atoi(optarg) != 0 ) || strcmp(optarg,"0") == 0)
                        {
                            uids[indexUids] = optarg;
                        }
                        /*else
                        {
                            fprintf(stderr, "Pri -u NEZADALI ste cislo");
                            return(-1);
                        }*/
                        //cout << indexUids << ".uid: " << uids [indexUids] << endl;
                        indexUids++;
                        if(argLogin)
                        {
                            argLogin2 = true;
                        }
                        argUid = true;
                        argLogin = false;
                    }
                }
                else
                {
                    fprintf(stderr, "Chybajuci argument v -u");
                    return(-1);
                }
                break;

            case 'L':
                if(argL)
                {
                    fprintf(stderr, "CHYBA");
                    return(-1);
                }
                else
                {
                   //cout << "-L" << endl;
                    argL = true;
                    sprava = sprava + " " + "L";
                    break;
                }


            case 'U':
                if(argU)
                {
                    fprintf(stderr, "CHYBA");
                    return(-1);
                }
                else
                {
                    //cout << "-U" << endl;
                    argU = true;
                    sprava = sprava + " " + "U";
                    break;
                }

            case 'G':
                if(argG)
                {
                    fprintf(stderr, "CHYBA");
                    return(-1);
                }
                else
                {
                    //cout << "-G" << endl;
                    argG = true;
                    sprava = sprava + " " + "G";
                    break;
                }
            case 'N':
                if(argN)
                {
                    fprintf(stderr, "CHYBA");
                    return(-1);
                }
                else
                {

                    //cout << "-N" << endl;
                    argN =true;
                    sprava = sprava + " " + "N";
                    break;
                }

            case 'H':
                if(argH)
                {
                    fprintf(stderr, "CHYBA");
                    return(-1);
                }
                else
                {
                    //cout << "-H" << endl;
                    argH = true;
                    sprava = sprava + " " + "H";
                    break;
                }

            case 'S':
                if(argS)
                {
                    fprintf(stderr, "CHYBA");
                    return(-1);
                }
                else
                {
                    //cout << "-S" << endl;
                    argS = true;
                    sprava = sprava + " " + "S";
                    break;
                }

            case 1:
                if(argLogin)
                {
                    logins[indexLogins] = optarg;
                    //cout << indexLogins << ". login :" << logins [indexLogins] << endl;
                    indexLogins++;
                    continue;
                }
                else if(argUid)
                {

                    //if ((atoi(optarg) != 0 ) || strcmp(optarg,"0") == 0)
                    {
                        uids[indexUids] = optarg;

                    }
                   /* else
                    {
                        fprintf(stderr, "Pri -u NEZADALI ste cislo");
                        return(-1);
                    }*/
                    //cout << indexUids << ".uid: " << uids [indexUids] << endl;
                    indexUids++;
                    continue;
                }
                break;
            case -1:
                break;
            default:
                fprintf(stderr, "Nespravny parameter");
                return(-1);
        }
    }

    if(argHost && argPort && (argLogin || argUid))
    {

        //cout << "param OK" << endl;
        //cout << atoi(port.c_str()) << endl;

        //convert int to string

        string posielanaSprava = "";
        int celaDlzka = dlzkaSpravy(  argLogin,  argUid,  argL,  argU,  argG,  argN,  argH,  argS,  indexLogins,  indexUids);


        if(argLogin)
        {
            posielanaSprava = "L";

            std::stringstream iLog;
            iLog << indexLogins;
            posielanaSprava = posielanaSprava + " " + iLog.str();

            std::stringstream pPar;
            pPar << (celaDlzka - indexLogins);
            posielanaSprava = posielanaSprava + " " + pPar.str();

            for(int i=0;i<indexLogins;i++)
            {
                posielanaSprava = posielanaSprava + " " + logins[i] ;
            }
        }
        if(argUid)
        {
            posielanaSprava = "U";

            std::stringstream iUid;
            iUid << indexUids;
            posielanaSprava = posielanaSprava + " " + iUid.str();

            std::stringstream pPar;
            pPar << (celaDlzka - indexUids);
            posielanaSprava = posielanaSprava + " " + pPar.str();

            for(int i=0;i<indexUids;i++)
            {
                posielanaSprava = posielanaSprava + " " + uids[i] ;
            }
        }

        posielanaSprava = posielanaSprava + sprava;

        //cout << posielanaSprava << endl;


        int sockfd, newfd;
        struct sockaddr_in clientAddr;
        struct hostent *hptr;               //uklada sa adresa o pripojeni
        char message[10000] = "";

        if ( (sockfd = socket(PF_INET, SOCK_STREAM, 0 ) ) < 0)
        {			// create socket
            fprintf(stderr, "error on socket");					// socket error
            return -1;
        }

        clientAddr.sin_family = PF_INET;							//set protocol family to Internet
        clientAddr.sin_port = htons(atoi(port.c_str()));				// set port no.

        if ( (hptr =  gethostbyname(hostname.c_str()) ) == NULL)
        {
            fprintf(stderr, "gethostname error: %s", hostname.c_str());
            return -1;
        }

        memcpy( &clientAddr.sin_addr, hptr->h_addr, hptr->h_length);

        if (connect (sockfd, (struct sockaddr *)&clientAddr, sizeof(clientAddr) ) < 0 )
        {
            fprintf(stderr, "error on connect");
            return -1;		// connect error
        }
        if ( write(sockfd, posielanaSprava.c_str(), strlen(posielanaSprava.c_str()) +1) < 0 )
        {														// send message to server
            fprintf(stderr, "error on write");
            return -1;		//  write error
        }
        if ( ( newfd = read(sockfd, message, sizeof(message) ) ) <0) // read message from server
        {  // read message from server
            fprintf(stderr, "error on read");
            return -1; //  read error
        }

        fprintf (stdout, "%s\n", message);  // print message to screen
        // close connection, clean up socket

        if (close(sockfd) < 0)
        {
            fprintf(stderr, "error on close");   // close error
            return -1;
        }
    }
    else
    {
        fprintf(stderr, "param NOT OK");
        return(-1);
    }
    return 0;
}

int dlzkaSpravy( bool argLogin, bool argUid, bool argL, bool argU, bool argG, bool argN, bool argH, bool argS, int indexLogins, int indexUids)
{
    int dlzka=0;

    if(argLogin)
        dlzka=indexLogins;
    if(argUid)
        dlzka=indexUids;
    if(argL)
        dlzka++;
    if(argU)
        dlzka++;
    if(argG)
        dlzka++;
    if(argN)
        dlzka++;
    if(argH)
        dlzka++;
    if(argS)
        dlzka++;

    return dlzka;
}











