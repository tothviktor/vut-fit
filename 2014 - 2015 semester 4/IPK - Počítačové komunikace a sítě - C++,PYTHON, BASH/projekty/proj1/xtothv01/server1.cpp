#include <iostream>
#include <cctype>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <sstream>
#include <fstream>
#include <pwd.h>


using namespace std;

template <typename T>
string NumberToString(T pNumber)
{
 ostringstream oOStrStream;
 oOStrStream << pNumber;
 return oOStrStream.str();
}


int main(int argc, char* argv[])
{
    if(argc == 3)
    {
        if ( strcmp(argv[1],"-p") == 0 && ((atoi(argv[2]) != 0 ) || strcmp(argv[2],"0") == 0))
        {
            //cout << "Parametre su v poriadku" << endl;
            string port = argv[2];

            int sockfd, newfd;

            struct sockaddr_in clientAddr;
            //unsigned int recvLen;
            socklen_t addr_size;

            char message[10000];

            fd_set read_set;
            //struct timeval tv;

            // initialize the fd set
            FD_ZERO(&read_set);

            // prepare the address struct for the first client
            bzero(&clientAddr,sizeof(clientAddr));                          //zero the struct
            clientAddr.sin_family = AF_INET;                              //address family (ipv4)
            clientAddr.sin_port = htons(atoi(port.c_str()));    //sets port to network byte order
            clientAddr.sin_addr.s_addr = INADDR_ANY;            // set IP addr to any interface
            addr_size = sizeof(clientAddr);

            if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) // create socket
            {
                fprintf(stdout, "Cannot create socket for client 0.\n"); // socket error
                fprintf(stdout, "Terminating program\n\n");
                exit(1);
            }
            else
            {
                //fprintf(stdout, "Socket established for client 0\n");
            }

            if (bind(sockfd, (struct sockaddr *)&clientAddr, sizeof(clientAddr)) < 0)
            {
                fprintf (stdout, "Binding failed for client 0\n\n");            // bind error
                exit (1);
            }
            else
            {
                //fprintf (stdout, "Binding successful for client 0\n");
            }
            if (listen(sockfd, 5))
            {
                printf ("error on listen\n");                       // listen error
                return -1;
            }

            // ERROR HAPPENS ON THE NEXT LINE
            while (true)
            {
                // accepting new connection request from client, socket id for the new connection is returned in newfd
                if ((newfd = accept(sockfd, (struct sockaddr *)&clientAddr, &addr_size)) < 0)
                {
                    fprintf(stdout, "Error accepting inbound data from client 0\n");
                    exit(EXIT_FAILURE);
                }
                else
                {
                    //fprintf(stdout, "\tSuccessfully accepted inbound connection from client 0\n");
                    int pid = fork();
                    if (pid < 0)
                    {
                        fprintf(stderr, "Error forking process!\n");
                        return -1;
                    }
                    if (pid == 0)
                    {

                        bzero(&clientAddr,sizeof(clientAddr));

                        if( read(newfd, message, sizeof(message) ) <0)    // read message from client
                        {
                            printf("error on read\n");         //  read error
                            return -1;
                        }

                        //cout << "length of message is " << strlen(message) << endl;
                        //cout << "message from client is: " << message << endl;

                        string logins [100];
                        string uids [100];

                        bool log = false;
                        bool uid = false;

                        std::stringstream stream(message);
                        std::string  word;

                        stream >> word;
                        //cout << word << endl;
                        if (word == "L")
                            log = true;
                        if (word == "U")
                            uid = true;

                        stream >> word;
                        //cout << word << endl;
                        int pocLogOrUids = atoi(word.c_str());

                        stream >> word;
                        // cout << word << endl;
                        int pocParam = atoi(word.c_str());

                        if (log)
                        {
                            for (int i = 0; i < pocLogOrUids; i++)
                            {
                                stream >> word;
                                logins[i] = word;
                                //cout << logins[i] << endl;
                            }
                        }
                        if(uid)
                        {
                            for (int i = 0; i < pocLogOrUids; i++)
                            {
                                stream >> word;
                                uids[i] = word;
                                //cout << uids[i] << endl;
                            }
                        }

                        string params[6] = {"0"};

                        for (int i = 0; i < pocParam; i++)
                        {
                            stream >> word;
                            //cout << word << endl;

                            if(word == "L")
                                params[i] = "L";
                            if(word == "U")
                                params[i] = "U";
                            if(word == "G")
                                params[i] = "G";
                            if(word == "N")
                                params[i] = "N";
                            if(word == "H")
                                params[i] = "H";
                            if(word == "S")
                                params[i] = "S";

                        }



                        string pwName,pwUid,pwGid,pwGecos,pwDir, pwShell;
                        stringstream longPwUid, longPwGid;

                        struct passwd * my_passwd;
                        string logOrUid;
                        string posielanaSprava = "";
                        if(log)
                        {
                            for (int i = 0; i < pocLogOrUids; i++)
                            {
                                logOrUid = logins[i];
                                my_passwd = getpwnam(logOrUid.c_str());
                                //cout <<"\n --------------\n"<< logins[i] << endl;

                                if (my_passwd == NULL)
                                {
                                    posielanaSprava = posielanaSprava + "Chyba: neznamy login " + logOrUid + " ";
                                }
                                else
                                {
                                    pwName = my_passwd->pw_name;
                                    pwUid = NumberToString((long)my_passwd->pw_uid);
                                    pwGid = NumberToString((long)my_passwd->pw_gid);
                                    pwGecos =  my_passwd->pw_gecos;
                                    pwDir = my_passwd->pw_dir;
                                    pwShell = my_passwd->pw_shell;

                                    for (int i = 0; i < pocParam; i++)
                                    {

                                        if(params[i] == "L")
                                            posielanaSprava = posielanaSprava + pwName + " ";
                                        if(params[i] == "U")
                                            posielanaSprava = posielanaSprava + pwUid + " ";
                                        if(params[i] == "G")
                                            posielanaSprava = posielanaSprava + pwGid + " ";
                                        if(params[i] == "N")
                                            posielanaSprava = posielanaSprava + pwGecos + " ";
                                        if(params[i] == "H")
                                            posielanaSprava = posielanaSprava + pwDir + " ";
                                        if(params[i] == "S")
                                            posielanaSprava = posielanaSprava + pwShell + " ";
                                    }
                                }
                                posielanaSprava.erase(posielanaSprava.end()-1,posielanaSprava.end());
                                posielanaSprava = posielanaSprava + "\n";

                            }

                        }
                        if(uid)
                        {
                            for (int i = 0; i < pocLogOrUids; i++)
                            {
                                logOrUid = uids[i];

                                if ((atoi(logOrUid.c_str()) != 0 ) || strcmp(logOrUid.c_str(),"0") ==0)
                                {
                                    my_passwd = getpwuid(atoi(logOrUid.c_str()));
                                }
                                else
                                {
                                    my_passwd = NULL;
                                }

                                //cout << uids[i] << endl;

                                if (my_passwd == NULL)
                                {
                                    posielanaSprava = posielanaSprava + "Chyba: neznamy UID " + logOrUid + " ";
                                }
                                else
                                {
                                    pwName = my_passwd->pw_name;
                                    pwUid = NumberToString((long)my_passwd->pw_uid);
                                    pwGid = NumberToString((long)my_passwd->pw_gid);
                                    pwGecos =  my_passwd->pw_gecos;
                                    pwDir = my_passwd->pw_dir;
                                    pwShell = my_passwd->pw_shell;

                                    for (int i = 0; i < pocParam; i++)
                                    {

                                        if(params[i] == "L")
                                            posielanaSprava = posielanaSprava + pwName + " ";
                                        if(params[i] == "U")
                                            posielanaSprava = posielanaSprava + pwUid + " ";
                                        if(params[i] == "G")
                                            posielanaSprava = posielanaSprava + pwGid + " ";
                                        if(params[i] == "N")
                                            posielanaSprava = posielanaSprava + pwGecos + " ";
                                        if(params[i] == "H")
                                            posielanaSprava = posielanaSprava + pwDir + " ";
                                        if(params[i] == "S")
                                            posielanaSprava = posielanaSprava + pwShell + " ";
                                    }
                                }
                                posielanaSprava.erase(posielanaSprava.end()-1,posielanaSprava.end());
                                posielanaSprava = posielanaSprava + "\n";
                            }
                        }
                        posielanaSprava.erase(posielanaSprava.end()-1,posielanaSprava.end());

                        if ( write(newfd, posielanaSprava.c_str(), strlen(posielanaSprava.c_str()) ) < 0 ) // echo message back
                        {
                            fprintf(stderr, "error on write");
                            return -1; //  write error
                        }
                        if (close(newfd) < 0) // close connection, clean up sockets
                        {
                            fprintf(stderr, "error on close");
                            return -1;
                        }
                    }
                }

            }  // not reach below
            if (close(sockfd) < 0)
            {
                fprintf(stderr, "error on close");
                return -1;
            }
        return 0;
        }
        else
        {
            fprintf(stderr, "Parametre NIE su v poriadku");
            return -1;
        }
    }
    else
	{
		fprintf(stderr, "Parametre NIE su v poriadku");
        return -1;
	}
	return 0;
}
