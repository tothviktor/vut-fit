#!/usr/bin/env python

import argparse
import sys
import getopt
import os
import re

from urlparse import urlparse
import socket
import datetime
import time

# NACITANIE ARGUMENTOV
ap = argparse.ArgumentParser()
ap.add_argument("URI",help="display the square of a given number")
opts = ap.parse_args()

URI = opts.URI
URI = urlparse(URI)

scheme = URI.scheme
netloc = URI.netloc
port = URI.port
path = URI.path

# CAS -> POTREBNE PRI UKLADANIE SUBOROV -> cast mena
localtime = time.localtime(time.time())
today = str(datetime.datetime.today())[:-7]

# vyfiltrujem IP ADRESU
dlzkaAdresy = netloc.find(':')
if dlzkaAdresy != -1:
	netlocCisty = netloc[:dlzkaAdresy]
else:
	netlocCisty = netloc
	
#--------------------------------------------------------
if scheme != "http" or netloc == None or path == None:
	sys.exit(-1)
if port == None:
	port = 10000

TCP_IP = netlocCisty
TCP_PORT = port
BUFFER_SIZE = 128

# POSIELANA SPRAVA NA  ---- SERVER ----
MESSAGE = str("GET "+path +" HTTP/1.1\n"+
				"Host: "+netloc+"\r\n")

# Ukladanie POSIELANEJ spravy -> out.log
try:
	myFileOutLog = open("ipkHttpClient-"+today+".out.log", "w")
	myFileOutLog.write(MESSAGE)
	myFileOutLog.close()
except:
	sys.out.write("Subor sa nepodarilo otvorit")
				

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
	s.connect((TCP_IP, TCP_PORT))
except:
	sys.stderr.write("nespravne TCP_IP alebo TCP_PORT\n")
	sys.exit(-1)

# POSIELAM SPRAVU NA SERVER
ok = False
while ok == False:
	try:
		s.send(MESSAGE)
		ok = True
	except:
		ok = False

# Prijimam spravu
data = ""	
ok = True
while ok:
	Xdata = s.recv(BUFFER_SIZE)
	if Xdata != "":
		data = data + Xdata
	else:
		ok = False

# Ukladanie PRIJATEJ spravy -> in.log
try:
	myFileInLog = open("ipkHttpClient-"+today+".in.log", "w")
	myFileInLog.write(data)
	myFileInLog.close()
except:
	sys.out.write("Subor sa nepodarilo otvorit")

# Ukladanie PRIJATEJ spravy -> .header
dlzka = data.find('\n\n')

dataHeader = data[:dlzka]

try:
	myFileHeader = open("ipkResp-"+today+".header", "w")
	myFileHeader.write(dataHeader)
	myFileHeader.close()
except:
	sys.out.write("Subor sa nepodarilo otvorit")
	
# Ukladanie PRIJATEJ spravy -> .payload
dlzka = data.find('\n\n')
okSprava = ""
celaSprava = data[dlzka+2:]

velkostChunk = celaSprava.find('\n')
velkostChunk = celaSprava[:velkostChunk]
while velkostChunk != 0 :
	chunkLen = celaSprava.find('\n')
	velkostChunk = str(celaSprava[:chunkLen])
	try:
		velkostChunk = int(velkostChunk, 16)
	except:
		break
	celaSprava = celaSprava[chunkLen+1:]
	okSprava = okSprava + celaSprava[:velkostChunk]
	celaSprava = celaSprava[velkostChunk+1:]

try:
	myFilePayload = open("ipkResp-"+today+".payload", "w")
	myFilePayload.write(okSprava)
	myFilePayload.close() 
except:
	sys.out.write("Subor sa nepodarilo otvorit")


print("\nThis is klient")
s.close()