# !/usr/bin/env python

import argparse
import sys
import getopt
import os
import re
import socket
import datetime
import time

# Use nargs to specify how many arguments an option should take.
ap = argparse.ArgumentParser()
ap.add_argument("-c","--chunk-max-size", type = int , required=False, 
				help = "Maximalni velkost dat v chunku")
ap.add_argument("-p","--port",  type = int, required=False, 
				help = "TCP port, na kterem bude server naslouchat")
ap.add_argument("-t","--min-chunk-time", type = int, required=False, 
				help = "(ms) minimalny cas cekani pred odeslanim dalsieho chunku, maximalni cas je 2X min-chunk-time")

# Grab the opts from argv
opts = ap.parse_args()

# This line will not be reached if none of a/b/c are specified.
# Usage/help will be printed instead.
max_chunk_time = 2*opts.min_chunk_time
min_chunk_time = opts.min_chunk_time
average_chunk_time = (3*opts.min_chunk_time)/2
chunk_max_size = opts.chunk_max_size
port = opts.port

localtime = time.localtime(time.time())
today = str(datetime.datetime.today())[:-7]

while 1:
	
	TCP_IP = '127.0.0.1'
	TCP_PORT = int(port)
	BUFFER_SIZE = 1024
	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	try:
		s.bind((TCP_IP, TCP_PORT))
	except:
		sys.stderr.write("nespravny port!!!")
		sys.exit(-1)
	while 1:
		try:
			s.listen(1)
			conn, addr = s.accept()
		except:
			sys.exit(0)

		data = conn.recv(BUFFER_SIZE)
		
		# Zapis do "ipkHttpServer" - prijata sprava
		try:
			myFileInLog		= open("ipkHttpServer-"+today+".in.log", "w")
			myFileInLog.write(data)
			myFileInLog.close()
		except:
			sys.stderr.write("Subor sa nepodarilo otvorit")

		if not data:
			break

		data = data[4:]
		x = data.find(' ')
		data = data[:x]
		
		# otvorim a nacitam subor - AK je mozne
		if os.path.isfile("www"+data):
			try:
				with open ("www"+data, "r") as myfile:
					fileData = myfile.readlines()
					fileData = ''.join(fileData)
					nasiel = True
					httpHeader = "HTTP/1.1 200 OK\n"
			except:
				nasiel = False
				sys.stderr.write("Subor sa nepodarilo otvorit")
				continue
		else:
			nasiel = False
			httpHeader = "HTTP/1.1 404 NOT FOUND\n"
			fileData = "HTTP 404"
			
		# Vytvorim a poslem HLAVICKU spravy
		HEADER_MESSAGE = (httpHeader+
				"Content-Type: text/plain\n"+
				"Transfer-Encoding: chunked\n\n")
		
		finalMessage = HEADER_MESSAGE;
		conn.send(HEADER_MESSAGE)
		
		# Vytvorim a poslem TELO spravy
		ok = False
		if fileData != "":
			dataChunk = fileData
			while dataChunk != "":
				dlzkaChunk = str(len(dataChunk[:chunk_max_size])) + "\n"
				finalMessage = finalMessage + dlzkaChunk			
				posielanyChunk = (dataChunk[:chunk_max_size])+"\n"
				finalMessage = finalMessage + posielanyChunk
				conn.send(dlzkaChunk)
				conn.send(posielanyChunk)
				dataChunk = dataChunk[chunk_max_size:]
		
		# Vytvorim a poslem UKONCOVACI ZNAK spravy -> 0
		ZERO_MESSAGE = "0"
		conn.send(ZERO_MESSAGE)
		finalMessage = finalMessage+"0\r\n"
				
		try:
			myFileOutLog = open("ipkHttpServer-"+today+".out.log", "w")
			myFileOutLog.write(finalMessage)
			myFileOutLog.close()
		except:
			sys.stderr.write("Subor sa nepodarilo otvorit")
		print ("ok")
		conn.close()

		
		
		
		
		
		
		
		
