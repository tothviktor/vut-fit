#!/usr/bin/env bash

printf "Script \"test.sh\" sa spustil"

INTERPRETER="python"
TASK_SERVER="ipkhttpserver"
TASK_CLIENT="ipkhttpclient"
#EXTENSION="py"
ok=false
i=1
PASS="[ \033[0;32mPASS\033[0;0m ]"
FAIL="[ \033[0;31mFAIL\033[0;0m ]"

rm -rf Tests/Results/*

# python ipkhttpserver.py
# python ipkhttpclient.py

# PRI NEZADANI popr. CHYBAJUCICH PRAMETROV, parametre budu DEFAULTNE ZVOLENE nasledovne:
# SERVER  -> --chunk-max-size : 100
#			--port			 : 8520
#			--min-chunk-time : 0
# 
# CLIENT -> z URI: "http://IP_ADDRESA:PORT/MENO_POZIADANEHO_DOKUMENTU"
#			bude zvoleny defaultne LEN PORT !
#			PORT -> 8520

# ----------------------------------------------------------------------------------------
# BEZ PARAMETROV
# SERVER  -> hodnota parametrov bude DEFAULTNE ZVOLENA
# CLIENT -> 

# ---  1 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER >Tests/Results/test1Server.out 2>Tests/Results/test01Server.err &
$INTERPRETER $TASK_CLIENT >Tests/Results/test1Client.out 2>Tests/Results/test01Client.err

pkill python > /dev/null

ok=true
printf "\nServer/Klient bez parametrov\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));


rm -rf *.header
rm -rf *.payload
rm -rf *.log
# OK

# ---  2 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c 10 -p 5000 -t 1 >Tests/Results/test2Server.out 2>Tests/Results/test02Server.err &
$INTERPRETER $TASK_CLIENT http:// >Tests/Results/test2Client.out 2>Tests/Results/test02Client.err

pkill python > /dev/null;

ok=true
printf "\nKlient bez IP adresy a bez cielovho priecinku\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log
# OK

# ---  3 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c 10 -p 5000 -t 1 >Tests/Results/test3Server.out 2>Tests/Results/test03Server.err &
$INTERPRETER $TASK_CLIENT http://localhost >Tests/Results/test3Client.out 2>Tests/Results/test03Client.err

pkill python > /dev/null;

ok=true

printf "\nKlient bez cieloveho priecinku bez zadaneho portu -> nastavy sa automaticky\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"

diff "$(ls -p | grep -v / | grep ^"ipkResp.*\.header")" "Tests/Reff/test3/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi


if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  4 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c 10 -p 5200 -t 1 >Tests/Results/test4Server.out 2>Tests/Results/test04Server.err &
$INTERPRETER $TASK_CLIENT http://localhost:5200 >Tests/Results/test4Client.out 2>Tests/Results/test04Client.err

pkill python > /dev/null;

ok=true
printf "\nKlient bez cieloveho priecinku so zadanym portom\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi


clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"

diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi



if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# --- 5 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c 10 -p 5200 -t 1 >Tests/Results/test5Server.out 2>Tests/Results/test05Server.err &
$INTERPRETER $TASK_CLIENT http://localhost:5200/test2 >Tests/Results/test5Client.out 2>Tests/Results/test05Client.err

pkill python > /dev/null;

ok=true
printf "\nTest na ocakavane argumenty\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi


clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"

diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi


if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  6 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c=10 -p=5200 -t=1 >Tests/Results/test6Server.out 2>Tests/Results/test06Server.err &
$INTERPRETER $TASK_CLIENT http://localhost:5200/NeExistujuciSubor >Tests/Results/test6Client.out 2>Tests/Results/test06Client.err

pkill python > /dev/null;

ok=true
printf "\nTest na neexistujuci subor\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi


clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"

diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi


if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  7 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c=12 -p=5111 --min-chunk-time=1 >Tests/Results/test7Server.out 2>Tests/Results/test07Server.err &
$INTERPRETER $TASK_CLIENT http://localhost:5111/test1 >Tests/Results/test7Client.out 2>Tests/Results/test07Client.err

pkill python > /dev/null;

ok=true
printf "\nTest s ocakavanymi orgumentmi a kombinovanie kratkych/dlhych argumentov\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi


clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"


diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi



if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  8 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c 10 -t 1 >Tests/Results/test8Server.out 2>Tests/Results/test08Server.err &
$INTERPRETER $TASK_CLIENT http://localhost/test2 >Tests/Results/test8Client.out 2>Tests/Results/test08Client.err


pkill python > /dev/null;

ok=true
printf "\nKlient a server bez zadaneho portu -> nastavy sa automaticky\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi


clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"


diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi



if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  9 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c 1 >Tests/Results/test9Server.out 2>Tests/Results/test09Server.err &
$INTERPRETER $TASK_CLIENT http://localhost/test3 >Tests/Results/test9Client.out 2>Tests/Results/test09Client.err

pkill python > /dev/null;

ok=true
printf "\nServer bez zadaneho portu a bez min-chunk-time, klient bez zadaneho portu -> vsetko sa nastavy automaticky\n"
diff "Tests/Results/test0${i}Server.err" "Tests/Reff/test${i}/test0${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test0${i}Client.err" "Tests/Reff/test${i}/test0${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"


diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi



if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  10 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c 1 -p 5159 -t 10 >Tests/Results/test10Server.out 2>Tests/Results/test10Server.err &
$INTERPRETER $TASK_CLIENT http://localhost:5159/test4 >Tests/Results/test10Client.out 2>Tests/Results/test10Client.err

pkill python > /dev/null;

ok=true
printf "\nOcakavane vstupne argumenty\n"
diff "Tests/Results/test${i}Server.err" "Tests/Reff/test${i}/test${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.err" "Tests/Reff/test${i}/test${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"

diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi


if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  11 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -c 0 -p 5159 >Tests/Results/test11Server.out 2>Tests/Results/test11Server.err &
$INTERPRETER $TASK_CLIENT http://localhost:10000/test4 >Tests/Results/test11Client.out 2>Tests/Results/test11Client.err

pkill python > /dev/null;

ok=true
printf "\nTest na chunk-max-size 0\n"
diff "Tests/Results/test${i}Server.err" "Tests/Reff/test${i}/test${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.err" "Tests/Reff/test${i}/test${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi



if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  12 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -p 5159 -t 10 >Tests/Results/test12Server.out 2>Tests/Results/test12Server.err &
$INTERPRETER $TASK_CLIENT http://localhost:5159/test2 >Tests/Results/test12Client.out 2>Tests/Results/test12Client.err

pkill python > /dev/null;

ok=true
printf "\nBez zadaneho chunk-max-size -> nastavy sa automaticky\n"
diff "Tests/Results/test${i}Server.err" "Tests/Reff/test${i}/test${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.err" "Tests/Reff/test${i}/test${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"


diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi


if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# ---  13 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER --chunk-max-size=10 -p 5000 >Tests/Results/test13Server.out 2>Tests/Results/test13Server.err &
$INTERPRETER $TASK_CLIENT http://localhost/test2 >Tests/Results/test13Client.out 2>Tests/Results/test13Client.err

pkill python > /dev/null;

ok=true
printf "\nBez zadaneho chunk min time a bez zadaneho portu na klientovy -> vsetko sa nastavy automaticky\n"
diff "Tests/Results/test${i}Server.err" "Tests/Reff/test${i}/test${i}Server.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.err" "Tests/Reff/test${i}/test${i}Client.err" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? != 0 ]; then ok=false; fi


clientHeader="$(ls -p | grep -v / | grep ^"ipkResp.*\.header")"

diff "$clientHeader" "Tests/Reff/test${i}/ipkResp.header" > /dev/null
if [ $? != 0 ]; then ok=false; fi



if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log

# --- 14 TEST  -----------------------------------------------------------------------------------------------------------
$INTERPRETER $TASK_SERVER -h >Tests/Results/test14Server.out 2>Tests/Results/test14Server.err &
$INTERPRETER $TASK_CLIENT --help >Tests/Results/test14Client.out 2>Tests/Results/test14Client.err

pkill python > /dev/null;

ok=true
printf "\nTest na pomoc pri klienta aj pri servera\n"
diff "Tests/Results/test${i}Server.err" "Tests/Reff/test${i}/test${i}Server.err" > /dev/null
if [ $? == 0 ]; then ok=true; else ok=false; fi

diff "Tests/Results/test${i}Client.err" "Tests/Reff/test${i}/test${i}Client.err" > /dev/null
if [ $? == 0 ]; then ok=true; else ok=false; fi

diff "Tests/Results/test${i}Server.out" "Tests/Reff/test${i}/test${i}Server.out" > /dev/null
if [ $? == 0 ]; then ok=true; else ok=false; fi

diff "Tests/Results/test${i}Client.out" "Tests/Reff/test${i}/test${i}Client.out" > /dev/null
if [ $? == 0 ]; then ok=true; else ok=false; fi

if [ $ok == true ]; then printf "\t$PASS"; else printf "$FAIL"; fi

i=$((i+1));

rm -rf *.header
rm -rf *.payload
rm -rf *.log


