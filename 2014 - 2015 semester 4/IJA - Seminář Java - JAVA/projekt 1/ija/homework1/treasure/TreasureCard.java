package ija.homework1.treasure;

import java.util.Objects;

/**
 *
 * @author viktor
 */
public class TreasureCard {
        
    private final Treasure karta;
    
    public TreasureCard(Treasure pkld)
    {
         this.karta = pkld;       
    }
        
    public int getCode()
    {
        return karta.code;
    }
    
    public boolean equals (TreasureCard obj)
    {
        if(false == obj instanceof TreasureCard)
        {
            return false;
        }
        return obj.getCode() == this.getCode();
    
    }
        
    @Override
    public int hashCode()
    {
        return karta.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TreasureCard other = (TreasureCard) obj;
        return Objects.equals(this.karta, other.karta);
    }
}