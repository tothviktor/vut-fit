package ija.homework1.treasure;

import java.util.ArrayList;

/**
 *
 * @author viktor
 */
public class Treasure {
    
    public int code;
    static int k = 24;
    //static Treasure poklad[] ;
    static ArrayList<Treasure> poklad = new ArrayList<>();
    
    private Treasure(int code)
    {
        this.code = code;
    }
    public static void createSet()
    {
        for(int i = 0; i < k; i++)
        {
            Treasure pkld = new Treasure(i);
            poklad.add(pkld);
        } 
    }
    public static Treasure getTreasure(int code)
    {    
        if(code>= k)
        {
            return null;
        }
        return poklad.get(code);
    }
}