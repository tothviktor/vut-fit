package ija.homework1.treasure;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.List;

/**
 *
 * @author viktor
 */
public class CardPack {
   
    private int maxSize;
    private int initSize;
    
    private final Queue<TreasureCard> balicek = new LinkedList<>();
    
    public CardPack(int maxSize, int initSize)
    {
        if(maxSize >=initSize)
        {
            this.maxSize = maxSize;
            this.initSize = initSize;
            
            for(int i = 0; i < maxSize; i++)
            {
                TreasureCard pkld = new TreasureCard(Treasure.getTreasure(i));
                
                balicek.add(pkld);
            }
        }
    }
    public TreasureCard popCard()
    {
        TreasureCard card = balicek.poll();
        return card;
    }
    public int size()
    {
        return balicek.size();
    }
    public void shuffle()
    {
        Collections.shuffle((List<?>) balicek, new Random());
    }
}
