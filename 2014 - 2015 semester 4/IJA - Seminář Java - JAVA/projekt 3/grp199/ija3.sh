#!/bin/sh

ulimit -v unlimited

# compile java files to class files
javac -d build src/ija/homework3/board/*.java
javac -d build -cp build src/ija/homework3/MainApplication.java

# compress class files to jar
jar cvfe dest-client/ija2015-client.jar ija.homework3.MainApplication -C build/ .

# run program
java -jar dest-client/ija2015-client.jar