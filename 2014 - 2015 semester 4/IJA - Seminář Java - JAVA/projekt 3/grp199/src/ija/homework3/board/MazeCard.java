/*
 *
 *
 */
package ija.homework3.board;

/**
 *
 * @author Viktor Toth - xtothv01, Marek Ochodek - xochod01
 */
public class MazeCard {

    private MazeCard(CANGO left, CANGO right, CANGO up, CANGO down)
    {
        this.up = up;
        this.right = right;
        this.down = down;
        this.left = left;
    }

    public static enum CANGO{LEFT, UP, RIGHT, DOWN};

    private CANGO up;
    private CANGO right;
    private CANGO down;
    private CANGO left;

    public static MazeCard create(String type){
        MazeCard maze;
        switch(type) {
            case "C":
                maze = new MazeCard(CANGO.LEFT, null, CANGO.UP, null);
                break;
            case "L":
                maze = new MazeCard(CANGO.LEFT, CANGO.RIGHT, null, null);
                break;
            case "F":
                maze = new MazeCard(CANGO.LEFT, CANGO.RIGHT, CANGO.UP, null);
                break;
            default:
                throw new IllegalArgumentException();
        }
        return maze;
    }
    
    public boolean canGo(MazeCard.CANGO dir){
        if(dir.equals(CANGO.LEFT))
            if(left != null)
                return true;
        if(dir.equals(CANGO.RIGHT))
            if(right != null)
                return true;
        if(dir.equals(CANGO.UP))
            if(up != null)
                return true;
        if(dir.equals(CANGO.DOWN))
            if(down != null)
                return true;
        return false;
    }
    
    public void turnRight(){
        CANGO tmp;
        tmp = up;
        up = left;
        left = down;
        down = right;
        right = tmp;

        if(up != null)
            up = CANGO.UP;
        if(down != null)
            down = CANGO.DOWN;
        if(left != null)
            left = CANGO.LEFT;
        if(right != null)
            right = CANGO.RIGHT;
        
    }
    
}
