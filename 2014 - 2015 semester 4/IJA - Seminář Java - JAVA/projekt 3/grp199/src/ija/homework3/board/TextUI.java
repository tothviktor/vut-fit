/*
 *
 *
 */
package ija.homework3.board;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Viktor Toth - xtothv01, Marek Ochodek - xochod01
 */
public class TextUI {

    /**
     * Constructor.
     */
    public TextUI() {
    }

    /**
     * Starts a process of reading commands from standard input and their processing. 
     * It calls the method {@link #processString} to process and execute commands.
     */
    public void start() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        boolean end = false;

        int n = 7;
        MazeBoard mb = MazeBoard.createMazeBoard(n);

        while (! end) {
            try {
                System.out.print("Prompt>");
                String str = in.readLine();
                end = processString(str, mb, n);
            } catch (IOException ex) {
                Logger.getLogger(TextUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Analyses the input string and executes the command.
     * If the command does not exist or the string is invalid, the method does nothing.
     * @param str Input st
     * @param mb
     * @param n
     * @return Returns true if the command end was typed; otherwrise it returns false.
     */

    protected boolean processString(String str, MazeBoard mb, int n) {

        boolean end = false;
              
        switch(str) {
            // zobrazeni hraci desky (policka, kameny a volny kamen)
            case "p"    :   
                            MazeCard freecard = mb.getFreeCard();

                            System.out.print("Free card: ");

                            if(mb.getFreeCard() != null)
                            {
                                if( (!freecard.canGo(MazeCard.CANGO.UP) && freecard.canGo(MazeCard.CANGO.RIGHT) && freecard.canGo(MazeCard.CANGO.DOWN) && freecard.canGo(MazeCard.CANGO.LEFT)) ||
                                    (freecard.canGo(MazeCard.CANGO.UP) && !freecard.canGo(MazeCard.CANGO.RIGHT) && freecard.canGo(MazeCard.CANGO.DOWN) && freecard.canGo(MazeCard.CANGO.LEFT)) ||
                                    (freecard.canGo(MazeCard.CANGO.UP) && freecard.canGo(MazeCard.CANGO.RIGHT) && !freecard.canGo(MazeCard.CANGO.DOWN) && freecard.canGo(MazeCard.CANGO.LEFT)) ||
                                    (freecard.canGo(MazeCard.CANGO.UP) && freecard.canGo(MazeCard.CANGO.RIGHT) && freecard.canGo(MazeCard.CANGO.DOWN) && !freecard.canGo(MazeCard.CANGO.LEFT)))
                                {
                                    System.out.print("F");
                                }
                                else if ((!freecard.canGo(MazeCard.CANGO.UP) && freecard.canGo(MazeCard.CANGO.RIGHT) && !freecard.canGo(MazeCard.CANGO.DOWN) && freecard.canGo(MazeCard.CANGO.LEFT)) ||
                                         (freecard.canGo(MazeCard.CANGO.UP) && !freecard.canGo(MazeCard.CANGO.RIGHT) && freecard.canGo(MazeCard.CANGO.DOWN) && !freecard.canGo(MazeCard.CANGO.LEFT)))
                                {
                                    System.out.print("L");
                                }
                                else
                                {
                                    System.out.print("C");
                                }
                                System.out.println("");
                            }
                            else
                            {
                                System.out.println("0");
                            }
                            System.out.println("  1 2 3 4 5 6 7");
                            for(int i=1;i <= n; i++)
                            {
                                for(int j=1;j <= n; j++)
                                {   
                                    MazeField mf1 = mb.get(i,j);
                                    MazeCard mc = mf1.getCard();
                                    if(mf1.getCard() != null)
                                    {
                                        if(j == 1)
                                        {
                                            System.out.print(i + " ");
                                        }
                                        if( (!mc.canGo(MazeCard.CANGO.UP) && mc.canGo(MazeCard.CANGO.RIGHT) && mc.canGo(MazeCard.CANGO.DOWN) && mc.canGo(MazeCard.CANGO.LEFT)) ||
                                            (mc.canGo(MazeCard.CANGO.UP) && !mc.canGo(MazeCard.CANGO.RIGHT) && mc.canGo(MazeCard.CANGO.DOWN) && mc.canGo(MazeCard.CANGO.LEFT)) ||
                                            (mc.canGo(MazeCard.CANGO.UP) && mc.canGo(MazeCard.CANGO.RIGHT) && !mc.canGo(MazeCard.CANGO.DOWN) && mc.canGo(MazeCard.CANGO.LEFT)) ||
                                            (mc.canGo(MazeCard.CANGO.UP) && mc.canGo(MazeCard.CANGO.RIGHT) && mc.canGo(MazeCard.CANGO.DOWN) && !mc.canGo(MazeCard.CANGO.LEFT)))
                                        {
                                            System.out.print("F");
                                        }
                                        else if ((!mc.canGo(MazeCard.CANGO.UP) && mc.canGo(MazeCard.CANGO.RIGHT) && !mc.canGo(MazeCard.CANGO.DOWN) && mc.canGo(MazeCard.CANGO.LEFT)) ||
                                                 (mc.canGo(MazeCard.CANGO.UP) && !mc.canGo(MazeCard.CANGO.RIGHT) && mc.canGo(MazeCard.CANGO.DOWN) && !mc.canGo(MazeCard.CANGO.LEFT)))
                                        {
                                            System.out.print("L");
                                        }
                                        else
                                        {
                                            System.out.print("C");
                                        }
                                    }
                                    else
                                    {
                                        if(j == 1)
                                        {
                                            System.out.print(i + " ");
                                        }
                                        System.out.print("0");
                                    }
                                    System.out.print(" ");
                                }
                                System.out.println("");
                            }

                            break;
            // nova hra
            case "n"    :   mb.newGame();
                            break;
            // konec aplikace
            case "q"    :   end = true;
                            break;
            // vsune kamen na pozici [R, C]; pokud nelze, nedela nic
            default     :
                            if ( str.length() == 3 &&  's' == str.charAt(0) && Character.isDigit(str.charAt(1)) && Character.isDigit(str.charAt(2)))
                            {
                                int Row = Character.getNumericValue(str.charAt(1));
                                int Column = Character.getNumericValue(str.charAt(2));

                                mb.shift(mb.get(Row, Column));
                            }
                            else
                            {
                                System.out.println("Zadali jste nespravny prikaz!");
                            }
        }
        return end;
    }
}
