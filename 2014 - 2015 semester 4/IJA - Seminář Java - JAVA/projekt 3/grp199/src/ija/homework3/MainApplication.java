/*
 *
 *
 */
package ija.homework3;

import ija.homework3.board.*;

/**
 *
 * @author Viktor Toth - xtothv01, Marek Ochodek - xochod01
 */
public class MainApplication {

    public static void main(String argv[]) {
        TextUI textUI = new TextUI();
        textUI.start();
    }
    
}
