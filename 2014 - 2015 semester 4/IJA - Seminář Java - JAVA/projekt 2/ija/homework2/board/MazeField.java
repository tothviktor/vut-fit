/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.homework2.board;

/**
 *
 * @author Viktor Toth - xtothv01
 */
public class MazeField 
{
    
    private int row;
    private int col;
    
    private MazeCard rock = null;
    
    public MazeField(int row, int col)
    {
        this.row = row;
        this.col = col;
    }
    
    public int row()
    {
        return row;
    }
    
    public int col()
    {
        return col;
    }
    
    public MazeCard getCard()
    {
        return rock;
    }
    
    public void putCard(MazeCard c)
    {
        rock = c;
    }
    
    public void putCoordinates(int row, int col)
    {
        this.col = col;
        this.row = row;
    }  
}
