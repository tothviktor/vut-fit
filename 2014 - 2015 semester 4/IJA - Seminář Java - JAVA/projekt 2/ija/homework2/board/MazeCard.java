/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.homework2.board;

/**
 *
 * @author Viktor Toth - xtothv01
 */
public class MazeCard {

    
    private MazeCard(CANGO left, CANGO right, CANGO up, CANGO down)
    {
        this.left = left;
        this.up = up;
        this.right = right;
        this.down = down;
    }
    
    public static enum CANGO{LEFT, UP, RIGHT, DOWN};
    
    private CANGO left;
    private CANGO up;
    private CANGO right;
    private CANGO down;
    
//   final static String c = "C";
//   final static String l = "L";
//   final static String f = "F";
    
    public static MazeCard create(String type){
        MazeCard maze;
        switch(type)
        {
            
            case "C":
                maze = new MazeCard(CANGO.LEFT, null, CANGO.UP, null);
                break;
            case "L":
                maze = new MazeCard(CANGO.LEFT, CANGO.RIGHT, null, null);
                break;
            case "F":
                maze = new MazeCard(CANGO.LEFT, CANGO.RIGHT, CANGO.UP, null);
                break;
            default:
                throw new IllegalArgumentException();
        }
        
        return maze;
    }
    
    public boolean canGo(MazeCard.CANGO dir){
        
        if(dir.equals(CANGO.LEFT))
        {
            if(left != null)
                return true;
        }
        if(dir.equals(CANGO.RIGHT))
        {
            if(right != null)
                return true;
        }
        if(dir.equals(CANGO.UP))
        {
            if(up != null)
                return true;
        }
        if(dir.equals(CANGO.DOWN))
        {
            if(down != null)
                return true;
        }
        
        return false;
    }
    
    public void turnRight(){
        
        CANGO tmp;
        tmp = up;
        up = left;
        left = down;
        down = right;
        right = tmp;
        
        if(up != null)
            up = CANGO.UP;
        if(down != null)
            down = CANGO.DOWN;
        if(left != null)
            left = CANGO.LEFT;
        if(right != null)
            right = CANGO.RIGHT;
        
    }
    
}
