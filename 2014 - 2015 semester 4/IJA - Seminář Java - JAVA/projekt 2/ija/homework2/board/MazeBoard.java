/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.homework2.board;

import java.util.Random;

/**
 *
 * @author Viktor Toth - xtothv01
 */
public class MazeBoard 
{      
    private final MazeField[][] mazeBoard;
    private MazeCard free;
    private final int numberOfFields;
    private static final String[] type = {"C","F","L"};

    
    private MazeBoard(int n)
    {
        mazeBoard = new MazeField[n+1][n+1];
        this.numberOfFields = n;
    }
    
    public static MazeBoard createMazeBoard(int n)
    {
        MazeBoard maze = new MazeBoard(n);
        
        for(int i=1;i <= n; i++)
        {
            for(int j=1;j <= n; j++)
            {   
                MazeField m = new MazeField(i, j);
                maze.mazeBoard[i][j] = m;
            }
        }
        return maze;
    }
    
    public void newGame()
    {
        for(int i=1; i <= numberOfFields; i++)
        {
            for(int j=1; j <= numberOfFields; j++)
            {
                String randomType = (type[new Random().nextInt(type.length)]);
                
                MazeCard card = MazeCard.create(randomType);
                mazeBoard[i][j].putCard(card);
            }
        }
        String randomType = (type[new Random().nextInt(type.length)]);
        MazeCard card = MazeCard.create(randomType);
        free = card;
    }
    
    public MazeCard getFreeCard()
    {
        return free;
    }
    
    public MazeField get(int r, int c)
    {
        try
        {
            return mazeBoard[r][c];
        }
        catch (IndexOutOfBoundsException e)
        {
            return null;
        }
    }
    
    public void shift(MazeField mf)
    {  
        int c = mf.col();
        int r = mf.row();
        MazeField tmp = new MazeField(r, c);
        
        if((r == 1) && (c%2 == 0)){
            
            tmp.putCard(mazeBoard[numberOfFields][c].getCard());
            
            for(int i=numberOfFields;i>1 ; i--)
            {
                mazeBoard[i][c].putCoordinates(i-1, c);
                mazeBoard[i][c].putCard(mazeBoard[i-1][c].getCard());
            }
            mazeBoard[1][c].putCard(free);
            free = tmp.getCard();
        }
        
        if((r == numberOfFields) && (c%2 == 0))
        {
            tmp.putCard(mazeBoard[1][c].getCard());
            
            for(int i=1; i<numberOfFields; i++)
            {
                mazeBoard[i][c].putCoordinates(i+1, c);
                mazeBoard[i][c].putCard(mazeBoard[i+1][c].getCard());
            }
            mazeBoard[numberOfFields][c].putCard(free);
            free = tmp.getCard();
        }
        
        if((c == 1) && (r%2 == 0))
        {
            tmp.putCard(mazeBoard[r][numberOfFields].getCard());
            
            for(int i=numberOfFields;i>1;i--)
            {
                mazeBoard[r][i].putCoordinates(r, i-1);
                mazeBoard[r][i].putCard(mazeBoard[r][i-1].getCard());
            }
            mazeBoard[r][1].putCard(free);
            free = tmp.getCard();
        }
        
        if((c == numberOfFields) && (r%2 == 0))
        {
            tmp.putCard(mazeBoard[r][1].getCard());
            
            for(int i=1;i<numberOfFields;i++)
            {
                mazeBoard[r][i].putCoordinates(r, i+1);
                mazeBoard[r][i].putCard(mazeBoard[r][i+1].getCard());
            }
            mazeBoard[r][numberOfFields].putCard(free);
            free = tmp.getCard();
        } 
    }  
}