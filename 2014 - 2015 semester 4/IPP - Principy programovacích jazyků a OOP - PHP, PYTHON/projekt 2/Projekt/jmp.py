﻿#! /usr/bin/python3

#JMP:xtothv01

import argparse
import sys
import getopt
import os
import re


def help():
	print("\n--help vypise mozne parametry")
	print("--input=filename zadany vstupni textovy soubor se zakladnim ASCII kodovanim")
	print("--output=filename textovy vystupni soubor tez v ASCII kodovani")
	print("--cmd=text text bude pred zacatkem zpracovani vstupu vlozen na zacatek vstupu")
	print("-r redefinice jiz definovaneho makra pomoci makra @def zpusobi chybu s kodem 57\n")
	
	
def getSize(fileobject):
    fileobject.seek(0,2) # move the cursor to the end of the file
    size = fileobject.tell()
    return size
	
def exist(array, string):
	i=0
	nasiel = False
	while i < len(array):
		#print (array[i], " vs. ", string )
		if array[i] == string:
			return i
		i+=1
	return -1
	
def pocetDef(string):
	vyraz = "@def@"
	count =0
	flag=True
	start=0
	while flag:
		a = string.find(vyraz,start)  # find() returns -1 if the word is not found, 
									#start i the starting index from the search starts(default value is 0)
		if a==-1:          #if pattern not found set flag to False
			flag=False
		else:               # if word is found increase count and set starting index to a+1
			count+=1        
			start=a+1
	return count

def isInArray(array, string):
	i=0
	count = 0
	while i < len.array:
		if array[i] == string:
			count+=1
		i+=1
	return count
	

argc = len(sys.argv[1:])
bezChyby = True
okParametre = [0,0,0,0]

if argc == 0:
	("Ziadny parameter")
	bezChyby = True
	
elif argc == 1 and sys.argv[1] == "--help":
	help()
	sys.exit(0)
	
elif argc != 1 and sys.argv[1] == "--help":
	sys.stderr.write("parameter --help sa nemoze kombinovat !\n")
	sys.exit(1)
elif argc >= 1:

	i=1
	input = None
	output = None
	cmd = None
	
	
	while i<=argc and bezChyby == True:
		param = sys.argv[i]
		
		if param[:7] == "--input":
			if param[:8] == "--input=" and len(param) > 8:
				input = param[8:]
			else:
				bezChyby = False
			okParametre[0] = (okParametre[0])+1
					
		elif param[:8] == "--output":
			if param[:9] == "--output=" and len(param) > 9:
				output = param[9:]
			else:
				bezChyby = False
			okParametre[1] = (okParametre[1])+1
					
		elif param[:5] == "--cmd":
			if param[:6] == "--cmd=" and len(param) > 6:
				cmd = param[6:]
			else:
				bezChyby = False
			okParametre[2] = (okParametre[2])+1
					
		elif param[:2] == "-r":
			okParametre[3] = (okParametre[3])+1
			
		else:
			bezChyby = False
		
		i+=1
	
	if bezChyby:
		i = 0
		while i < 4:
			if okParametre[i] > 1:
				bezChyby = False
			i+=1
	
	#print(bezChyby)
	#print("parmas:", okParametre)
	
if(bezChyby):
	
	absPath = (os.path.abspath(sys.argv[0]))[:-(len(sys.argv[0]))]
	
	if okParametre[0]:									#input
		if os.path.isfile(input):
			with open (input, "r") as myfile:
				data=myfile.readlines()
				#print(data)
				data= ''.join(data)
				#print(data)
	
		else:
			sys.stderr.write("Zadana file neexistuje!\n")
			sys.exit(2)
	else:
		data = sys.stdin.readline()
		data = data[:-1]
	
	if okParametre[2]:									#cmd
		#print ("param cmd")
		data = cmd + data
		#print(data)
	
	vystup = ""
	makro = ""
	isDef = False
	isnDef = False
	isUndef = False
	isnUndef = False
	isSet = False
	isnSet = False
	zatvorka = 0
	nacitaneMakra = []
	i=0
	
	ok = re.findall("@def@", data)
	#print (ok)
	
	
	


	
	#sys.exit(0)
	
	while i < len(data):
		
		# je nastaveny  " SET / __SET__"
		if isSet or isnSet:
			if data[i] == " ":
				break
		
		#zaciatok BLOKU
		if data[i] == "{":
			zatvorka += 1
			
			if zatvorka != 1:
				vystup = vystup + data[i]
		
		#koniec BLOKU
		elif data[i] == "}":		
			zatvorka -= 1
			
			if zatvorka < 0:
				sys.stderr.write("Nadmerny pocet zatvoriek typu: \"}\"\n")
				#print ()
				sys.exit(55)
				
			elif zatvorka != 0:
				vystup = vystup + data[i]
		
		# MAKRO popripade "ESCAPE SEKVENCIA"
		elif data[i] == "@":
			
			# ---------  posledny nacitany znak je "@" ,CHYBA --------------
			if i == len(data)-1:
				sys.stderr.write("Po \"@\" chyba meno makra!\n")
				sys.exit(55)
			
			# ---------- Nasli sme "escape sekvenciu" - ZNAK " @ { } $ "  ----------------
			elif data[i+1] in r'@{}$':
				i += 1
				vystup = vystup + data[i]
				
			# ---------- def, __def__, undef, __undef__, set, __set__, meno_makra -----------------------	
			elif re.compile('([a-zA-Z_])').match(data[i+1]):
				i+=1										#vynecham "@"
				
				while data[i] != "@" or i < len(data):
					
					makro = makro + data[i]
									
					if makro == "def":
						if i == len(data)-1:
							sys.stderr.write("Chybajuce parametre, makro \"@def\" vyzaduje dasie parametre!\n")
							sys.exit(56)
						elif data[i+1] == "@" or data[i+1] == " ":
							isDef = True
							makro = ""

							#print ("Nasiel sa \"def\"")
							break
					
					elif makro == "__def__":
						if i == len(data)-1:
							sys.stderr.write("Chybajuce parametre, makro \"@__def__\" vyzaduje dasie parametre!\n")
							sys.exit(56)
						elif data[i+1] == "@" or data[i+1] == " ":
							isnDef = True
							makro = ""
							#print ("Nasiel sa \"__def__\"")
							break
						
					elif makro == "undef":
						if i >= len(data)-2:
							sys.stderr.write("Chybajuce parametre, makro \"@undef\" vyzaduje dasie parametre!\n")
							sys.exit(56)
						#DORIESIT !!!!!!!!!!!!!	
						elif data[i+1] == "@" or data[i+1] == " ":
							makro = ""
							i+=2
							#print ("Nasliel sa \"undef\"")
							
							while i < len(data):
								#print ("ok")
								if re.compile('([a-zA-Z0-9_])').match(data[i]):
									makro = makro + data[i]
									#print (makro)
									if i == len(data)-1:
										existMakro = exist(nacitaneMakra, makro)
										if existMakro != -1:
											nacitaneMakra.pop(existMakro)
											#print ("makro je vymazane")
											break
										else:
											sys.stderr.write("Makro sa nenasiel")
											sys.exit(56)
								else:
									existMakro = exist(nacitaneMakra, makro)
									if existMakro != -1:
										nacitaneMakra.pop(existMakro)
										#print ("makro je vymazane")
										break
									else:
										sys.stderr.write("Makro sa nenasiel")
										sys.exit(56)
								i+=1
							break
					elif makro == "__undef__":
						if i >= len(data)-2:
							sys.stderr.write("Chybajuce parametre, makro \"@__undef__\" vyzaduje dasie parametre!\n")
							sys.exit(56)
						elif data[i+1] == "@" or data[i+1] == " ":
							makro = ""
							i+=2
							#print ("Nasliel sa \"__undef__\"")
							
							while i < len(data):
								if re.compile('([a-zA-Z0-9_])').match(data[i]):
									makro = makro + data[i]
									if i == len(data)-1:
										existMakro = exist(nacitaneMakra, makro)
										if existMakro != -1:
											nacitaneMakra.pop(existMakro)
											#print ("makro je vymazane")
											break
										else:
											sys.stderr.write("Makro sa nenasiel")
											sys.exit(56)
								else:
									existMakro = exist(nacitaneMakra, makro)
									if existMakro != -1:
										nacitaneMakra.pop(existMakro)
										#print ("makro je vymazane")
										break
									else:
										sys.stderr.write("Makro sa nenasiel")
										sys.exit(56)
								i+=1
							break
					
					# ------------------------------- "set" -------------------------------					
					elif makro == "set" and data[i+1] == "{":
						makro = ""
						#print (i, data [i])
						while i < len(data):
							i+=1
							makro = makro + data[i]
							
							if makro == "{+INPUT_SPACES}":
								isSet = True
								#print ("Nasiel sa \"+INPUT_SPACES\"")
								break
							elif makro == "{-INPUT_SPACES}":
								isSet = False
								#print ("Nasiel sa \"-INPUT_SPACES\"")
								break
							
						if makro != "{+INPUT_SPACES}" and makro != "{-INPUT_SPACES}":
							sys.stderr.write("Nespravna hodnota v \"set\"")
							sys.exit(55)
						else:
							break
					
					# ------------------------------- "__set__" -------------------------------
					elif makro == "__set__" and data[i+1] == "{":
						makro = ""
						#print (i, data [i])
						while i < len(data):
							i+=1
							makro = makro + data[i]
							
							if makro == "{+INPUT_SPACES}":
								isnSet = True
								#print ("Nasiel sa \"+INPUT_SPACES\"")
								break
							elif makro == "{-INPUT_SPACES}":
								isnSet = False
								#print ("Nasiel sa \"-INPUT_SPACES\"")
								break
							
						if makro != "{+INPUT_SPACES}" and makro != "{-INPUT_SPACES}":
							sys.stderr.write("Nespravna hodnota v \"__set__\"")
							sys.exit(55)
						else:
							break
					
					elif isDef or isnDef:
						if i < len(data)-1 and data[i+1] == "{":
							#print ("Naslo sa makro: ", makro)
							nacitaneMakra.append(makro)
							makro = ""
														
							if i >= len(data)-1:
								sys.stderr.write("Chybajuce parametre, makro \"@__def__\" vyzaduje dasie parametre!\n")
								sys.exit(56)
							
							
							zatvorkaDefLav = 1
							zatvorkaDefPrav = 0
							premenna = 0
							defPremenna1 = []
							defPremenna2 = []
							i+=2
							
							while i<len(data):
								if data[i] == " ":
									if isSet or isnSet:
										i+=1
									else:
										sys.stderr.write("Nedovolena medzera!\n")
										sys.exit(56)
										
								elif data[i] == "}":
									zatvorkaDefPrav += 1
									premenna = 2
									i+=1
								elif data[i] == "{":
									zatvorkaDefLav += 1
									premenna = 2
									i+=1
									
								elif data[i] == "$" and premenna == 0:
									premenna = 1
									#print ("ok")
									i+=1
									
								elif premenna == 1:
									defPremenna1.append("$" + data[i])
									premenna = 0
									#print(defPremenna1)
									i+=1
								
								elif zatvorkaDefLav == zatvorkaDefPrav+1 and premenna == 2:
									if data[i] == "$":
										i+=1
										if exist(defPremenna1, data[i]): 
											#print("ok")
											qqqq=0
										else:
											#print ("chyba")
											sys.exit(56)
									else:
										defPremenna2.append(data[i])
										#print("ok ine nieco: ", data[i])
									i+=1
								else:
									i+=1
									
							sys.exit(0)		
							isDef = False
							isnDef = False
							break
					
					#print (i , " ", data[i], makro)
					
					if i == len(data):
						continue
					
					#nacitanie MENA makra
					elif re.compile('([a-zA-Z0-9_])').match(data[i+1]):
						i+=1
						
					elif re.compile('([@{])').match(data[i+1]):
						break
					else:
						sys.stderr.write("Nedovoleny znak v NAZVU makra!\n")
						#print(data[i])
						sys.exit(55)
				
				#koniec Hlavneho while cyklu
			else:
				sys.stderr.write("Nedovoleny znak!\n")
				sys.exit(55)
				
		elif data[i] == "$":
			sys.stderr.write("Nedovoleny znak typu \"$\"\n")
			sys.exit(55)
			
		else:
			vystup = vystup + data[i]		
		#print (i, data [i])
		if i < len(data):
			i += 1
		else:
			break
	if zatvorka !=0:
		sys.stderr.write("Nadmerny pocet zatvoriek typu: \"}\"\n")
		#print ()
		sys.exit(55)

	if okParametre[3]:									#redef
		#print ("param redef")
		x = 0
	#output
	if okParametre[1]:									
		outFile = open(output, "w")
		outFile.write(vystup)
		outFile.close()
		sys.exit(0)
	else:
		sys.output(vystup)
		sys.exit(0)
else:
	sys.stderr.write("Nespravne parametre !\n")
	sys.exit(1)
	
	

	
	

		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



	
