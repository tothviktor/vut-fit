<?php

#CHA:xtothv01

function pomoc()
{
	echo
"Ulohou skriptu je spracovavanie hlavickovych suborov (.h) jazyka C, ktore musia byt napisane podla standardu ISO C-99.
Vystup (formatovanie a obsah) je ovplyvneny so vstupnymi parametrami. 
Ani jeden parameter nie je povinny, ale mozme ich rozmne kombinovat, pricom ziadny parameter nesmie byt napisany duplicitne.
Vysledok je smerovany do XML suboru alebo na standardny vystup.\n
Script ma nasledujuce vstupne parametre:
\t--help			Kratky opis o scriptu, mozne vstupne parametre.\n
\t--input=fileordir	Zadany vstupny priecinok alebo soubor. Ak NIE je zadany, tak sa rovna korenovym priecinkom.\n
\t--output=filename	Zadany vystupny priecinok.Ak NIE je zadany, tak vystup je nasmerovany na standardny vystup.\n
\t--pretty-xml=k\t	Zformatuje vysledny XML dokument. \"k\"oznacuje pocet medzier pri novom zanoreni, predvolene je k=4.\n
\t--no-inline		Preskoci funkcie deklarovane so specifikatorom \"inline\"\n
\t--max-par=n		Budu spracovane iba funkcie, ktore maju \"n\" alebo menej parametrov.\n
\t--no-duplicates\t	Spomedzi funkci s rovnakym meno bude spracovany iba prvy.\n
\t--remove-whitespace\tNahradi vsetky biele prebytocne znaky s jednou medzerou.\n\n";
}

/**
 * kontrolaParametrov 	- kontrola parametrov podla indexu pola
 * @param pole 			- pole reťazcov
 * @param hodnota 		- index pola (vstupny parameter)
 * @param chyba 		- naznacuje spravnost dotereaz nacitanych parametrov
 */
function kontrolaParametrov($pole,$hodnota,$chyba)
{
	if ($pole[$hodnota] < 1)
	{
		$pole[$hodnota] = ($pole[$hodnota])+1;
	}
	else
	{
		$pole[$hodnota] = ($pole[$hodnota])+1;
		$chyba = false;
	}
}
/**
 * rekPrehladavanie 	- rekurzívne prehladavanie podpriecinkov a suborov, a vratienie cesty k nájdeným suborom .h
 * @param priecinok		- rekurzívne prehladávanie od daneho priečinka
 * @param prefix 		- prefix 
 * @return				- cesty veduce k suborom .h
 */
function rekPrehladavanie($priecinok, $prefix = '')
{
	$priecinok = rtrim($priecinok, '\\/');
	$vysledok = array();

    foreach (array_diff(scandir($priecinok), array('..', '.')) as $suborAleboPriecinok) 
	{
		if (is_dir("$priecinok/$suborAleboPriecinok")) 
		{
			$vysledok = array_merge($vysledok, rekPrehladavanie("$priecinok/$suborAleboPriecinok", "$prefix$suborAleboPriecinok/"));
        } 
		else
		{
			$vysledok[] = $prefix.$suborAleboPriecinok;
        }
    }
	return preg_grep("/\.h$/", $vysledok);				//Z najdenich suborov sa vyfiltruju iba subory koncovkou ".h"
}
/**
 * WinAleboUnix 	- Bezparametrova funkcia, zisti typ operacneho systemu a podla toho vrati typ lomitka
 * @return			- lomitko
 */
function WinAleboUnix()
{
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
	{
		return '\\';									//Windows
	}
	else 
	{
		return '/';										//UNIX
	}
}
/**
 * fPriecZac 				- Sablona na ulahcenie vypisu vysledku
 * @param precinok 			- prevzata cesta od parametru "--input"
 * @return					- cesta nasadena do sablony
 */
function fPriecZac($precinok)
{
	return "<functions dir=\"$precinok\">";
}
/**
 * fPriecKon 				- Sablona na ulahcenie vypisu vysledku
 * @return					- Sablona
 */
function fPriecKon()
{
	return "</functions>";
}
/**
 * fFunZ 					- Sablona na ulahcenie vypisu vysledku
 * @param subor 			- cesta k suboru
 * @param meno 				- meno funkcie
 * @param premenArgument 	- premenlivy pocet argumentov (ano/nie)
 * @param vratTyp 			- navratovy typ funkcie
 * @return					- Sablona s nasadenymi hodnotami
 */
function fFunZ ($subor, $meno, $premenArgument, $vratTyp)
{
	return "<function file=\"$subor\" name=\"$meno\" varargs=\"$premenArgument\" rettype=\"$vratTyp\">";
}
/**
 * fFunK 					- Sablona na ulahcenie vypisu vysledku
 * @return					- Sablona, zakoncovanie funkcie
 */
function fFunK ()
{
	return "</function>";
}
/**
 * fPar 					- Sablona na ulahcenie vypisu vysledku
 * @param cisParametru 		- poradove cislo parametru
 * @param typ 				- typ parametru
 * @return					- Sablona s nasadenymi hodnotami
 */
function fPar ($cisParametru, $typ)
{
	return "<param number=\"$cisParametru\" type=\"$typ\" />";
}
/**
 * formatVystup 			- Funkcia na zformatovanie jedneho riadka
 * @param riadok 			- riadok na formatovani
 * @param jePovolene 		- povolene/nepovolenie formatovanie zavisi na parametru "--pretty-xml" 
 * @param odsadenie 		- pocet medzier pri novom zanoreni
 * @return					- formatovany riadok
 */
function formatVystup ($riadok, $jePovolene, $odsadenie)
{
	if($jePovolene == 1)												
	{
		for ($i = 0; $i < $odsadenie; $i++)
		{
			$riadok = " "  . $riadok;
		}
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
		{
			$riadok = $riadok . "\r\n";
		}
		else
		{
			$riadok = $riadok . "\n";
		}
	}
	return $riadok;
}

// ++++++++++++++++++ ----------------- HLAVNY PROGRAM ----------------- ++++++++++++++++++

$chybovaSprava = "Zadali ste nespravny vstup !";

// 			------------------- NACITANIE A KONTROLA PARAMETROV ------------------
	
	$bezChyby = true;						// Riadiaca premenna: true == parametre sa nacitali spravne
	$okParametre = array (0,0,0,0,0,0,0);	// 3 stavy jednotlivych prvkov v poli: 0 - nezadany parameter				== spravne
																				// 1 - raz zadany parameter				== spravne
																				// 2 - viac ako raz zadany parameter	== NEspravne
	$PriecinokAleboSubor = NULL;
	$nacitanaCesta = NULL;
	$pocetMedzier = NULL;
	$pocetMaxParametrov = NULL;

if ($argc == 1)												// Ziadny parameter				== nespravny vstup !
{
	$bezChyby = true;
}
elseif ($argv[1] == '--help' && $argc == 2)					// Iba parameter --help 		== spravny vstup ! + skoncim
{
	pomoc();
	exit(0);
}
elseif ($argv[1] == '--help' && $argc != 2)					// Parameter --help a dalsie argumenty 	== nespravny vstup ! 
{
	file_put_contents('php://stderr', "$chybovaSprava\n");
	exit(1);
}
		// Ostatne parametre : 
// --input=fileordir, --output=filename --pretty-xml=k --no-inline --max-par=n --no-duplicates --remove-whitespace
elseif( $argc >= 2 )
{
	for($i=1; $i<$argc; $i++)
	{
		if(strncasecmp($argv[$i],"--input",7) == 0)						//parameter --input // '0'
		{
			if((strncasecmp($argv[$i],"--input=",8) == 0) 
				&& (strncasecmp($argv[$i],"--input=",9) > 0))
			{
				$PriecinokAleboSubor = WinAleboUnix() . substr($argv[$i],8);
			}
			else
			{
				$bezChyby = false;
			}
			kontrolaParametrov(&$okParametre,0,&$bezChyby);
		}
		elseif(strncasecmp($argv[$i],"--output",8) == 0)				//parameter --output // '1'
		{
			if((strncasecmp($argv[$i],"--output=",9) == 0) 
				&& (strncasecmp($argv[$i],"--output=",10) > 0))	
			{
				$nacitanaCesta = substr($argv[$i],9);
			}
			else
			{
				$bezChyby = false;
			}
			kontrolaParametrov(&$okParametre,1,&$bezChyby);
		}
		elseif(strncasecmp($argv[$i],"--pretty-xml",12) == 0)			// parameter --pretty-xml // '2'
		{
			if ((strlen($argv[$i])) == 12)
			{
				$pocetMedzier = 4;
			}
			elseif((strncasecmp($argv[$i],"--pretty-xml=",13) == 0) 
				&& (strncasecmp($argv[$i],"--pretty-xml=",14) > 0) 
				&& (is_numeric(substr($argv[$i],13)))
				&& ((substr($argv[$i],13)) >= 0))
					
			{
				$pocetMedzier = substr($argv[$i],13);
			}
			else
			{
				$bezChyby = false;
			}
			kontrolaParametrov(&$okParametre,2,&$bezChyby);
		}
		elseif($argv[$i] == "--no-inline")								//parameter --no-inline // '3'
		{
			kontrolaParametrov(&$okParametre,3,&$bezChyby);
		}
		elseif((strncasecmp($argv[$i],"--max-par=",10) == 0) 			//parameter --max-par=n // '4'
			&& (strncasecmp($argv[$i],"--max-par=",11) > 0)
			&& (is_numeric(substr($argv[$i],10)))
			&& ((substr($argv[$i],10)) >= 0))
		{
			$pocetMaxParametrov = substr($argv[$i],10);
			kontrolaParametrov(&$okParametre,4,&$bezChyby);
		}
		elseif($argv[$i] == "--no-duplicates")							//parameter --no-duplicates // '5'
		{
			kontrolaParametrov(&$okParametre,5,&$bezChyby);
		}
		elseif($argv[$i] == "--remove-whitespace")						//parameter --remove-whitespace // '6'
		{
			kontrolaParametrov(&$okParametre,6,&$bezChyby);
		}
		else															//nespravny parameter
		{
			$bezChyby = false;
		}
	}
}
	// 	-------------------SPRACOVANIE PARAMETROV ------------------	
	
if ($bezChyby)						// Parametre su zadane spravne
{
	$testSuborAleboPriecinok = true;
	$realnaCesta = NULL;
	$spracovanySubor = array();
	$hSuborySCestou = array();		//realne cesty k subourom ".h"
	$absolutnaCesta = getcwd();		// nacitanie absolutnej cesty
	$jePriecinok = false;
	$jeSubor = false;
	$OkVyslednySubor = NULL;
	$menoFunkcie = NULL;
	$vyslednySubor = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	$vyslednySubor = formatVystup($vyslednySubor, $okParametre[2], ($pocetMedzier)*0);

	$realnaCesta = $absolutnaCesta . $PriecinokAleboSubor;				//absolutna + zadana cesta (--input)
	
		// 	------------------- '--input'  ------------------
	
	if (is_dir($realnaCesta))									//bol zadany priecinok: vyhladaju sa všetky subory ".h" aj v podpriecinkoch
	{
		$hSuborySCestou = (rekPrehladavanie($realnaCesta));
		$jePriecinok = true;
	}
	elseif(is_file($realnaCesta))								//bola zadana presna cesta
	{
		$hSuborySCestou[0] = substr($PriecinokAleboSubor,1);
		$realnaCesta = $absolutnaCesta;
		$jeSubor = true;
	}
	else														//osetrenie: chybna cesta k priecinku alebo k suboru
	{
		file_put_contents('php://stderr', "Zadana cesta neexistuje !\n");		
		exit (2);								
	}
			
	foreach($hSuborySCestou as $cestaKSuboru)							//uloz do $spracovanySubor(pole) obsah vetkych *.h
	{
		$duplikaty = NULL;
		$realnaCestaOK =  $realnaCesta . WinAleboUnix() . $cestaKSuboru;
		
		if( is_readable($realnaCestaOK))
		{
			// Dekomponované  filtrácie na primitívne časti
			$spracovanySubor = trim(file_get_contents($realnaCestaOK));					//zmazem:
			$spracovanySubor = preg_replace("/[\s]*\/\/.*/","", $spracovanySubor);					//zacinajuci s '//'
			$spracovanySubor = preg_replace("/#ifdef[\S\s]*?#endif/", "", $spracovanySubor);
			$spracovanySubor = preg_replace("/{[\S\s]*?}/", ";", $spracovanySubor);					//telo '{ }' napr. funkcie				
			$spracovanySubor = preg_replace("/;/", "\n", $spracovanySubor);			
			$spracovanySubor = preg_replace("/\s*;.*\)/", "", $spracovanySubor);			
			$spracovanySubor = preg_replace("/[\s]*#.*/", "", $spracovanySubor);					//zacinajuci s '#'							
			$spracovanySubor = preg_replace("/.*\/\*[\S\s]*?(\*\/).*/", "", $spracovanySubor);		//viacriadkove komentare '/* komentar */'												
			$spracovanySubor = preg_replace("/^\s*\w+\s*\(.*\).*/", "", $spracovanySubor);
			$spracovanySubor = preg_replace("/\n\s/", ";\n", $spracovanySubor);						//prazdne riadky
			$spracovanySubor = preg_replace("/;.*/", ";", $spracovanySubor);							//vsetko po ';'			
			$spracovanySubor = preg_replace("/.*\)/", "$0;", $spracovanySubor);						//pridam na koniec funkcii ';'
			$spracovanySubor = preg_replace("/\n/", " ", $spracovanySubor);
			$spracovanySubor = preg_replace("/;+/", ";", $spracovanySubor);
			$spracovanySubor = preg_replace("/;/", "\n", $spracovanySubor);
			$spracovanySubor = preg_replace("/.*\(.*\)/", "$0;", $spracovanySubor);
					// po filtrácii vyberem funkciu ako retazec, každu do iného pola
			preg_match_all("/\s*\w+((\s*\*)*\s*\w+)+\s*\(.*\);.*|\s*\w+((\s*\*)*\s+\w+)+\s*\(.*\);.*/", $spracovanySubor, $spracovanySubor); 
						
			// -----------------  RIESENIE 'dir' ------------------------------
			if ($testSuborAleboPriecinok)
			{
				if($okParametre[0] == 0)
				{
					$OkVyslednySubor = $OkVyslednySubor . formatVystup(fPriecZac("./"), $okParametre[2], ($pocetMedzier)*0);
				}
				elseif($jePriecinok == true)
				{
					$PriecinokAleboSubor = preg_replace('/^\\/+|^\\\\+(.*)/', '\\1', $PriecinokAleboSubor);
					
					$OkVyslednySubor = $OkVyslednySubor . formatVystup(fPriecZac($PriecinokAleboSubor), $okParametre[2], ($pocetMedzier)*0);
				}
				elseif($jeSubor == true)
				{
					$OkVyslednySubor = $OkVyslednySubor . formatVystup(fPriecZac(""), $okParametre[2], ($pocetMedzier)*0);
				}
				$testSuborAleboPriecinok = false;
			}
			// -----------------  RIESENIE 'function' ------------------------------

			foreach($spracovanySubor[0] as $nefiltrovane)					//cely riadok funkcie
			{														//napr.: unsigned int hFunc(const char *str,...)		
				$varargs=NULL;
				if(preg_match("/\(.*\.\.\.\)/", $nefiltrovane))		//filter na PREMENNY pocet premennych "..."
				{
					$varargs = "yes";
				}
				else
				{
					$varargs = "no";
				}
				preg_match("/\w*\s*\(/", $nefiltrovane, $nefiltrMeno);				//filter na prve slovo pred zatvorkou a aj '('	napr.: "copyTable("			
				preg_match("/\w*/", $nefiltrMeno[0], $menoFunkcie);					//filter na prve slovo == meno funkce 			napr.: "copyTable"
				preg_match("/.*?\(/", $nefiltrovane, $nefiltrVratTyp);				//necha vsetko pred zatvorkou 					napr.: "Thtab_t * copyTable("
				
				$vratTyp = preg_replace("/\s*\w*\s*\(/", "", $nefiltrVratTyp[0]);	//filter na navratovu hodnotu
				$vratTyp = preg_replace("/^\s*/", "", $vratTyp);
				if($okParametre[6])													//parameter --remove-whitespace // '6'
				{
					$vratTyp = preg_replace("/\s+/", " ", $vratTyp);
					$vratTyp = preg_replace("/\s\*/", "*", $vratTyp);
					$vratTyp = preg_replace("/^\s*/", "", $vratTyp);
					
				}
				if(($okParametre[3]))												//parameter --no-inline //3
				{
					if(preg_match("/\s+inline\s+/", $vratTyp))
					{
						continue;
					}
					
				}
				preg_match("/\(.*\)/", $nefiltrovane, $nefiltrFunkc);					//necha parametre zo zatvorkami napr.: "(const char *str,...)"
				
				$nefiltrFunkc[0] = preg_replace("/^\(/", "", $nefiltrFunkc[0]);   	//vymazem: prvu zatvorku
				$nefiltrFunkc[0] = preg_replace("/\)$/", "", $nefiltrFunkc[0]);		//poslednu zatvorku
				
				$typPole = NULL;
				
				if($nefiltrFunkc[0] != NULL)
				{
					preg_match_all("/,/", $nefiltrFunkc[0], $pocetCiar);
					$cisloParam = count($pocetCiar[0])+1;
					
					$nefiltrFunkc[0] = preg_replace("/.*/", "$0,", $nefiltrFunkc[0]);		//prida na koniec ','
					$nefiltrFunkc[0] = preg_replace("/,,/", ",", $nefiltrFunkc[0]);		// ',,' nahradi ','
					preg_match_all("/.*?,/", $nefiltrFunkc[0], $nefiltrTyp);
					
					for($i = 0; $i < $cisloParam; $i++)
					{
						$typ = preg_replace("/\s*\w+\s*,$/", "", $nefiltrTyp[0][$i]);				//vymaze posledne slovo/meno_parametru
						$typ = preg_replace("/^\s*/", "", $typ);
						if(preg_match("/\s*\.\.\.\s*,*/", $typ) || $typ == "..." || $typ == "" || $typ == "void" || $typ == ",")  		//uvedene znaky alebo retazce NEPATRIA do kategorii parameter funkcie
						{
							break;
						}
						else
						{
							if($okParametre[6])												//parameter --remove-whitespace // '6'
							{
								$$typ = preg_replace("/\s+/", " ", $typ);
								$$typ = preg_replace("/\s\*/", "*", $typ);
							}
							$typPole[$i] = $typ;
						}
					}
				}
				$pocetPar = count($typPole);
				
				if($okParametre[4])												//parameter --max-par=n // '4'
				{
					if($okParametre[5])											//parameter --no-duplicates // '5'
					{
						$parameterFunkcie = $menoFunkcie[0];
						$duplikaty = $duplikaty . " " . $parameterFunkcie;
						if(!(preg_match("/\s+$parameterFunkcie\s+/", $duplikaty)))
						{					
							if($pocetPar <= $pocetMaxParametrov)
							{
								$OkVyslednySubor = $OkVyslednySubor . formatVystup(fFunZ($cestaKSuboru,$menoFunkcie[0],$varargs,$vratTyp), $okParametre[2], ($pocetMedzier)*1);
								
								for($i = 0; $i < $pocetPar; $i++)
								{
									$OkVyslednySubor = $OkVyslednySubor . formatVystup(fPar(($i)+1,$typPole[$i]), $okParametre[2], ($pocetMedzier)*2);
								}
								$OkVyslednySubor = $OkVyslednySubor . formatVystup(fFunK(), $okParametre[2], ($pocetMedzier)*1);
							}				
						}
					}
					else
					{
						if($pocetPar <= $pocetMaxParametrov)
						{
							$OkVyslednySubor = $OkVyslednySubor . formatVystup(fFunZ($cestaKSuboru,$menoFunkcie[0],$varargs,$vratTyp), $okParametre[2], ($pocetMedzier)*1);
							
							for($i = 0; $i < $pocetPar; $i++)
							{
								$OkVyslednySubor = $OkVyslednySubor . formatVystup(fPar(($i)+1,$typPole[$i]), $okParametre[2], ($pocetMedzier)*2);
							}
							$OkVyslednySubor = $OkVyslednySubor . formatVystup(fFunK(), $okParametre[2], ($pocetMedzier)*1);
						}
					}
				}
				else
				{
					if($okParametre[5])											//parameter --no-duplicates // '5'
					{
					
						$parameterFunkcie = $menoFunkcie[0];
						$duplikaty = $duplikaty . " " . $parameterFunkcie;
						if(!(preg_match("/\s+$parameterFunkcie\s+/", $duplikaty)))
						{
							$OkVyslednySubor = $OkVyslednySubor . formatVystup(fFunZ($cestaKSuboru,$menoFunkcie[0],$varargs,$vratTyp), $okParametre[2], ($pocetMedzier)*1);
								
							for($i = 0; $i < $pocetPar; $i++)
							{
								$OkVyslednySubor = $OkVyslednySubor . formatVystup(fPar(($i)+1,$typPole[$i]), $okParametre[2], ($pocetMedzier)*2);
							}
							$OkVyslednySubor = $OkVyslednySubor . formatVystup(fFunK(), $okParametre[2], ($pocetMedzier)*1);
						}
					}
					else
					{
						$OkVyslednySubor = $OkVyslednySubor . formatVystup(fFunZ($cestaKSuboru,$menoFunkcie[0],$varargs,$vratTyp), $okParametre[2], ($pocetMedzier)*1);
							
						for($i = 0; $i < $pocetPar; $i++)
						{
							$OkVyslednySubor = $OkVyslednySubor . formatVystup(fPar(($i)+1,$typPole[$i]), $okParametre[2], ($pocetMedzier)*2);
						}
						$OkVyslednySubor = $OkVyslednySubor . formatVystup(fFunK(), $okParametre[2], ($pocetMedzier)*1);
					}
				}			
			}
			$vyslednySubor = $vyslednySubor . $OkVyslednySubor;					//zapis udajov do retazca, ktory na konci programu bude vypisany
			$OkVyslednySubor = NULL;	
		}
		else													//osetrenie chyboveho stavu: "neexistujuci zadany vstupny subor"
		{
			file_put_contents('php://stderr', "Chyba pri otvarani suboru !\n");
			exit (2);
		}	
	}
	
	$OkVyslednySubor = $OkVyslednySubor . formatVystup(fPriecKon(), $okParametre[2], ($pocetMedzier)*0);
	$vyslednySubor = $vyslednySubor . $OkVyslednySubor;
	
	// 	------------------- '--output'------------------
	if( $nacitanaCesta != NULL)												//parameter --output // '1'
	{
		if((file_put_contents($nacitanaCesta, $vyslednySubor) === false))
		{
			file_put_contents('php://stderr', "Chyba pri zapisu do suboru !\n");
			exit (3);
		}
		elseif(is_writable($nacitanaCesta))
		{
			file_put_contents($nacitanaCesta,$vyslednySubor);
			exit(0);
		}
		else
		{
			file_put_contents('php://stderr', "Chyba pri zapisu do suboru !\n");
			exit (3);
		}
	}
	else
	{
		echo $vyslednySubor;
		exit(0);
	}
}
else																//nespravny parameter
{
	file_put_contents('php://stderr', "$chybovaSprava\n");
	exit(1);
}

?>