BEGIN
    FOR rec IN (
        SELECT 'DROP ' || object_type || ' ' || object_name || DECODE ( object_type, 'TABLE', ' CASCADE CONSTRAINTS PURGE' ) AS v_sql
        FROM user_objects
        WHERE object_type IN ( 'TABLE', 'VIEW', 'PACKAGE', 'TYPE', 'PROCEDURE', 'FUNCTION', 'TRIGGER', 'SEQUENCE' )
        ORDER BY object_type, object_name
    ) LOOP
        EXECUTE IMMEDIATE rec.v_sql;
    END LOOP;
END;
/

CREATE SEQUENCE osoba_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE osoba 
(
    id_osoba    INTEGER         CONSTRAINT PK_osoba_id         PRIMARY KEY,
    meno        VARCHAR2(20)    CONSTRAINT NN_osoba_meno       NOT NULL,
    priezvisko  VARCHAR2(20)    CONSTRAINT NN_osoba_priezvisko NOT NULL,
    adresa      VARCHAR2(50)    CONSTRAINT NN_osoba_adresa     NOT NULL,
    telefon     INTEGER         CONSTRAINT NN_osoba_telefon    NOT NULL,
    mail        VARCHAR2(20)    CONSTRAINT NN_osoba_mail       NOT NULL
);

CREATE SEQUENCE opravnenia_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE opravnenia 
(
    id_opravnenia INTEGER         CONSTRAINT PK_opravnenia_id         PRIMARY KEY,
    meno          VARCHAR2(20)    CONSTRAINT NN_opravnenia_meno       NOT NULL
);

CREATE SEQUENCE uzivatel_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE uzivatel 
( 
    id_uzivatel      INTEGER         CONSTRAINT PK_uzivatel_id           PRIMARY KEY,
    osoba_id         INTEGER         CONSTRAINT FK_uzivatel_osoba        REFERENCES osoba(id_osoba), 
    opravnenia_id    INTEGER         CONSTRAINT FK_uzivatel_opravnenia   REFERENCES opravnenia(id_opravnenia),  
    login            VARCHAR2(20)    CONSTRAINT NN_uzivatel_login        NOT NULL,
    heslo            VARCHAR2(20)    CONSTRAINT NN_uzivatel_heslo        NOT NULL

);

CREATE SEQUENCE kategoria_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE kategoria 
(
    id_kategoria  INTEGER         CONSTRAINT PK_kategoria_id          PRIMARY KEY,
    meno          VARCHAR2(20)    CONSTRAINT NN_kategoria_meno        NOT NULL
);

CREATE SEQUENCE software_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE software 
(
    id_software   INTEGER         CONSTRAINT PK_software_id            PRIMARY KEY,
    typ_licencie  VARCHAR2(20)    CONSTRAINT NN_software_typ_licencie  NOT NULL,
    konfiguracia  VARCHAR2(100)   CONSTRAINT NN_software_konfiguracia  NOT NULL
);

CREATE SEQUENCE hardware_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE hardware 
(
    id_hardware   INTEGER         CONSTRAINT PK_hardware_id            PRIMARY KEY,
    gramaz        NUMBER(10,2)    CONSTRAINT NN_hardware_gramaz        NOT NULL,
    rozmery       VARCHAR2(20)    CONSTRAINT NN_hardware_rozmery       NOT NULL
);

CREATE SEQUENCE tovar_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE tovar 
(
    id_tovar      INTEGER         CONSTRAINT PK_tovar_id              PRIMARY KEY,
    nazov         VARCHAR2(50)    CONSTRAINT NN_tovar_nazov           NOT NULL,
    popis         VARCHAR2(50)	  CONSTRAINT NN_tovar_popis           NOT NULL,
    pocet         INTEGER         CONSTRAINT NN_tovar_pocet           NOT NULL,
    znacka        VARCHAR2(20)    CONSTRAINT NN_tovar_znacka		  NOT NULL,
    cena		  NUMBER(10,2)    CONSTRAINT NN_tovar_cena			  NOT NULL,
    dph 		  NUMBER(10,2)    CONSTRAINT NN_tovar_dph			  NOT NULL,
    skladom       CHAR(1)		  CONSTRAINT NN_tovar_skladom         CHECK (skladom in ('A', 'N')),
    software_id   INTEGER		  CONSTRAINT FK_tovar_software        REFERENCES software(id_software),
    hardware_id   INTEGER		  CONSTRAINT FK_tovar_hardware        REFERENCES hardware(id_hardware),
    kategoria_id  INTEGER  		  CONSTRAINT FK_tovar_kategoria		  REFERENCES kategoria(id_kategoria)
);

CREATE SEQUENCE hodnotenie_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE hodnotenie 
(
    id_hodnotenie INTEGER         CONSTRAINT PK_hodnotenie_id         PRIMARY KEY,
    uzivatel_id   INTEGER  		  CONSTRAINT FK_hodnotenie_uzivatel   REFERENCES uzivatel(id_uzivatel),
    tovar_id      INTEGER 		  CONSTRAINT FK_hodnotenie_tovar      REFERENCES tovar(id_tovar),
    znamka        INTEGER	 	  CONSTRAINT NN_hodnotenie_znamka     CHECK (znamka BETWEEN 1 AND 5)
);

CREATE SEQUENCE kosik_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE kosik 
(
    id_kosik      INTEGER         CONSTRAINT PK_kosik_id             PRIMARY KEY,
    uzivatel_id   INTEGER         CONSTRAINT FK_kosik_tovar          REFERENCES uzivatel(id_uzivatel)
);

CREATE SEQUENCE pol_kosik_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE pol_kosik 
(
    id_pol_kosik  INTEGER         CONSTRAINT PK_pol_kosik_id         PRIMARY KEY,
    kosik_id      INTEGER  		  CONSTRAINT FK_pol_kosik_uzivatel   REFERENCES kosik(id_kosik),
    tovar_id      INTEGER         CONSTRAINT FK_pol_kosik_tovar      REFERENCES tovar(id_tovar),
    mnozstvo      INTEGER         CONSTRAINT NN_pol_kosik_mnozstvo   NOT NULL
);

CREATE SEQUENCE objednavka_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE objednavka 
(
    id_objednavka    INTEGER          CONSTRAINT PK_objednavka_id            PRIMARY KEY,
    zaplatene        CHAR(1)		  CONSTRAINT NN_objednavka_zaplatene     CHECK (zaplatene in ('A', 'N')),
    expedovane       CHAR(1)		  CONSTRAINT NN_objednavka_expedovane    CHECK (expedovane in ('A', 'N')),
    uzivatel_id      INTEGER		  CONSTRAINT FK_objednavka_uzivatel_id   REFERENCES uzivatel(id_uzivatel),
    vybavil_id       INTEGER          CONSTRAINT FK_objednavka_vybavil_id	 REFERENCES uzivatel(id_uzivatel),
    kosik_id         INTEGER          CONSTRAINT FK_objednavka_kosik_id	     REFERENCES kosik(id_kosik),
    cena             NUMBER(10,2)      CONSTRAINT NN_objednavka_cena         NOT NULL
);

INSERT INTO osoba (ID_OSOBA, MENO, PRIEZVISKO, ADRESA, TELEFON, MAIL) VALUES (osoba_seq.nextval, 'Misel', 'Sobotkova', 'Fugnerova 101', '0948304516', 'mismisacek@gmail.com');
INSERT INTO osoba (ID_OSOBA, MENO, PRIEZVISKO, ADRESA, TELEFON, MAIL) VALUES (osoba_seq.nextval, 'Viktor', 'Toth', 'Neznama 37', '0912446555', 'viktor@icloud.com');
INSERT INTO osoba (ID_OSOBA, MENO, PRIEZVISKO, ADRESA, TELEFON, MAIL) VALUES (osoba_seq.nextval, 'Vlado', 'Ptacin', 'Spartakovska 18', '0912444555', 'vptacin@icloud.com');
INSERT INTO osoba (ID_OSOBA, MENO, PRIEZVISKO, ADRESA, TELEFON, MAIL) VALUES (osoba_seq.nextval, 'Filip', 'Bajanik', 'Podhajska 13', '0944333222', 'niorko@icloud.com');
INSERT INTO osoba (ID_OSOBA, MENO, PRIEZVISKO, ADRESA, TELEFON, MAIL) VALUES (osoba_seq.nextval, 'Martin', 'Pavelka', 'Hlboka 18', '0948304516', 'dlxko@icloud.com');

INSERT INTO opravnenia (ID_OPRAVNENIA, MENO) VALUES (opravnenia_seq.nextval, 'admin');
INSERT INTO opravnenia (ID_OPRAVNENIA, MENO) VALUES (opravnenia_seq.nextval, 'manager');
INSERT INTO opravnenia (ID_OPRAVNENIA, MENO) VALUES (opravnenia_seq.nextval, 'user');

INSERT INTO uzivatel (ID_UZIVATEL, OSOBA_ID, OPRAVNENIA_ID, LOGIN, HESLO) VALUES (uzivatel_seq.nextval, osoba_seq.currval-4, opravnenia_seq.currval-2, 'admin1', 'hesloA');
INSERT INTO uzivatel (ID_UZIVATEL, OSOBA_ID, OPRAVNENIA_ID, LOGIN, HESLO) VALUES (uzivatel_seq.nextval, osoba_seq.currval-3, opravnenia_seq.currval-2, 'admin2', 'hesloB');
INSERT INTO uzivatel (ID_UZIVATEL, OSOBA_ID, OPRAVNENIA_ID, LOGIN, HESLO) VALUES (uzivatel_seq.nextval, osoba_seq.currval, opravnenia_seq.currval-1, 'xpavel27', 'heslo');
INSERT INTO uzivatel (ID_UZIVATEL, OSOBA_ID, OPRAVNENIA_ID, LOGIN, HESLO) VALUES (uzivatel_seq.nextval, osoba_seq.currval-1, opravnenia_seq.currval, 'xbajan01', 'heslo2');
INSERT INTO uzivatel (ID_UZIVATEL, OSOBA_ID, OPRAVNENIA_ID, LOGIN, HESLO) VALUES (uzivatel_seq.nextval, osoba_seq.currval-2, opravnenia_seq.currval, 'xptaci01', 'heslo3');

INSERT INTO kategoria (ID_KATEGORIA, MENO) VALUES (kategoria_seq.nextval, 'notebook');
INSERT INTO kategoria (ID_KATEGORIA, MENO) VALUES (kategoria_seq.nextval, 'processor');
INSERT INTO kategoria (ID_KATEGORIA, MENO) VALUES (kategoria_seq.nextval, 'os');

INSERT INTO software (ID_SOFTWARE, TYP_LICENCIE, KONFIGURACIA) VALUES (software_seq.nextval, 'Freeware', 'Mac OSX');
INSERT INTO software (ID_SOFTWARE, TYP_LICENCIE, KONFIGURACIA) VALUES (software_seq.nextval, 'GPL', 'Linux');
INSERT INTO software (ID_SOFTWARE, TYP_LICENCIE, KONFIGURACIA) VALUES (software_seq.nextval, 'Malware', 'Windows');

INSERT INTO hardware (ID_HARDWARE, GRAMAZ, ROZMERY) VALUES (hardware_seq.nextval, '3500,5', '30x30x30');
INSERT INTO hardware (ID_HARDWARE, GRAMAZ, ROZMERY) VALUES (hardware_seq.nextval, '2500,5', '20x20x20');
INSERT INTO hardware (ID_HARDWARE, GRAMAZ, ROZMERY) VALUES (hardware_seq.nextval, '1500,5', '10x10x10');

INSERT INTO tovar (ID_TOVAR, NAZOV, POPIS, POCET, ZNACKA, CENA, DPH, SKLADOM, HARDWARE_ID, KATEGORIA_ID) VALUES (tovar_seq.nextval, 'MacBook Pro', 'MacBook Pro Retina 2014', '1', 'Apple', '1000', '200', 'A', hardware_seq.currval, kategoria_seq.currval-2);
INSERT INTO tovar (ID_TOVAR, NAZOV, POPIS, POCET, ZNACKA, CENA, DPH, SKLADOM, SOFTWARE_ID, KATEGORIA_ID) VALUES (tovar_seq.nextval, 'Intel Core I7', 'Processor od intelu', '2', 'Intel', '500', '100', 'N', hardware_seq.currval-1, kategoria_seq.currval-1);
INSERT INTO tovar (ID_TOVAR, NAZOV, POPIS, POCET, ZNACKA, CENA, DPH, SKLADOM, SOFTWARE_ID, KATEGORIA_ID) VALUES (tovar_seq.nextval, 'Lenovo G500', 'Netobook od lenova', '3', 'Lenovo', '250', '50', 'A', hardware_seq.currval-2, kategoria_seq.currval-2);

INSERT INTO tovar (ID_TOVAR, NAZOV, POPIS, POCET, ZNACKA, CENA, DPH, SKLADOM, SOFTWARE_ID, KATEGORIA_ID) VALUES (tovar_seq.nextval, 'Mac OSX Yosemite', 'Operacny system od Apple', '1', 'Apple', '1000', '200', 'N', software_seq.currval-2, kategoria_seq.currval);
INSERT INTO tovar (ID_TOVAR, NAZOV, POPIS, POCET, ZNACKA, CENA, DPH, SKLADOM, SOFTWARE_ID, KATEGORIA_ID) VALUES (tovar_seq.nextval, 'Ubuntu 10.04', 'Operacny system od komunity', '2', 'Canoncial', '100', '100', 'A', software_seq.currval-1, kategoria_seq.currval);
INSERT INTO tovar (ID_TOVAR, NAZOV, POPIS, POCET, ZNACKA, CENA, DPH, SKLADOM, SOFTWARE_ID, KATEGORIA_ID) VALUES (tovar_seq.nextval, 'Windows 7', 'Virus od Mrkvosofta', '3', 'Microsoft', '250', '50', 'N', software_seq.currval, kategoria_seq.currval);

INSERT INTO hodnotenie (ID_HODNOTENIE, UZIVATEL_ID, TOVAR_ID, ZNAMKA) VALUES (hodnotenie_seq.nextval, uzivatel_seq.currval-2, tovar_seq.currval-5, '1');
INSERT INTO hodnotenie (ID_HODNOTENIE, UZIVATEL_ID, TOVAR_ID, ZNAMKA) VALUES (hodnotenie_seq.nextval, uzivatel_seq.currval-1, tovar_seq.currval-4, '2');
INSERT INTO hodnotenie (ID_HODNOTENIE, UZIVATEL_ID, TOVAR_ID, ZNAMKA) VALUES (hodnotenie_seq.nextval, uzivatel_seq.currval, tovar_seq.currval-3, '3');
INSERT INTO hodnotenie (ID_HODNOTENIE, UZIVATEL_ID, TOVAR_ID, ZNAMKA) VALUES (hodnotenie_seq.nextval, uzivatel_seq.currval-2, tovar_seq.currval-2, '4');
INSERT INTO hodnotenie (ID_HODNOTENIE, UZIVATEL_ID, TOVAR_ID, ZNAMKA) VALUES (hodnotenie_seq.nextval, uzivatel_seq.currval-1, tovar_seq.currval-1, '5');
INSERT INTO hodnotenie (ID_HODNOTENIE, UZIVATEL_ID, TOVAR_ID, ZNAMKA) VALUES (hodnotenie_seq.nextval, uzivatel_seq.currval, tovar_seq.currval, '5');

INSERT INTO kosik (ID_KOSIK, UZIVATEL_ID) VALUES (kosik_seq.nextval, uzivatel_seq.currval);
INSERT INTO kosik (ID_KOSIK, UZIVATEL_ID) VALUES (kosik_seq.nextval, uzivatel_seq.currval-1);
INSERT INTO kosik (ID_KOSIK, UZIVATEL_ID) VALUES (kosik_seq.nextval, uzivatel_seq.currval-2);

INSERT INTO pol_kosik (ID_POL_KOSIK, KOSIK_ID, TOVAR_ID, MNOZSTVO) VALUES (pol_kosik_seq.nextval, kosik_seq.currval, tovar_seq.currval, 1);
INSERT INTO pol_kosik (ID_POL_KOSIK, KOSIK_ID, TOVAR_ID, MNOZSTVO) VALUES (pol_kosik_seq.nextval, kosik_seq.currval-1, tovar_seq.currval-1, 2);
INSERT INTO pol_kosik (ID_POL_KOSIK, KOSIK_ID, TOVAR_ID, MNOZSTVO) VALUES (pol_kosik_seq.nextval, kosik_seq.currval-2, tovar_seq.currval-2, 3);
INSERT INTO pol_kosik (ID_POL_KOSIK, KOSIK_ID, TOVAR_ID, MNOZSTVO) VALUES (pol_kosik_seq.nextval, kosik_seq.currval, tovar_seq.currval-3, 4);
INSERT INTO pol_kosik (ID_POL_KOSIK, KOSIK_ID, TOVAR_ID, MNOZSTVO) VALUES (pol_kosik_seq.nextval, kosik_seq.currval-1, tovar_seq.currval-4, 5);
INSERT INTO pol_kosik (ID_POL_KOSIK, KOSIK_ID, TOVAR_ID, MNOZSTVO) VALUES (pol_kosik_seq.nextval, kosik_seq.currval, tovar_seq.currval, 7);
INSERT INTO pol_kosik (ID_POL_KOSIK, KOSIK_ID, TOVAR_ID, MNOZSTVO) VALUES (pol_kosik_seq.nextval, kosik_seq.currval-1, tovar_seq.currval-1, 8);
INSERT INTO pol_kosik (ID_POL_KOSIK, KOSIK_ID, TOVAR_ID, MNOZSTVO) VALUES (pol_kosik_seq.nextval, kosik_seq.currval-2, tovar_seq.currval-2, 9);

INSERT INTO objednavka (ID_OBJEDNAVKA, ZAPLATENE, EXPEDOVANE, UZIVATEL_ID, VYBAVIL_ID, KOSIK_ID, CENA) VALUES (objednavka_seq.nextval, 'A', 'N', uzivatel_seq.currval, uzivatel_seq.currval-3, kosik_seq.currval, '3000');
INSERT INTO objednavka (ID_OBJEDNAVKA, ZAPLATENE, EXPEDOVANE, UZIVATEL_ID, VYBAVIL_ID, KOSIK_ID, CENA) VALUES (objednavka_seq.nextval, 'N', 'A', uzivatel_seq.currval-1, uzivatel_seq.currval-3, kosik_seq.currval-1, '3000');
INSERT INTO objednavka (ID_OBJEDNAVKA, ZAPLATENE, EXPEDOVANE, UZIVATEL_ID, VYBAVIL_ID, KOSIK_ID, CENA) VALUES (objednavka_seq.nextval, 'A', 'N', uzivatel_seq.currval-2, uzivatel_seq.currval-3, kosik_seq.currval-2, '3000');

/* Zobraz cislo objednavky, cena, priezvisko, adresu a telefon
   z tabuliek objednavka, uzivatel, osoba
   pri objednavkach nad 1000Kc */
SELECT ID_OBJEDNAVKA, CENA, MENO, PRIEZVISKO, ADRESA, TELEFON
FROM OBJEDNAVKA O INNER JOIN  UZIVATEL  U ON (O.UZIVATEL_ID = U.ID_UZIVATEL) INNER JOIN OSOBA OS ON (U.OSOBA_ID = OS.ID_OSOBA)
WHERE CENA > 1000;

/* Zobraz loginy a mena 
   z tabuliek uzivatel a opravnenia
   iba uzivatelov, ktory su zamestnanci */
SELECT LOGIN, MENO
FROM UZIVATEL U INNER JOIN  OPRAVNENIA  O ON (U.OPRAVNENIA_ID = O.ID_OPRAVNENIA) 
WHERE ID_OPRAVNENIA < 3;

/* Zobraz cislo kosika, nazov tovaru, mnozstvo v kosiku, cenu tovaru a vyslednu cenu pri mnozstve
   z tabuliek pol_kosik a tovar 
   pri tovare kde je cena nad 250 Kc */
SELECT KOSIK_ID, NAZOV, MNOZSTVO, CENA, MNOZSTVO*CENA DOKOPY
FROM POL_KOSIK P INNER JOIN  TOVAR  T ON (P.TOVAR_ID = T.ID_TOVAR) 
WHERE CENA > 250;

/* Zobraz cislo kosika, celkovu sumu celeho kosika
   z tabuliek pol_kosik a tovar
   a zgrupujeme polozky kde je rovnake id kosika */
SELECT KOSIK_ID, SUM(CENA*MNOZSTVO)  KOSIK_SUMA
FROM POL_KOSIK P INNER JOIN  TOVAR  T ON (P.TOVAR_ID = T.ID_TOVAR) 
GROUP BY KOSIK_ID;

/* Zobraz pocet poloziek tovaru pre kazdu kategoriu
   z tabuliek tovar a kategoria
   zgrupujeme polozky kde je rovnaka kategoria */
SELECT COUNT(*) POCET_V_KATEGORII
FROM TOVAR T INNER JOIN KATEGORIA K ON (T.KATEGORIA_ID = K.ID_KATEGORIA)
GROUP BY ID_KATEGORIA;

/* Vypise nazov tovaru
    z tabulky tovar
    ktory nikto nema v kosiku, teda ho neobjednava - vyhladava v pol_kosik */
SELECT NAZOV
FROM TOVAR T
WHERE NOT EXISTS 
(SELECT * FROM POL_KOSIK P WHERE TOVAR_ID = T.ID_TOVAR );

/* Vypise vsetky osobne informacie o administratoroch 
   z tabulky osoba
   vybera iba prvky kde v tabulke uzivatel je opravnenie administrator */
SELECT *
FROM OSOBA
WHERE ID_OSOBA IN 
(SELECT OSOBA_ID FROM UZIVATEL WHERE OPRAVNENIA_ID = 1 );
