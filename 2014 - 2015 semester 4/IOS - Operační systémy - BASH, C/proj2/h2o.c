// VUT FIT 2015
// IOS projekt c. 2
// Meno: Viktor Tóth
// Login: xtothv01

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <pthread.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/stat.h>

#define NEVYHOVUJUCE_CISLO -1

// struktura pre parametry
typedef struct parametre{
    int pocetKyslik;            //N
    int genHydrogen;           	//GH
    int genOxygen;             	//GO
    int fBond;					//fBond
}Tparam;

typedef struct initMem
{
	int cisloAkcie;				// int action_id;
	int VodikCislo;             // int vodik_id;
	int KyslikCislo;            // int kyslik_id;
	int VodikPocitadlo;         // int counter_vodik;
	int KyslikPocitadlo;        // int counter_kyslik;
	int BondPriprava;           // int bond_prepare;
	int Bonded;                 // int bonded;
	int VodikPocitadlo2;        // int pocitadlo_vod;
	int KyslikPocitadlo2;       // int pocitadlo_kys;
	
  
  	sem_t vytvorenie;
	sem_t hydrogen;
	sem_t oxygen;
	sem_t koniecHydrogen;
	sem_t koniecOxygen;
	sem_t dalsi;
	sem_t zapis;

}TMemory;

FILE *subor = NULL;
TMemory *memory=NULL;
int memoryCounter=0;

void help();
int nahodneCislo(int hodnota);
int string_to_number(char *retazec);
bool nacitajParametre(Tparam *parameter,char *argv []);
void kontrolaParametrov (int argc, char *argv[], Tparam *parameter);
void otvorSubor();
void creatMemory();
void closeMemory();
void Hydrogens(Tparam *parameter);
void Oxygens(Tparam *parameter);


int main(int argc, char *argv[])
{
	pid_t OxygenPID, HydrogenPID;
	/*
    stuktura do kotoreho neskor budu nacitane parametre
    */
    Tparam parameter;
	
	/*
    pri nevhodnych parametrov skonci program exit(1)
    pri vhodnych parametrov parametre sa zapisu do struktury parameter
    */
	kontrolaParametrov(argc,argv,&parameter);
	
	creatMemory();
	otvorSubor();
	fflush(subor);
	
	
	//vytvorenie VODIK processu
	HydrogenPID=fork();
	if(HydrogenPID == 0)
	{
		Hydrogens(&parameter);
	}
	else if(HydrogenPID < 0)
	{
		fprintf(stderr,"chyba pri vytvoreni procesu.\n");
		closeMemory();
		exit(2);
	}
	else if(HydrogenPID > 0)
	{
		//vytvorenie KYSLIK procesu 
		OxygenPID=fork();
		if(OxygenPID == 0)
		{
			Oxygens(&parameter);
		}
		else if(OxygenPID < 0)
		{
			fprintf(stderr,"chyba pri vytvoreni procesu.\n");
			closeMemory();
			exit(2);
		}	
	}
	
	closeMemory();
	return 0;
}

// -----------------------------------   HYDROGENS   ---------------------------------------------
void Hydrogens(Tparam *parameter)
{
	printf("toto je hydrogen\n");
	
	pid_t HydrogenPID2;
	
	for (int i = 1; i <= (parameter->pocetKyslik * 2); i++)
	{
		if (parameter->genHydrogen != 0)
		{
			usleep(nahodneCislo(parameter->genHydrogen));
		}
		HydrogenPID2 = fork();
		if(HydrogenPID2 < 0)
		{
			fprintf(stderr,"chyba pri vytvoreni Oxygen procesu.\n");
			closeMemory();
			exit(2);
		}
		if(HydrogenPID2 == 0)
		{
			sem_wait(&memory->zapis);
			memory->VodikPocitadlo ++;
			memory->VodikCislo ++;
			memory->cisloAkcie ++;
			
			int HydrogenID;
			HydrogenID = memory->VodikCislo;
			
			fprintf(subor,"%d: H %d: started\n", (memory->cisloAkcie), HydrogenID);
			fflush(subor);
			
			sem_post(&memory->zapis);
			sem_wait(&memory->dalsi);
			
			if((memory->VodikPocitadlo) >= 2 && (memory->KyslikPocitadlo) >= 1)
			{
				sem_wait(&memory->zapis);
				memory->cisloAkcie ++;
				fprintf(subor,"%d: H %d: ready\n", (memory->cisloAkcie), HydrogenID);
				fflush(subor);
				
				sem_post(&memory->zapis);
				sem_post(&memory->oxygen);
				
				for(int j=0;j<2;j++)
				{
					sem_post(&memory->hydrogen);
				}

				memory->VodikPocitadlo = memory->VodikPocitadlo - 2;
				memory->KyslikPocitadlo --;
			}
			else
			{
				sem_post(&memory->dalsi);
				sem_wait(&memory->zapis);
				memory->cisloAkcie ++;
				fprintf(subor,"%d: H %d: waiting\n", (memory->cisloAkcie), HydrogenID);
				fflush(subor);
				sem_post(&memory->zapis);
			}
			// priprava spajania
			
			sem_wait(&memory->hydrogen);

			sem_wait(&memory->zapis);
			(memory->cisloAkcie)++;
			fprintf(subor,"%d: H %d: begin bonding\n", (memory->cisloAkcie), HydrogenID);
			fflush(subor);
			(memory->BondPriprava)++;

			if((memory->BondPriprava) == 3)
			{
				for(int j=1; j<=3; j++)
				{
					sem_post(&memory->vytvorenie);
				}
				(memory->BondPriprava) = 0;
			}
			sem_post(&memory->zapis);

			// spojovani
			sem_wait(&memory->vytvorenie);
			sem_wait(&memory->zapis);
			(memory->cisloAkcie)++;
			fprintf(subor,"%d: H %d: bonded\n", (memory->cisloAkcie), HydrogenID);
			fflush(subor);

			(memory->Bonded)++;
			if((memory->Bonded) == 3)    
			{
			  sem_post(&memory->dalsi);
			  (memory->Bonded) = 0;
			}
			sem_post(&memory->zapis);

			sem_wait(&memory->zapis);
			printf("%d\n", memory->VodikPocitadlo2);
			(memory->VodikPocitadlo2)++;
			if((parameter->pocetKyslik*2) == (memory->VodikPocitadlo2))
			{
				for(int k=1;k<=(parameter->pocetKyslik*3);k++)
				{
					sem_post(&memory->koniecHydrogen);
				}
			}
			sem_post(&memory->zapis);
			sem_wait(&memory->koniecHydrogen);
			sem_wait(&memory->koniecOxygen);
			sem_wait(&memory->zapis);
			(memory->cisloAkcie)++;
			
			fprintf(subor,"%d: H %d: finished\n", (memory->cisloAkcie), HydrogenID);
			fflush(subor);
			sem_post(&memory->zapis);

			exit(1); //WTF ----------------------------------------------------------------------------------
		}
	}
	
	int status;
	for(int i=1; i<=(parameter->pocetKyslik*2); i++)
	{
		wait(&status);
	}
	
}

// ------------------------------------   OXYGENS   ---------------------------------------------
void Oxygens(Tparam *parameter)
{
	printf("toto je oxygen\n");
	
	pid_t OxygenPID2;
	
	for (int i = 1; i <= (parameter->pocetKyslik); i++)
	{
		if(parameter->genOxygen != 0)
		{
			usleep(nahodneCislo(parameter->genOxygen));
		}
		OxygenPID2 = fork();
		if(OxygenPID2 < 0)
		{
			fprintf(stderr,"chyba pri vytvoreni Oxygen procesu.\n");
			closeMemory();
			exit(2);
		}
		if (OxygenPID2 == 0)
		{
			sem_wait(&memory->zapis);
			memory->KyslikPocitadlo ++;
			memory->KyslikCislo ++;
			memory->cisloAkcie ++;
			int OxigenID = memory->KyslikCislo;
			
			fprintf(subor,"%d: O %d: started\n", (memory->cisloAkcie), OxigenID);
			fflush(subor);
			sem_post(&memory->zapis);

			sem_wait(&memory->dalsi);

			if((memory->VodikPocitadlo) >= 2 && (memory->KyslikPocitadlo) >= 1)
			{

				sem_wait(&memory->zapis);
				memory->cisloAkcie ++;
				fprintf(subor,"%d: O %d: ready\n", (memory->cisloAkcie), OxigenID);
				fflush(subor);
				sem_post(&memory->zapis);

				sem_post(&memory->oxygen);
				for(int j=0;j<2;j++)
				{
					sem_post(&memory->hydrogen);
				}

				memory->VodikPocitadlo = memory->VodikPocitadlo - 2;
				memory->KyslikPocitadlo --;
			}
			else
			{
				sem_post(&memory->dalsi);
				sem_wait(&memory->zapis);
				memory->cisloAkcie ++;
				fprintf(subor,"%d: O %d: waiting\n", (memory->cisloAkcie), OxigenID);
				fflush(subor);
				sem_post(&memory->zapis);
			}

			// priprava na spojovani
			sem_wait(&memory->oxygen);

			sem_wait(&memory->zapis);
			memory->cisloAkcie ++;
			memory->BondPriprava ++;
			
			fprintf(subor,"%d: O %d: begin bonding\n", (memory->cisloAkcie), OxigenID);
			fflush(subor);
			

			if((memory->BondPriprava) == 3)
			{
				for(int j=1; j<=3; j++)
				{
					sem_post(&memory->vytvorenie);
				}
				(memory->BondPriprava) = 0;
			}
			sem_post(&memory->zapis);

			// spojovani
			sem_wait(&memory->vytvorenie);
			sem_wait(&memory->zapis);
			memory->cisloAkcie ++;
			fprintf(subor,"%d: O %d: bonded\n", (memory->cisloAkcie), OxigenID);
			fflush(subor);

			memory->Bonded ++;
			if((memory->Bonded) == 3)    
			{
				if(parameter->fBond != 0)
				{
					usleep(nahodneCislo(parameter->fBond));  // uspani procesu
				}
				sem_post(&memory->dalsi);
				(memory->Bonded) = 0;
			}
			sem_post(&memory->zapis);

			sem_wait(&memory->zapis);
			(memory->KyslikPocitadlo2)++;
			if((parameter->pocetKyslik) == (memory->KyslikPocitadlo2))
			{
				for(int k=1; k<=(parameter->pocetKyslik*3); k++)
				{
					sem_post(&memory->oxygen);
				}
			}
			sem_post(&memory->zapis);


			sem_wait(&memory->oxygen);
			sem_wait(&memory->hydrogen);
			sem_wait(&memory->zapis);
			memory->cisloAkcie ++;
			fprintf(subor,"%d: O %d: finished\n", (memory->cisloAkcie), OxigenID);
			fflush(subor);
			sem_post(&memory->zapis);

			exit(1);
		}
	}
	
	int status;
	for(int i=1; i <= (parameter->pocetKyslik); i++)
	{
		wait(&status);
	}
}




void otvorSubor()
{
    subor=fopen("h2o.out", "w+");
    if (subor == NULL)
    {
        closeMemory();
    }
    setbuf(subor, NULL);
}

void kontrolaParametrov (int argc, char *argv[], Tparam *parameter)
{
    if(argc == 5)
    {
        if(true == nacitajParametre(parameter,argv))
        {
            // printf("parametre sa nacitali uspesne \n");
        }
        else
        {
            // help();
			fprintf(stderr,"Chyba pri nacitani parametrov.\n");
            exit(1);
        }
    }
    else
    {
        // help();
		fprintf(stderr,"Chyba pri nacitani parametrov.\n");
        exit(1);
    }
}

bool nacitajParametre(Tparam *parameter,char *argv [])
{
    int nacitanyArgument[4];

    for(int i=1; i<=4; i++ )
    {
        nacitanyArgument[i-1]=string_to_number(argv[i]);
        if(nacitanyArgument[i-1] == -1)
        {
            return false;
        }
    }

    //int arg= nacitanyArgument[0];

    if(nacitanyArgument[0] > 0 &&
       nacitanyArgument[1] < 5001 &&
       nacitanyArgument[2] < 5001 &&
       nacitanyArgument[3] < 5001)
    {
        parameter -> pocetKyslik = nacitanyArgument[0];
        parameter -> genHydrogen = nacitanyArgument[1];
        parameter -> genOxygen = nacitanyArgument[2];
        parameter -> fBond = nacitanyArgument[3];
        return true;
    }
    else
    {
        return false;
    }

}
//retazec na cele císlo, ktoré je v intervale 0 <= x <= 5000
int string_to_number(char *retazec)
{
    char *a;
    a=NULL;
    double cislo = strtod(retazec,&a);
    if(*a == 0 && cislo >= 0 && cislo < 5001)
    {
        int celeCislo = cislo;
        if( 1.0*(cislo-celeCislo) == 0.0)
        {
            return celeCislo;
        }
        else
        {
            return NEVYHOVUJUCE_CISLO;
        }
    }
    else
    {
        return NEVYHOVUJUCE_CISLO;
    }
}

int nahodneCislo(int hodnota)
{
    return (( rand() % hodnota)*1000);
}

//funkcia HELP
void help()
{
	// printf("Chyba\n\n");
    printf( "IOS: Projekt 2 - Problem Building H2O\n");
    printf( "AUTOR:     Viktor Toth, xtothv01@stud.fit.vutbr.cz\n");
    printf( "POPIS:     Implementace modifikovaneho synchronizacniho problemu\n");
    printf( "POUZITIE:   ./h2o [N] [GH] [GO] [B]\n");
    printf( "           - Vystup programu se zapise do suboru ./h2o.out\n\n");
    printf( "POPIS PARAMETROV:\n\n");
    printf( " - Vsechny parametry jsou celociselne hodnoty a su povinne\n");
    printf( " - Casove udaje parametru [H] a [S] jsou dany v miliseknudach\n\n");
    printf( " [N]  - pocet procesov reprezentujuci kyslik (oxygen)\n");
    printf( " [GH] - maximalna hodnota doby (ms), po ktorom je generovany novy proces pre vodik\n");
    printf( " [GO] - maximalna hodnota doby (ms), po ktorom je generovany novy proces pre kyslik\n");
    printf( " [B]  - maximalna hodnota doby (ms) prevedeny funkcie bond\n\n");
    printf( " [N]   >  0 ||\t P\n");
    printf( " [GH]  >= 0 ||\t P < 5001\n");
    printf( " [GO]  >= 0 ||\t P < 5001\n\n");
    printf( " [B]   >= 0 ||\t P < 5001\n\n\n");
    printf( "PRIKLAD:   ./h2o 3 5 15 10\n");
    printf( "           ./h2o 1 0 0 0\n");
}

void creatMemory()
{
	bool chyba = false;
	if ((memoryCounter = shmget(IPC_PRIVATE, sizeof(TMemory), IPC_CREAT | 0666)) < 0)
	{
		chyba = true;
	}
	if ((memory = (TMemory *)shmat(memoryCounter, NULL, 0)) == NULL)
	{
		chyba = true;
	}


	if (sem_init(&memory->zapis, 1, 1) < 0)
	{
		chyba = true;
	}
	if (sem_init(&memory->dalsi, 1, 1) < 0) 
	{
		chyba = true;
	}
	if (sem_init(&memory->hydrogen, 1, 0) < 0)
	{
		chyba = true;
	}
	if (sem_init(&memory->oxygen, 1, 0) < 0) 
	{
		chyba = true;
	}
	if (sem_init(&memory->vytvorenie, 1, 0) < 0) 
	{
		chyba = true;
	}
	if (sem_init(&memory->koniecHydrogen, 1, 0) < 0) 
	{
		chyba = true;
	}
	if (sem_init(&memory->koniecHydrogen, 1, 0) < 0) 
	{
		chyba = true;
	}

	if(chyba)
	{
		closeMemory();
		exit(1);
	}
}

/*
 * Cisteni pameti a uzavirani suboru
 */
void closeMemory()
{
	sem_destroy(&memory->zapis);
	sem_destroy(&memory->dalsi);
	sem_destroy(&memory->hydrogen);
	sem_destroy(&memory->oxygen);
	sem_destroy(&memory->vytvorenie);
	sem_destroy(&memory->koniecHydrogen);
	sem_destroy(&memory->koniecHydrogen);
	
	fclose(subor);
}























