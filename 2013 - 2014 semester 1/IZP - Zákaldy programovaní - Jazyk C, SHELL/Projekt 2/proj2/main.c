#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EPS 1e-11
#define PI 3.14159265359
#define NAN 0.0/0.0
#define INF 1.0/0.0
#define NESPRAVNY_VYSLEDOK "nan\n"

void volajHelp (int argc, char *argv[]);
void volajSqrt (int argc, char *argv[]);
void volajAsin (int argc, char *argv[]);
void volajTriangle (int argc, char *argv[]);
int volajOsetrenie (int argc, char *argv[]);
double my_sqrt(double x);
double my_asin(double x);
void triangle (double AX, double AY, double BX, double BY, double CX, double CY);
double string_to_number(char *retazec);
double my_fabs(double x);
double my_pow (double cislo, double mocnina);
double my_factx2 (double x);
double my_pitagoras (double odvesnaX, double odvesnaY);

int main(int argc, char *argv[])
{
    volajHelp( argc, argv );
    volajSqrt( argc, argv );
    volajAsin( argc, argv);
    volajTriangle( argc, argv );
    return volajOsetrenie(argc, argv );
}

void volajHelp (int argc, char *argv[])
{
    if(argc == 2 && (strcmp(argv[1],"--help") == 0))
    {
        printf("\nLogin:\t\t xtothv01\nMeno:\t\t Viktor\nPriezvisko:\t Toth\nDatum:\t\t 30.11.2013\n\n");
        printf("Argumenty programu: \n\n");
        printf("--help\n");
        printf(" Zpusobi, ze program vytiskne napovedu pouzivani programu a skonci.\n\n");
        printf("--sqrt [X] \n");
        printf(" Vypocita a vypise druhou odmocninu z cisla X.\n\n");
        printf("--asin [X] \n");
        printf(" Vypocita a vypise arcus sinus z cisla X.\n\n");
        printf("--triangle [AX] [AY] [BX] [BY] [CX] [CY] \n");
        printf(" Vypocita a vypise tri uhly troujuhelniku, ktery je dan vrcholy A=AX,AY, B=BX,BY a C=CX,CY.\n");
    }
}

void volajSqrt (int argc, char *argv[])
{
    if(argc == 3 && (strcmp(argv[1],"--sqrt") == 0))
    {
        if(string_to_number(argv[2]) >=0)
        {
            printf("%.10e\n",my_sqrt(string_to_number(argv[2])));
        }
        else
        {
            printf(NESPRAVNY_VYSLEDOK);
        }
    }
}

void volajAsin (int argc, char *argv[])
{
    if(argc == 3 && (strcmp(argv[1],"--asin") == 0))
    {
        if(string_to_number(argv[2]) >= -1 && string_to_number(argv[2]) <= 1)
        {
            printf("%.10e\n",my_asin(string_to_number(argv[2])));
        }
        else
        {
            printf(NESPRAVNY_VYSLEDOK);
        }
    }
}

void volajTriangle (int argc, char *argv[])
{
    if(argc == 8 && (strcmp(argv[1],"--triangle") == 0))
    {
        if( string_to_number(argv[2]) != NAN &&
            string_to_number(argv[3]) != NAN &&
            string_to_number(argv[4]) != NAN &&
            string_to_number(argv[5]) != NAN &&
            string_to_number(argv[6]) != NAN &&
            string_to_number(argv[7]) != NAN )
        {
            triangle(string_to_number(argv[2]),string_to_number(argv[3]),
                     string_to_number(argv[4]),string_to_number(argv[5]),
                     string_to_number(argv[6]),string_to_number(argv[7]));
        }
        else
        {
            printf(NESPRAVNY_VYSLEDOK);
            printf(NESPRAVNY_VYSLEDOK);
            printf(NESPRAVNY_VYSLEDOK);
        }
    }
}

int volajOsetrenie (int argc, char *argv[])
{
    if((argc == 2 && (strcmp(argv[1],"--help") == 0)) ||
       (argc == 3 && (strcmp(argv[1],"--sqrt") == 0)) ||
       (argc == 3 && (strcmp(argv[1],"--asin") == 0)) ||
       (argc == 8 && (strcmp(argv[1],"--triangle") == 0)))
       {
           return EXIT_SUCCESS;
       }
    else
    {
        printf(NESPRAVNY_VYSLEDOK);
        return EXIT_FAILURE;
    }
}

double string_to_number(char *retazec)
{
    char *a;
    a=NULL;
    double cislo = strtod(retazec,&a);
    if(*a == 0 && cislo != INF)
    {
        return cislo;
    }
    else
    {
        return NAN;
    }
}
double my_sqrt(double x)
{
    if(x>=0)
    {
        if(x == 0)
        {
            return x;
        }
        else
        {
            double medziVysledok=1;
            double vysledok = 1;
            vysledok = (x/vysledok + vysledok)/2;

            do
            {
                medziVysledok=vysledok;
                vysledok = (x/vysledok + vysledok)/2;
            }while(my_fabs(vysledok-medziVysledok) > my_fabs(EPS*vysledok));
            return vysledok;
        }
    }
    else
    {
        return EXIT_SUCCESS;
    }
}

double my_asin(double x)
{
    int znamienko = 1;
    if(x<0)
    {
        znamienko = -1;
        x = x*(-1);
    }
    double y = 3;
    double medzivysledok = x;
    double vysledok = x;

    if(x == 0)
    {
        return x;
    }
    else if(x == 1)
    {
        vysledok = (PI/2)*znamienko;
        return vysledok;
    }
    else if(x < 0.94 && x > 0)
    {
        while(my_fabs(medzivysledok)>(EPS*my_fabs(vysledok)))
        {
            medzivysledok = (my_factx2(y-2)/my_factx2(y-1))*(my_pow(x,y)/y);
            y = y+2;
            vysledok +=medzivysledok;
        }
        vysledok *=znamienko;
        return vysledok;
    }
    else
    {
        medzivysledok = my_sqrt(1-(x*x));
        medzivysledok = 0.5*PI - my_asin(medzivysledok);
        vysledok = medzivysledok*znamienko;
        return vysledok;
    }
}

void triangle (double AX, double AY, double BX, double BY, double CX, double CY)
{
    double stranaA;
    double stranaB;
    double stranaC;

    stranaA = my_pitagoras(BX-CX,BY-CY);
    stranaB = my_pitagoras(CX-AX,CY-AY);
    stranaC = my_pitagoras(BX-AX,BY-AY);

    if(stranaA + stranaB <= stranaC || stranaA + stranaC <= stranaB || stranaB + stranaC <= stranaA)
    {
        printf(NESPRAVNY_VYSLEDOK);
        printf(NESPRAVNY_VYSLEDOK);
        printf(NESPRAVNY_VYSLEDOK);
    }
    else
    {
        double cosAlfa;
        double cosBeta;
        double cosGama;
        double sinAlfa;
        double sinBeta;
        double sinGama;
        double uholA;
        double uholB;
        double uholC;

        cosAlfa = ((stranaA*stranaA)-(stranaC*stranaC)-(stranaB*stranaB))/(-2*stranaC*stranaB);
        cosBeta = ((stranaB*stranaB)-(stranaA*stranaA)-(stranaC*stranaC))/(-2*stranaA*stranaC);
        cosGama = ((stranaC*stranaC)-(stranaA*stranaA)-(stranaB*stranaB))/(-2*stranaA*stranaB);

        sinAlfa = my_sqrt((1-(cosAlfa*cosAlfa)));
        sinBeta = my_sqrt((1-(cosBeta*cosBeta)));
        sinGama = my_sqrt((1-(cosGama*cosGama)));

        if(cosAlfa<0)
        {
            uholA = PI - (my_asin(sinGama) + my_asin(sinBeta));
            uholB = my_asin(sinBeta);
            uholC = my_asin(sinGama);
        }
        else if(cosBeta<0)
        {

            uholA = my_asin(sinAlfa);
            uholB = PI - (my_asin(sinAlfa) + my_asin(sinGama));
            uholC = my_asin(sinGama);
        }
        else if(cosGama<0)
        {
            uholA = my_asin(sinAlfa);
            uholB = my_asin(sinBeta);
            uholC = PI - (my_asin(sinAlfa) + my_asin(sinBeta));
        }
        else
        {
            uholA = my_asin(sinAlfa);
            uholB = my_asin(sinBeta);
            uholC = my_asin(sinGama);
        }
        printf("%.10e\n",uholA);
        printf("%.10e\n",uholB);
        printf("%.10e\n",uholC);
    }
}

double my_fabs(double x)
{
    if (x < 0)
    {
        x = -x;
    }
    return x;
}

double my_pow (double cislo, double mocnina)
{
    double vysledok = cislo;
    while(mocnina>1)
    {
        vysledok *=cislo;
        mocnina--;
    }
    return vysledok;
}

double my_factx2 (double x)
{
    double vysledok = 1;
    while (x>1)
    {
        vysledok *= x;
        x = x -2;
    }
    return vysledok;
}

double my_pitagoras (double odvesnaX, double odvesnaY)
{
    return my_sqrt((odvesnaX*odvesnaX)+(odvesnaY*odvesnaY));
}
