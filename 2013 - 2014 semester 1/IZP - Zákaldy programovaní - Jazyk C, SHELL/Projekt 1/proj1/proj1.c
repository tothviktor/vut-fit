//Viktor Tóth
//xtothv01

#include <stdio.h>
#include <stdlib.h> //EXIT_FAILURE/SUCCESS
#include <string.h>

// PROTOTYPY
void help();                                                          //napoveda
void debug (int predchadzajuciZnak,int znak);                         //ladiaci rezim, "predchadzajuciZnak" je predposledny nacitani znak (ak este alebo vobec neexistuje tak hodnota je 0), znak je naposledy nacitany znak
int vratParameterPismenoCisloMinusPodciarknik(int cislo);             //separovanie integerov (znakov) podla ASCII tabulky: (A-Z,a-z,0-9,-,_) -> vráti zadany integer, inom prípade vrati 0 ako ASCII hodnotu
int vratSpravnyPrvyArgument(char *argv[]);                            //vid. vratParameterPismenoCisloMinusPodciarknik + moze vratit aj (:,^,.)
int vratSpravnyDruhyTretiArgument(char *argv[],int cisloArgumentu);   //separovanie obsahu argumenta, vstup (cele cislo, -d) -> vrati cele cislo, alebo len 'd' (BEZ znamienka '-') ; inak vrati 0
void NacitavanieZnakov (int arg1, int arg2, int arg3);                //VSTUP -> 3 argumenty ; nacitavanie znakov a aplikovanie vlastnych funkcii ; VYSTUP -> vysledok
int argvCislo(int cislo1, int cislo2);                                //vyber cisla z dvoch integerov ( cele cislo, d) -> vrati cele cislo
int argv_d(int cislo1, int cislo2);                                   //vyber d (ako debug) z dvoch integerov ( cele cislo, d) -> vrati d ako ASCII hodnotu
int retazecNaCislo(char *argv[],int cisloArgumentu,int i);            //prevod vybraneho (cisloArgumentu) argumenta, ktory ma urcity pocet znakov (i), na cele cislo
int argument1(int arg1,int nacitanyZnak, int vysledok);               //porovna nacitanyZnak s arg1 podla kriterii (':','^','.',staticky integer napr.: 'a'), pri zhode zvysi hodnotu integera "vysledok" o jedna
void jedenArgument(char *argv[]);                                     //spracovanie prveho argumenta
void dveArgumenty(int argc, char *argv[]);                            //spracovanie druheho argumenta
void triArgumenty (char *argv[], int argv1, int argv2);               //spracovanie tretieho argumenta

int main(int argc, char *argv[])
{
    if (argc == 1)                                                          //MALO PARAMETROV
    {
        printf("Zadali ste malo parametrov! Program skonci.\n");
        return EXIT_FAILURE;
    }
    else if(argc > 4)                                                       //VELA PARAMETROV
    {
        printf("Zadali ste moc vela parametrov! Program skonci.\n");
        return EXIT_FAILURE;
    }
    else if (argc == 2)                                                     // 1 zadany argument ([a-z],[A-Z],[0-9],-,_,:,^,.)
    {
        jedenArgument(argv);
    }
    else if(argc == 3 || argc == 4)                                         // 2 alebo 3 zadane argumenty
    {
        dveArgumenty(argc,argv);
    }
    else
    {
        printf("Zadali ste nespravny parameter! Program skonci.\n");
        return EXIT_FAILURE;
    }
    return 0;
}
//         -----------------------    K O N I E C    M A I N   ->    VLASTE FUNKCIE   -----------------------
void jedenArgument(char *argv[])
{
    if((strcmp(argv[1],"--help"))==0)
    {
        help();
    }
    else if((vratSpravnyPrvyArgument(argv)) != 0)
    {
        NacitavanieZnakov(argv[1][0],0,0);                              //vid.: funkciu "NacitavanieZnakov"
    }
}

void dveArgumenty(int argc, char *argv[])
{
    int argv1 = vratSpravnyPrvyArgument(argv);                                        //Nacitanie 1. zadaneho argumenta
    int argv2 = vratSpravnyDruhyTretiArgument(argv,2);                                //Nacitanie 2. zadaneho argumenta

    if(argc == 4)                  // -------- 3 ZADANE ARGUMENTY --------
    {
        triArgumenty(argv, argv1, argv2);
    }
    else if(argc == 3)             // -------- 2 ZADANE ARGUMENTY --------
    {
        if(argv1 !=0 && argv2 !=0 )                                     //ARGUMENTY su vyhovujuce ?
        {
            NacitavanieZnakov(argv1,argv2,0);                           //vid.: funkciu "NacitavanieZnakov"
        }
    }
}

void triArgumenty (char *argv[], int argv1, int argv2)
{
    int argv3 = vratSpravnyDruhyTretiArgument(argv,3);                            //Nacitanie 1. zadaneho argumenta

    if(argv1 !=0 && argv2 !=0 && argv3 !=0 && ((argv2 == 'd' && argv3 != 'd') || (argv2 != 'd' && argv3 == 'd' )))      //ARGUMENTY su vyhovujuce ?
    {
        NacitavanieZnakov(argv1,argv2,argv3);                                     //vid.: funkciu "NacitavanieZnakov"
    }
    if((argv2 != 0 && argv3 != 0) && ((argv2 == 'd' && argv3 == 'd') || (argv2 != 'd' && argv3 != 'd' )))          //DVUHY a TRETI ARGUMENT su rovnake?
    {
        printf("Druhy a treti argument je rovnakeho typu.\n");
    }
}

void help()
{
    printf("PRVY ARGUMENT:\n");
    printf("Moze byt znak (A-Z,a-z,0-9,-,_,.,^,:)\n");
    printf("\tPri zadani (A-Z,a-z,0-9,-,_) program zisti pocet slov v ktorych sa nachadza prave ten zadany znak.\n");
    printf("\tPri zadani \".\" program zisti pocet slov v zadanom vete.\n" );
    printf("\tPri zadani \"^\" program zisti pocet slov v ktorych sa nachadza velke pismeno (A-Z).\n");
    printf("\tPri zadani \":\" program zisti pocet slov v ktorych sa nachadza cislo (0-9).\n\n");
    printf("DRUHY A TRETI ARGUMENT:\n");
    printf("Moze byt cislo (0-2147483647) alebo \"-d\".\n");
    printf("Poradie cisla a -d je mozne zamenit.\n");
    printf("\tPri zadani \"-d\" program vypise pod sebou vsetky zadane slova.\n" );
    printf("\tPri zadani cisla (0-2147483647) program zisti, ci na danom mieste v slove sa nachadza znak zadany v prvom argumente.\n");
}

void debug (int predchadzajuciZnak,int znak)                                     //LADIACI rezim, "predchadzajuciZnak" je predposledny nacitani znak
{                                                                                 //(ak este alebo vobec neexistuje tak jeho hodnota je 0),
    if(znak == vratParameterPismenoCisloMinusPodciarknik(znak))                                                 //znak je naposledy nacitany znak
    {
        printf("%c",znak);
    }
    else if (znak != vratParameterPismenoCisloMinusPodciarknik(znak) && vratParameterPismenoCisloMinusPodciarknik(znak) != vratParameterPismenoCisloMinusPodciarknik(predchadzajuciZnak) && znak != 0)
    {
        printf("\n");
    }
}

int vratParameterPismenoCisloMinusPodciarknik(int cislo)
{
    if((cislo>='a' && cislo<='z') ||                                  //Separovanie integerov, vid. prototypy vratParameterPismenoCisloMinusPodciarknik
      (cislo>='A' && cislo<='Z') ||
      (cislo>='0' && cislo<='9') ||
      (cislo=='-') ||
      (cislo=='_'))
    {
        return cislo;                                                 //SPRAVNY, VRACIA vstupnu HODNOTU <-ako ASCII hodnota
    }
    else
    {
        return 0;                                                     //NESPRAVNY, VRACIA 0 <-ako ASCII hodnota
    }
}

int vratSpravnyPrvyArgument(char *argv[])
{                                                                               //Prvy zadany parameter ([a-z],[A-Z],[0-9],-,_,:,^,. )
    if(argv[1][1] != 0)
    {
        printf("V prvom argumente je moc vela parametrov.\n");
        return 0;
    }
    else if((argv[1][0] == vratParameterPismenoCisloMinusPodciarknik(argv[1][0]))||             //SPRAVNY parameter VRACIA vstupnu HODNOTU <-ako ASCII hodnota
            (argv[1][0] == ':') ||
            (argv[1][0] == '^') ||
            (argv[1][0] == '.'))
    {
        return argv [1][0];
    }
    else                                                              //NESPRAVNY parameter VRACIA 0 <-ako ASCII hodnota
    {
        printf("Prvy argument je zly\n");
        return 0;
    }
}

int vratSpravnyDruhyTretiArgument(char *argv[],int cisloArgumentu)              //Druhy zadany parameter moze byt ([0 - 2147483647],"-d" )
{
    for(int i=0,j=1;argv [cisloArgumentu][i] >='0' && argv [cisloArgumentu][i]<='9';i++,j++)       //SPRAVNY parameter VRACIA cele cislo, alebo ...
    {
        if(argv [cisloArgumentu][i]!=0 && argv[cisloArgumentu][j]==0)
        {
            return retazecNaCislo(argv,cisloArgumentu,i);
        }
    }

    if((strcmp(argv[cisloArgumentu],"-d"))==0 && argv [cisloArgumentu][2] == 0)                    //.. pri parametri -d iba znak 'd' <-ako ASCII hodnota
    {
        return 'd';
    }
    else                                                                                           //NESPRAVNY parameter VRACIA 0 <-ako ASCII hodnota
    {
        printf("Nezadali ste spravny argument.\n");
        return 0;
    }
}

void NacitavanieZnakov (int arg1, int arg2, int arg3)
{
  int vysledok = 0;                                                                                //uplny vysledok
  int zmena = 0;                                                                                   //zmeny vo vysledku pocas behu programu
  int predchadzajuciZnak = 0;                                                                      //predchadzajuci nacitany znak
  int nacitanyZnak;                                                                                //naposledy nacitany znak
  int poradie = argvCislo(arg2,arg3);                                                              //argument [N]
  int d = argv_d(arg2,arg3);
  int dlzkaSlova = 1;
  int slovo = 0;                                                                     //argument [-d]

  while((nacitanyZnak=getchar()) != '\n')               //NACITAVANIE ZNAKOV
  {
    if(nacitanyZnak == vratParameterPismenoCisloMinusPodciarknik(nacitanyZnak))                                              //porovnanie nacitaneho znaku s nacitanym znakom,ktory uz bol separovany
    {                                                                                                 //VYHOVUJE -> program pokracuje dalej
        if(arg1 != 0 && poradie == 0 && slovo == 0)                                                // 1 argument
        {
            vysledok = argument1(arg1,nacitanyZnak,vysledok);
            if(zmena != vysledok)
            {
                zmena = vysledok;
                slovo ++;
            }
        }
        else if (arg1 != 0 && poradie == dlzkaSlova && slovo == 0)                                 // 2 argumenty : [X] a [N]
        {
            vysledok = argument1(arg1,nacitanyZnak,vysledok);
            if(zmena != vysledok)
            {
                zmena = vysledok;
                slovo ++;
            }
        }
    }
    else if(nacitanyZnak != vratParameterPismenoCisloMinusPodciarknik(nacitanyZnak))                                         //porovnanie nacitaneho znaku s nacitanym znakom,ktory uz bol separovany
    {                                                                                                 //NEVYHOVUJE -> nacitany znak je prazdny (koniec slova)
        dlzkaSlova=0;
        slovo = 0;
    }
    if (d != 0 &&  dlzkaSlova <=80)                                                                //ladenie programu
    {
        debug(predchadzajuciZnak,nacitanyZnak);
    }
    predchadzajuciZnak = nacitanyZnak;
    dlzkaSlova ++;
  }
  if(predchadzajuciZnak == vratParameterPismenoCisloMinusPodciarknik(predchadzajuciZnak) && nacitanyZnak == '\n' && d != 0 && predchadzajuciZnak !=0)      //odriadkovanie pri zvastnych pripadoch
  {
      printf("\n");
  }
  printf("%d\n",vysledok);                                                      //vypis vysledku
}

int argument1(int arg1,int nacitanyZnak, int vysledok)                          //POROVNANIE nacitaneho znaku s 1. argumnetom, Vyhovuje vrati ++vysledok , NEVYHOVUJE vrati vysledok
{
    if(arg1 == '^')                                                             // (A - Z)
    {
        if(nacitanyZnak >= 'A' && nacitanyZnak <= 'Z')
        {
            return ++vysledok;
        }
        else
        {
          return vysledok;
        }
    }
    else if (arg1 == ':')                                                       // (0 - 9)
    {
        if(nacitanyZnak >= '0' && nacitanyZnak <= '9')
        {
            return ++vysledok;
        }
        else
        {
          return vysledok;
        }
    }
    else if (arg1 == '.')                                                       //(A-Z,a-z,0-9,-,_)
    {
        if(nacitanyZnak == vratParameterPismenoCisloMinusPodciarknik(nacitanyZnak))
        {
            return ++vysledok;
        }
        else
        {
          return vysledok;
        }
    }
    else
    {
        if(nacitanyZnak == arg1)                                                //presne zadany znak (napr.: arg1 == 'a'), ktory sa ma kontrolovat
        {
            return ++vysledok;
        }
        else
        {
          return vysledok;
        }
    }
}

int argvCislo(int cislo1, int cislo2)                                           //vyber cisla z dvoch integerov
{                                                                                 //( cele cislo, d ) -> vrati cele cislo
    if(cislo1 != 'd' && cislo1 !=0)
    {
        return cislo1;
    }
    else if(cislo2 != 'd' && cislo2 !=0)
    {
        return cislo2;
    }
    else if (cislo1 == 'd' && cislo2 == 'd')
    {
        return 100;
    }
    else
    {
        return 0;
    }
}

int argv_d(int cislo1, int cislo2)                                              //vyber d (ako debug) z dvoch integerov
{                                                                                 //( cele cislo, d ) -> vrati d ako ASCII hodnotu
    if(cislo1 == 'd')
    {
        return cislo1;
    }
    if(cislo2 == 'd')
    {
        return cislo2;
    }
    else
    {
        return 0;
    }
}

int retazecNaCislo(char *argv[],int cisloArgumentu,int i)                       //prevod vybraneho (cisloArgumentu) argumenta,
{                                                                                  //ktory ma urcity pocet znakov (i),
    int cislo =0;                                                                    //na cele cislo, navacsie cislo je 2147483647, ktorym este program spravne pracuje
    for(int j=1;i>=0;i--,j=j*10)                                                        // pri vacsom cisle ako 4294967295 program zlyha a skonci
    {
      cislo = cislo + ((argv[cisloArgumentu] [i]-'0')*j);
    }
    return cislo;
}
