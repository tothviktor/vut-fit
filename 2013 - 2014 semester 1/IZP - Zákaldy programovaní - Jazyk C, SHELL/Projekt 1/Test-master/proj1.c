#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// PROTOTYPY
void help();                                                    //napoveda
void debug (int predchadzajuciZnak,int znak);                   //ladiaci rezim, "predchadzajuciZnak" je predposledny nacitani znak (ak este alebo vobec neexistuje tak hodnota je 0), znak je naposledy nacitany znak
int dobryParameter0(int cislo);                                 //separovanie integerov (znakov) podla ASCII tabulky: (A-Z,a-z,0-9,-,_) -> vráti zadany integer, inom prípade vrati 0 ako ASCII hodnotu
int dobryParameter1(char *argv[]);                              //vid. dobryParameter0 + moze vratit aj (:,^,.)
int dobryParameter2(char *argv[],int cisloArgumentu);           //separovanie obsahu argumenta, vstup (cele cislo, -d) -> vrati cele cislo, alebo len 'd' (BEZ znamienka '-') ; inak vrati 0
void NacitavanieZnakov (int arg1, int arg2, int arg3);          //VSTUP -> 3 argumenty ; nacitavanie znakov a aplikovanie vlastnych funkcii ; VYSTUP -> vysledok
int argvCislo(int cislo1, int cislo2);                          //vyber cisla z dvoch integerov ( cele cislo, d) -> vrati cele cislo
int argv_d(int cislo1, int cislo2);                             //vyber d (ako debug) z dvoch integerov ( cele cislo, d) -> vrati d ako ASCII hodnotu
int retazecNaCislo(char *argv[],int cisloArgumentu,int i);      //prevod vybraneho (cisloArgumentu) argumenta, ktory ma urcity pocet znakov (i), na cele cislo
int argument1(int arg1,int nacitanyZnak, int vysledok);         //porovna nacitanyZnak s arg1 podla kriterii (':','^','.',staticky integer napr.: 'a'), pri zhode zvysi hodnotu integera "vysledok" o jedna

int main(int argc, char *argv[])
{
    if (argc == 1)                                                          //MALO PARAMETROV
    {
        printf("Zadali ste malo parametrov! Program skonci.");
        return EXIT_FAILURE;
    }
    else if(argc > 4)                                                       //VELA PARAMETROV
    {
        printf("Zadali ste moc vela parametrov! Program skonci.");
        return EXIT_FAILURE;
    }

    //         -----------------------------  1 ZADANY ARGUMENT (dve argumenty)   -----------------------------

    else if (argc == 2 && (strcmp(argv[1],"--help"))==0 )                   //napoveda
    {
        help();
        return EXIT_SUCCESS;
    }
    else if (argc == 2)                                 // 1 parameter ([a-z],[A-Z],[0-9],-,_,:,^,.)
    {
        argv[1][0] = dobryParameter1(argv);
        if(argv [1][0] != 0)
        {
            NacitavanieZnakov(argv[1][0],0,0);                              //vid.: funkciu "NacitavanieZnakov"
            return EXIT_SUCCESS;
        }
        else
        {
            return EXIT_FAILURE;
        }
    }

    //         ---------------------------------  2 ALEBO 3 ZADANE ARGUMENTY  ---------------------------------

    else if(argc == 3 || argc == 4)
    {
        int argv1 = dobryParameter1(argv);                                  //Nacitanie 1. zadaneho argumenta
        int argv2 = dobryParameter2(argv,2);                                //Nacitanie 2. zadaneho argumenta

        if(argc == 4)                  // -------- 3 ZADANE ARGUMENTY --------
        {
            int argv3 = dobryParameter2(argv,3);                            //Nacitanie 1. zadaneho argumenta

            if(argv1 !=0 && argv2 !=0 && argv3 !=0 && ((argv2 == 'd' && argv3 != 'd') || (argv2 != 'd' && argv3 == 'd' )))      //ARGUMENTY su vyhovujuce ?
            {
                NacitavanieZnakov(argv1,argv2,argv3);                       //vid.: funkciu "NacitavanieZnakov"
            }
            else if((argv2 == 'd' && argv3 == 'd') || (argv2 != 'd' && argv3 != 'd' ))                                          //DVUHY a TRETI ARGUMENT su rovnake?
            {
                printf("Druhy a treti argument je rovnaky.\n");
                return EXIT_FAILURE;
            }
            else                                                            //argumenty NEVYHOVIJU
            {
                return EXIT_FAILURE;
            }
        }
        else if(argc == 3)             // -------- 2 ZADANE ARGUMENTY --------
        {
            if(argv1 !=0 && argv2 !=0 )                                     //ARGUMENTY su vyhovujuce ?
            {
                NacitavanieZnakov(argv1,argv2,0);                           //vid.: funkciu "NacitavanieZnakov"
            }
            else                                                            //argumenty NEVYHOVIJU
            {
              return EXIT_FAILURE;
            }
        }
    }
    else
    {
        printf("Zadali ste nespravny parameter! Program skonci.");
        return EXIT_FAILURE;
    }
}
//         ---------------------------------    K O N I E C    M A I N    ---------------------------------
void help()
{
    printf("Pri zadani \".\" program zisti pocet slov zacinajuce hociakym znakom.\n" );
    printf("Pri zadani \"^\" program zisti pocet slov zacinajuce s velkymi pismenami.\n" );
    printf("Pri zadani hociakeho maleho pismena sa prekontroluje prave to pismeno v zadanom retazci.\n" );
    printf("Pri zadani \"-d\" program vypise vsetky zadane slova pod sebou, ktore sa nachadzaju v zadanom retazci." );
}

void debug (int predchadzajuciZnak,int znak)                                      //LADIACI, "predchadzajuciZnak" je predposledny nacitani znak
{                                                                                   //(ak este alebo vobec neexistuje tak hodnota je 0),
    if(znak == dobryParameter0(znak))                                                 //znak je naposledy nacitany znak
    {
        printf("%c",znak);
    }
    else if (znak != dobryParameter0(znak) && dobryParameter0(znak) != dobryParameter0(predchadzajuciZnak) && znak != 0)
    {
        printf("\n");
    }
}

int dobryParameter0(int cislo)
{
    if((cislo>='a' && cislo<='z') ||                                  //Separovanie integerov, vid. prototypy dobryParameter0
      (cislo>='A' && cislo<='Z') ||
      (cislo>='0' && cislo<='9') ||
      (cislo=='-') ||
      (cislo=='_'))
    {
        return cislo;                                                 //SPRAVNY, VRACIA vstupnu HODNOTU <-ako ASCII hodnota
    }
    else
    {
        return 0;                                                     //NESPRAVNY, VRACIA 0 <-ako ASCII hodnota
    }
}

int dobryParameter1(char *argv[])
{                                                                     //Prvy zadany parameter ([a-z],[A-Z],[0-9],-,_,:,^,. )
    if(argv[1][1] != 0)
    {
        printf("V prvom argumente je moc vela parametrov");
        return 0;
    }
    else if((argv[1][0] == dobryParameter0(argv[1][0]))||             //SPRAVNY parameter VRACIA vstupnu HODNOTU <-ako ASCII hodnota
            (argv[1][0] == ':') ||
            (argv[1][0] == '^') ||
            (argv[1][0] == '.'))
    {
        return argv [1][0];
    }
    else                                                              //NESPRAVNY parameter VRACIA 0 <-ako ASCII hodnota
    {
      printf("Prvy argument je zly");
      return 0;
    }
}

int dobryParameter2(char *argv[],int cisloArgumentu)                                               //Druhy zadany parameter moze byt ([0 - 2147483647],"-d" )
{
    for(int i=0,j=1;argv [cisloArgumentu][i] >='0' && argv [cisloArgumentu][i]<='9';i++,j++)       //SPRAVNY parameter VRACIA cele cislo, alebo ...
    {
        if(argv [cisloArgumentu][i]!=0 && argv[cisloArgumentu][j]==0)
        {
            return retazecNaCislo(argv,cisloArgumentu,i);
        }
    }

    if((strcmp(argv[cisloArgumentu],"-d"))==0 && argv [cisloArgumentu][2] == 0)                    //.. pri parametri -d iba znak 'd' <-ako ASCII hodnota
    {
        return 'd';
    }
    else                                                                                           //NESPRAVNY parameter VRACIA 0 <-ako ASCII hodnota
    {
        printf("Nezadali ste spravny argument.\n");
        return 0;
    }
}

void NacitavanieZnakov (int arg1, int arg2, int arg3)
{
  int vysledok = 0;                                                                                //uplny vysledok
  int zmena = 0;                                                                                   //zmeny vo vysledku pocas behu programu
  int predchadzajuciZnak = 0;                                                                      //predchadzajuci nacitany znak
  int nacitanyZnak;                                                                                //naposledy nacitany znak
  int poradie = argvCislo(arg2,arg3);                                                              //argument [N]
  int d = argv_d(arg2,arg3);                                                                       //argument [-d]

  for (int dlzkaSlova = 1, slovo =0 ;(nacitanyZnak=getchar()) != EOF ;dlzkaSlova++)               //NACITAVANIE ZNAKOV
  {
    if(nacitanyZnak == dobryParameter0(nacitanyZnak))                                              //porovnanie nacitaneho znaku s nacitanym znakom,ktory uz bol separovany
    {                                                                                                 //VYHOVUJE -> program pokracuje dalej
        if(arg1 != 0 && poradie == 0 && slovo == 0)                                                // 1 argument
        {
            vysledok = argument1(arg1,nacitanyZnak,vysledok);
            if(zmena != vysledok)
            {
                zmena = vysledok;
                slovo ++;
            }
        }
        else if (arg1 != 0 && poradie == dlzkaSlova && slovo == 0)                                 // 2 argumenty : [X] a [N]
        {
            vysledok = argument1(arg1,nacitanyZnak,vysledok);
            if(zmena != vysledok)
            {
                zmena = vysledok;
                slovo ++;
            }
        }
    }
    else if(nacitanyZnak != dobryParameter0(nacitanyZnak))                                         //porovnanie nacitaneho znaku s nacitanym znakom,ktory uz bol separovany
    {                                                                                                 //NEVYHOVUJE -> nacitany znak je prazdny (koniec slova)
        dlzkaSlova=0;
        slovo = 0;
    }
    if (d != 0 &&  dlzkaSlova <=80)                                                                //ladenie programu
    {
        debug(predchadzajuciZnak,nacitanyZnak);
    }
    predchadzajuciZnak = nacitanyZnak;
  }
  if(predchadzajuciZnak == dobryParameter0(predchadzajuciZnak) && nacitanyZnak == EOF && d != 0 && predchadzajuciZnak !=0)       //odriadkovanie pri zvastnych pripadoch
  {
      printf("\n");
  }
  printf("%d\n",vysledok);                                                      //vypis vysledku
}

int argument1(int arg1,int nacitanyZnak, int vysledok)                          //POROVNANIE nacitaneho znaku s 1. argumnetom, Vyhovuje vrati ++vysledok , NEVYHOVUJE vrati vysledok
{
    if(arg1 == '^')                                                             // (A - Z)
    {
        if(nacitanyZnak >= 'A' && nacitanyZnak <= 'Z')
        {
            return ++vysledok;
        }
        else
        {
          return vysledok;
        }
    }
    else if (arg1 == ':')                                                       // (0 - 9)
    {
        if(nacitanyZnak >= '0' && nacitanyZnak <= '9')
        {
            return ++vysledok;
        }
        else
        {
          return vysledok;
        }
    }
    else if (arg1 == '.')                                                       //(A-Z,a-z,0-9,-,_)
    {
        if(nacitanyZnak == dobryParameter0(nacitanyZnak))
        {
            return ++vysledok;
        }
        else
        {
          return vysledok;
        }
    }
    else
    {
        if(nacitanyZnak == arg1)                                                //presne zadany znak (napr.: arg1 == 'a'), ktory sa ma kontrolovat
        {
            return ++vysledok;
        }
        else
        {
          return vysledok;
        }
    }
}

int argvCislo(int cislo1, int cislo2)                                           //vyber cisla z dvoch integerov
{                                                                                 //( cele cislo, d ) -> vrati cele cislo
  if(cislo1 != 'd' && cislo1 !=0)
  {
    return cislo1;
  }
  else if(cislo2 != 'd' && cislo2 !=0)
  {
    return cislo2;
  }
  else if (cislo1 == 'd' && cislo2 == 'd')
  {
    return 100;
  }
  else
  {
    return 0;
  }
}

int argv_d(int cislo1, int cislo2)                                              //vyber d (ako debug) z dvoch integerov
{                                                                                 //( cele cislo, d ) -> vrati d ako ASCII hodnotu
  if(cislo1 == 'd')
  {
    return cislo1;
  }
  if(cislo2 == 'd')
  {
    return cislo2;
  }
  else
  {
    return 0;
  }
}

int retazecNaCislo(char *argv[],int cisloArgumentu,int i)                       //prevod vybraneho (cisloArgumentu) argumenta,
{                                                                                  //ktory ma urcity pocet znakov (i),
    int cislo =0;                                                                    //na cele cislo, navacsie cislo je 2147483647, ktorym este program spravne pracuje
    for(int j=1;i>=0;i--,j=j*10)                                                        // pri vacsom cisle ako 4294967295 program zlyha a skonci
    {
      cislo = cislo + ((argv[cisloArgumentu] [i]-'0')*j);
    }
    return cislo;
}
