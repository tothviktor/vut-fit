#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct
{
    int rows;
    int cols;
    char *cells;
} Bitmap;

//halvne funkcie
void volajHelp (int argc, char *argv[]);
void volajTest (int argc, char *argv[]);
void volajHline (int argc, char *argv[]);
void volajVline (int argc, char *argv[]);
void volajSquare (int argc, char *argv[]);
bool volajOsetrenie (int argc, char *argv[]);

// raz vnorene funkcie
int find_vline(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2);
int find_hline(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2);
int find_square(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2);
bool nacitavanieFileu(Bitmap *bitmap, char *argv[]);
char getcolor(Bitmap *bitmap, int x, int y);

// viac krat vnorene funkcie
int vratSuradnicu(FILE **myFile);
bool naplnPoleSMaticami(FILE **myFile,Bitmap **bitmap, int velkostMatice);
char getcolor(Bitmap *bitmap, int x, int y);

int main (int argc,char *argv[])
{
    volajHelp (argc, argv);
    volajTest (argc, argv);
    volajHline (argc, argv);
    volajVline (argc, argv);
    volajSquare (argc, argv);
    volajOsetrenie (argc, argv);
}

//hlavne funkcie
void volajHelp (int argc, char *argv[])
{
    if(argc == 2 && (strcmp(argv[1],"--help") == 0))
    {
        printf("\nLogin:\t\t xtothv01\nMeno:\t\t Viktor\nPriezvisko:\t Toth\nDatum:\t\t 30.11.2013\n\n");
        printf("Argumenty programu: \n\n");
        printf("--help\n");
        printf("zpusobi, ze program vytiskne napovedu pouzivani programu a skonci.\n");
        printf("--test\n");
        printf("pouze zkontroluje, ze soubor dany druhym argumentem programu obsahuje radnou definici bitmapového obrazku. V pripade, ze format obrazku odpovida definici (viz nize), vytiskne Valid. V opacnem pripade (napr. ilegalnich znaku, chybejicich udaju nebo spatnych hodnot) program tiskne Invalid.\n");
        printf("--hline\n");
        printf("v danem obrazku nalezne a vytiskne pocatecni a koncovou souradnici prvni nejdelsi horizontalni usecky.\n");
        printf("--vline\n");
        printf("v danem obrazku nalezne a vytiskne pocatecni a koncovou souradnici prvni nejdelsi vertikalni usecky.\n");
        printf("--square\n");
        printf("v danem obrazku nalezne a vytiskne pocatecni a koncovou souradnici prvniho nejvetsiho ctverce.\n");
    }
}

void volajTest (int argc, char *argv[])
{
    if(argc == 3 && (strcmp(argv[1],"--test") == 0))
    {
        Bitmap bitmap;
        if(nacitavanieFileu(&bitmap, argv))
        {
            printf("Valid\n");
        }
        else
        {
            printf("Invalid\n");
        }
    }
}

void volajHline (int argc, char *argv[])
{
    if(argc == 3 && (strcmp(argv[1],"--hline") == 0))
    {
        Bitmap bitmap;
        if(nacitavanieFileu(&bitmap, argv))
        {
            int x1;
            int x2;
            int y1;
            int y2;
            if (find_hline(&bitmap,&x1,&y1,&x2,&y2))
            {
                printf("%d %d %d %d\n",x1,y1,x2,y2);
            }
            else
            {
                printf("Pozadovany utvar sa nenasiel!\n");
            }
        }
        else
        {
            printf("Invalid\n");
        }
    }
}
void volajVline (int argc, char *argv[])
{
    if(argc == 3 && (strcmp(argv[1],"--vline") == 0))
    {
        Bitmap bitmap;
        if(nacitavanieFileu(&bitmap, argv))
        {
            int x1;
            int x2;
            int y1;
            int y2;
            if (find_vline(&bitmap,&x1,&y1,&x2,&y2))
            {
                printf("%d %d %d %d\n",x1,y1,x2,y2);
            }
            else
            {
                printf("Pozadovany utvar sa nenasiel!\n");
            }
        }
        else
        {
            printf("Invalid\n");
        }
    }
}
void volajSquare (int argc, char *argv[])
{
    if(argc == 3 && (strcmp(argv[1],"--square") == 0))
    {
        Bitmap bitmap;
        if(nacitavanieFileu(&bitmap, argv))
        {
            int x1;
            int x2;
            int y1;
            int y2;
            if (find_square(&bitmap,&x1,&y1,&x2,&y2))
            {
                printf("%d %d %d %d\n",x1,y1,x2,y2);
            }
            else
            {
                printf("Pozadovany utvar sa nenasiel!\n");
            }
        }
        else
        {
            printf("Invalid\n");
        }
    }
}
bool volajOsetrenie (int argc, char *argv[])
{
    if((argc == 2 && (strcmp(argv[1],"--help") == 0)) ||
       (argc == 3 && (strcmp(argv[1],"--test") == 0)) ||
       (argc == 3 && (strcmp(argv[1],"--hline") == 0)) ||
       (argc == 3 && (strcmp(argv[1],"--vline") == 0)) ||
       (argc == 3 && (strcmp(argv[1],"--square") == 0)))
    {
        return true;
    }
    else
    {
        printf("Zly argument !\n");
        return false;
    }
}

//dalsie funkcie
int vratSuradnicu(FILE **myFile)
{
    double cislo=0;
    int nacitanyZnak;
    int i = 1;
    bool medzera = true;

    while(((nacitanyZnak = fgetc(*myFile))>= '0' && nacitanyZnak <= '9' ) || ((nacitanyZnak == ' ' || nacitanyZnak == '\t') && medzera))
    {
        if(nacitanyZnak >= '0' && nacitanyZnak <='9')
        {
            cislo = cislo + (nacitanyZnak-48.0)/(i*1.0);
            i = i*10;
            medzera = false;
        }
    }
    if((nacitanyZnak>='0' && nacitanyZnak <='9' ) || (nacitanyZnak == ' ' || nacitanyZnak =='\n' || nacitanyZnak == '\t'))
    {
        return (int)(cislo*(i/10)+0.5);
    }
    else
    {
        return 0;
    }
}

bool naplnPoleSMaticami(FILE **myFile,Bitmap **bitmap, int velkostMatice)
{
    int nacitanyZnak=0;
    int i = 0;
    bool bielyZnak = true;
    while((nacitanyZnak = fgetc(*myFile)) != EOF)
    {
        if(i<=velkostMatice && (nacitanyZnak == '1' || nacitanyZnak == '0' || nacitanyZnak == '\n' || nacitanyZnak == ' ' || nacitanyZnak == '\t'))
        {
            if( nacitanyZnak == '1' || nacitanyZnak == '0')
            {
                if(bielyZnak)
                {
                    (*bitmap)->cells[i] = nacitanyZnak-48;
                    i++;
                    bielyZnak = false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                bielyZnak = true;
            }
        }
        else
        {
            return false;
        }
    }
    if(velkostMatice != i)
    {
        return false;
    }
    return true;
}

bool nacitavanieFileu(Bitmap *bitmap, char *argv[])
{
    FILE *myFile = fopen(argv[2],"r");

    if(myFile == NULL)
    {
        return false;
    }
    else
    {
        bitmap->rows = vratSuradnicu(&myFile);
        if(bitmap->rows < 2)
        {
            return false;
        }
        else
        {
            bitmap->cols = vratSuradnicu(&myFile);
        }
    }
    if (bitmap->cols < 2)
    {
        return false;
    }
    int velkostMatice = bitmap->rows * bitmap->cols;

    bitmap->cells = (char*)malloc(velkostMatice*sizeof(char));

    if(naplnPoleSMaticami (&myFile,&bitmap,velkostMatice))
    {
        return true;
    }
    else
    {
        return false;
    }
    free(bitmap->cells);
    fclose(myFile);
}
char getcolor(Bitmap *bitmap, int x, int y)
{
    return bitmap->cells[(x*bitmap->cols)+y];
}

int find_hline(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2)
{
    int xa = 0;
    int xb = 0;
    int ya = 0;
    int yb = 0;
    int maxDlzka = 0;
    int aktualDlzka = 0;
    int znak;
    bool priznak = true;   //TRUE - predchadzajuci nacitany znak bola NULA // FALSE - predchadzajuci nacitany znak bola JEDNA

    for(int x = 0; x<bitmap->rows; x++)
    {
        for(int y = 0; y<bitmap->cols; y++)
        {
            znak = getcolor(&(*bitmap),x,y);

            if(znak == 1)
            {
                if(priznak == true)
                {
                    xa = x;
                    ya = y;
                    aktualDlzka++;
                    priznak =false;
                }
                else if (priznak == false)
                {
                    xb = x;
                    yb = y;
                    aktualDlzka++;
                }
            }

            if((znak == 0 || (bitmap->cols-1)==y) || (znak == 1 && (bitmap->cols-1)==y) )
            {
                if(maxDlzka<aktualDlzka)
                {
                    *x1 = xa;
                    *x2 = xb;
                    *y1 = ya;
                    *y2 = yb;
                    maxDlzka=aktualDlzka;
                }
                aktualDlzka = 0;
                priznak = true;
            }
        }
    }
    if(maxDlzka == 0)
    {
        return 0;
    }
    else
    {
        if(*x2 == 0 && *y2 == 0)
        {
            *x2 = *x1;
            *y2 = *y1;
        }
        return 1;
    }
}

int find_vline(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2)
{
    int xa = 0;
    int xb = 0;
    int ya = 0;
    int yb = 0;
    int maxDlzka = 0;
    int aktualDlzka = 0;
    int znak;
    bool priznak = true;

    for(int y = 0; y<bitmap->cols; y++)
    {
        for(int x = 0; x<bitmap->rows; x++)
        {
            znak = getcolor(&(*bitmap),x,y);

            if(znak == 1)
            {
                if(priznak == true)
                {
                    xa = x;
                    ya = y;
                    aktualDlzka++;
                    priznak =false;
                }
                else if (priznak == false)
                {
                    xb = x;
                    yb = y;
                    aktualDlzka++;
                }
            }

            if((znak == 0 || (bitmap->rows-1)==x) || (znak == 1 && (bitmap->rows-1)==x) )
            {
                if(maxDlzka<aktualDlzka || (*x1>xa && maxDlzka == aktualDlzka ))
                {
                    *x1 = xa;
                    *x2 = xb;
                    *y1 = ya;
                    *y2 = yb;
                    maxDlzka=aktualDlzka;
                }
                aktualDlzka = 0;
                priznak = true;
            }
        }
    }
    if(maxDlzka == 0)
    {
        return 0;
    }
    else
    {
        if(*x2 == 0 && *y2 == 0)
        {
            *x2 = *x1;
            *y2 = *y1;
        }
        return 1;
    }
}

int find_square(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2)
{
    int xa = 0;
    int xb = 0;
    int ya = 0;
    int yb = 0;
    int px1 = 0;
    int py1 = 0;
    int px2 = 0;
    int py2 = 0;
    int maxDlzka = 1;
    int maxDlzka2 = 1;
    int aktualDlzka = 0;
    int znak;
    bool priznak = true;

    // H1
    for(int xh1 = 0; xh1<bitmap->rows; xh1++)
    {
        int yh1;
        int y = 0;
        for(yh1=y; yh1<bitmap->cols; yh1++)
        {
            znak = getcolor(&(*bitmap),xh1,yh1);

            if(znak == 1)
            {
                if(priznak == true)
                {
                    xa = xh1;
                    ya = yh1;
                    aktualDlzka++;
                    priznak =false;
                }
                else if (priznak == false)
                {
                    aktualDlzka++;
                }
            }
            if((znak == 0 || (bitmap->cols-1)==yh1) || (znak == 1 && (bitmap->cols-1)==yh1) )
            {
                bool priznakv1 = true;
                if(maxDlzka<aktualDlzka)
                {
                    px1 = xa;
                    py1 = ya;
                    px2 = xb;
                    py2 = yb;
                    maxDlzka2=aktualDlzka;

                }
                else if(znak == 0 && aktualDlzka <= maxDlzka2)
                {
                    priznakv1 = false;

                }
                if(znak == 0)
                {
                    y++;
                }
                aktualDlzka = 0;
                priznak = true;

                //V1
                int yv1 = py1;

                for(int xv1 = px1; (xv1<bitmap->rows) && (maxDlzka2>=aktualDlzka) && priznakv1; xv1++)
                {

                    znak = getcolor(&(*bitmap),xv1,yv1);
                    if(znak == 1)
                    {
                        if(priznak == true)
                        {
                            aktualDlzka++;
                            priznak =false;
                        }
                        else if (priznak == false)
                        {
                            xb = xv1;
                            aktualDlzka++;
                        }

                    }
                    if((znak == 0 || (bitmap->rows-1)==xv1) || ((znak == 1 && (bitmap->rows-1)==xv1) || (aktualDlzka == maxDlzka2)))
                    {

                        bool priznakh2 = false;
                        if( maxDlzka2>=aktualDlzka && aktualDlzka>maxDlzka)
                        {
                            px2 = xb;
                            maxDlzka = aktualDlzka;
                            maxDlzka2 = aktualDlzka;
                            priznakv1 = false;
                            priznakh2 = true;
                        }
                        aktualDlzka = 0;
                        priznak = true;

                        //H2

                        int yh2 = py1;
                        int xh2 = px2;


                        for(; (yh2<bitmap->cols) && (maxDlzka>=aktualDlzka) && priznakh2; yh2++)
                        {

                            znak = getcolor(&(*bitmap),xh2,yh2);

                            if(znak == 1)
                            {
                                if(priznak == true)
                                {
                                    aktualDlzka++;
                                    priznak =false;
                                }
                                else if (priznak == false)
                                {
                                    yb = yh2;
                                    aktualDlzka++;
                                }

                            }
                            if((znak == 0) || (aktualDlzka == maxDlzka))
                            {
                                bool priznakv2 = true;
                                priznak = true;

                                if(znak == 1 && maxDlzka==aktualDlzka)
                                {
                                    py2 = yb;
                                    priznakh2 = false;
                                }
                                else if (znak == 0 && maxDlzka==aktualDlzka)
                                {
                                    priznakv2 =false;
                                    priznakv1 =true;
                                    maxDlzka2--;
                                }
                                if (znak == 0)
                                {
                                    if(*y2 == 0)
                                    {
                                        maxDlzka = 1;
                                    }
                                }
                                aktualDlzka = 0;

                                //V2
                                int yv2 = py2;
                                int xv2 = px1;

                                for(; (xv2<bitmap->rows) && (maxDlzka>=aktualDlzka) && priznakv2; xv2++)
                                {

                                    znak = getcolor(&(*bitmap),xv2,yv2);

                                    if(znak == 1)
                                    {
                                        if(priznak == true)
                                        {
                                            aktualDlzka++;
                                            priznak =false;
                                        }
                                        else if (priznak == false)
                                        {
                                            aktualDlzka++;
                                        }
                                    }
                                    if((znak == 0) || (aktualDlzka == maxDlzka))
                                    {
                                        if(znak== 1 && maxDlzka==aktualDlzka)
                                        {

                                            *x1 = px1;
                                            *y1 = py1;
                                            *x2 = px2;
                                            *y2 = py2;
                                            priznakv2 = false;
                                        }
                                        else
                                        {
                                            px1 = 0;
                                            py1 = 0;
                                            px2 = 0;
                                            py2 = 0;
                                            maxDlzka = 1;
                                            priznakv2 = false;
                                        }
                                        aktualDlzka = 0;
                                        priznak = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if(maxDlzka == 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
