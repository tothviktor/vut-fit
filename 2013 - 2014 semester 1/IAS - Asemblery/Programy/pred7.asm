%include 'rw32.inc'     

[segment .data use32]   
retez1: resb (100)
retez2: resb (100)  
str_ne: db "Nenasel",0
str_jo: db "Nasel",0

[segment .code use32]
                        
prologue

	mov ebx,100
	mov edi,retez1
	call ReadString
	
	mov ecx,100
	mov al,0
	mov edi,retez1
	repnz scasb

	sub edi,retez1 + 1
	mov eax,edi
	mov ecx,edi
	call WriteUInt32
	call WriteNewLine
	
	mov al,'a'
	mov edi,retez1
	repnz scasb
	
	mov esi,str_jo
	jz nasel
	
	mov esi,str_ne

nasel:	
	call WriteString
	call WriteNewLine
	
epilogue 