%include 'rw32.inc'
 
[segment .data use32]

[segment .code use32]
prologue
	
	call ReadUInt8
	mov cx,ax
	jcxz exit
	mov ax,0
	while:
		inc ax
		loop while	
	exit:
	call WriteUInt8
	call WriteNewLine
	
epilogue