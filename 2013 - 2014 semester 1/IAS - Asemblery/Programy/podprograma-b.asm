%include 'rw32.inc'
 
[segment .data use32]
[segment .code use32]

minus:
	sub ebx,ecx
	ret

prologue
	
	mov ebx,5
	mov ecx,2
	push ebx
	call minus
	mov eax,ebx
	call WriteUInt8
	call WriteNewLine
	pop ebx
	mov eax,ebx
	call WriteUInt8
	call WriteNewLine
	
epilogue