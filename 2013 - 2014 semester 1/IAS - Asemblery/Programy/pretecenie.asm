%include "rw32.inc"
[segment .data use32]
IsCharInString:
 push bp
 mov bp, sp
 mov ax, [bp + 6] ; symbol ch
 mov bx, [bp + 4] ; retezec
 xor cx, cx
@opakuj:
 cmp ax, [bx + cx] ; porovnavam symbol "ch" z symbolem s retezce
 je @vrat_1 ; kdyz nasel, tak vrat 1
 cmp ax, 0 ; kdyz konec retezci, tak vrat 0
 je @vrat_0 ; kdyz nenasel, tak vrat 0
 add cx, 2 ; nasledujici symbol v retezci
 jmp @opakuj
@vrat_0:
 mov ax, 0
 jmp @end
@vrat_1:
 mov ax, 1
 jmp @end
@end:
 mov bp, sp
 retf
[segment .code use32]
prologue
 mov ax, [ch] ; nahrat symbol, ktery budeme hledat
 push ax 
 mov ax, str ; nahrat retezec
 push str
 call far IsCharInString ; volani vunkci
epilogue