%include 'rw32.inc'
 
[segment .data use32]

[segment .code use32]
prologue
	
	call ReadUInt8
	mov cx,ax
	jcxz exit
	mov ax,0
	whilee:
		inc ax
		dec cx
		cmp cx,2
		ja whilee
	exit:
	call WriteUInt8
	call WriteNewLine
	
epilogue