%include 'rw32.inc'
 
[segment .data use32]

c dw 0

[segment .code use32]

prologue
	mov ax,0
	sub ax,5+1 	; ax =
	mov dx,3
	or dx,3 	; dx =
	imul dx 	; ax =
	sar ax,6 	; ax =
	adc ax,118
	and dx,0 	; dx =
	mov cx,3
	div cx		; ax =
	xor dx,0 	; dx =
	call WriteUInt16
	call WriteNewLine
	
	mov word [c],118
	
	skok1:
	fadd st0,st0
	loop skok1
	fild word [c]
	fsub st0,st1
	fldz
	fmulp st1,st0
	fsin
	fcomip st0,st1
	mov bx,0
	ja skok2
	dec bx
	skok2:
	inc bx
	
epilogue