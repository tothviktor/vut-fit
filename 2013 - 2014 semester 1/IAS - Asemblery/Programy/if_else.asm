%include 'rw32.inc'
 
[segment .data use32]

nula     	db  "je nula", 10, 13, 0
nenula		db	"nie je nula", 10, 13, 0

[segment .code use32]
prologue
	
	call ReadInt8
	mov dl,al
	cmp dl,0
	jnz notNull
		mov esi,nula
		call WriteString
		jmp exit	
	notNull:
		mov esi,nenula
		call WriteString
	
	exit:
	
epilogue