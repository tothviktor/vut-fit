%include 'rw32.inc'
 
[segment .data use32]

x	DB 	5

[segment .code use32]

minus:
	push ebp
	mov ebp,esp
	sub esp,4
	
	mov eax,[ebp+8]
	cdq
	idiv dword [ebp+12]
	mov [ebp-4],eax
	
	mov eax,[ebp+16]
	mov [eax],edx
	
	mov eax,[ebp-4]
	mov esp,ebp
	pop ebp
	ret 12

prologue
	push x
	push dword 4
	push dword 6
	call minus

	call WriteUInt8
	call WriteNewLine
	
epilogue