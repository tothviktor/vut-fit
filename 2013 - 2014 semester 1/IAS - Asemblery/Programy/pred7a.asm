%include 'rw32.inc'     

[segment .data use32]   
retez1: db "bfdsjurerger",0

[segment .code use32]
                        
prologue
	
	mov esi,retez1
	call WriteString
	call WriteNewLine
	
;	EDI = 0;
;	while (r1[EDI] != 0)
;	{
;		ESI = EDI+1;
;		while (r1[ESI] != 0)
;		{
;			if (r1[ESI] < r1[EDI])
;			{
;				prohod r1[EDI] a r1[ESI]
;			}
;			ESI++;
;		}
;		EDI++;
;	}
	
	xor edi,edi

while1_beg:	
	cmp [edi + retez1], byte 0
	jz while1_end
	mov esi,edi
	inc esi
	
while2_beg:
	mov al,[esi + retez1]
	cmp al,0
	jz while2_end
	
	cmp al,[edi + retez1]
	jae skip
	
	;mov ah,[edi + retez1]
	;mov [edi + retez1],al
	;mov [esi + retez1],ah
	xchg al,[edi + retez1]
	mov [esi + retez1],al
skip:
	inc esi
	jmp while2_beg
	
while2_end:
	inc edi
	jmp while1_beg
	
while1_end:
	mov esi,retez1
	call WriteString
	call WriteNewLine
	
epilogue 