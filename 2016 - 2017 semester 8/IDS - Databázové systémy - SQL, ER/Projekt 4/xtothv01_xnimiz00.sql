/* ------------ Projekt do IDS -------------
	|  ---------  Vytvoril: ----------  |
	|  Viktor T�th 		   - xtothv01   |
	|  Vladislav Nimizhman - xnimiz00 	| 
	|  -------------------------------  |
*/

/* ------------------------------------------------------------------------ */
/* ------------------------ Zmazanie poloziek z databaze ------------------ */
/* ------------------------------------------------------------------------ */

SET serveroutput ON;

DROP MATERIALIZED VIEW LOG ON tovar;
DROP MATERIALIZED VIEW LOG ON poloskovy_kosik;
DROP MATERIALIZED VIEW predajnost;

BEGIN
   FOR cur_rec IN (SELECT object_name, object_type
                     FROM user_objects
                    WHERE object_type IN
                             ('TABLE',
                              'VIEW',
                              'PACKAGE',
                              'PROCEDURE',
                              'FUNCTION',
                              'SEQUENCE'
                             ))
   LOOP
      BEGIN
         IF cur_rec.object_type = 'TABLE'
         THEN
            EXECUTE IMMEDIATE    'DROP '
                              || cur_rec.object_type
                              || ' "'
                              || cur_rec.object_name
                              || '" CASCADE CONSTRAINTS';
         ELSE
            EXECUTE IMMEDIATE    'DROP '
                              || cur_rec.object_type
                              || ' "'
                              || cur_rec.object_name
                              || '"';
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (   'FAILED: DROP '
                                  || cur_rec.object_type
                                  || ' "'
                                  || cur_rec.object_name
                                  || '"'
                                 );
      END;
   END LOOP;
END;
/

/* ------------------------------------------------------------------------ */
/* ------------------------ 2. Cast projektu ------------------------------ */
/* ------------------------------------------------------------------------ */

CREATE SEQUENCE osoba_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE osoba
(
	id_osoba		INTEGER		 	CONSTRAINT PK_osoba_id  				PRIMARY KEY,
	meno			VARCHAR(20)		CONSTRAINT NN_osoba_meno				NOT NULL,
	priezvisko		VARCHAR(20)		CONSTRAINT NN_osoba_priezvisko			NOT NULL,
	ulica			VARCHAR(50)		CONSTRAINT NN_osoba_ulica				NOT NULL,
	cislo_ulice		INTEGER			CONSTRAINT NN_osoba_cislo_ulice			NOT NULL,
	mesto			VARCHAR(50)		CONSTRAINT NN_osoba_mesto				NOT NULL,
	PSC				INTEGER			CONSTRAINT NN_osoba_PSC					NOT NULL,
	telefon			INTEGER			CONSTRAINT NN_osoba_telefon				NOT NULL,
	email			VARCHAR(40)		CONSTRAINT NN_osoba_email				NOT NULL
);

CREATE SEQUENCE opravnenia_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE opravnenia
(
	id_opravnenia	INTEGER			CONSTRAINT PK_opravnenia_id				PRIMARY KEY,
	meno			VARCHAR(20)		CONSTRAINT NN_opravnenia_meno			NOT NULL
);

CREATE SEQUENCE uzivatel_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE uzivatel
(
	id_uzivatel		INTEGER			CONSTRAINT PK_uzivatel_id				PRIMARY KEY,
	osoba_id		INTEGER			CONSTRAINT FK_uzivatel_osoba_id			REFERENCES osoba(id_osoba),
	opravnenia_id	INTEGER			CONSTRAINT FK_uzivatel_opravnenia_id	REFERENCES opravnenia(id_opravnenia),
	login			VARCHAR(20)		CONSTRAINT NN_uzivatel_login			NOT NULL,
	heslo			VARCHAR(20)		CONSTRAINT NN_uzivatel_heslo			NOT NULL
);

CREATE SEQUENCE kosik_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE kosik
(
	id_kosik		INTEGER			CONSTRAINT PK_kosik_id					PRIMARY KEY,
	uzivatel_id		INTEGER			CONSTRAINT FK_kosik_uzivatel_id			REFERENCES uzivatel(id_uzivatel)
);

CREATE SEQUENCE objednavka_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE objednavka
(
	id_objednavka	INTEGER			CONSTRAINT PK_objednavka_id				PRIMARY KEY,
	uzivatel_id		INTEGER			CONSTRAINT FK_objednavka_uzivatel_id	REFERENCES uzivatel(id_uzivatel),	
	vybavil_id		INTEGER			CONSTRAINT FK_objednavka_vybavil_id		REFERENCES uzivatel(id_uzivatel),
	kosik_id		INTEGER			CONSTRAINT FK_objednavka_kosik_id		REFERENCES kosik(id_kosik),
	cena			NUMBER(10,2)	CONSTRAINT NN_objednavka_cena			NOT NULL,		
	zaplatene		CHAR(1)			CONSTRAINT NN_objednavka_zaplatene		CHECK (zaplatene in('A', 'N')),
	expandovane		CHAR(1)			CONSTRAINT NN_objednavka_expandovane	CHECK (expandovane in('A', 'N'))
);

CREATE TABLE objednavka_stara
(
	id_objednavka	INTEGER,
	uzivatel_id		INTEGER,
	vybavil_id		INTEGER,
	kosik_id		INTEGER,
	cena			NUMBER(10,2),
	zaplatene		CHAR(1),
	expandovane		CHAR(1)
);

CREATE SEQUENCE kategoria_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE kategoria
(
	id_kategoria	INTEGER			CONSTRAINT PK_kategoria_id				PRIMARY KEY,
	meno			VARCHAR(20)		CONSTRAINT NN_kategoria_meno			NOT NULL
);

CREATE SEQUENCE software_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE software
(
	id_software		INTEGER			CONSTRAINT PK_software_id				PRIMARY KEY,
	typ_licencie	VARCHAR(20)		CONSTRAINT NN_software_typ_licencie		NOT NULL,
	konfiguracia	VARCHAR(100)	CONSTRAINT NN_software_konfiguracia		NOT NULL
);

CREATE SEQUENCE hardware_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE hardware
(
	id_hardware		INTEGER			CONSTRAINT PK_hardware_id				PRIMARY KEY,
	hmotnost		NUMBER(10,2)	CONSTRAINT NN_hardware_hmotnost			NOT NULL,
	rozmery_vyska	INTEGER			CONSTRAINT NN_hardware_rozmery_vyska	NOT NULL,
	rozmery_sirka	INTEGER			CONSTRAINT NN_hardware_rozmery_sirka	NOT NULL,
	rozmery_hlbka	INTEGER			CONSTRAINT NN_hardware_rozmery_hlbka	NOT NULL
);

CREATE SEQUENCE tovar_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE tovar
(
	id_tovar		INTEGER			CONSTRAINT PK_tovar_id					PRIMARY KEY,	
	kategoria_id	INTEGER			CONSTRAINT FK_tovar_kategoria_id		REFERENCES kategoria(id_kategoria),
	hardware_id		INTEGER			CONSTRAINT FK_tovar_hardware_id			REFERENCES hardware(id_hardware),
	software_id		INTEGER			CONSTRAINT FK_tovar_software_id			REFERENCES software(id_software),
	nazov			VARCHAR(40)		CONSTRAINT NN_tovar_nazov				NOT NULL,
	popis			VARCHAR(100)	CONSTRAINT NN_tovar_popis				NOT NULL,
	znacka			VARCHAR(40)		CONSTRAINT NN_tovar_znacka				NOT NULL,
	cena			NUMBER(10,2)	CONSTRAINT NN_tovar_cena				NOT NULL,
	dph				NUMBER(10,2)	CONSTRAINT NN_tovar_dph					NOT NULL,
	skladom			CHAR(1)			CONSTRAINT NN_tovar_skladom				CHECK(skladom in('A', 'N')),
	zlava			INTEGER			CONSTRAINT NN_tovar_zlava				NOT NULL
);

CREATE SEQUENCE poloskovy_kosik_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE poloskovy_kosik
(
	id_poloskovy_kosik	INTEGER		CONSTRAINT PK_poloskovy_kosik_id		PRIMARY KEY,		
	kosik_id			INTEGER		CONSTRAINT FK_poloskovy_kosik_kosik_id	REFERENCES kosik(id_kosik),
	tovar_id			INTEGER		CONSTRAINT FK_poloskovy_kosik_tovar_id	REFERENCES tovar(id_tovar),
	mnozstvo			INTEGER		CONSTRAINT NN_poloskovy_kosik_mnozstvo	NOT NULL
);

CREATE SEQUENCE hodnotenie_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE hodnotenie
(
	id_hodnotenie	INTEGER			CONSTRAINT PK_hodnotenie_id				PRIMARY KEY,
	uzivatel_id		INTEGER			CONSTRAINT FK_hodnotenie_uzivatel_id	REFERENCES uzivatel(id_uzivatel),
	tovar_id		INTEGER			CONSTRAINT FK_hodnotenie_tovar_id		REFERENCES tovar(id_tovar),
	znamka			INTEGER			CONSTRAINT NN_hodnotenie_znamka			CHECK (znamka BETWEEN 1 AND 5)
);

INSERT INTO osoba (id_osoba, meno, priezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'Viktor', 'Toth', 'Kolejni', '2', 'Brno', '61200', '0911969969', 'tothviktor11@gmail.com');
INSERT INTO osoba (id_osoba, meno, priezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'Vladislav', 'Nimizhman', 'Tererova', '25', 'Brno', '61300', '0911888963', 'vlad.nimik@gmail.com');
INSERT INTO osoba (id_osoba, meno, priezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'Martin', 'Pavelka', 'Kolejni', '2', 'Brno', '61200', '0915123456', 'martinpavelka@icloud.com');
INSERT INTO osoba (id_osoba, meno, priezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'Jakub', 'Kelecseni', 'Nitrianska', '15', 'Nove Zamky', '94115', '0920546132', 'kelecsenijak@icloud.com');
INSERT INTO osoba (id_osoba, meno, priezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'David', 'Toth', 'Nitrianska', '20', 'Nitra', '94000', '0920116132', 'tothvdavid@icloud.com');

INSERT INTO opravnenia(id_opravnenia, meno) VALUES (opravnenia_seq.NEXTVAL, 'zakaznik');
INSERT INTO opravnenia(id_opravnenia, meno) VALUES (opravnenia_seq.NEXTVAL, 'manazer');
INSERT INTO opravnenia(id_opravnenia, meno) VALUES (opravnenia_seq.NEXTVAL, 'admin');
INSERT INTO opravnenia(id_opravnenia, meno) VALUES (opravnenia_seq.NEXTVAL, 'technik');

INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL, opravnenia_seq.CURRVAL, 'xtothd00', 'heslo1');
INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL-1, opravnenia_seq.CURRVAL, 'xkelec01', 'heslo2');
INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL-2, opravnenia_seq.CURRVAL-1, 'xpavel02', 'heslo3');
INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL-3, opravnenia_seq.CURRVAL-2, 'xnimih00', 'heslo4');
INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL-4, opravnenia_seq.CURRVAL-3, 'xtothv01', 'heslo5');
	
INSERT INTO kategoria (id_kategoria, meno) VALUES (kategoria_seq.NEXTVAL, 'notebooky a PC');
INSERT INTO kategoria (id_kategoria, meno) VALUES (kategoria_seq.NEXTVAL, 'mobilne telefony');
INSERT INTO kategoria (id_kategoria, meno) VALUES (kategoria_seq.NEXTVAL, 'televizoty');
INSERT INTO kategoria (id_kategoria, meno) VALUES (kategoria_seq.NEXTVAL, 'komponenty');

INSERT INTO software (id_software, typ_licencie, konfiguracia) VALUES (software_seq.NEXTVAL, 'Antivirus', 'ESET 10.1');
INSERT INTO software (id_software, typ_licencie, konfiguracia) VALUES (software_seq.NEXTVAL, 'Operacny System', 'Windows 10');
INSERT INTO software (id_software, typ_licencie, konfiguracia) VALUES (software_seq.NEXTVAL, 'Operacny System', 'RedHat');
INSERT INTO software (id_software, typ_licencie, konfiguracia) VALUES (software_seq.NEXTVAL, 'Hra', 'Windows 10');

INSERT INTO hardware (id_hardware, hmotnost, rozmery_vyska, rozmery_sirka, rozmery_hlbka) VALUES (hardware_seq.NEXTVAL, '5,25', 10, 15, 15);
INSERT INTO hardware (id_hardware, hmotnost, rozmery_vyska, rozmery_sirka, rozmery_hlbka) VALUES (hardware_seq.NEXTVAL, '1,55', 20, 15, 10);
INSERT INTO hardware (id_hardware, hmotnost, rozmery_vyska, rozmery_sirka, rozmery_hlbka) VALUES (hardware_seq.NEXTVAL, '2,00', 50, 50, 50);
INSERT INTO hardware (id_hardware, hmotnost, rozmery_vyska, rozmery_sirka, rozmery_hlbka) VALUES (hardware_seq.NEXTVAL, '3,88', 30, 25, 30);
	
INSERT INTO tovar (id_tovar, kategoria_id, hardware_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL-1, hardware_seq.CURRVAL-1, 'BL2411', 'super LCD moniotor', 'Lenovo', '15000,99', '20', 'A', '0');
INSERT INTO tovar (id_tovar, kategoria_id, hardware_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL-3, hardware_seq.CURRVAL, 'Thinkpad Edge E530', 'vykonny notebook', 'Lenovo', '45999,99', '20', 'N', '0');
INSERT INTO tovar (id_tovar, kategoria_id, hardware_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL, hardware_seq.CURRVAL-2, 'GT500', 'graficka karta', 'Nvidea', '2000,10', '25', 'A', '20');
INSERT INTO tovar (id_tovar, kategoria_id, hardware_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL, hardware_seq.CURRVAL-3, 'E-Blue', 'bezdrotova mys', 'Fresco', '300,01', '19', 'A', '50');
INSERT INTO tovar (id_tovar, kategoria_id, software_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL-3, software_seq.CURRVAL, 'Total War Rome', 'strategia', 'EAgames', '100', '40', 'A', '40');
INSERT INTO tovar (id_tovar, kategoria_id, software_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL-3, software_seq.CURRVAL-3, 'NOD 10.1', 'antivirus', 'ESET', '899', '10', 'N', '20');

INSERT INTO kosik (id_kosik, uzivatel_id) VALUES (kosik_seq.NEXTVAL, uzivatel_seq.CURRVAL-4);
INSERT INTO kosik (id_kosik, uzivatel_id) VALUES (kosik_seq.NEXTVAL, uzivatel_seq.CURRVAL-3);
INSERT INTO kosik (id_kosik, uzivatel_id) VALUES (kosik_seq.NEXTVAL, uzivatel_seq.CURRVAL-2);

INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL, tovar_seq.CURRVAL-2, 3);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL, tovar_seq.CURRVAL-4, 3);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL, tovar_seq.CURRVAL-3, 3);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL-1, tovar_seq.CURRVAL-3, 1);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL-2, tovar_seq.CURRVAL-3, 1);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL-2, tovar_seq.CURRVAL, 1);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL-2, tovar_seq.CURRVAL-1, 1);

INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-2, tovar_seq.CURRVAL-2, 5);
INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-2, tovar_seq.CURRVAL-5, 3);
INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-3, tovar_seq.CURRVAL-3, 1);
INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-4, tovar_seq.CURRVAL-1, 2);
INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-4, tovar_seq.CURRVAL-4, 1);

INSERT INTO objednavka (id_objednavka, uzivatel_id, vybavil_id, kosik_id, cena, zaplatene, expandovane) VALUES (objednavka_seq.NEXTVAL, uzivatel_seq.CURRVAL-4, uzivatel_seq.CURRVAL-3, kosik_seq.CURRVAL, '1000', 'A', 'N');
INSERT INTO objednavka (id_objednavka, uzivatel_id, vybavil_id, kosik_id, cena, zaplatene, expandovane) VALUES (objednavka_seq.NEXTVAL, uzivatel_seq.CURRVAL-3, uzivatel_seq.CURRVAL-3, kosik_seq.CURRVAL, '1500', 'N', 'N');
INSERT INTO objednavka (id_objednavka, uzivatel_id, vybavil_id, kosik_id, cena, zaplatene, expandovane) VALUES (objednavka_seq.NEXTVAL, uzivatel_seq.CURRVAL-2, uzivatel_seq.CURRVAL-3, kosik_seq.CURRVAL, '3000', 'A', 'A');

/* ------------------------------------------------------------------------ */
/* ------------------------ 3. Cast projektu ------------------------------ */
/* ------------------------------------------------------------------------ */

/*Dotaz zobrazi 	  	 stlpce: 	konfiguracia softwaru
					 z tabuliek:	software
	pri ktor�ch plati podmienka:	kde typ licencie je Operacny System
*/
SELECT konfiguracia
FROM software
WHERE typ_licencie = 'Operacny System';

/*Dotaz zobrazi 	  	 stlpce: 	cislo objednavy, meno, priezvisko, cena, zaplatene
					 z tabuliek:	Osoba, Uzivatel a Objednavka 
	pri ktor�ch plati podmienka:	objednavky boli zaplatene
*/
SELECT id_objednavka, meno, priezvisko, cena, zaplatene
FROM objednavka OB INNER JOIN uzivatel U ON ( OB.uzivatel_id = U.id_uzivatel) 
				   INNER JOIN osoba OS ON ( U.osoba_id = OS.id_osoba)
WHERE OB.zaplatene = 'A';

/*Dotaz zobrazi 	  	 stlpce: 	cislo objednavy, meno, priezvisko, cena, zaplatene
					 z tabuliek:	Osoba, Uzivatel a Objednavka 
	pri ktor�ch plati podmienka:	objednavky neboli zaplatene a vyska ceny objednavky je vyssia ako 1000
*/
SELECT id_objednavka, meno, priezvisko, cena, zaplatene
FROM objednavka OB INNER JOIN uzivatel U ON ( OB.uzivatel_id = U.id_uzivatel) 
				   INNER JOIN osoba OS ON ( U.osoba_id = OS.id_osoba)
WHERE OB.zaplatene = 'N' AND OB.cena > 1000;

/*Dotaz zobrazi 	  	 stlpce: 	meno, prizvisko, login, ��slo opravnenia
					 z tabuliek:	Osoba, Uzivatel, Opravnenia
	pri ktor�ch plati podmienka:	su zamestnanci (technik, admin, manazer)
*/
SELECT OS.meno, OS.priezvisko, login, OP.meno
FROM osoba OS INNER JOIN uzivatel U ON ( OS.id_osoba = U.id_uzivatel ) 
			  INNER JOIN opravnenia OP ON (U.opravnenia_id = OP.id_opravnenia)
WHERE id_opravnenia > 1;

/*Dotaz zobrazi 	  	 stlpce: 	cislo kosika, celkovu sumu poloziek v kosiku
					 z tabuliek:	Poloskovy_kosik, Tovar
	pri ktor�ch plati podmienka:	Zgrupujeme polozky kde je rovnake id kosika
*/
SELECT kosik_id, SUM(cena*mnozstvo) celkova_suma
FROM poloskovy_kosik P INNER JOIN tovar T ON (P.tovar_id = T.id_tovar)
GROUP BY kosik_id;

/*Dotaz zobrazi 	  	 stlpce: 	nazov tovaru, popis
					 z tabuliek:	Tovar, Poloskovy_kosik
	pri ktor�ch plati podmienka:	dan� tovar nikto neobjedn�va 
*/
SELECT nazov, popis
FROM tovar T
WHERE NOT EXISTS
	(SELECT * 
	FROM poloskovy_kosik P
	WHERE tovar_id = T.id_tovar);

/*Dotaz zobrazi 	  	 stlpce: 	pocet tovarov v danom kategorii, id kategorie
					 z tabuliek:	Tovar, kategoria
	pri ktor�ch plati podmienka:	Zgrupuje polosky kde je rovnaka kategoria 
*/
SELECT COUNT(*) pocet_v_kategorii, K.id_kategoria
FROM tovar T INNER JOIN kategoria K ON (T.kategoria_id = K.id_kategoria)
GROUP BY id_kategoria;

/*Dotaz zobrazi 	  	 stlpce: 	vsetky informacie
					 z tabuliek:	Osoba
	pri ktor�ch plati podmienka:	je technik, tj opravnenie_id ma 4
*/
SELECT *
FROM osoba
WHERE id_osoba IN 
(SELECT osoba_id FROM uzivatel WHERE opravnenia_id = 4 );

/* ------------------------------------------------------------------------ */
/* ------------------------ 4. Cast projektu ------------------------------ */
/* ------------------------------------------------------------------------ */

/* TRIGGER
Ak vkladame hodnotu NULL, pricom porusujeme obmedzenie, tak nahradi tuto hodnotu hodnotou zo sekvencie */
CREATE OR REPLACE TRIGGER uzivatel_ai_null
BEFORE INSERT ON uzivatel
FOR EACH ROW 
	BEGIN
	IF    :new.id_uzivatel IS NULL THEN 
		  :new.id_uzivatel := uzivatel_seq.nextval;
		   DBMS_OUTPUT.put_line('UZIVATEL_ID uspesne generovany zo sekvencie.');
	END IF;
	END;
	/
/* Vyskusaj insert s null */
INSERT INTO UZIVATEL (OSOBA_ID, OPRAVNENIA_ID, LOGIN, HESLO) 
VALUES ('1', '1', 'skuska', 'skuska');
select * from uzivatel;


/* TRIGER
Ulozi zmazanu polosku z objednavky do zaloznej tabulky, tak, ze vytvori historiu objednavok */
CREATE OR REPLACE TRIGGER ulo�_objednavku
BEFORE DELETE ON objednavka
FOR EACH ROW
	BEGIN
		INSERT INTO objednavka_stara
		VALUES (:old.id_objednavka, :old.uzivatel_id, :old.vybavil_id, :old.kosik_id, 
				:old.cena, :old.zaplatene, :old.expandovane);
		DBMS_OUTPUT.put_line('Objednavka bola uspesne zalohovana!');
	END;
/

SELECT * FROM objednavka_stara;
SELECT * FROM objednavka;

/* Vyskusaj zmazat z tabulky objednavka a zobrazit tabulku zaloh */
DELETE FROM objednavka WHERE id_objednavka = 1;

SELECT * FROM objednavka_stara;
SELECT * FROM objednavka;



/* PROCEDURA
zadanemu tovaru znizi o zadane percento cenu, 
v parametroch sa zadava cislo tovaru a percento */
CREATE OR REPLACE PROCEDURE zlava (cislo_tovaru IN NUMBER, percento IN NUMBER) AS
	/* Deklaracia premennych */
	zle_percenta EXCEPTION;
	cislo_tovar tovar.id_tovar%TYPE;
	BEGIN
		/* Overenie ci sa tovar nachadza medzi ostatnymi tovarmi */
		SELECT id_tovar INTO cislo_tovar
		FROM tovar
		WHERE id_tovar = cislo_tovaru;
		
		/* Overenie ci nebolo zadane zle percento */
		IF percento <= 0 OR percento >= 100 THEN
			RAISE zle_percenta;
		END IF;
		
		/* Upravenie ceny tovaru */
		UPDATE tovar
		SET cena = cena * (100 - percento) / 100
		WHERE id_tovar = cislo_tovaru;
		
		/* Vypis uspech a nahraj (COMMIT) */
		DBMS_OUTPUT.PUT_LINE ('Cena tovaru bola upravena!');
		COMMIT;

	EXCEPTION
		/* Vynimka ked boli zadane zle percenta */
		WHEN zle_percenta THEN
		DBMS_OUTPUT.PUT_LINE('Percento je mozne zadat v rozmedzi 1 - 99 !');
		ROLLBACK;
		
		/* Nenasiel sa tovar */
		WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE('Tovar nebol najdeny!');
		ROLLBACK;
	END;
/
/* Zobracenie tovaru pred a po zlave (prikaz zlava) */

SELECT * FROM tovar WHERE id_tovar = 3;
EXECUTE zlava(3,50);
SELECT * FROM tovar WHERE id_tovar = 3;


/* PROCEDURA
zisti znamku tovaru a ak niektory tovar ma znamku 1 tak jeho cenu zlacni o 40%*/
CREATE OR REPLACE PROCEDURE zlacni AS
	/* Deklaracia premennych */
	CURSOR kurzor1 IS SELECT tovar_id 
	FROM hodnotenie 
	WHERE znamka = 1;
	
	loc_tovar hodnotenie.tovar_id%TYPE;
	loc_skladom tovar.skladom%TYPE;
	
	BEGIN
		OPEN kurzor1;
		LOOP
			/* Nacitame (FETCH) aktualny ukazovatel */
			FETCH kurzor1 INTO loc_tovar;
			EXIT WHEN kurzor1%NOTFOUND;
			
			/* Ziskamie informaciu, ci je na sklade */
			SELECT skladom INTO loc_skladom
			FROM tovar
			WHERE id_tovar = loc_tovar;
			
			/* Ak je skladom, tak zlacnime tovar a vypise sa sprava */
			IF loc_skladom = 'A' THEN
				zlava(loc_tovar, 40);       /* BOLO UPRAVENE*/
				DBMS_OUTPUT.put_line('Tovar mal hodnotenie 1, zlacni sa o 40%' || loc_tovar);
			END IF;
		END LOOP;
		CLOSE kurzor1;
		COMMIT;
		DBMS_OUTPUT.put_line('Zlacnenie tovaru je ukoncene.');
	
	EXCEPTION
		WHEN OTHERS THEN
		DBMS_OUTPUT.put_line('Chyba pri zlacnovani tovaru!');
		ROLLBACK;
	END;
/

/* Zobrazenie tovaru pred zlacnenim */

SELECT * FROM tovar;
EXECUTE zlacni;
/* Zobrazenie tovaru po zlacnenim */
SELECT * FROM tovar;

/* EXPLAIN PLAN

/* Pytame sa na znacku, tak ju jednoducho indexujeme */
CREATE INDEX poloskovyKosikIndex ON POLOSKOVY_KOSIK ("TOVAR_ID");
CREATE UNIQUE INDEX INDEX1 ON TOVAR (ID_TOVAR DESC);
CREATE INDEX indexovane ON tovar (znacka);
/* Znova zobrazime explain plan */
SELECT * FROM TABLE (DBMS_XPLAN.DISPLAY);

/*Kolko produktov od Lenovo sa nachadza v kosikoch */
EXPLAIN PLAN FOR
SELECT kosik_id, SUM(mnozstvo) pocet_Lenovo_v_kosiku
FROM poloskovy_kosik P INNER JOIN tovar T ON (P.tovar_id = T.id_tovar)
WHERE T.znacka = 'Lenovo'
GROUP BY kosik_id;

/* Zobrazenie explain planu */
SET Linesize 130
SET Pagesize 0
SELECT * FROM TABLE (DBMS_XPLAN.DISPLAY);



/* MATERIALIZOVAN� POHLAD
Vytvorenie pomocnych logov pre nasledny materializovany pohlad */
CREATE MATERIALIZED VIEW LOG ON tovar
WITH PRIMARY KEY, 
ROWID (nazov)
INCLUDING NEW VALUES;

CREATE MATERIALIZED VIEW LOG ON poloskovy_kosik
WITH PRIMARY KEY, 
ROWID (tovar_id, mnozstvo)
INCLUDING NEW VALUES;

/* Vytvorenie materializovaneho pohladu, ktory vypise pocet objednanych poloziek tovaru */	
CREATE MATERIALIZED VIEW predajnost
REFRESH FAST ON COMMIT AS
SELECT T.nazov, SUM(PK.mnozstvo) AS predanych
FROM poloskovy_kosik PK, tovar T
WHERE PK.tovar_id = T.id_tovar
GROUP BY T.nazov;
	
/* Zobrazenie materializovaneho pohladu */
SELECT * FROM tovar;
SELECT * FROM poloskovy_kosik;
SELECT * FROM predajnost;


/* Pridelenie opravneni druhemu clena tymu */
GRANT ALL ON osoba TO xnimiz00;
GRANT ALL ON opravnenia TO xnimiz00;
GRANT ALL ON uzivatel TO xnimiz00;
GRANT ALL ON kosik TO xnimiz00;
GRANT ALL ON objednavka TO xnimiz00;
GRANT ALL ON objednavka_stara TO xnimiz00;
GRANT ALL ON kategoria TO xnimiz00;
GRANT ALL ON software TO xnimiz00;
GRANT ALL ON hardware TO xnimiz00;
GRANT ALL ON tovar TO xnimiz00;
GRANT ALL ON poloskovy_kosik TO xnimiz00;
GRANT ALL ON hodnotenie TO xnimiz00;
GRANT ALL ON predajnost TO xnimiz00;