/* ------------ Projekt do IDS -------------
	|  ---------  Vytvoril: ----------  |
	|  Viktor T�th 		   - xtothv01   |
	|  Vladislav Nimizhman - xnimiz00 	| 
	|  -------------------------------  |
*/

/* ------------------------------------------------------------------------ */
/* ------------------------ 2. Cast projektu ------------------------------ */
/* ------------------------------------------------------------------------ */

BEGIN
    FOR rec IN (
        SELECT 'DROP ' || object_type || ' ' || object_name || DECODE ( object_type, 'TABLE', ' CASCADE CONSTRAINTS PURGE' ) AS v_sql
        FROM user_objects
        WHERE object_type IN ( 'TABLE', 'VIEW', 'PACKAGE', 'TYPE', 'PROCEDURE', 'FUNCTION', 'TRIGGER', 'SEQUENCE' )
        ORDER BY object_type, object_name
    ) LOOP
        EXECUTE IMMEDIATE rec.v_sql;
    END LOOP;
END;
/

CREATE SEQUENCE osoba_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE osoba
(
	id_osoba		INTEGER		 	CONSTRAINT PK_osoba_id  				PRIMARY KEY,
	meno			VARCHAR(20)		CONSTRAINT NN_osoba_meno				NOT NULL,
	prezvisko		VARCHAR(20)		CONSTRAINT NN_osoba_priezvisko			NOT NULL,
	ulica			VARCHAR(50)		CONSTRAINT NN_osoba_ulica				NOT NULL,
	cislo_ulice		INTEGER			CONSTRAINT NN_osoba_cislo_ulice			NOT NULL,
	mesto			VARCHAR(50)		CONSTRAINT NN_osoba_mesto				NOT NULL,
	PSC				INTEGER			CONSTRAINT NN_osoba_PSC					NOT NULL,
	telefon			INTEGER			CONSTRAINT NN_osoba_telefon				NOT NULL,
	email			VARCHAR(40)		CONSTRAINT NN_osoba_email				NOT NULL
);

CREATE SEQUENCE opravnenia_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE opravnenia
(
	id_opravnenia	INTEGER			CONSTRAINT PK_opravnenia_id				PRIMARY KEY,
	meno			VARCHAR(20)		CONSTRAINT NN_opravnenia_meno			NOT NULL
);

CREATE SEQUENCE uzivatel_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE uzivatel
(
	id_uzivatel		INTEGER			CONSTRAINT PK_uzivatel_id				PRIMARY KEY,
	osoba_id		INTEGER			CONSTRAINT FK_uzivatel_osoba_id			REFERENCES osoba(id_osoba),
	opravnenia_id	INTEGER			CONSTRAINT FK_uzivatel_opravnenia_id	REFERENCES opravnenia(id_opravnenia),
	login			VARCHAR(20)		CONSTRAINT NN_uzivatel_login			NOT NULL,
	heslo			VARCHAR(20)		CONSTRAINT NN_uzivatel_heslo			NOT NULL
);

CREATE SEQUENCE kosik_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE kosik
(
	id_kosik		INTEGER			CONSTRAINT PK_kosik_id					PRIMARY KEY,
	uzivatel_id		INTEGER			CONSTRAINT FK_kosik_uzivatel_id			REFERENCES uzivatel(id_uzivatel)
);

CREATE SEQUENCE objednavka_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE objednavka
(
	id_objednavka	INTEGER			CONSTRAINT PK_objednavka_id				PRIMARY KEY,
	uzivatel_id		INTEGER			CONSTRAINT FK_objednavka_uzivatel_id	REFERENCES uzivatel(id_uzivatel),	
	vybavil_id		INTEGER			CONSTRAINT FK_objednavka_vybavil_id		REFERENCES uzivatel(id_uzivatel),
	kosik_id		INTEGER			CONSTRAINT FK_objednavka_kosik_id		REFERENCES kosik(id_kosik),
	cena			NUMBER(10,2)	CONSTRAINT NN_objednavka_cena			NOT NULL,		
	zaplatene		CHAR(1)			CONSTRAINT NN_objednavka_zaplatene		CHECK (zaplatene in('A', 'N')),
	expandovane		CHAR(1)			CONSTRAINT NN_objednavka_expandovane	CHECK (expandovane in('A', 'N'))
);

CREATE TABLE objednavka_stara
(
	id_objednavka	INTEGER,
	uzivatel_id		INTEGER,
	vybavil_id		INTEGER,
	kosik_id		INTEGER,
	cena			NUMBER(10,2),
	zaplatene		CHAR(1),
	expandovane		CHAR(1)
);

CREATE SEQUENCE kategoria_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE kategoria
(
	id_kategoria	INTEGER			CONSTRAINT PK_kategoria_id				PRIMARY KEY,
	meno			VARCHAR(20)		CONSTRAINT NN_kategoria_meno			NOT NULL
);

CREATE SEQUENCE software_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE software
(
	id_software		INTEGER			CONSTRAINT PK_software_id				PRIMARY KEY,
	typ_licencie	VARCHAR(20)		CONSTRAINT NN_software_typ_licencie		NOT NULL,
	konfiguracia	VARCHAR(100)	CONSTRAINT NN_software_konfiguracia		NOT NULL
);

CREATE SEQUENCE hardware_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE hardware
(
	id_hardware		INTEGER			CONSTRAINT PK_hardware_id				PRIMARY KEY,
	hmotnost		NUMBER(10,2)	CONSTRAINT NN_hardware_hmotnost			NOT NULL,
	rozmery_vyska	INTEGER			CONSTRAINT NN_hardware_rozmery_vyska	NOT NULL,
	rozmery_sirka	INTEGER			CONSTRAINT NN_hardware_rozmery_sirka	NOT NULL,
	rozmery_hlbka	INTEGER			CONSTRAINT NN_hardware_rozmery_hlbka	NOT NULL
);

CREATE SEQUENCE tovar_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE tovar
(
	id_tovar		INTEGER			CONSTRAINT PK_tovar_id					PRIMARY KEY,	
	kategoria_id	INTEGER			CONSTRAINT FK_tovar_kategoria_id		REFERENCES kategoria(id_kategoria),
	hardware_id		INTEGER			CONSTRAINT FK_tovar_hardware_id			REFERENCES hardware(id_hardware),
	software_id		INTEGER			CONSTRAINT FK_tovar_software_id			REFERENCES software(id_software),
	nazov			VARCHAR(40)		CONSTRAINT NN_tovar_nazov				NOT NULL,
	popis			VARCHAR(100)	CONSTRAINT NN_tovar_popis				NOT NULL,
	znacka			VARCHAR(40)		CONSTRAINT NN_tovar_znacka				NOT NULL,
	cena			NUMBER(10,2)	CONSTRAINT NN_tovar_cena				NOT NULL,
	dph				NUMBER(10,2)	CONSTRAINT NN_tovar_dph					NOT NULL,
	skladom			CHAR(1)			CONSTRAINT NN_tovar_skladom				CHECK(skladom in('A', 'N')),
	zlava			INTEGER			CONSTRAINT NN_tovar_zlava				NOT NULL
);

CREATE SEQUENCE poloskovy_kosik_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE poloskovy_kosik
(
	id_poloskovy_kosik	INTEGER		CONSTRAINT PK_poloskovy_kosik_id		PRIMARY KEY,		
	kosik_id			INTEGER		CONSTRAINT FK_poloskovy_kosik_kosik_id	REFERENCES kosik(id_kosik),
	tovar_id			INTEGER		CONSTRAINT FK_poloskovy_kosik_tovar_id	REFERENCES tovar(id_tovar),
	mnozstvo			INTEGER		CONSTRAINT NN_poloskovy_kosik_mnozstvo	NOT NULL
);

CREATE SEQUENCE hodnotenie_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE hodnotenie
(
	id_hodnotenie	INTEGER			CONSTRAINT PK_hodnotenie_id				PRIMARY KEY,
	uzivatel_id		INTEGER			CONSTRAINT FK_hodnotenie_uzivatel_id	REFERENCES uzivatel(id_uzivatel),
	tovar_id		INTEGER			CONSTRAINT FK_hodnotenie_tovar_id		REFERENCES tovar(id_tovar),
	znamka			INTEGER			CONSTRAINT NN_hodnotenie_znamka			CHECK (znamka BETWEEN 1 AND 5)
);

INSERT INTO osoba (id_osoba, meno, prezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'Viktor', 'Toth', 'Kolejni', '2', 'Brno', '61200', '0911969969', 'tothviktor11@gmail.com');
INSERT INTO osoba (id_osoba, meno, prezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'Vladislav', 'Nimizhman', 'Tererova', '25', 'Brno', '61300', '0911888963', 'vlad.nimik@gmail.com');
INSERT INTO osoba (id_osoba, meno, prezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'Martin', 'Pavelka', 'Kolejni', '2', 'Brno', '61200', '0915123456', 'martinpavelka@icloud.com');
INSERT INTO osoba (id_osoba, meno, prezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'Jakub', 'Kelecseni', 'Nitrianska', '15', 'Nove Zamky', '94115', '0920546132', 'kelecsenijak@icloud.com');
INSERT INTO osoba (id_osoba, meno, prezvisko, ulica, cislo_ulice, mesto, PSC, telefon, email) VALUES (osoba_seq.NEXTVAL, 'David', 'Toth', 'Nitrianska', '20', 'Nitra', '94000', '0920116132', 'tothvdavid@icloud.com');

INSERT INTO opravnenia(id_opravnenia, meno) VALUES (opravnenia_seq.NEXTVAL, 'zakaznik');
INSERT INTO opravnenia(id_opravnenia, meno) VALUES (opravnenia_seq.NEXTVAL, 'manazer');
INSERT INTO opravnenia(id_opravnenia, meno) VALUES (opravnenia_seq.NEXTVAL, 'admin');
INSERT INTO opravnenia(id_opravnenia, meno) VALUES (opravnenia_seq.NEXTVAL, 'technik');

INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL, opravnenia_seq.CURRVAL, 'xtothv01', 'heslo1');
INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL-1, opravnenia_seq.CURRVAL, 'xnimih00', 'heslo2');
INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL-2, opravnenia_seq.CURRVAL-1, 'xpavel02', 'heslo3');
INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL-3, opravnenia_seq.CURRVAL-2, 'xkelec01', 'heslo4');
INSERT INTO uzivatel (id_uzivatel, osoba_id, opravnenia_id, login, heslo) VALUES (uzivatel_seq.NEXTVAL, osoba_seq.CURRVAL-4, opravnenia_seq.CURRVAL-3, 'xtothd00', 'heslo5');
	
INSERT INTO kategoria (id_kategoria, meno) VALUES (kategoria_seq.NEXTVAL, 'notebooky a PC');
INSERT INTO kategoria (id_kategoria, meno) VALUES (kategoria_seq.NEXTVAL, 'mobilne telefony');
INSERT INTO kategoria (id_kategoria, meno) VALUES (kategoria_seq.NEXTVAL, 'televizoty');
INSERT INTO kategoria (id_kategoria, meno) VALUES (kategoria_seq.NEXTVAL, 'komponenty');

INSERT INTO software (id_software, typ_licencie, konfiguracia) VALUES (software_seq.NEXTVAL, 'Antivirus', 'ESET 10.1');
INSERT INTO software (id_software, typ_licencie, konfiguracia) VALUES (software_seq.NEXTVAL, 'Operacny System', 'Windows 10');
INSERT INTO software (id_software, typ_licencie, konfiguracia) VALUES (software_seq.NEXTVAL, 'Operacny System', 'RedHat');
INSERT INTO software (id_software, typ_licencie, konfiguracia) VALUES (software_seq.NEXTVAL, 'Hra', 'Windows 10');

INSERT INTO hardware (id_hardware, hmotnost, rozmery_vyska, rozmery_sirka, rozmery_hlbka) VALUES (hardware_seq.NEXTVAL, '5,25', 10, 15, 15);
INSERT INTO hardware (id_hardware, hmotnost, rozmery_vyska, rozmery_sirka, rozmery_hlbka) VALUES (hardware_seq.NEXTVAL, '1,55', 20, 15, 10);
INSERT INTO hardware (id_hardware, hmotnost, rozmery_vyska, rozmery_sirka, rozmery_hlbka) VALUES (hardware_seq.NEXTVAL, '2,00', 50, 50, 50);
INSERT INTO hardware (id_hardware, hmotnost, rozmery_vyska, rozmery_sirka, rozmery_hlbka) VALUES (hardware_seq.NEXTVAL, '3,88', 30, 25, 30);
	
INSERT INTO tovar (id_tovar, kategoria_id, hardware_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL-1, hardware_seq.CURRVAL-1, 'BL2411', 'super LCD moniotor', 'BENQ', '15000,99', '20', 'A', '0');
INSERT INTO tovar (id_tovar, kategoria_id, hardware_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL-3, hardware_seq.CURRVAL, 'Thinkpad Edge E530', 'vykonny notebook', 'Lenovo', '45999,99', '20', 'N', '0');
INSERT INTO tovar (id_tovar, kategoria_id, hardware_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL, hardware_seq.CURRVAL-2, 'GT500', 'graficka karta', 'Nvidea', '2000,10', '25', 'A', '20');
INSERT INTO tovar (id_tovar, kategoria_id, hardware_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL, hardware_seq.CURRVAL-3, 'E-Blue', 'bezdrotova mys', 'Fresco', '300,01', '19', 'A', '50');
INSERT INTO tovar (id_tovar, kategoria_id, software_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL-3, software_seq.CURRVAL, 'Total War Rome', 'strategia', 'EAgames', '100', '40', 'A', '40');
INSERT INTO tovar (id_tovar, kategoria_id, software_id, nazov, popis, znacka, cena, dph, skladom, zlava) VALUES (tovar_seq.NEXTVAL, kategoria_seq.CURRVAL-3, software_seq.CURRVAL-3, 'NOD 10.1', 'antivirus', 'ESET', '899', '10', 'N', '20');

INSERT INTO kosik (id_kosik, uzivatel_id) VALUES (kosik_seq.NEXTVAL, uzivatel_seq.CURRVAL-4);
INSERT INTO kosik (id_kosik, uzivatel_id) VALUES (kosik_seq.NEXTVAL, uzivatel_seq.CURRVAL-3);
INSERT INTO kosik (id_kosik, uzivatel_id) VALUES (kosik_seq.NEXTVAL, uzivatel_seq.CURRVAL-2);

INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL, tovar_seq.CURRVAL-2, 3);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL, tovar_seq.CURRVAL-4, 3);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL, tovar_seq.CURRVAL-3, 3);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL-1, tovar_seq.CURRVAL-3, 1);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL-2, tovar_seq.CURRVAL-4, 1);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL-2, tovar_seq.CURRVAL, 1);
INSERT INTO poloskovy_kosik (id_poloskovy_kosik, kosik_id, tovar_id, mnozstvo) VALUES (poloskovy_kosik_seq.NEXTVAL, kosik_seq.CURRVAL-2, tovar_seq.CURRVAL-1, 1);

INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-2, tovar_seq.CURRVAL-2, 5);
INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-2, tovar_seq.CURRVAL-4, 5);
INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-3, tovar_seq.CURRVAL-3, 3);
INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-4, tovar_seq.CURRVAL-1, 3);
INSERT INTO hodnotenie (id_hodnotenie, uzivatel_id, tovar_id, znamka) VALUES (hodnotenie_seq.NEXTVAL, uzivatel_seq.CURRVAL-4, tovar_seq.CURRVAL-4, 3);

INSERT INTO objednavka (id_objednavka, uzivatel_id, vybavil_id, kosik_id, cena, zaplatene, expandovane) VALUES (objednavka_seq.NEXTVAL, uzivatel_seq.CURRVAL-4, uzivatel_seq.CURRVAL-3, kosik_seq.CURRVAL, '1000', 'A', 'N');
INSERT INTO objednavka (id_objednavka, uzivatel_id, vybavil_id, kosik_id, cena, zaplatene, expandovane) VALUES (objednavka_seq.NEXTVAL, uzivatel_seq.CURRVAL-3, uzivatel_seq.CURRVAL-3, kosik_seq.CURRVAL, '1500', 'N', 'N');
INSERT INTO objednavka (id_objednavka, uzivatel_id, vybavil_id, kosik_id, cena, zaplatene, expandovane) VALUES (objednavka_seq.NEXTVAL, uzivatel_seq.CURRVAL-2, uzivatel_seq.CURRVAL-3, kosik_seq.CURRVAL, '3000', 'A', 'A');


















