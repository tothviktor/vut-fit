--Meno	: Viktor T�th
--Trieda	: 3BIT
--Login	: xtothv01

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package mat_pack is
	
	type mask_t is
		record
			top        		: std_logic;
			left        	: std_logic;
			right        	: std_logic;
			bottom        	: std_logic;
		end record;
	
	function getmask(x,y : natural; COLUMNS, ROWS : natural) return mask_t;

end mat_pack;

package body mat_pack is

	function getmask(x,y : natural; COLUMNS, ROWS : natural) return mask_t is	

	variable mapa : mask_t;
	
	begin
		if (COLUMNS > 0 and COLUMNS-1 >= x and x >= 0 and ROWS > 0 and ROWS-1 >= y and y >= 0 ) then
			if (COLUMNS-1 = 0) then
				mapa.left :='0';
				mapa.right :='0';
			elsif (x = 0) then
				mapa.left := '0';
				mapa.right := '1';
			elsif (x = COLUMNS-1) then
				mapa.left := '1';
				mapa.right := '0';
			else
				mapa.left := '1';
				mapa.right := '1';
			end if;
			
			if (ROWS-1 = 0) then
				mapa.top :='0';
				mapa.bottom :='0';
			elsif (y = 0) then
				mapa.top := '0';
				mapa.bottom := '1';
			elsif (y = ROWS-1) then
				mapa.top := '1';
				mapa.bottom := '0';
			else
				mapa.top := '1';
				mapa.bottom := '1';
			end if;
		else
			mapa.top := '0';
			mapa.left :='0';	
			mapa.right :='0';
			mapa.bottom := '0';
		end if;	
		
		return mapa;
	end getmask;
 
end mat_pack;
