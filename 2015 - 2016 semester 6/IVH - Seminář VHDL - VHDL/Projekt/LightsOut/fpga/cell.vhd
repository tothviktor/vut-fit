--Meno	: Viktor Tóth
--Trieda	: 3BIT
--Login	: xtothv01

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.mat_pack.ALL;

entity cell is
    generic ( 
		MASK 		: mask_t := (others => '1')
	);
	
    Port(
		INVERT_REQ_IN     : in   STD_LOGIC_VECTOR (3 downto 0);
		INVERT_REQ_OUT    : out  STD_LOGIC_VECTOR (3 downto 0);
		KEYS              : in   STD_LOGIC_VECTOR (4 downto 0);
		SELECT_REQ_IN     : in   STD_LOGIC_VECTOR (3 downto 0);
		SELECT_REQ_OUT    : out  STD_LOGIC_VECTOR (3 downto 0);
		INIT_ACTIVE       : in   STD_LOGIC;
		ACTIVE            : out  STD_LOGIC;
		INIT_SELECTED     : in   STD_LOGIC;
		SELECTED          : out  STD_LOGIC;
		CLK               : in   STD_LOGIC;
		RESET             : in   STD_LOGIC
    );
end cell;

architecture Behavioral of cell is

	constant IDX_TOP    	: NATURAL := 0; 
	constant IDX_LEFT   	: NATURAL := 1;
	constant IDX_RIGHT  	: NATURAL := 2; 
	constant IDX_BOTTOM 	: NATURAL := 3; 
	constant IDX_ENTER  	: NATURAL := 4; 
	signal cell_cell_ON 	: STD_LOGIC;
	signal cell_cursor  	: STD_LOGIC;

begin
	ACTIVE <= cell_cursor;
	SELECTED <= cell_cell_ON;

	main : process(CLK, RESET)
	begin
		if (RESET = '1') then
			cell_cursor   <= INIT_ACTIVE;
			cell_cell_ON <= INIT_SELECTED;
			SELECT_REQ_OUT <= (others => '0');
			INVERT_REQ_OUT <= (others => '0');

		elsif rising_edge(CLK) then 		
			INVERT_REQ_OUT <= (others => '0');
			SELECT_REQ_OUT <= (others => '0');

			if (cell_cell_ON = '0') then
				if ((INVERT_REQ_IN and (MASK.BOTTOM & MASK.RIGHT & MASK.LEFT & MASK.TOP)) /= "0000" ) then
					cell_cursor <= not cell_cursor;	
				end if;
				if ((SELECT_REQ_IN and (MASK.BOTTOM & MASK.RIGHT & MASK.LEFT & MASK.TOP)) /= "0000" ) then
					cell_cell_ON <= not cell_cell_ON;
				end if;	
			else
				if (KEYS(IDX_ENTER) = '1') then 
					cell_cursor <= not cell_cursor;
					INVERT_REQ_OUT <= (IDX_TOP => MASK.TOP, IDX_LEFT => MASK.LEFT, IDX_RIGHT  => MASK.RIGHT, IDX_BOTTOM => MASK.BOTTOM);
				elsif (KEYS(IDX_TOP) = '1' and MASK.TOP = '1') then
					SELECT_REQ_OUT <= (IDX_TOP => '1', others => '0');
					cell_cell_ON <= '0';
				elsif (KEYS(IDX_LEFT) = '1' and MASK.LEFT = '1') then
					SELECT_REQ_OUT <= (IDX_LEFT => '1', others => '0');
					cell_cell_ON <= '0'; 
				elsif (KEYS(IDX_RIGHT) = '1' and MASK.RIGHT = '1') then
					SELECT_REQ_OUT <= (IDX_RIGHT => '1', others => '0');
					cell_cell_ON <= '0';
				elsif (KEYS(IDX_BOTTOM) = '1' and MASK.BOTTOM = '1') then
					SELECT_REQ_OUT <= (IDX_BOTTOM => '1', others => '0');
					cell_cell_ON <= '0';			
				end if;
			end if;
		end if;	
	end process;
end Behavioral;