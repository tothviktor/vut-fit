#! /usr/bin/python3
# -*- coding: utf-8 -*-
#XQR:xtothv01

import sys
import os
import xml.etree.ElementTree as etree 
#import re
#import string
#import getopt
#import argparse
#import parser
#import xml.etree.ElementTree as ET

SELECT_childs = []
FROM_child = []
"""
 * attributeName 		- vrati identifikator (Meno) attributu       priklad: "id"="bk104"   ->     id
 * @param attribute 	- attribut (uz formatovany)
"""
def attributeName(attribute):
	attribute = attribute.split('=')[0]
	return attribute

"""
 * attributeText 		- vrati text z attributu       priklad: "id"="bk104"   ->     bk104
 * @param attribute 	- attribut (uz formatovany)
"""	
def attributeText(attribute):
	attribute = attribute.split('=')[1][1:]
	return attribute

"""
 * attributeAll 		- zformatuje atribut na pouzitelny string a odreze nadbytocne znaky      	priklad: 'id': 'bk104'   ->     "id"="bk104"
 * @param orgAttribute 	- attribut ( NEformatovany )
"""		
def attributeAll(orgAttribute):
	attribute = orgAttribute
	attribute = str(attribute)
	attribute = attribute[1:]
	attribute = attribute[:-1]
	attribute = attribute.replace('\'','',1)
	attribute = attribute.replace(': \'','=\"',1)
	attribute = attribute.replace('\'','\"',1)
	return attribute

"""
 * attributeSplit 		- zformatuje atribut na pouzitelny string a odreze nadbytocne znaky
 * @param attribute 	- attribut ( NEformatovany )
 * @param list 			- pole na vratenie uz sformatovanych atributov
 * @param case 			- moznost formatovania
"""		
def attributeSplit(attribute, list, case):
	if attribute != None:
		NewList = str(attribute)
		NewList = NewList[1:]
		NewList = NewList[:-1]
		NewList = NewList.split(', ')
		for castPole in NewList:
			if case == 1:
				list.append(attributeName(attributeAll(castPole)))   #priklad: 'id': 'bk104'   ->     id
			elif case == 2:
				list.append(attributeAll(castPole))				     #priklad: 'id': 'bk104'   ->     id="bk104"
			elif case == 3:
				list.append(attributeText(attributeAll(castPole)))	 #priklad: 'id': 'bk104'   ->     bk104

"""
 * attributeJoin 		- spoji jedneho alebo viac attributov do jedneho retazca
 * @param orgAttribute 	- attribut ( uz formatovany )
"""	
def attributeJoin(attribute):
	list = []
	result = ""
	attributeSplit(attribute,list,2)
	for attr in list:
		if attr != "":
			result = result + " " + attr+"\""
	return result

"""
 * recursiveScanningSpecifically 		- rekurzivne prehladavanie XML stromu, pri najdeny element prida do pola SELECT_childs
 * @param root 							- rootovsky element
 * @param SELECT_element 				- hladany element
"""	
def recursiveScanningSpecifically(root,SELECT_element):
	elements = []
	#kontrola rootovskeho elementu
	if root.tag == SELECT_element:
		SELECT_childs.append(root)
	
	for newChild in root:						#zanorenie o uroven nissie
		elements.append(newChild)
	for element in elements:
		if element.tag == SELECT_element:
			SELECT_childs.append(element)
		else:
			recursiveScanningSpecifically(element,SELECT_element)

"""
 * recursiveScanningSpecificallyMore 	- rekurzivne prehladavanie XML stromu, pri najdenom elemente alebo attributu vrati element pomocou FROM_child
 * @param root 							- rootovsky element
 * @param FROM_element 					- hladany element
 * @param FROM_attribute 				- hladany attribute
"""				
def recursiveScanningSpecificallyMore(root,FROM_element,FROM_attribute):
	elements = []
	
	#kontrola rootovskeho elementu
	if root.attrib != None:
		list=[]
		attributeSplit(root.attrib,list,1)
	
	if FROM_element != None and FROM_attribute != None:
		for attributeOK in list:
			if root.tag == FROM_element and attributeOK == FROM_attribute:
				FROM_child.append(root)
				return root
			
	elif FROM_element != None:
		if root.tag == FROM_element:
			FROM_child.append(root)
			return root
	
	elif FROM_attribute != None:
		for attributeOK in list:
			if attributeOK == FROM_attribute:
				FROM_child.append(root)
				return root
	
	for newChild in root:									#zanorenie o uroven nissie
		elements.append(newChild)
	
	# kontrola vnorenych elementov
	for element in elements:
		if element.attrib != None:
			list=[]
			attributeSplit(element.attrib,list,1)
		
		if FROM_element != None and FROM_attribute != None:
			for attributeOK in list:
				if element.tag == FROM_element and attributeOK == FROM_attribute:
					FROM_child.append(element)
					return element
					
			recursiveScanningSpecificallyMore(element,FROM_element,FROM_attribute)
				
		elif FROM_element != None:
			if element.tag == FROM_element:
				FROM_child.append(element)
				return element
			else:
				recursiveScanningSpecificallyMore(element,FROM_element,FROM_attribute)
		
		elif FROM_attribute != None:
			for attributeOK in list:
				if attributeOK == FROM_attribute:
					FROM_child.append(element)
					return element
			recursiveScanningSpecificallyMore(element,FROM_element,FROM_attribute)

"""
 * Scanning_NOT_Specifically 		- ziskanie elementov stromu z dalsej urovni stromu
 * @param root 						- rootovsky element
 * @param child 					- pole na vratenie najdenych podelementov
"""	
def Scanning_NOT_Specifically(root,child):
	for newChild in root:
		child.append(newChild)

"""
 * JoinTextWithQuotationMarksInList 	- spajanie retazcov v poli podla uvodzoviek
 * @param list 							- vstupne pole
"""	
def JoinTextWithQuotationMarksInList(list):		# napr. [ Ja, "Viktor, Toth" , kodim ] -> [ Ja, "Viktor Toth" , kodim ]
	i = 0
	while i <= len(list)-1:
		if list[i][:1] == "\"" and list[i][(len(list[i])-1):] != "\"":
			k = i+1
			while k <= len(list)-1:
				if list[k][(len(list[k])-1):] == "\"":
					list[i:k+1] = [" ".join(list[i:k+1])]	
				k+=1
		i+=1

"""
 * RepresentsFloat 		- kontrola retazca na float
 * @param s 			- retazec
"""	
def RepresentsFloat(s):
    try: 
        float(s)
        return True
    except ValueError:
        return False

"""
 * RepresentsInt 		- kontrola retazca na int
 * @param s 			- retazec
"""	
def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

"""
 * readFile 			- Nacitanie suboru
 * @param file 			- cesta k suboru
 * @param errorCode 	- cislo chyby pri neuspechu otvarani suboru
"""	
def readFile(file,errorCode):
	if os.path.isfile(file):
		with open (file, "r") as myfile:
			data= myfile.readlines()
			data= ''.join(data)
			return data
	else:
		sys.stderr.write("Zadany file neexistuje!\n")
		sys.exit(errorCode)

"""
 * reservedWords 			- kontrola a porovnanie retazca s rezervovanymi slovami
 * @param word 				- kontorlovany retazec
 * @param literal 			- False/True -> True  - retazec moze byt cislo
										 -> False - retazec NEmoze byt cislo 
"""
def reservedWords(word,literal):
	if word == "SELECT" or word == "LIMIT" or word == "FROM" or word == "WHERE" or word == "ORDER" or word == "BY" or word == "ROOT" or word == "NOT" or word == "CONTAINS" or word == "ASC" or word == "DESC":
		sys.stderr.write("Chyba v XML dotazu!\n")
		sys.exit(80)
	if literal == False and RepresentsFloat(word):
		sys.stderr.write("Chyba v XML dotazu!\n")
		sys.exit(80)
"""
 * help 			- pomoc
"""		
def help():
	print("\n--help vypise mozne parametry")
	print("--input=filename zadany vstupni textovy soubor se zakladnim ASCII kodovanim")
	print("--output=filename textovy vystupni soubor tez v ASCII kodovani")
	print("--query='dotaz' zadany dotaz v dotazovacim jazyce definovaném nize (v pripade zadanitimto zpusobem nebude dotaz obsahovat symbol apostrof)")
	print("--qf=filename dotaz v dotazovacim jazyce definovanem nize zadany v externim textovem souboru (nelze kombinovat s --query)")
	print("-n negenerovat XML hlavicku na vystup skriptu")
	print("--root=element jmeno paroveho korenoveho elementu obalujicí vysledky. Pokud nebudezadan, tak se vysledky neobaluji korenovym elementem, ac to porusuje validitu XML.")
	
# ++++++++++++++++++ ----------------- HLAVNY PROGRAM ----------------- ++++++++++++++++++

argc = len(sys.argv[1:])
withoutErrors = True		  #Riadiaca premenna: true == parametre sa nacitali spravne
OK_Parameters = [0,0,0,0,0,0] # 3 stavy jednotlivych prvkov v poli: 0 - nezadany parameter			== spravne
																# 1 - raz zadany parameter			== spravne
																# 2 - viac ako raz zadany parameter	== NEspravne
																
#  			------------------- NACITANIE A KONTROLA PARAMETROV ------------------
if argc == 0:										# Ziadny parameter				== nespravny vstup !
	withoutErrors = False
	
elif argc == 1 and sys.argv[1] == "--help":			# Iba parameter --help 		== spravny vstup ! + skoncim
	help()
	sys.exit(0)
	
elif argc != 1 and sys.argv[1] == "--help":			# Parameter --help a dalsie argumenty 	== nespravny vstup ! 
	sys.stderr.write("parameter --help sa nemoze kombinovat !\n")
	sys.exit(1)

#Ostatne parametre : 
#   --input=param_input
#	--output=param_output
#	--query=param_query
#	--qf=param_qf
#	-n=param_n
#	--root=param_root

elif argc >= 1:

	i=1
	param_input = None
	param_output = None
	param_query = None
	param_qf = None
	param_n  = None
	param_root = None
		
	while i<=argc and withoutErrors == True:
		param = sys.argv[i]
		
		if param[:7] == "--input":								#vstup
			if param[:8] == "--input=" and len(param) > 8:
				param_input = param[8:]
			else:
				withoutErrors = False
			OK_Parameters[0] = (OK_Parameters[0])+1
					
		elif param[:8] == "--output":							#vystup
			if param[:9] == "--output=" and len(param) > 9:
				param_output = param[9:]
			else:
				withoutErrors = False
			OK_Parameters[1] = (OK_Parameters[1])+1
					
		elif param[:7] == "--query":							#dotaz
			if param[:8] == "--query=" and len(param) > 8:
				param_query = param[8:]
			else:
				withoutErrors = False
			OK_Parameters[2] = (OK_Parameters[2])+1
		
		elif param[:4] == "--qf":								#dotaz zo SUBORU
			if param[:5] == "--qf=" and len(param) > 5:
				param_qf = param[5:]
			else:
				withoutErrors = False
			OK_Parameters[3] = (OK_Parameters[3])+1
		
		elif param[:2] == "-n":									#bez XML hlavicky
			OK_Parameters[4] = (OK_Parameters[4])+1
			
		elif param[:6] == "--root":								#korenovy element obalujuci vysledok
			if param[:7] == "--root=" and len(param) > 7:
				param_root = param[7:]
			else:
				withoutErrors = False
			OK_Parameters[5] = (OK_Parameters[5])+1	
		else:													#vsetko ostatne == chyba
			withoutErrors = False
		
		i+=1
	
	if withoutErrors:
		i = 0
		while i < 6:								#viacnasobny vyskit parametrov -> CHYBA
			if OK_Parameters[i] > 1:
				withoutErrors = False
			i+=1
		if OK_Parameters[2] == 1 and OK_Parameters[3] == 1:			# kombinuje sa query | qf -> CHYBA
			withoutErrors = False
		
# -------------------SPRACOVANIE PARAMETROV ------------------			
if withoutErrors:
	if OK_Parameters[0]:												# --input  ( vstup )
		inputFile = readFile(param_input,2)
	else:
		sys.stderr.write("Chyba parameter --input\n")
		sys.exit(2)
	
	if OK_Parameters[3]:												#--qf  ( dotaz zo SUBORU )
		XML_Query = readFile(param_qf,80)
	elif OK_Parameters[2]:												#--query  ( dotaz z TERMINALU )
		XML_Query = param_query
	else:
		XML_Query = ""
	# ------------------------------ FORMATOVANIE DOTAZU -----------------------------
	# spocitanie zatvoriek
	left_parenthesis = XML_Query.count('(')
	right_parenthesis = XML_Query.count(')')
	
	# odstranenie zatvoriek
	if left_parenthesis != right_parenthesis:
		sys.stderr.write("Chyba v XML dotazu!\n")
		sys.exit(80)
	else:										# TODO pri ROZSIRENIACH
		XML_Query = XML_Query.replace('(','')
		XML_Query = XML_Query.replace(')','')
	
	#pridanie medzeri pred a po operatorov kvoli jednoduchsiemu spracovania dotazu
	XML_Query = XML_Query.replace('=',' = ')
	XML_Query = XML_Query.replace('>',' > ')
	XML_Query = XML_Query.replace('<',' < ')
	
	splited_XML_Query=XML_Query.split()						# XML dotaz rozsekam do pole retazcov, podla medzier
	JoinTextWithQuotationMarksInList(splited_XML_Query)		
	splited_XML_Query_Size = len(splited_XML_Query)-1 		# zacinam od 0 !!!
	
	# ------------------------------ SPRACOVANIE DOTAZU -----------------------------
	# +++++++++++++++++++++++++++++++++ SELECT +++++++++++++++++++++++++++++++++
	if splited_XML_Query_Size >= 2 and splited_XML_Query[0] == "SELECT":
		FROM_flag = False
		WHERE_flag = False
		FROM_attribute = None
		FROM_element = None
		# +++++++++++++++++++++++++++++++++ FROM +++++++++++++++++++++++++++++++++
		if splited_XML_Query[2] == "FROM" and (splited_XML_Query[1].rfind('<') == -1 or splited_XML_Query[1].rfind('>') == -1):
			SELECT_element = splited_XML_Query[1]
			reservedWords(SELECT_element,False)
			limit = None
			i=0  # riadiaca premenna
			if splited_XML_Query_Size == 2:
				FROM_flag = True
			
		# ---------------------------- LIMIT + FROM ---------------------------------
		elif splited_XML_Query[2] == "LIMIT" and RepresentsInt(splited_XML_Query[3]) and splited_XML_Query[4] == "FROM" and splited_XML_Query_Size >= 5 and (splited_XML_Query[1].rfind('<') == -1 or splited_XML_Query[1].rfind('>') == -1):
			SELECT_element = splited_XML_Query[1]
			if RepresentsInt(splited_XML_Query[3]) == True:
				limit=int(splited_XML_Query[3])
				i=2 # riadiaca premenna
				if splited_XML_Query_Size == 4:		# dotaz FROM bez elementu/attributu
					FROM_flag = True
			else:
				sys.stderr.write("Chyba v XML dotazu!\n")
				sys.exit(80)
		else:
			sys.stderr.write("Chyba v XML dotazu!\n")
			sys.exit(80)
		# +++++++++++++++++++++++++++++++++ FROM element/attribut +++++++++++++++++++++++++++++++++++++++++++++
		if FROM_flag == False:
			# --------------------- ROOT ------------------------
			if splited_XML_Query[3+i] == "ROOT":
				FROM_element = "ROOT"
			# --------------------- chyba ------------------------
			elif splited_XML_Query[3+i] == "ORDER" or splited_XML_Query[3+i] == "WHERE":
				sys.stderr.write("Chyba v XML dotazu!\n")
				sys.exit(80)
			# --------------------- element/attribut --------------------------------------
			else:
				FROM_attribute = None
				if splited_XML_Query[3+i].rfind('<') == -1 or splited_XML_Query[3+i].rfind('>') == -1 or splited_XML_Query[3+i].rfind("ROOT"):
					
					index = splited_XML_Query[3+i].rfind('.') 								# podla znaku "." zistim ci mam attribut
					
					if index == -1:															# iba element
						FROM_element = splited_XML_Query[3+i]
						reservedWords(FROM_element,False)
					elif index == 0 and splited_XML_Query[3+i][1:].rfind('.') == -1:		# element aj attribut
						FROM_element = None
						FROM_attribute = splited_XML_Query[3+i][1:]
						reservedWords(FROM_attribute,False)
					elif index != "0" and splited_XML_Query[3+i][:index].rfind('.') == -1:	# iba attribut
						FROM_element = splited_XML_Query[3+i][:index]
						reservedWords(FROM_element,False)
						FROM_attribute = splited_XML_Query[3+i][index+1:]
						reservedWords(FROM_attribute,False)
					else:																	# chyba 
						sys.stderr.write("Chyba v XML dotazu!\n")
						sys.exit(80)
				else:
					
					sys.stderr.write("Chyba v XML dotazu!\n")
					sys.exit(80)

			# +++++++++++++++++++++++++++++++++ WHERE +++++++++++++++++++++++++++++++++++++++++++++	
			if splited_XML_Query_Size >= 4+i:
				if splited_XML_Query[4+i] == "WHERE" and splited_XML_Query_Size >= 7+i:
					
					#priznaky
					WHERE_flag = True
					
					#polia
					WHERE_not_array = []
					WHERE_element_array = []
					WHERE_attribute_array = []
					WHERE_relation_operator_array = []
					WHERE_literal_array = []
					WHERE_operator_array = []				
					
					# cyklus kvoli vianasobnemu dotazu WHERE ( vyuzite ale NEimplementovane vo funkcnej casti )
					while 5+i+3 <= splited_XML_Query_Size+1:
						
						#Premenne
						WHERE_not_flag = False
						WHERE_not = None
						WHERE_element = None
						WHERE_attribute = None
						WHERE_relation_operator = None
						WHERE_literal = None
						WHERE_operator = None
						
						if splited_XML_Query[5+i] == "ORDER":
							break
						# --------------------- NOT ------------------------
						if splited_XML_Query[5+i] == "NOT":
							try:
								while splited_XML_Query[5+i] == "NOT":			# viacnasobny dotaz NOT
									if WHERE_not_flag == False:
										WHERE_not = True
									else:
										WHERE_not = False
									i+=1
							except:
								sys.stderr.write("Chyba v XML dotazu!\n")
								sys.exit(80)
						else:
							WHERE_not = False
						WHERE_not_array.append(WHERE_not)	
								 
						if 5+i+3 <= splited_XML_Query_Size+1:
						
							#----------------- element / attribut -------------------------------------------------------
							if splited_XML_Query[5+i].rfind('<') == -1 or splited_XML_Query[5+i].rfind('>') == -1:
								
								index = splited_XML_Query[5+i].rfind('.')									# podla znaku "." zistim ci mam attribut
								
								if index == -1:                                                             # iba element
									WHERE_element = splited_XML_Query[5+i]                                  
									reservedWords(WHERE_element,False)                                      
								elif index == 0 and splited_XML_Query[5+i][1:].rfind('.') == -1:            # element aj attribut
									WHERE_element = None                                                    
									WHERE_attribute = splited_XML_Query[5+i][1:]                            
									reservedWords(WHERE_attribute,False)                                    
								elif index != "0" and splited_XML_Query[5+i][:index].rfind('.') == -1:      # iba attribut
									WHERE_element = splited_XML_Query[5+i][:index]                          
									reservedWords(WHERE_element,False)                                      
									WHERE_attribute = splited_XML_Query[5+i][index+1:]                      
									reservedWords(WHERE_attribute,False)                                    
								else:                                                                       # chyba 
									sys.stderr.write("Chyba v XML dotazu!\n")                               
									sys.exit(80)
							else:
								sys.stderr.write("Chyba v XML dotazu!\n")
								sys.exit(80)
							i+=1
							
							# ------------------------- RELACNY OPERATOR -----------------------------------------------
							if splited_XML_Query[5+i] == "CONTAINS":				# CONTAINS
								WHERE_relation_operator = "CONTAINS"
							elif splited_XML_Query[5+i] == "=":						# =
								WHERE_relation_operator = "="
							elif splited_XML_Query[5+i] == ">":						# >
								WHERE_relation_operator = ">"
							elif splited_XML_Query[5+i] == "<":						# <
								WHERE_relation_operator = "<"
							else:
								sys.stderr.write("Chyba v XML dotazu!\n")
								sys.exit(80)
							i+=1
							
							# ------------------------------- LITERAL -----------------------------------------------
							# nacitanie literalu ak relacny operator je CONTAINS
							if WHERE_relation_operator == "CONTAINS":									
								if splited_XML_Query[5+i][:1] == "\"" and splited_XML_Query[5+i][-1:] == "\"":
									WHERE_literal = splited_XML_Query[5+i][1:]
									WHERE_literal = WHERE_literal[:-1]
									reservedWords(WHERE_literal,True)
								else:
									sys.stderr.write("Chyba v XML dotazu!\n")
									sys.exit(80)
							
							# nacitanie literalu ak relacny operator je > alebo < alebo =
							elif WHERE_relation_operator == "=" or WHERE_relation_operator == ">" or WHERE_relation_operator == "<": 
								WHERE_literal = splited_XML_Query[5+i]
								reservedWords(WHERE_literal,True)
								if WHERE_literal[-1:] == "\"":					#retazec 
									WHERE_literal = WHERE_literal[:-1]
								elif RepresentsFloat(WHERE_literal) == False:	#cislo
									sys.stderr.write("Chyba v XML dotazu!\n")
									sys.exit(80)
							else:
								sys.stderr.write("Chyba v XML dotazu!\n")
								sys.exit(80)
							i+=1
							
							#priradenie premennych do polov
							WHERE_element_array.append(WHERE_element)
							WHERE_attribute_array.append(WHERE_attribute)
							WHERE_relation_operator_array.append(WHERE_relation_operator)
							WHERE_literal_array.append(WHERE_literal)
							
							# --------------------- AND - OR - ORDER BY ------------------------
							if splited_XML_Query_Size >= (5+i):					# dotaz s AND
								if splited_XML_Query[5+i] == "AND":
									WHERE_operator = "AND"
									if splited_XML_Query_Size < (8+i):
										
										sys.stderr.write("Chyba v XML dotazu!\n")
										sys.exit(80)
								elif splited_XML_Query[5+i] == "OR":			# dotaz s OR
									WHERE_operator = "OR"
									if splited_XML_Query_Size < (8+i):
										sys.stderr.write("Chyba v XML dotazu!\n")
										sys.exit(80)
								
								elif splited_XML_Query[5+i] == "ORDER":			# dotaz bez AND a OR a pokracujem dalej v kontrolovani ORDER BY
									WHERE_operator_array.append(WHERE_operator)
									break
								else:
									sys.stderr.write("Chyba v XML dotazu!\n")
									sys.exit(80)
								
								i+=1	

							WHERE_operator_array.append(WHERE_operator)
						else:
							sys.stderr.write("Chyba v XML dotazu!\n")
							sys.exit(80)

				if WHERE_flag:
					i+=1
				# ++++++++++++++++++++++++++++++++++++++++++++++ ORDER BY ++++++++++++++++++++++++++++++++++++++++++++++++++
				ORDER_BY_flag = False
				if splited_XML_Query_Size >= 4+i:
					# kontrola na presny syntax
					if splited_XML_Query[4+i] == "ORDER" and splited_XML_Query_Size == 7+i and splited_XML_Query[5+i] == "BY" and (splited_XML_Query[7+i] == "ASC" or splited_XML_Query[7+i] == "DESC"): 
						ORDER_BY_flag = True
						
						index = splited_XML_Query[6+i].rfind('.')								# podla znaku "." zistim ci mam attribut
						#	----------------- element / attribut -------------------------------------------------------                                                                        
						if index == -1:                                                         # iba element
							ORDER_BY_element = splited_XML_Query[6+i]                           
							reservedWords(ORDER_BY_element,False)                               
						elif index == 0 and splited_XML_Query[6+i][1:].rfind('.') == -1:        # element aj attribut
							ORDER_BY_attribute = splited_XML_Query[6+i]                         
							reservedWords(ORDER_BY_attribute,False)                             
						elif index != "0" and splited_XML_Query[6+i][:index].rfind('.') == -1:  # iba attribut
							ORDER_BY_element = splited_XML_Query[6+i][:index]                   
							reservedWords(ORDER_BY_element,False)                               
							ORDER_BY_attribute = splited_XML_Query[6+i][index:]                 
							reservedWords(ORDER_BY_attribute,False)                             
						else:                                                                   # chyba 
							sys.stderr.write("Chyba v XML dotazu!\n")                           
							sys.exit(80)
						#	--------------------- ASC / DESC --------------------------------------------------------
						if splited_XML_Query[7+i] == "ASC":
							ORDERING = "ASC"
						elif splited_XML_Query[7+i] == "DESC":
							ORDERING = "DESC"
						else:
							sys.stderr.write("Chyba v XML dotazu!\n")
							sys.exit(80)
					else:
						sys.stderr.write("Chyba v XML dotazu!\n")
						sys.exit(80)
	else:
		sys.stderr.write("Chyba v XML dotazu!\n")
		sys.exit(80)
	
	if OK_Parameters[4]:													#-n  ( bez XML hlavicky )
		AbsResult=""
	else:
		AbsResult="<?xml version=\"1.0\" encoding=\"utf-8\"?>"
	
	if OK_Parameters[5]:													#--root ( meno paroveho korenoveho elementu obalujuci vysledky )
		AbsResult = AbsResult + "<" + param_root + ">"
	
	
	if OK_Parameters[0]:													# --input 
		XML_etree = etree.parse(param_input)								# nacitanie suboru
	else:
		inputFile = sys.stdin.readline()
		inputFile = inputFile[:-1]
		XML_etree = etree.parse(param_input)
	
	root = XML_etree.getroot()												# nacitanie rootovskeho elementu
	result = ""

	# +++++++++++++++++++++++++++++++++++++++++++++++++ Filtorvanie XML suboru podla ziskaneho dotazu ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------
	if FROM_element != None or FROM_attribute != None:
		if FROM_element != "ROOT":
			recursiveScanningSpecificallyMore(root,FROM_element, FROM_attribute)
			if len(FROM_child) != 0:		# FROM element najdeny
				root = FROM_child[0]		# novy rootovy element bude PRAVE ten najdeny element v orginal hlavnom root elemente
			
			# ---- FROM element NEnajdeny -> program skonci ( prazdny vysledok ) --------
			else:							
				if OK_Parameters[5]:
					AbsResult = AbsResult + "</" + param_root + ">\n"
				else:
					AbsResult = AbsResult + "\n"
					
				output=AbsResult
				if OK_Parameters[1]:												
					try:
						outFile = open(param_output, "w")
					except:
						sys.stderr.write("Chyba pri otvarani vystupneho suboru !\n")
						sys.exit(3)
					
					outFile.write(output)
					outFile.close()
					sys.exit(0)
				else:
					sys.stdout.write(output)
					sys.exit(0)
			# -----------------------------------------------------------------------------		
		
		recursiveScanningSpecifically(root,SELECT_element) # rekurzivne prehladavanie root (novy) elementu podla elementu SELECT, vrati vsetky najdene elementy do globalneho pola SELECT_childs
		
		for SELECT_child in SELECT_childs[0:(limit)]:		#cyklicka kontrola elementov v poli ( 1. zanorenie )
				elements2=[]
				HeadElement2 = False
				
				if len(SELECT_child) != 0:					# kontrola ci element ma podelementy
					Scanning_NOT_Specifically(SELECT_child,elements2)	# zistenie podelementov
					HeadElement2 = True									
																		
				else:                                               
					elements2.append(SELECT_child)
					HeadElement2 = False
				
				
				result_Flag = False							# priznak -> XML sa nasielo hladany text/cislo -> povoli zapis do konecneho vysledku
				for element2 in elements2:					# cyklicka kontrola elementov v poli ( 2. zanorenie )
					elements=[]
					HeadElement = False
					HeadElementFlag = False
					UltimateFlag = False
					if len(element2) != 0:					# kontrola ci element ma podelementy
						Scanning_NOT_Specifically(element2,elements)	# zistenie podelementov									
						HeadElement = True							
																	
					else:
						elements = elements2
						HeadElement = False
						UltimateFlag = True
					
					temporaryResult = ""		# medzivysledok kam sa zapise kazdy najspodnejsi element  <ELEMENT> TEXT </ELEMENT>
					result_Flag = False			# priznak -> XML sa nasielo hladany text/cislo -> povoli zapis do konecneho vysledku
					for element in elements:	# cyklicka kontrola najspodnejsich elementov
					
						temporaryResult = temporaryResult + "\n      \t\t<" + element.tag + ">" + element.text + "</" + element.tag + ">"
						
						# +++++++++++++++++++++++++++++++++++++++ BOL zadany v dotazu WHERE ++++++++++++++++++++++++++++++++++++++++++++
						if WHERE_flag == True:
							
							# ---------------- NEBOL zadany alebo bol zadany ale kvoli parnemu poctu NEAKTIVNY  NOT  -----------------------
							if WHERE_not_array[0] == False and (WHERE_element_array[0] == element.tag or (len(WHERE_attribute_array) != 0)):
								
								# ------------------------------ bol zadany iba ELEMENT ---------------------------------------------------
								if WHERE_element_array[0] == element.tag:
									if(WHERE_relation_operator_array[0] == "CONTAINS"):						# porovnavanie podla CONTAINS
										if WHERE_literal_array[0] in element.text:
											result_Flag = True
									elif(WHERE_relation_operator_array[0] == "="):							# porovnavanie podla operatoru " = "
										if WHERE_literal_array[0][:1] == "\"":
											if element.text == WHERE_literal_array[0][1:]:					# je retazec
												result_Flag = True
										else:
											try:
												if float(element.text) == float(WHERE_literal_array[0]):	# je cislo
													result_Flag = True
											except:
												continue
									elif(WHERE_relation_operator_array[0] == ">"):							# porovnavanie podla operatoru " > "
										if WHERE_literal_array[0][:1] == "\"":                              
											if element.text > WHERE_literal_array[0][1:]:                   # je retazec
												result_Flag = True                                          
										else:                                                               
											try:                                                            
												if float(element.text) > float(WHERE_literal_array[0]):     # je cislo
													result_Flag = True
											except:
												continue
									elif(WHERE_relation_operator_array[0] == "<"):							# porovnavanie podla operatoru " < "
										if WHERE_literal_array[0][:1] == "\"":                              
											if element.text < WHERE_literal_array[0][1:]:                   # je retazec
												result_Flag = True                                          
										else:                                                               
											try:                                                            
												if float(element.text) < float(WHERE_literal_array[0]):     # je cislo
													result_Flag = True
											except:
												continue
								
								# ------------------------------ bol zadany iba ELEMENT / ATTRIBUT ---------------------------------------------------
								elif len(WHERE_attribute_array) != 0 and SELECT_child.attrib != None:
									
									listAttributeNames = []
									attributeSplit(SELECT_child.attrib, listAttributeNames,1)
									
									for listAttributeName in listAttributeNames:						# ziskanie MIEN atributov
										if WHERE_attribute_array[0] == listAttributeName:
											
											listAttributeTexts = []
											attributeSplit(SELECT_child.attrib,listAttributeTexts,3)
											
											for listAttributeText in listAttributeTexts:				# ziskanie TEXTU z atributov
												
												if(WHERE_relation_operator_array[0] == "CONTAINS"):									# porovnavanie podla CONTAINS
													if WHERE_literal_array[0] in listAttributeText:                                 
														result_Flag = True                                                          
												elif(WHERE_relation_operator_array[0] == "="):                                      # porovnavanie podla operatoru " = "
													if WHERE_literal_array[0][:1] == "\"":                                          
														if listAttributeText == WHERE_literal_array[0][1:]:                         # je retazec
															result_Flag = True                                                      
													else:                                                                           
														try:                                                                        
															if float(listAttributeText) == float(WHERE_literal_array[0]):           # je cislo
																result_Flag = True                                                  
														except:                                                                     
															continue                                                                
												elif(WHERE_relation_operator_array[0] == ">"):                                      # porovnavanie podla operatoru " > "
													if WHERE_literal_array[0][:1] == "\"":                                          
														if listAttributeText > WHERE_literal_array[0][1:]:                          # je retazec
															result_Flag = True                                                      
													else:                                                                           
														try:                                                                        
															if float(listAttributeText) > float(WHERE_literal_array[0]):            # je cislo
																result_Flag = True                                                  
														except:                                                                     
															continue                                                                
												elif(WHERE_relation_operator_array[0] == "<"):                                      # porovnavanie podla operatoru " < "
													if WHERE_literal_array[0][:1] == "\"":                                          
														if listAttributeText < WHERE_literal_array[0][1:]:                          # je retazec
															result_Flag = True                                                      
													else:                                                                           
														try:                                                                        
															if float(listAttributeText) < float(WHERE_literal_array[0]):            # je cislo
																result_Flag = True
														except:
															continue
							
							# --- NOT JE aktivny  ->  NEGACIA WHERE dotazu -----------------------------------------------------------
							elif WHERE_not_array[0] == True and (WHERE_element_array[0] == element.tag or (len(WHERE_attribute_array) != 0)):
								# ------------------------------ bol zadany iba ELEMENT ---------------------------------------------------
								if WHERE_element_array[0] == element.tag:
									if(WHERE_relation_operator_array[0] == "CONTAINS"):					   # porovnavanie podla CONTAINS
										if WHERE_literal_array[0] in element.text:                          
											result_Flag = False                                             
										else:                                                               
											result_Flag = True                                              
									elif(WHERE_relation_operator_array[0] == "="):                         # porovnavanie podla operatoru " = " , ale kvoli NOT !=
										if WHERE_literal_array[0][:1] == "\"":                              
											if element.text != WHERE_literal_array[0][1:]:                 # je retazec 
												result_Flag = True                                          
										else:                                                               
											try:                                                            
												if float(element.text) != float(WHERE_literal_array[0]):   # je cislo 
													result_Flag = True                                      
											except:                                                         
												continue                                                    
									elif(WHERE_relation_operator_array[0] == ">"):                         # porovnavanie podla operatoru " > " , ale kvoli NOT <
										                                                                    
										if WHERE_literal_array[0][:1] == "\"":                             # je retazec 
											if element.text < WHERE_literal_array[0][1:]:                   
												result_Flag = True                                          
										else:                                                               
											try:                                                           # je cislo 
												if float(element.text) < float(WHERE_literal_array[0]):     
													result_Flag = True                                      
											except:                                                         
												continue                                                   # porovnavanie podla operatoru " < " , ale kvoli NOT >
									elif(WHERE_relation_operator_array[0] == "<"):                          
										if WHERE_literal_array[0][:1] == "\"":                             # je retazec 
											if element.text > WHERE_literal_array[0][1:]:                   
												result_Flag = True                                          
										else:                                                              
											try:                                                           # je cislo
												if float(element.text) > float(WHERE_literal_array[0]):
													result_Flag = True
											except:
												continue
								
								# ------------------------------ bol zadany iba ELEMENT / ATTRIBUT ---------------------------------------------------
								elif len(WHERE_attribute_array) != 0 and SELECT_child.attrib != None:
									
									listAttributeNames = []
									attributeSplit(SELECT_child.attrib, listAttributeNames,1)
									
									for listAttributeName in listAttributeNames:							# ziskanie MIEN atributov
										if WHERE_attribute_array[0] == listAttributeName:
											
											listAttributeTexts = []
											attributeSplit(SELECT_child.attrib,listAttributeTexts,3)		# ziskanie TEXTOV z atributov
											
											for listAttributeText in listAttributeTexts:
												if(WHERE_relation_operator_array[0] == "CONTAINS"):							# porovnavanie podla CONTAINS
													if WHERE_literal_array[0] in listAttributeText:                          
														result_Flag = False                                                  
													else:                                                                    
														result_Flag = True                                                   
												elif(WHERE_relation_operator_array[0] == "="):                              # porovnavanie podla operatoru " = " , ale kvoli NOT !=
													if WHERE_literal_array[0][:1] == "\"":                                   
														if listAttributeText != WHERE_literal_array[0][1:]:                 # je retazec 
															result_Flag = True                                               
													else:                                                                    
														try:                                                                 
															if float(listAttributeText) != float(WHERE_literal_array[0]):   # je cislo 
																result_Flag = True                                           
														except:                                                              
															continue                                                         
												elif(WHERE_relation_operator_array[0] == ">"):                              # porovnavanie podla operatoru " > " , ale kvoli NOT <
													if WHERE_literal_array[0][:1] == "\"":                                   
														if listAttributeText < WHERE_literal_array[0][1:]:                  # je retazec 
															result_Flag = True                                               
													else:                                                                    
														try:                                                                 
															if float(listAttributeText) < float(WHERE_literal_array[0]):    # je cislo 
																result_Flag = True                                           
														except:                                                              
															continue                                                         
												elif(WHERE_relation_operator_array[0] == "<"):                              # porovnavanie podla operatoru " < " , ale kvoli NOT >
													if WHERE_literal_array[0][:1] == "\"":                                   
														if listAttributeText > WHERE_literal_array[0][1:]:                  # je retazec 
															result_Flag = True                                               
													else:                                                                    
														try:                                                                
															if float(listAttributeText) > float(WHERE_literal_array[0]):    # je cislo
																result_Flag = True
														except:
															continue
									
				# ---------------------------- priradenie medzivysledku podla zanoreni --------------------------------
					if result_Flag == True or WHERE_flag == False:
						
						if HeadElement == True:
							HeadElementFlag = True
							if element2.attrib is not None:
								result = result + "<" + element2.tag + attributeJoin(element2.attrib) + ">"
							else:
								result = result + "<" + element2.tag + ">"
						
						result = result +  temporaryResult
						
						if HeadElement == True:
							result = result + "\n   \t</" + element2.tag + ">"
					
					if UltimateFlag == True:
						break
					
				if result_Flag == True or WHERE_flag == False:
					
					if HeadElement2 == True:
						if HeadElement == False and HeadElementFlag == True:
							if SELECT_child.attrib is not None:
								result = result + "<" + SELECT_child.tag + attributeJoin(SELECT_child.attrib) + ">"
							else:
								result = result + "<" + SELECT_child.tag + ">"
						else:
							if SELECT_child.attrib is not None:
								result = "<" + SELECT_child.tag + attributeJoin(SELECT_child.attrib) + ">" + result
							else:
								result = "<" + SELECT_child.tag + ">" + result

					if HeadElement2 == True:
						result = result + "\n   </" + SELECT_child.tag + ">"	
				
				AbsResult = AbsResult + result # konecny vysledok
				result = ""	
								
	if OK_Parameters[5]:													#--root (meno paroveho korenoveho elementu obalujuci vysledky.)
		AbsResult = AbsResult + "</" + param_root + ">\n"					# pridanie KONIEC paroveho korenoveho elementu obalujuci vysledky
	else:
		AbsResult = AbsResult + "\n"										# nezabalujem vysledky do paroveho korenoveho elementu
		
	output=AbsResult
	if OK_Parameters[1]:													#--output  ( vystup )
		try:
			outFile = open(param_output, "w")								# otvorim vystupny subor 
		except:
			sys.stderr.write("Chyba pri otvarani vystupneho suboru !\n")
			sys.exit(3)
		
		outFile.write(output)												# konecny vysledok vypisem do suboru
		outFile.close()
		sys.exit(0)
	else:
		sys.stdout.write(output)											# konecny vysledok vypisem na terminal
		sys.exit(0)
else:
	sys.stderr.write("Nespravne parametre !\n")
	sys.exit(1)