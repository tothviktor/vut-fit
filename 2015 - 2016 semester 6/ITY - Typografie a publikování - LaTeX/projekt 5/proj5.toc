\beamer@endinputifotherversion {3.36pt}
\select@language {czech}
\beamer@sectionintoc {1}{Hist\'oria}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Vznik a~v\'yvoj p\IeC {\'\i }sma}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Typy a~v\'yvoj p\IeC {\'\i }sma}{4}{0}{1}
\beamer@sectionintoc {2}{Po\v c\IeC {\'\i }ta\v cov\'a sadzba}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Typ po\v c\IeC {\'\i }ta\v covej sadzby}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Off-line syst\'emy}{6}{0}{2}
\beamer@subsectionintoc {2}{3}{WYSIWYG syst\'emy}{7}{0}{2}
\beamer@sectionintoc {3}{Pou\v zit\'e zdroje}{8}{0}{3}
