Ahoj,
v tomto reposit�ry sa nach�dzaj� vsetky moje projekty, ktor� som spravil po�as st�dia 
v Nov�ch Z�mkoch na strednej �kole (2009 - 2013) a 
v Brne na VUT FIT (2013 - 2017).

R�chly prehlad:

Obdobie		      | Predmet					      | Projekt / Programovac� jazyk

2009 - 2013 Stred. sk.: 1. - 3. ro�n�k					C++
			3. ro�n�k					C#
			4. ro�n�k					JAVA
			KOP 2013					Z�znam �innosti elektriza�nej s�stavy s alternat�vnymi zdrojmi energie

2013 - 2014 semester 1: IAS - Asemblery					ASSEMBLER
			ITO - Teorie obvodu
			IUS - �vod do softwarov�ho in�en�rstv�		ER/USE CASE diagram
			IZP - Z�kaldy programovan�			C

2013 - 2014 semester 2: IFY - Fyzika
			IMA - Matematick� anal�za
			INC - N�vrh ��slicov�ch syst�m�			VHDL
			IOS - Opera�n� syst�my				SHELL, C

2014 - 2015 semester 3: IAL - Algoritmy					C
			IFJ - Form�ln� jazyky a p�eklada�e		C
			INP - N�vrh po��ta�ov�ch syst�m�		VHDL
			ISS - Sign�ly a syst�my				MatLab

2014 - 2015 semester 4: IDS - Datab�zov� syst�my			SQL, ER/USE CASE diagram
			IJA - Semin�� Java				JAVA
			IOS - Opera�n� syst�my				BASH, C
			IPK - Po��ta�ov� komunikace a s�t�		C++, PYTHON, BASH
			IPP - Principy programovac�ch jazyk� a OOP	PHP, PYTHON
			IZG - Z�klady po��ta�ov� grafiky		C++

2015 - 2016 semester 5: IAL - Algoritmy					Jazyk C
			IIS - Informa�n� syst�my			PHP, SQL, HTML
			IMP - Mikroprocesorov� a vestav�n� syst�my	C
			ITU - Tvorba uzivatelsk�ch rozhran�

2015 - 2016 semester 6: IPP - Principy programovac�ch jazyk� a OOP	PHP, PYTHON
			ITW - Tvorba webov�ch str�nek			HTML,CSS
			ITY - Typografie a publikov�n�			LaTeX
			IVH - Semin�� VHDL				VHDL
			IW5 - Programov�n� v .NET a C#			C#

2016 - 2017 semester 7: IMS - Modelov�n� a simulace			C++
			ISA - S�tov� aplikace a spr�va s�t�		C++
			ISP - Semestr�ln� projekt

2016 - 2017 semester 8: IDS - Datab�zov� syst�my			SQL, ER/USE CASE diagram

2017 - 2018 semester 9: IDS - Modelov�n� a simulace			C++