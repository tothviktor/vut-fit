\select@language {czech}
\contentsline {section}{\'Uvod}{1}{section*.2}
\contentsline {section}{\numberline {1}Anal\'yza probl\'emu a princ\'ip jeho rie\v {s}enia}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Zadanie probl\'emu}{1}{subsection.1.1}
\contentsline {section}{\numberline {2}Lexik\'alny analyz\'ator}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Diagram kone\v {c}n\'eho automatu}{2}{subsection.2.1}
\contentsline {section}{\numberline {3}Syntaktick\'y analyz\'ator}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}LL - gramatika}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Preceden\v {c}n\'a tabu\'lka}{3}{subsection.3.2}
\contentsline {section}{\numberline {4}S\'emantick\'y analyz\'ator}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Tabu\'lka symbolov}{4}{subsection.4.1}
\contentsline {section}{\numberline {5}Interpret}{5}{section.5}
\contentsline {subsection}{\numberline {5.1}N\'avrh}{5}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Implement\'acia}{5}{subsection.5.2}
\contentsline {section}{\numberline {6}Vstavan\'e funkcie}{5}{section.6}
\contentsline {subsection}{\numberline {6.1}Knuth-Moris-Prattov algoritmus}{5}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Shell sort}{6}{subsection.6.2}
\contentsline {section}{\numberline {7}Pr\'aca v t\'yme}{6}{section.7}
\contentsline {section}{\numberline {8}Z\'aver}{7}{section.8}
\contentsline {section}{Literat\'ura}{7}{section*.3}
\contentsline {section}{Metriky K\'odu}{8}{section*.4}
