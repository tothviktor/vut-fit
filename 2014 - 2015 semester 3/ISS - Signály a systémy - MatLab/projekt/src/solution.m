I = imread('xtothv01.bmp');
H = [-0.5 -0.5 -0.5; -0.5  5.0 -0.5; -0.5 -0.5 -0.5];
I1 = imfilter(I,H);
imwrite(I1, 'step1.bmp');
I2 = flip(I1,2);
imwrite(I2, 'step2.bmp');
I3 = medfilt2 (I2, [5,5]);
imwrite(I3, 'step3.bmp');
H = [1 1 1 1 1; 1 3 3 3 1; 1 3 9 3 1; 1 3 3 3 1; 1 1 1 1 1]/49;
I4 = imfilter(I3,H);
imwrite(I4, 'step4.bmp');
chyba = 0;
I4df = double(flip(I4, 2));
Id = double(I);
for(x=1:512)
    for(y = 1:512)
        chyba = chyba+double(abs(Id(x,y)-I4df(x,y)));
    end;
end;
chyba= chyba/512/512
I4d = im2double(I4);
vstupMin = min(min(I4d));
vstupMax = max(max(I4d));
vystupMin = 0.0;
vystupMax = 1.0;
I5 = imadjust(I4, [vstupMin, vstupMax],[vystupMin, vystupMax]);
imwrite (I5, 'step5.bmp');
meanNoHist = mean2(I4d)*255
stdNoHist = std2(I4d)*255
I5d = im2double(I5);
meanHist = mean2(I5d)*255
stdHist = std2(I5d)*255
I5d = double(I5);
N=2;
a=0;
b=255;
for(x = 1:512)
    for(y = 1:512)
        I6(x,y) = round(((2^N)-1)*(I5d(x,y)-a)/(b-a))*(b-a)/((2^N)-1)+a;
    end;
end;
I6 = uint8(I6);
imwrite(I6, 'step6.bmp');