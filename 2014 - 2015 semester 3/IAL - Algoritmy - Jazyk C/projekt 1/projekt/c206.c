// Meno: Viktor T�th
// Login: xtothv01
// Vyhotovenie: 31.10.2014
/* c206.c **********************************************************}
{* T�ma: Dvousm�rn� v�zan� line�rn� seznam
**
**                   N�vrh a referen�n� implementace: Bohuslav K�ena, ��jen 2001
**                            P�epracovan� do jazyka C: Martin Tu�ek, ��jen 2004
**                                            �pravy: Bohuslav K�ena, ��jen 2014
**
** Implementujte abstraktn� datov� typ dvousm�rn� v�zan� line�rn� seznam.
** U�ite�n�m obsahem prvku seznamu je hodnota typu int.
** Seznam bude jako datov� abstrakce reprezentov�n prom�nnou
** typu tDLList (DL znamen� Double-Linked a slou�� pro odli�en�
** jmen konstant, typ� a funkc� od jmen u jednosm�rn� v�zan�ho line�rn�ho
** seznamu). Definici konstant a typ� naleznete v hlavi�kov�m souboru c206.h.
**
** Va��m �kolem je implementovat n�sleduj�c� operace, kter� spolu
** s v��e uvedenou datovou ��st� abstrakce tvo�� abstraktn� datov� typ
** obousm�rn� v�zan� line�rn� seznam:
**
**      DLInitList ...... inicializace seznamu p�ed prvn�m pou�it�m,
**      DLDisposeList ... zru�en� v�ech prvk� seznamu,
**      DLInsertFirst ... vlo�en� prvku na za��tek seznamu,
**      DLInsertLast .... vlo�en� prvku na konec seznamu, 
**      DLFirst ......... nastaven� aktivity na prvn� prvek,
**      DLLast .......... nastaven� aktivity na posledn� prvek, 
**      DLCopyFirst ..... vrac� hodnotu prvn�ho prvku,
**      DLCopyLast ...... vrac� hodnotu posledn�ho prvku, 
**      DLDeleteFirst ... zru�� prvn� prvek seznamu,
**      DLDeleteLast .... zru�� posledn� prvek seznamu, 
**      DLPostDelete .... ru�� prvek za aktivn�m prvkem,
**      DLPreDelete ..... ru�� prvek p�ed aktivn�m prvkem, 
**      DLPostInsert .... vlo�� nov� prvek za aktivn� prvek seznamu,
**      DLPreInsert ..... vlo�� nov� prvek p�ed aktivn� prvek seznamu,
**      DLCopy .......... vrac� hodnotu aktivn�ho prvku,
**      DLActualize ..... p�ep��e obsah aktivn�ho prvku novou hodnotou,
**      DLSucc .......... posune aktivitu na dal�� prvek seznamu,
**      DLPred .......... posune aktivitu na p�edchoz� prvek seznamu, 
**      DLActive ........ zji��uje aktivitu seznamu.
**
** P�i implementaci jednotliv�ch funkc� nevolejte ��dnou z funkc�
** implementovan�ch v r�mci tohoto p��kladu, nen�-li u funkce
** explicitn� uvedeno n�co jin�ho.
**
** Nemus�te o�et�ovat situaci, kdy m�sto leg�ln�ho ukazatele na seznam 
** p�ed� n�kdo jako parametr hodnotu NULL.
**
** Svou implementaci vhodn� komentujte!
**
** Terminologick� pozn�mka: Jazyk C nepou��v� pojem procedura.
** Proto zde pou��v�me pojem funkce i pro operace, kter� by byly
** v algoritmick�m jazyce Pascalovsk�ho typu implemenov�ny jako
** procedury (v jazyce C procedur�m odpov�daj� funkce vracej�c� typ void).
**/

#include "c206.h"

int solved;
int errflg;

void DLError() {
/*
** Vytiskne upozorn�n� na to, �e do�lo k chyb�.
** Tato funkce bude vol�na z n�kter�ch d�le implementovan�ch operac�.
**/	
    printf ("*ERROR* The program has performed an illegal operation.\n");
    errflg = TRUE;             /* glob�ln� prom�nn� -- p��znak o�et�en� chyby */
    return;
}

void DLInitList (tDLList *L) {
/*
** Provede inicializaci seznamu L p�ed jeho prvn�m pou�it�m (tzn. ��dn�
** z n�sleduj�c�ch funkc� nebude vol�na nad neinicializovan�m seznamem).
** Tato inicializace se nikdy nebude prov�d�t nad ji� inicializovan�m
** seznamem, a proto tuto mo�nost neo�et�ujte. V�dy p�edpokl�dejte,
** �e neinicializovan� prom�nn� maj� nedefinovanou hodnotu.
**/
    L->First = NULL;        //Ukazatel na prv� prvok
    L->Last = NULL;         //Ukazatel na posledn� prvok
    L->Act = NULL;          //Ukazatel na aktualn� prvok
}

void DLDisposeList (tDLList *L) {
/*
** Zru�� v�echny prvky seznamu L a uvede seznam do stavu, v jak�m
** se nach�zel po inicializaci. Ru�en� prvky seznamu budou korektn�
** uvoln�ny vol�n�m operace free. 
**/
    tDLElemPtr auxPtr = NULL;       //vytvorim si pomocny ukazatel
    while (L->First != NULL)
    {
        auxPtr = L->First;
        L->First = auxPtr->rptr;
        free(auxPtr);
    }
    L->Last = NULL;
    L->Act = NULL;
}

void DLInsertFirst (tDLList *L, int val) {
/*
** Vlo�� nov� prvek na za��tek seznamu L.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
    tDLElemPtr auxPtr = malloc(sizeof(struct tDLElem));     //vytvorim si pomocny ukazatel a allokujem pam�t

    if(auxPtr == NULL)              //Ak zlyhala alok�cia pam�ti zahl�s chybu
    {
        DLError();
        return;
    }

    auxPtr->lptr = NULL;
    auxPtr->rptr = L->First;
    auxPtr->data = val;

    if((L->First == NULL) && (L->Last == NULL))
    {
        L->Last = auxPtr;           //Ak bol zoznam pr�zdny, tak posledn� je zaroven prv�
        L->First = auxPtr;
        return;
    }
    else                            //Ak v zoznamu nie�o bolo
    {
        auxPtr->rptr = L->First;    //navia�em prv� prvok na druh�
        L->First->lptr = auxPtr;    //druhy prvok na prv�
        L->First=auxPtr;            //nastavim prv� prvok prv�m
    }
}

void DLInsertLast(tDLList *L, int val) {
/*
** Vlo�� nov� prvek na konec seznamu L (symetrick� operace k DLInsertFirst).
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/ 	
    tDLElemPtr auxPtr = malloc(sizeof(struct tDLElem)); //vytvorim si pomocny ukazatel a allokujem pamet

    if(auxPtr == NULL)              //Ak zlyhala alok�cia pam�ti zahl�s chybu
    {
        DLError();
        return;
    }
    auxPtr->lptr = L->Last;
    auxPtr->rptr = NULL;
    auxPtr->data = val;

    if((L->First == NULL) && (L->Last == NULL))
    {
        L->Last = auxPtr;           //Ak bol zoznam pr�zdn�, tak posledn� je zarove� prv�
        L->First = auxPtr;
        return;
    }
    else                            //Ak v zoznamu nie�o bolo
    {
        L->Last->rptr = auxPtr;     //predposledn� na posledn�
        L->Last = auxPtr;           //nastav�m posledn� prvok na posledn�
    }
}

void DLFirst (tDLList *L) {
/*
** Nastav� aktivitu na prvn� prvek seznamu L.
** Funkci implementujte jako jedin� p��kaz (nepo��t�me-li return),
** ani� byste testovali, zda je seznam L pr�zdn�.
**/
	L->Act = L->First;              //Nastav� aktivitu na prv� prvok seznamu L
}

void DLLast (tDLList *L) {
/*
** Nastav� aktivitu na posledn� prvek seznamu L.
** Funkci implementujte jako jedin� p��kaz (nepo��t�me-li return),
** ani� byste testovali, zda je seznam L pr�zdn�.
**/
    L->Act = L->Last;               //Nastav� aktivitu na posledn� prvok zoznamu L.
}

void DLCopyFirst (tDLList *L, int *val) {
/*
** Prost�ednictv�m parametru val vr�t� hodnotu prvn�ho prvku seznamu L.
** Pokud je seznam L pr�zdn�, vol� funkci DLError().
**/
    if(L->First == NULL)            //Ak je zoznam L pr�zdn�, vol� funkciu DLError().
    {
        DLError();
        return;
    }
    else
    {
        *val=L->First->data;       //Prostrednictvom parametru val vr�ti hodnotu prv�ho prvku seznamu L.
    }
}

void DLCopyLast (tDLList *L, int *val) {
/*
** Prost�ednictv�m parametru val vr�t� hodnotu posledn�ho prvku seznamu L.
** Pokud je seznam L pr�zdn�, vol� funkci DLError().
**/
    if(L->First == NULL)            //Ak je zoznam L pr�zdny, vol� funkciu DLError().
    {
        DLError();
        return;
    }
    else
    {
        *val=L->Last->data;         //Prostrednictvom parametru val vr�t� hodnotu posledn�ho prvku zoznamu L.
    }
}

void DLDeleteFirst (tDLList *L) {
/*
** Zru�� prvn� prvek seznamu L. Pokud byl prvn� prvek aktivn�, aktivita 
** se ztr�c�. Pokud byl seznam L pr�zdn�, nic se ned�je.
**/
    if(L->First == NULL)
    {
        return;
    }
    tDLElemPtr auxPtr = L->First;           //vytvorim si pomocn� ukazatel

    if(L->Act == L->First)                  //ak je prv� prvok akt�vny, tak zru�im ativitu
    {
        L->Act = NULL;
    }
    if(L->First == L->Last)                 //ak je prv� prvok z�rove� posledn�
    {
        L->First = NULL;                    //prv� nulujem
        L->Last = NULL;                     //posledn� nulujem
    }
    else
    {
        L -> First = L -> First -> rptr;
        L -> First -> lptr = NULL;          //Prv� ukazatel je NULL
    }

    free(auxPtr);                           //Uvolnim pam�t
}	

void DLDeleteLast (tDLList *L) {
/*
** Zru�� posledn� prvek seznamu L. Pokud byl posledn� prvek aktivn�,
** aktivita seznamu se ztr�c�. Pokud byl seznam L pr�zdn�, nic se ned�je.
**/ 
    if(L->Last == NULL)
    {
        return;
    }

    tDLElemPtr auxPtr = L->Last;    //vytvorim si pomocn� ukazatel

    if(L->Act == L->Last)           //ak je prv� prvok aktivn�
    {
        L->Act = NULL;              //tak zru�im ativitu
    }
    if(L->First == L->Last)         //ak je prv� prvok z�rove� posledn�
    {
        L->First = NULL;            //tak prv� nulujem
        L->Last = NULL;             //tak posledn� nulujem
    }
    else
    {
        L -> Last = L -> Last -> lptr;
        L -> Last -> rptr = NULL;           //Posledn� ukazatel je NULL
    }

    free(auxPtr);                   //Uvolnim pam�t
}

void DLPostDelete (tDLList *L) {
/*
** Zru�� prvek seznamu L za aktivn�m prvkem.
** Pokud je seznam L neaktivn� nebo pokud je aktivn� prvek
** posledn�m prvkem seznamu, nic se ned�je.
**/
    if((L->Act == NULL) || (L->Act == L->Last))
    {
        return;
    }

    tDLElemPtr auxPtr = L->Act->rptr;           //vytvorim si pomocny ukazatel
    if(auxPtr->rptr != NULL)                    //ak nie je ru�eny posledn�m prvkom
    {
        L->Act->rptr = auxPtr->rptr;
        auxPtr->rptr->lptr = L->Act;
    }
    else                                        //aktivn� prvok sa stav� zaroven posledn�m
    {
        L->Act->rptr = NULL;
        L->Last = L->Act;
    }
    free(auxPtr);                               //Uvoln�m pam�t
}

void DLPreDelete (tDLList *L) {
/*
** Zru�� prvek p�ed aktivn�m prvkem seznamu L .
** Pokud je seznam L neaktivn� nebo pokud je aktivn� prvek
** prvn�m prvkem seznamu, nic se ned�je.
**/
    if((L->Act == NULL) || (L->First == L->Last) || (L->Act->lptr == NULL))
    {
        return;
    }

    tDLElemPtr auxPtr = L->Act->lptr;           //vytvorim si pomocny ukazatel
    if(auxPtr->lptr != NULL)                    //ak nie je ru�eny prv�m prvkom
    {
        L->Act->lptr = auxPtr->lptr;
        auxPtr->lptr->rptr = L->Act;
    }
    else                                        //aktivny prvok sa st�va z�rove� prv�m
    {
        L->Act->lptr = NULL;
        L->First = L->Act;
    }
    free(auxPtr);
}

void DLPostInsert (tDLList *L, int val) {
/*
** Vlo�� prvek za aktivn� prvek seznamu L.
** Pokud nebyl seznam L aktivn�, nic se ned�je.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
    tDLElemPtr auxPtr = malloc(sizeof(struct tDLElem));     //vytvorim si pomocny ukazatel a allokujem pamet
    if(L->Act == NULL)
    {
        return;
    }
    if(auxPtr == NULL)                  //pri nedostatku pam�ti sa vol� funkcia DLError()
    {
        DLError();
        return;
    }

    if(L->Act->rptr == NULL)            //ak budem vkladat na konec zoznamu ...
    {
        L->Last = auxPtr;               //vkladany prvok bude zarove� posledn�
    }
    else
    {
        L->Act->rptr->lptr = auxPtr;    //inak previa�em nasleduj�ci prvek s vkladan�m
    }

    auxPtr->rptr = L->Act->rptr;
    auxPtr->lptr = L->Act;
    L->Act->rptr = auxPtr;

    auxPtr->data = val;                 //nahraj data
}

void DLPreInsert (tDLList *L, int val) {
/*
** Vlo�� prvek p�ed aktivn� prvek seznamu L.
** Pokud nebyl seznam L aktivn�, nic se ned�je.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
    tDLElemPtr auxPtr = malloc(sizeof(struct tDLElem));     //vytvorim si pomocny ukazatel a allokujem pam�t
    if(L->Act == NULL)
    {
        return;
    }
    if(auxPtr == NULL)                  //pri nedostatku pameti sa vol� funkca DLError()
    {
        DLError();
        return;
    }
    if(L->Act->lptr == NULL)            //pokud budu vkladat na za�iatok zoznamu
    {
        L->First = auxPtr;              //vkladany prvok bude zarove� prvni
    }
    else
    {
        L->Act->lptr->rptr = auxPtr;    //inak previa�em predchadzaj�ci prvok s vkladanym
    }

    auxPtr->lptr = L->Act->lptr;
    auxPtr->rptr = L->Act;
    L->Act->lptr = auxPtr;

    auxPtr->data = val;                 //nahraj data
}

void DLCopy (tDLList *L, int *val) {
/*
** Prost�ednictv�m parametru val vr�t� hodnotu aktivn�ho prvku seznamu L.
** Pokud seznam L nen� aktivn�, vol� funkci DLError ().
**/
	if(L->Act == NULL)          //Ak zoznam L nie je aktivn�, vol� funkcia DLError ()
		{
			DLError();
			return;
		}
		*val = L->Act->data;        //Prostrednictvom parametru *val vr�t� hodnotu aktivn�ho prvku

}

void DLActualize (tDLList *L, int val) {
/*
** P�ep��e obsah aktivn�ho prvku seznamu L.
** Pokud seznam L nen� aktivn�, ned�l� nic.
**/
    if(L->Act == NULL)      //Ak seznam L nie je aktivn�, funkcia skon��.
    {
        return;
    }
    L->Act->data = val;     //Prep��e obsah aktivn�ho prvku seznamu L
}

void DLSucc (tDLList *L) {
/*
** Posune aktivitu na n�sleduj�c� prvek seznamu L.
** Nen�-li seznam aktivn�, ned�l� nic.
** V�imn�te si, �e p�i aktivit� na posledn�m prvku se seznam stane neaktivn�m.
**/
	if(L->Act != NULL)              //Pokud akt�vn� prvok NIE je pr�zdny
	{
		L->Act = L->Act->rptr;      //Posunie aktivitu na n�sleduj�c� prvok seznamu L
	}
}


void DLPred (tDLList *L) {
/*
** Posune aktivitu na p�edchoz� prvek seznamu L.
** Nen�-li seznam aktivn�, ned�l� nic.
** V�imn�te si, �e p�i aktivit� na prvn�m prvku se seznam stane neaktivn�m.
**/
	
	
   if(L->Act != NULL)              //Ak akt�vn� prvok NIE je pr�zdny
   {
		L->Act = L->Act->lptr;      //Posunie aktivitu na predch�dzaj�ci prvok zoznamu L.
   }
}

int DLActive (tDLList *L) {
/*
** Je-li seznam aktivn�, vrac� true. V opa�n�m p��pad� vrac� false.
** Funkci implementujte jako jedin� p��kaz.
**/
    return (L->Act != NULL) ? TRUE : FALSE;     //Ak ja zoznam aktivn�, vr�t� TRUE. V opa�n�m pr�pade vr�ti FALSE.
}
/* Konec c206.c*/
