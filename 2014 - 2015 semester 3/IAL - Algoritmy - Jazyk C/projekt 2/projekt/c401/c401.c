/* 2.DOMACA ULOHA
**Meno a priezvisko: Viktor T�th
**Login: xtothv01
**Trieda: 2BIT
**D�tum: 30.11.2014
*/

/* c401.c: **********************************************************}
{* T�ma: Rekurzivn� implementace operac� nad BVS
**                                         Vytvo�il: Petr P�ikryl, listopad 1994
**                                         �pravy: Andrea N�mcov�, prosinec 1995
**                                                      Petr P�ikryl, duben 1996
**                                                   Petr P�ikryl, listopad 1997
**                                  P�evod do jazyka C: Martin Tu�ek, ��jen 2005
**                                         �pravy: Bohuslav K�ena, listopad 2009
**                                         �pravy: Karel Masa��k, ��jen 2013
**                                         �pravy: Radek Hranick�, ��jen 2014
**
** Implementujte rekurzivn�m zp�sobem operace nad bin�rn�m vyhled�vac�m
** stromem (BVS; v angli�tin� BST - Binary Search Tree).
**
** Kl��em uzlu stromu je jeden znak (obecn� j�m m��e b�t cokoliv, podle
** �eho se vyhled�v�). U�ite�n�m (vyhled�van�m) obsahem je zde integer.
** Uzly s men��m kl��em le�� vlevo, uzly s v�t��m kl��em le�� ve stromu
** vpravo. Vyu�ijte dynamick�ho p�id�lov�n� pam�ti.
** Rekurzivn�m zp�sobem implementujte n�sleduj�c� funkce:
**
**   BSTInit ...... inicializace vyhled�vac�ho stromu
**   BSTSearch .... vyhled�v�n� hodnoty uzlu zadan�ho kl��em
**   BSTInsert .... vkl�d�n� nov� hodnoty
**   BSTDelete .... zru�en� uzlu se zadan�m kl��em
**   BSTDispose ... zru�en� cel�ho stromu
**
** ADT BVS je reprezentov�n ko�enov�m ukazatelem stromu (typ tBSTNodePtr).
** Uzel stromu (struktura typu tBSTNode) obsahuje kl�� (typu char), podle
** kter�ho se ve stromu vyhled�v�, vlastn� obsah uzlu (pro jednoduchost
** typu int) a ukazatel na lev� a prav� podstrom (LPtr a RPtr). P�esnou definici typ� 
** naleznete v souboru c401.h.
**
** Pozor! Je t�eba spr�vn� rozli�ovat, kdy pou��t dereferen�n� oper�tor *
** (typicky p�i modifikaci) a kdy budeme pracovat pouze se samotn�m ukazatelem 
** (nap�. p�i vyhled�v�n�). V tomto p��kladu v�m napov� prototypy funkc�.
** Pokud pracujeme s ukazatelem na ukazatel, pou�ijeme dereferenci.
**/

#include "c401.h"
int solved;

void BSTInit (tBSTNodePtr *RootPtr) {
/*   -------
** Funkce provede po��te�n� inicializaci stromu p�ed jeho prvn�m pou�it�m.
**
** Ov��it, zda byl ji� strom p�edan� p�es RootPtr inicializov�n, nelze,
** proto�e p�ed prvn� inicializac� m� ukazatel nedefinovanou (tedy libovolnou)
** hodnotu. Program�tor vyu��vaj�c� ADT BVS tedy mus� zajistit, aby inicializace
** byla vol�na pouze jednou, a to p�ed vlastn� prac� s BVS. Proveden�
** inicializace nad nepr�zdn�m stromem by toti� mohlo v�st ke ztr�t� p��stupu
** k dynamicky alokovan� pam�ti (tzv. "memory leak").
**	
** V�imn�te si, �e se v hlavi�ce objevuje typ ukazatel na ukazatel.	
** Proto je t�eba p�i p�i�azen� p�es RootPtr pou��t dereferen�n� oper�tor *.
** Ten bude pou�it i ve funkc�ch BSTDelete, BSTInsert a BSTDispose.
**/
	
	*RootPtr = NULL;
	
}	

int BSTSearch (tBSTNodePtr RootPtr, char K, int *Content)	{
/*  ---------
** Funkce vyhled� uzel v BVS s kl��em K.
**
** Pokud je takov� nalezen, vrac� funkce hodnotu TRUE a v prom�nn� Content se
** vrac� obsah p��slu�n�ho uzlu.�Pokud p��slu�n� uzel nen� nalezen, vrac� funkce
** hodnotu FALSE a obsah prom�nn� Content nen� definov�n (nic do n� proto
** nep�i�azujte).
**
** P�i vyhled�v�n� v bin�rn�m stromu bychom typicky pou�ili cyklus ukon�en�
** testem dosa�en� listu nebo nalezen� uzlu s kl��em K. V tomto p��pad� ale
** probl�m �e�te rekurzivn�m vol�n� t�to funkce, p�i�em� nedeklarujte ��dnou
** pomocnou funkci.
**/
	if (RootPtr != NULL)                        //strom nie je prazdny
    {
        if(RootPtr->Key == K)                   //kluc sa nasiel
        {
            *Content = RootPtr->BSTNodeCont;
            return TRUE;
        }
        else
        {
            if(RootPtr->Key < K)                //kluc je vacsi hladaj vpravo
            {
                return BSTSearch(RootPtr->RPtr,K,Content);
            }
            else if(RootPtr->Key > K)           //kluc je vacsi hladaj vlavo
            {
                return BSTSearch(RootPtr->LPtr,K,Content);
            }
        }
    }
    return FALSE;                               //strom bol prazdny
} 


void BSTInsert (tBSTNodePtr* RootPtr, char K, int Content)	{	
/*   ---------
** Vlo�� do stromu RootPtr hodnotu Content s kl��em K.
**
** Pokud ji� uzel se zadan�m kl��em ve stromu existuje, bude obsah uzlu
** s kl��em K nahrazen novou hodnotou. Pokud bude do stromu vlo�en nov�
** uzel, bude vlo�en v�dy jako list stromu.
**
** Funkci implementujte rekurzivn�. Nedeklarujte ��dnou pomocnou funkci.
**
** Rekurzivn� implementace je m�n� efektivn�, proto�e se p�i ka�d�m
** rekurzivn�m zano�en� ukl�d� na z�sobn�k obsah uzlu (zde integer).
** Nerekurzivn� varianta by v tomto p��pad� byla efektivn�j�� jak z hlediska
** rychlosti, tak z hlediska pam�ov�ch n�rok�. Zde jde ale o �koln�
** p��klad, na kter�m si chceme uk�zat eleganci rekurzivn�ho z�pisu.
**/
	if(*RootPtr == NULL)
    {
        tBSTNodePtr newPtr;                             //vytvorime si novi uzol
        newPtr = malloc(sizeof(struct tBSTNode));
        if(newPtr == NULL)                              //chybna alokacie
        {
            return;
        }
        newPtr->Key = K;                        //zapis kluca K
        newPtr->BSTNodeCont = Content;          //zapiseme hodnotu Content
        newPtr->LPtr = NULL;
        newPtr->RPtr = NULL;
        *RootPtr = newPtr;                      //priradenie do stromu
    }
    else
    {
        if((*RootPtr)->Key > K)                 //hladaj vpravo
        {
            BSTInsert(&((*RootPtr)->LPtr),K,Content);
        }
        else if((*RootPtr)->Key < K)            //hladaj vlavo
        {
            BSTInsert(&((*RootPtr)->RPtr),K,Content);
        }
        else
        {
            (*RootPtr)->BSTNodeCont=Content;    //aktualizuj obsah
        }
    }
}

void ReplaceByRightmost (tBSTNodePtr PtrReplaced, tBSTNodePtr *RootPtr) {
/*   ------------------
** Pomocn� funkce pro vyhled�n�, p�esun a uvoln�n� nejprav�j��ho uzlu.
**
** Ukazatel PtrReplaced ukazuje na uzel, do kter�ho bude p�esunuta hodnota
** nejprav�j��ho uzlu v podstromu, kter� je ur�en ukazatelem RootPtr.
** P�edpokl�d� se, �e hodnota ukazatele RootPtr nebude NULL (zajist�te to
** testov�n�m p�ed vol�n� t�to funkce). Tuto funkci implementujte rekurzivn�. 
**
** Tato pomocn� funkce bude pou�ita d�le. Ne� ji za�nete implementovat,
** p�e�t�te si koment�� k funkci BSTDelete(). 
**/
	tBSTNodePtr auxPtr = NULL;                  //pomocny ukazatel bude sluzit na zmazanie

    if(RootPtr != NULL)
    {
        if((*RootPtr)->RPtr != NULL)
        {
            ReplaceByRightmost(PtrReplaced, (&((*RootPtr)->RPtr)));
        }
        else if((*RootPtr)->RPtr == NULL)
        {
            PtrReplaced->Key = (*RootPtr)->Key;                 //prepisem kluc
            PtrReplaced->BSTNodeCont = (*RootPtr)->BSTNodeCont; //prepisem obsah
            auxPtr = *RootPtr;                                  //ulizim ukazatel na koren
            *RootPtr = (*RootPtr)->LPtr;                        //uvolnim uzol
            free(auxPtr);                                       //odstranim uzol - vycistim si pamat
        }
    }
}

void BSTDelete (tBSTNodePtr *RootPtr, char K) 		{
/*   ---------
** Zru�� uzel stromu, kter� obsahuje kl�� K.
**
** Pokud uzel se zadan�m kl��em neexistuje, ned�l� funkce nic. 
** Pokud m� ru�en� uzel jen jeden podstrom, pak jej zd�d� otec ru�en�ho uzlu.
** Pokud m� ru�en� uzel oba podstromy, pak je ru�en� uzel nahrazen nejprav�j��m
** uzlem lev�ho podstromu. Pozor! Nejprav�j�� uzel nemus� b�t listem.
**
** Tuto funkci implementujte rekurzivn� s vyu�it�m d��ve deklarovan�
** pomocn� funkce ReplaceByRightmost.
**/
	
	tBSTNodePtr auxPtr = NULL;              //pomocny ukazatel bude sluzit na zmazanie

    if(*RootPtr == NULL)                    //ak je prazdny skoncim
    {
        return;
    }
    else
    {
        if((*RootPtr)->Key > K)            //hladaj vpravo
        {
            BSTDelete(&((*RootPtr)->LPtr),K);
        }
        else if ((*RootPtr)->Key < K)       //hladaj vlavo
        {
            BSTDelete(&((*RootPtr)->RPtr),K);
        }
        else                                //uzol ktory chceme zrusit
        {
            auxPtr = *RootPtr;              //uchovanie ukazatela
            if(auxPtr->RPtr == NULL)        //overime pravy podstrom
            {
                *RootPtr = auxPtr->LPtr;    //pripojim lavy podstrom
                free(auxPtr);               //cislim pamat
            }
            else if(auxPtr->LPtr == NULL)   //overime lavy podstrom
            {
                *RootPtr = auxPtr->RPtr;    //pripojim pravy podstrom
                free(auxPtr);               //cistim pamat
            }
            else                            //najpravejsi
            {
                ReplaceByRightmost(*RootPtr,(&((*RootPtr)->LPtr)));
            }
        }
    }
}

void BSTDispose (tBSTNodePtr *RootPtr) {	
/*   ----------
** Zru�� cel� bin�rn� vyhled�vac� strom a korektn� uvoln� pam�.
**
** Po zru�en� se bude BVS nach�zet ve stejn�m stavu, jako se nach�zel po
** inicializaci. Tuto funkci implementujte rekurzivn� bez deklarov�n� pomocn�
** funkce.
**/
	if(*RootPtr != NULL)
    {
        BSTDispose(&(*RootPtr)->LPtr);      //prechod do lava
        BSTDispose(&(*RootPtr)->RPtr);      //prechod do prava
        free(*RootPtr);                     //odstranim uzol, cistim pamat
        *RootPtr = NULL;
    }
}

/* konec c401.c */

