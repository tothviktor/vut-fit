-- cpu.vhd: Simple 8-bit CPU (BrainFuck interpreter)
-- Copyright (C) 2014 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): xtothv01, Viktor Tóth, 2014.12.19
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(12 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (0) / zapis (1)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

   signal CNT_increment		: std_logic;
   signal CNT_decrement		: std_logic;
   signal CNT_register		: std_logic;
   signal CNT_Value			: std_logic_vector (12 downto 0);
   
   signal PC_increment		: std_logic;
   signal PC_decrement		: std_logic;
   signal PC_register		: std_logic_vector (11 downto 0);
   
   signal PTR_increment		: std_logic;
   signal PTR_decrement		: std_logic;
   signal PTR_register		: std_logic_vector (7 downto 0);
   
   signal MX1_selector		: std_logic := '1';
   signal MX2_selector		: std_logic_vector (1 downto 0);
   
   signal CNT_is_zero		: std_logic;

   type fsm_state is (idle, fetch, fetch1, incPtr0, incPtr1, decPtr0, decPtr1, incVal0, incVal1, incVal2, decVal0, decVal1, decVal2,
					print0, print1, print2, read0, read1, read2, INST_LEFT0, INST_LEFT1, INST_RIGHT0, INST_RIGHT1, HALT,
					B_WHILE0, B_WHILE1, B_WHILE2, B_WHILE3, B_WHILE4, E_WHILE0, E_WHILE1, E_WHILE2, E_WHILE3, E_WHILE4, E_WHILE5);
					
   signal instruction 		: fsm_state := idle;
   signal pState 			: fsm_state := idle;
   signal nState 			: fsm_state := idle;
   
begin
	
	-- register pre vnorene cykli-------------------------------------------
	CNT : process(RESET, CLK)
	begin
		if (RESET = '1') then					-- reset
			CNT_Value <= ( others => '0');
		elsif (RESET = '0') and (CLK'event) and (CLK = '1') then
			if (CNT_increment = '1') then
				CNT_Value <= CNT_Value + 1;
			elsif (CNT_decrement = '1') then
				CNT_Value <= CNT_Value - 1;
			end if;
			if (CNT_Value = "0000000000000") then
				CNT_register <= '1';
			else
				CNT_register <= '0';
			end if;
		end if;
	end process;
	
	-- PC Program Counter >/<----------------------------------------------
	PC : process(RESET, CLK)					
	begin
		if (RESET = '1') then						-- reset
			PC_register <= (others => '0');
		elsif (CLK'event) and (CLK = '1') then		-- nabezna hrana CLK
			if (PC_decrement = '1') then      		
				PC_register <= PC_register - 1;		-- dekrementujem obsah PC registru
			elsif (PC_increment = '1') then   		
				PC_register <= PC_register + 1;		-- inkrementujem obsah PC registru
			end if;
		end if;
	end process;

	-- while cyklus ------------------------------------------------------
	PTR : process(RESET, CLK)
	begin
		if (RESET = '1') then						-- reset
			PTR_register <= (others => '0');
		elsif (CLK'event) and (CLK = '1') then		-- nabezna hrana CLK
			if (PTR_decrement = '1') then
					PTR_register <= PTR_register - 1;	-- dekrementujem obsah PC registru	
			elsif (PTR_increment = '1') then				
					PTR_register <= PTR_register + 1;	-- inkrementujem obsah PC registru
			end if;
		end if;
	end process;
	
	
	-- CNT je nula -------------------------------------------------------
	CNTisZero : process(CLK)
	begin
		 if(DATA_RDATA = "00000000") then
			CNT_is_zero <= '1';
		 else
			CNT_is_zero <= '0';
		 end if;
	end process;
	
	-- MX1 multiplexor 1 --------------------------------------------------
	MX1 : process(CLK)
	begin
		case MX1_selector is
			when '1' 	=> DATA_ADDR <= '0' & PC_register;
			when '0' 	=> DATA_ADDR <= "10000" & PTR_register;
			when others => DATA_ADDR <= "10000" & PTR_register;
		end case;
	end process;
	
	-- MX2 multiplexor 2 --------------------------------------------------
	MX2 : process(CLK)
	begin
		case MX2_selector is
			when "00" 	=> DATA_WDATA <= IN_DATA;
			when "01" 	=> DATA_WDATA <= DATA_RDATA - 1;
			when "10" 	=> DATA_WDATA <= DATA_RDATA + 1;
			when others	=> DATA_WDATA <= DATA_RDATA;
		end case;
	end process;

	-- Dekoder instrukci -----------------------------------------------------
	instructionDecoder: process(DATA_RDATA)
	begin
		case (DATA_RDATA) is
			when X"3E" => instruction <= incPtr0;    	-- " > " 		
			when X"3C" => instruction <= decPtr0;      	-- " < " 
			when X"2B" => instruction <= incVal0;	   	-- " + " 
			when X"2D" => instruction <= decVal0;	   	-- " - " 
			when X"5B" => instruction <= B_WHILE0;	   	-- " [ " 
			when X"5D" => instruction <= E_WHILE0;      -- " ] " 
			when X"2E" => instruction <= print0;	   	-- " . " 
			when X"2C" => instruction <= read0;		   	-- " , " 
			when X"00" => instruction <= HALT;        	-- " null " 
			when others => instruction <= fetch;        -- " biely/nedefinovany znak ... PRESKOC
		end case;
	end process;

	-- FSM present state -----------------------------------------------------
	FSM_present_state : process(RESET, CLK)
	begin
		if (RESET = '1') then
			pState <= idle;
		elsif (CLK'event) and (CLK ='1') then
			if (EN = '1') then
				pState <= nState;
			end if;
		end if;
	end process;

	-- FSM next state logic, output logic --------------------------------
	konecny_automat : process(pState, OUT_BUSY, IN_VLD, CNT_register)
	begin
		CNT_increment		<= '0';
		CNT_decrement		<= '0';
		PC_increment		<= '0';
		PC_decrement		<= '0';
		PTR_increment		<= '0';
		PTR_decrement		<= '0';
		MX1_selector		<= '1';
		MX2_selector		<= (others => '0');
		IN_REQ				<= '0';
		OUT_WE				<= '0';
		DATA_RDWR			<= '0';
		DATA_EN				<= '0';
		
		case pState is

			when idle =>		DATA_EN <= '1';
								nState <= fetch;

			when fetch =>		DATA_EN <= '1';
								nState <= fetch1;

			when fetch1 =>		nState <= instruction;
								PC_increment <= '1';
								PC_decrement <= '0'; 
								
			when incPtr0 =>		PTR_decrement <= '1';
								nState <= incPtr1;

			when incPtr1 =>		DATA_EN <= '1';
								nState <= fetch;

			when decPtr0 =>		PTR_increment <= '1';
								nState <= decPtr1;

			when decPtr1 =>		DATA_EN <= '1';
								nState <= fetch;
								
			when incVal0 =>		MX1_selector <= '0';
								MX2_selector <= "10";
								DATA_EN <= '1';
								nState <= incVal1;

			when incVal1 =>		MX1_selector <= '0';
								MX2_selector <= "10";
								DATA_EN <= '1';
								DATA_RDWR <= '1';
								nState <= incVal2;

			when incVal2 =>		DATA_EN <= '1';
								nState <= fetch;

			when decVal0 =>		MX1_selector <= '0';
								MX2_selector <= "01";
								DATA_EN <= '1';
								nState <= decVal1;

			when decVal1 =>		MX1_selector <= '0';
								MX2_selector <= "01";
								DATA_EN <= '1';
								DATA_RDWR <= '1';
								nState <= decVal2;

			when decVal2 =>		DATA_EN <= '1';
								nState <= fetch;					

			when B_WHILE0 =>	CNT_increment <= '1';
								DATA_EN <= '1';
								MX1_selector <= '0';
								nState <= B_WHILE1;

			when B_WHILE1 =>	DATA_EN <= '1';
								MX1_selector <= '0';
								nState <= B_WHILE2;

			when B_WHILE2 =>	if (CNT_is_zero = '1') then
									nState <= INST_RIGHT0;
								else
									DATA_EN <= '1';
									CNT_decrement <= '1';
									nState <= fetch;
								end if;

			when B_WHILE3 =>	DATA_EN <= '1';
								nState <= B_WHILE4;

			when B_WHILE4 =>	if (instruction = B_WHILE0) then
									CNT_increment <= '1';
								elsif (instruction = E_WHILE0) then
									CNT_decrement <= '1';
								else
									if (CNT_register = '1') then
										DATA_EN <= '1';
										nState <= fetch;
									else
										nState <= INST_RIGHT0;
									end if;
								end if;
								
			when INST_RIGHT0 =>	PC_increment <= '1';
								PC_decrement <= '0'; 
								nState <= INST_RIGHT1;

			when INST_RIGHT1 =>	DATA_EN <= '1';
								nState <= B_WHILE3;

			when E_WHILE0 =>	CNT_increment <= '1';
								DATA_EN <= '1';
								PC_decrement <= '1';
								PC_increment <= '0';
								MX1_selector <= '0';
								nState <= E_WHILE1;

			when E_WHILE1 =>	DATA_EN <= '1';
								MX1_selector <= '0';
								nState <= E_WHILE2;

			when E_WHILE2 =>	if (CNT_is_zero = '1') then
									DATA_EN <= '1';
									PC_increment <= '1';
									PC_decrement <= '0';      
									nState <= fetch;
								else
									nState <= INST_LEFT0;
								end if;

			when E_WHILE3 =>	DATA_EN <= '1';
								nState <= E_WHILE4;

			when E_WHILE4 =>	if (instruction = B_WHILE0) and (CNT_register = '0') then
									CNT_decrement <= '1';
								elsif (instruction = E_WHILE0) then
									CNT_increment <= '1';
								else
									if (CNT_register = '1') then
										PC_increment <= '1';
										PC_decrement <= '0';    
										nState <= E_WHILE5;
									else
										nState <= INST_LEFT0;
									end if;
								end if;

			when E_WHILE5 =>	DATA_EN <= '1';
								nState <= fetch;

			when INST_LEFT0 =>	PC_decrement <= '1';
								PC_increment <= '0';
								nState <= INST_LEFT1;

			when INST_LEFT1 =>	DATA_EN <= '1';
								nState <= E_WHILE3;

			when print0 =>		MX1_selector <= '0';
								DATA_EN <= '1';
								if (OUT_BUSY = '1') then
									nState <= print0;
								else
									nState <= print1;
								end if;

			when print1 =>		MX1_selector <= '0';
								DATA_EN <= '1';
								OUT_DATA <= DATA_RDATA;
								OUT_WE <= '1';
								nState <= print2;

			when print2 =>		DATA_EN <= '1';
								nState <= fetch;

			when read0 =>		MX1_selector <= '0';
								IN_REQ <= '1';
								MX2_selector <= "00";
								DATA_RDWR <= '1';
								nState <= read1;

			when read1 =>		if (IN_VLD = '1') then
									MX1_selector <= '0';
									IN_REQ <= '1';
									MX2_selector <= "00";
									DATA_RDWR <= '1';
									DATA_EN <= '1';
									nState <= read2;
								else
									MX1_selector <= '0';
									IN_REQ <= '1';
									MX2_selector <= "00";
									DATA_RDWR <= '1';
									nState <= read1;
								end if;

			when read2 =>		DATA_EN <= '1';
								nState <= fetch;

			when HALT =>		nState <= HALT;
	
			when others =>		DATA_EN <= '1';
								nState <= fetch;
		end case;
	end process konecny_automat;
end behavioral;


 
