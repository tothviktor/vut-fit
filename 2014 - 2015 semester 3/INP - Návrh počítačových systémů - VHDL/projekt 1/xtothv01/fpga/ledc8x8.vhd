-- Projekt INP1, Viktor Tóth, xtothv01, 10.10.2014

library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_unsigned.all;
	use IEEE.std_logic_arith.all;

	entity ledc8x8 is
		port (
   			SMCLK, RESET: in std_logic;
   			ROW, LED: out std_logic_vector(0 to 7)
			);
	end ledc8x8;

	architecture spravanie of ledc8x8 is
    	signal ce: std_logic;
    	signal cisloRiadku,iniciala: std_logic_vector(7 downto 0) := "01111111";
    	signal pocitadlo: std_logic_vector(7 downto 0) := "00000000";
		begin
     		process (SMCLK,RESET)
     			begin
         			if (RESET = '1') then
            			pocitadlo <= "00000000";
         			elsif (SMCLK'event and SMCLK = '1') then
            			pocitadlo <= pocitadlo + 1;
						if pocitadlo (7 downto 0) = "11111111" then
							ce <= '1';
						else
							ce <= '0';
	    			end if;
         		end if;
      		end process;
			process (RESET,SMCLK,ce,cisloRiadku)
    			begin
      				if (RESET = '1') then
          				cisloRiadku <= "10000000";
      				elsif (SMCLK'event and SMCLK = '1' and ce = '1') then
          				cisloRiadku <= cisloRiadku(0) & cisloRiadku(7 downto 1);
      				end if;
      				ROW <= cisloRiadku;
    		end process;
			dec: process (cisloRiadku) begin
				case (cisloRiadku) is
					when "10000000" => iniciala <= "11101110";
    				when "01000000" => iniciala <= "11110101";
					when "00100000" => iniciala <= "11110101";
					when "00010000" => iniciala <= "11111011";
					when "00001000" => iniciala <= "00000111";
					when "00000100" => iniciala <= "11011111";
					when "00000010" => iniciala <= "11011111";
					when "00000001" => iniciala <= "11011111";
					when others     => iniciala <= "11111111";
				end case;
			end process;
			ROW <= cisloRiadku;
			LED <= iniciala;
	end spravanie;
