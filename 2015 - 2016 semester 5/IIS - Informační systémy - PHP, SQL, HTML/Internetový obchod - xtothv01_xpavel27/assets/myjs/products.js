function changeboxes()
{
  if($('#product_type').val() == 1) 
  {
    $('#hardware').show();
    $('#hardwareC').show();
    $('#software').hide();
    $('#softwareC').hide();
  } 
  else 
  {
    $('#software').show();
    $('#softwareC').show();
    $('#hardware').hide();
    $('#hardwareC').hide();
  } 
}

function activaTab(tab)
{
  $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};