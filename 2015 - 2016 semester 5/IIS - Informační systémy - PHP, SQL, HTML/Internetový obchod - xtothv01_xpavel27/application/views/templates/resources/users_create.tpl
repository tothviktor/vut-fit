  <!-- CREATE USER PANEL -->
  <div id="create" class="tab-pane fade">
  <h3>Create new user</h3>
    
  <!-- CREATE USER FORM -->
  <form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/users/createNewUser">
  <div class="form-group">
  <div class="has-error">
    <p class="bg-danger" style="padding: 10px 10px 10px 10px">
      <label for="user_login">Login:</label>
      <input type="text" class="form-control" id="user_login" placeholder="Login" name="user_login" value="{$inputdata.login}"> 
      <label for="user_password">Password:</label>
      <input type="text" class="form-control" id="user_password" placeholder="Password" name="user_password" value="{$inputdata.password}">
    </p>
  </div>

  <p class="bg-warning" style="padding: 10px 10px 10px 10px">
  <label for="user_name">Name:</label>
  <input type="text" class="form-control" id="user_name" placeholder="Name" name="user_name" value="{$inputdata.name}"> 
  <label for="user_surname">Surname:</label>
  <input type="text" class="form-control" id="user_surname" placeholder="Surname" name="user_surname" value="{$inputdata.surname}"> 
  <br><br>
  <label for="user_mail">Mail:</label>
  <input type="text" class="form-control" id="user_mail" placeholder="Mail" name="user_mail" value="{$inputdata.mail}">
  <label for="user_telephone">Telephone:</label>
  <input type="text" class="form-control" id="user_telephone" placeholder="Telephone" name="user_telephone" value="{$inputdata.telephone}">
  <br><br>
  <label for="user_address">Address:</label>
  <input type="text" class="form-control" id="user_address" placeholder="Address" name="user_address" value="{$inputdata.address}"> 
  </p>
  
  <!-- PERMISSIONS SELECTBOX -->
  <p class="bg-info" style="padding: 10px 10px 10px 10px">
  <label for="user_permission">Select permissions:</label>
  <select class="form‐control" id="user_permission" name="user_permission">
  <option value="0">Select permissions</option>
  {foreach from=$permissions item=permission}
    <option value="{$permission.ID}" {if $inputdata.permission_id == $permission.ID}selected{/if}>{$permission.NAME}</option>
  {/foreach}
  </select>
  </p>
  
  <!-- SUBMIT BUTTON -->
  <button type="submit" class="btn btn-primary">Submit</button>
  </div>
  </form>
  </div>