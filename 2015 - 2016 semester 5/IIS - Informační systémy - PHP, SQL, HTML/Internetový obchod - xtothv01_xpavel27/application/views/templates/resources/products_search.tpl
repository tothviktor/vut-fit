<!-- PRODUCT SEARCH PANEL -->
<div id="search" class="tab-pane fade in active"> 
<h2>Search for the product</h2>
  
<!-- SEARCH FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/products/show/1">
<div class="form-group">
<input type="text" class="form-control" id="seach_product_id" placeholder="Product id" name="seach_product_id" value="{$SearchByID}"> 
<input type="text" class="form-control" id="seach_product_name" placeholder="Product name" name="seach_product_name" value="{$SearchByName}">
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>

<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>Name</th>
<th>Brand</th>
<th>Count</th>
<th>Price</th>
<th>VAT</th>
<th>CATEGORY</th>
<th>COMPANY</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
{foreach from=$products item=product}
{if $product.STOCK == "Y"}
 	<tr class="success">
{else}
  <tr class="danger">
{/if}
  
<td>{$product.ID}</td>
<td>{$product.NAME}</td>
<td>{$product.BRAND}</td>
<td>{$product.COUNT}</td>
<td>{$product.PRICE}</td>
<td>{$product.VAT}</td>
<td>{$product.CATEGORY}</td>
<td>{$product.COMPANY}</td>

<!-- SEARCH TABLE OPTIONS -->
<td width="10%">
{if !$ismerch && !$isadm}
	<a class="glyphicon glyphicon-download-alt" aria-hidden="true" href="{$indexpath}/trashes/addItem/{$product.ID}" ></a>&nbsp;&nbsp;
{/if}
<a class="glyphicon glyphicon-search" aria-hidden="true" href="{$indexpath}/products/productDetail/{$product.ID}"></a>&nbsp;&nbsp;
{if $ismerch || $isadm}
	<a class="glyphicon glyphicon-trash" aria-hidden="true" href="{$indexpath}/products/deleteProduct/{$product.ID}" ></a>
{/if}
</td>
</tr>
{/foreach}

</tbody>
</table>
</div>
</div>
