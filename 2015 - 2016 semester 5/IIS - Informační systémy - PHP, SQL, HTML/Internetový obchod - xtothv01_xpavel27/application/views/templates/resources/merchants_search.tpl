<!-- SEARCH PANEL -->
<div id="search" class="tab-pane fade in active"> 
<h2>Search for the user</h2>

<!-- SEARCH FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/merchants/show/1">
<div class="form-group">
<input type="text" class="form-control" id="seach_merchant_id" placeholder="Merchant id" name="seach_merchant_id" value="{$SearchByID}"> 
<input type="text" class="form-control" id="seach_merchant_company" placeholder="Company name" name="seach_merchant_company" value="{$SearchByName}">
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>

<!-- SEARCH TABLE HEAD -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>ACM</th>
<th>COMPANY</th>
<th>DELIVERY</th>
<th>WEBSITE</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
{foreach from=$merchants item=merchant}
<tr class="success">
<td>{$merchant.ID}</td>
<td>{$merchant.NAME} {$merchant.SURNAME} ({$merchant.LOGIN})</td>
<td>{$merchant.COMPANY}</td>
<td>{$merchant.DELIVERY}</td>
<td>{$merchant.WEBSITE}</td>

<!-- OPTIONS -->
<td width="5%">
<a class="glyphicon glyphicon-search" aria-hidden="true" href="{$indexpath}/users/userDetail/{$merchant.USER_ID}"></a>&nbsp;&nbsp;
<a class="glyphicon glyphicon-trash" aria-hidden="true" href="{$indexpath}/merchants/deleteMerchant/{$merchant.ID}" ></a>
</td>
</tr>
{/foreach}

</tbody>
</table>
</div>
</div>