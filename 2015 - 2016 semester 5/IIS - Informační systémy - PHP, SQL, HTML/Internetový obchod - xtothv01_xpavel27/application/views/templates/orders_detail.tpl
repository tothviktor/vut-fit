<!-- ORDER DETAILS HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- HEADER -->
<h2>Order details: {$order_id} (User: {$order_info.LOGIN})</h2>
<div style="width:100%">

<!-- LEFT INFO -->
<div class="col-xs-6 col-md-3" style="float: left; width:23%">

	<!-- IMAGE -->
	<a href="#" class="thumbnail">
	<img src="{$basepath}/images/products/1.jpeg" alt="...">
	</a>

	<!-- EXTRACT POST -->
	<div class="col-md-6">
		<form method="post" accept-charset="utf-8" action="{$indexpath}/orders/orderDetail/{$order_id}">
		<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
		</form>
	</div>

	<!-- STATUS -->
	<div class="col-md-6">
		{if $order_info.EXPANDED == "Y"}
		<button class="btn btn-success">Expanded</button>
		{elseif $order_info.PAYED == "Y"}
		<button class="btn btn-primary">Payed</button>
		{else}
		<button class="btn btn-danger">Waiting</button>
		{/if}
	</div>
</div>
   
<!-- SECOND INFO PANEL 1 -->
<div class="panel panel-default" style="float: right; width:77%;">
	
	<!-- CUSTOMER INFO -->
	<div class="panel-heading"><b>Customer</b></div>
	<div class="panel-body">
	<b>Name:&nbsp; </b>{$order_info.NAME}<br>
	<b>Surname:&nbsp; </b>{$order_info.SURNAME}<br>
	<b>Email:&nbsp; </b>{$order_info.MAIL}<br>
	<b>Telephone:&nbsp; </b>{$order_info.TELEPHONE}<br>
	<b>Address:&nbsp; </b>{$order_info.ADDRESS}<br>
	</div>

	<!-- SECOND INFO PANEL 2 -->
	<div class="panel-heading"><b>Status</b></div>
	<div class="panel-body">
	<b>Expanded:&nbsp; </b>{$order_info.EXPANDED}<br>
	<b>Payed:&nbsp; </b> {$order_info.PAYED}<br>
	<b>Employee:&nbsp; </b> {$order_info.EMPLOYEE_ID}<br>
	</div>

	<!-- SECOND INFO PANEL 3 -->
	<div class="panel-heading"><b>Price</b></div>
	<div class="panel-body">
	<b>Price:&nbsp; </b> {$order_info.PRICE} <br>
	</div>
</div>
</div>

<!-- INFO TABLE HEADER -->
<div class="span3">
	<table class="table">
	<thead>
	<tr>
	<th>NAME</th>
	<th>PRICE</th>
	<th>VAT</th>
	<th>COMPANY</th>
	<th>DETAILS</th>
	</tr>
</thead>

<!-- INFO TABLE BODY -->
<tbody>
	{foreach from=$items item=item}
	{if $merchant.COMPANY == $item.COMPANY}
	<tr class="success">
	{else}
	<tr class="danger">
	{/if}
	<td>{$item.NAME}</td>
	<td>{$item.PRICE}</td>
	<td>{$item.VAT}</td>
	<td>{$item.COMPANY}</td>

	<!-- OPTIONS -->
	<td width="5%">
	<a class="glyphicon glyphicon-search" aria-hidden="true" href="{$indexpath}/products/productDetail/{$item.PRODUCT_ID}"></a>
	{if $isadm || $ismerch}
	&nbsp;&nbsp;
	<a class="glyphicon glyphicon-trash" aria-hidden="true" href="{$indexpath}/orders/deleteItem/{$order_id}/{$item.ITEM_ID}" >
	{/if}
	</a>
	</td>
	</tr>
	{/foreach}
</tbody>

</table>
</div>
</div>
</div>
</body>
</html>