<!-- FEEDBACKS HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>

</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">
  	 
<!-- IMAGE -->
<img src="{$basepath}/images/feedbacks.jpg" alt="..." class="img-rounded" style="width:100%">
<br>

<!-- ERRORS -->
<br>
<div class="alert alert-danger" role="alert" style="display:{if $errors == ""}none{/if}">
{$errors} 
</div>

<!-- SEARCH FEEDBACK -->
<h2>Search for the category</h2>  

<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/categories/show">
<div class="form-group">
<input type="text" class="form-control" id="seach_category_name" placeholder="Category name" name="seach_category_name" value="{$SearchByName}"> 

<select class="form‐control" id="seach_category_type" name="seach_category_type">
          <option value="0">Select type</option>
          <option value="S" {if $SearchByType == "S"}selected{/if}>Software</option>
          <option value="H" {if $SearchByType == "H"}selected{/if}>Hardware</option>
</select>

</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
<button type="submit" class="btn btn-warning" name="Create" value="1">Create</button>
</form>
  
<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>Name</th>
<th>Type</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
{foreach from=$categories item=category}
{if $category.TYPE == "S"}
<tr class="info">
{else}
<tr class="warning">
{/if}

<td>{$category.ID}</td>     
<td>{$category.NAME}</td>          
<td>{$category.TYPE}</td>      
  
<!-- OPTIONS -->
<td width="5%">
<a class="glyphicon glyphicon-trash" aria-hidden="true" href="{$indexpath}/categories/deleteCategory/{$category.ID}" ></a>
</td>
</tr>
{/foreach}
</tbody>
</table>

<!-- FINNISH -->
</div>
</div>
</div>
</div>
</body>
</html>