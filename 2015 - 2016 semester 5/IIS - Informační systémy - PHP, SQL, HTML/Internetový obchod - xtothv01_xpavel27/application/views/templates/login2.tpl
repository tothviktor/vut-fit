<!-- LOGIN HEADER -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
    <link href="{$basepath}/mycss/login.css" rel="stylesheet">
    <script src="{$basepath}/js/jquery.backstretch.min.js"></script>
     <link rel="stylesheet" href="{$basepath}/css/noAccess.css">
    <link rel="stylesheet" href="{$basepath}/mycss/noAccess.css">
</head>

<div class="top-content">
<div class="container">
<div class="row" id="aftermenuarray">
    
    <!-- Right head text -->
    <div class="col-md-6 text" id="rightheadtext">   
        
        <!-- Head header -->
        <h1><strong>Webstore</strong> login</h1>
        
        <!-- Head description -->
        <div class="description">
            <p> Please login to our web store!</p>
        </div>

    </div>
    
    <!-- Left login elements -->
    <div class="col-md-6 form-box" id="leftloginelems">
        
   <div class="account-wall">
    <img class="profile-img" src="{$basepath}/images/login.png"alt="">

    {if $error == "1"}
        <p class="bg-danger" style="text-align:center">User not found!</p>
    {elseif $error == "2"}
        <p class="bg-warning" style="text-align:center">User already exists!</p>
    {elseif $error == "3"}
        <p class="bg-danger" style="text-align:center">Wrong informations entered!</p>
    {elseif $error == "4"}
        <p class="bg-primary" style="text-align:center">User successfully created!</p>
    {/if}
    <form class="form-signin" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/login/index/0">
    <input type="text" class="form-control" placeholder="Login" id="login_user" name="login_user" required autofocus>
    <input type="password" class="form-control" placeholder="Password" id="login_password" name="login_password"required>
    <button class="btn btn-lg btn-success btn-block" type="submit" name="btn_login">Sign in</button>
    <button class="btn btn-lg btn-primary btn-block" type="submit" name="btn_create" value="Create">Create account</button>
    </form>
    </div>

    </div>
</div>
</div>
</div>

{literal}<script>

jQuery(document).ready(function() {
    
  $.backstretch([
    "{/literal}{$basepath}{literal}/images/login/1.jpg",
    "{/literal}{$basepath}{literal}/images/login/2.jpg",
    "{/literal}{$basepath}{literal}/images/login/3.jpg",
    "{/literal}{$basepath}{literal}/images/login/4.jpg",
    "{/literal}{$basepath}{literal}/images/login/5.jpg"     
  ], {duration: 10000, fade:1000});
    
});

</script>{/literal}
</body>
</html>