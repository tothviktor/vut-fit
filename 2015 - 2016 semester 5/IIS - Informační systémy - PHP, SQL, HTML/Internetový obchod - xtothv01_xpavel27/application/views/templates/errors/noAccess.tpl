<!-- LOGIN HEADER -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>

    <script src="{$basepath}/js/jquery.backstretch.min.js"></script>
     <link rel="stylesheet" href="{$basepath}/css/noAccess.css">
    <link rel="stylesheet" href="{$basepath}/mycss/noAccess.css">
</head>

<div class="top-content">
<div class="container">
<div class="row" id="aftermenuarray">
    
    <!-- Right head text -->
    <div class="col-md-12 text">   
        
        <!-- Head header -->
        <h1><strong>Access</strong> denied!</h1>
        
        <!-- Head description -->
        <div class="description">
            <p> Something went wrong! Try to log in again... Thanks</p>
        </div>

    </div>
   
</div>
</div>
</div>

{literal}<script>

jQuery(document).ready(function() {
    
  $.backstretch([
    "{/literal}{$basepath}{literal}/images/login/1.jpg",
    "{/literal}{$basepath}{literal}/images/login/2.jpg",
    "{/literal}{$basepath}{literal}/images/login/3.jpg",
    "{/literal}{$basepath}{literal}/images/login/4.jpg",
    "{/literal}{$basepath}{literal}/images/login/5.jpg"     
  ], {duration: 10000, fade:1000});
    
});

</script>{/literal}
</body>
</html>