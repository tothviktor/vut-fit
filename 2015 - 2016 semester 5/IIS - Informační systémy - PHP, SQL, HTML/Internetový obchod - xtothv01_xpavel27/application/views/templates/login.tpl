<!-- LOGIN HEADER -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <link href="{$basepath}/mycss/login.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
</head>

<body>

<!-- LOGIN FORM -->
<div class="container">

<div class="row">
<div class="col-sm-6 col-md-4 col-md-offset-4">

    <div class="account-wall">
    <img class="profile-img" src="{$basepath}/images/login.png"alt="">

    {if $error == "1"}
        <p class="bg-danger" style="text-align:center">User not found!</p>
    {elseif $error == "2"}
        <p class="bg-warning" style="text-align:center">User already exists!</p>
    {elseif $error == "3"}
        <p class="bg-danger" style="text-align:center">Wrong informations entered!</p>
    {elseif $error == "4"}
        <p class="bg-primary" style="text-align:center">User successfully created!</p>
    {/if}
    <form class="form-signin" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/login/index/0">
    <input type="text" class="form-control" placeholder="Login" id="login_user" name="login_user" required autofocus>
    <input type="password" class="form-control" placeholder="Password" id="login_password" name="login_password"required>
    <button class="btn btn-lg btn-success btn-block" type="submit" name="btn_login">Sign in</button>
    <button class="btn btn-lg btn-primary btn-block" type="submit" name="btn_create" value="Create">Create account</button>
    </form>
    </div>

</div>

</div>
</body>
</html>