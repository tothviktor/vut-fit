<!-- USER DETAIL HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
    <script src="{$basepath}/myjs/users_detail.js"></script>
</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- LEFT INFO -->
<h2>User details: {$user_info.LOGIN} (ID: {$user_id})</h2> <br>
<div style="width:100%">
<div class="col-xs-6 col-md-3" style="float: left; width:25%">
<a href="#" class="thumbnail">
<img src="{$basepath}/images/products/1.jpeg" alt="...">
</a>
  
<!-- EXTRACT POST -->
<div class="col-md-6">
<form method="post" accept-charset="utf-8" action="{$indexpath}/users/userDetail/{$user_id}">
<button type="submit" class="btn btn-info" name="Search" value="1">Extract info</button>
</form>
</div>

<!-- MODIFY -->
  <div class="col-md-6">
    
    <button class="btn btn-warning" name="Edit" id="toogle">Modify!</button>

  
  </div>
</div>

<!-- RIGHT PANEL -->  
<div id="getInfoShow"> 
<div class="panel panel-default" style="float: right; width:75%;">
<div class="panel-heading"><b>Informations</b></div>
<div class="panel-body">
<b>Name:&nbsp; </b>{$user_info.PERS_NAME}<br>
<b>Surname:&nbsp; </b> {$user_info.SURNAME}<br>
<b>Mail:&nbsp; </b> {$user_info.MAIL}<br>
<b>Telephone:&nbsp; </b> {$user_info.TELEPHONE}<br>
<b>Address:&nbsp; </b> {$user_info.ADDRESS}<br>
<b>Permissions:&nbsp; </b> {$user_info.PERM_NAME}<br>
</div>
   
</div>
</div>

<div id="editInfoShow" style="display:none;">
<div style="width:75%;float:right" >
  
  <!-- HEAD -->
  <form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/users/editUser/{$user_id}/{$user_info.PERS_ID}">
  <div class="form-group">

  <!-- BASIC INFO -->
  <p class="bg-danger" style="padding: 10px 10px 10px 10px">

  <label for="edit_name">Name:</label>
  <input type="text" class="form-control" id="edit_name" placeholder="Name" name="edit_name" value="{$user_info.PERS_NAME}" > 
  
  <label for="edit_surname">Surname:</label>
  <input type="text" class="form-control" id="edit_surname" placeholder="Surname" name="edit_surname" value="{$user_info.SURNAME}"> 
  <br><br>

  <label for="edit_surname">Mail:</label>
  <input type="text" class="form-control" id="edit_mail" placeholder="Mail" name="edit_mail" value="{$user_info.MAIL}"> 

  <label for="edit_surname">Telephone:</label>
  <input type="text" class="form-control" id="edit_telephone" placeholder="Telephone" name="edit_telephone" value="{$user_info.TELEPHONE}">
  <br><br>

  <label for="edit_surname">Address:</label>
  <input type="text" class="form-control" id="edit_address" placeholder="Address" name="edit_address" value="{$user_info.ADDRESS}">
  </p>

  {if $isadm}
    <p class="bg-info" style="padding: 10px 10px 10px 10px">
    <label for="edit_permission">Select permissions:</label>
    <select class="form‐control" id="edit_permission" name="edit_permission">
    <option value="0">Select permissions</option>
    {foreach from=$permissions item=permission}
      <option value="{$permission.ID}" {if $user_info.PERM_NAME == $permission.NAME}selected{/if}>{$permission.NAME}</option>
    {/foreach}
    </select>
    </p>
  {/if}
  
  <button type="submit" class="btn btn-primary">Submit</button>

  </div>
  </form>
 
</div>
</div>

</div>
</div>
</div>
</div>
</body>
</html>