<html>
<head>
</head>
<body>

  <h2> Webshop Name Order </h2>
  <h2>Order details: {$order_id} ({$order_info.LOGIN})
  {if $order_info.EXPANDED == "Y"}
    <font color="green">Expanded</font>
  {elseif $order_info.PAYED == "Y"}
    <font color="orange">Payed</font>
  {else}
    <font color="red">Waiting</font>
    {/if}
  </h2>
 
  <font color="red"><b>Customer</b></font><br>
  <b>Name: </b>{$order_info.NAME}<br>
  <b>Surname: </b> {$order_info.SURNAME}<br>
  <b>Email: </b> {$order_info.MAIL}<br>
  <b>Telephone: </b> {$order_info.TELEPHONE}<br>
  <b>Address: </b> {$order_info.ADDRESS}<br><br>

  <font color="red"><b>Status</b></font><br>
  <b>Expanded: </b>{$order_info.EXPANDED}<br>
  <b>Payed: </b> {$order_info.PAYED}<br>
  <b>Employee: </b> {$order_info.EMPLOYEE_ID}<br><br>
  
  <font color="red"><b>Price</b></font><br>
  <b>Price: </b>{$order_info.PRICE}<br><br>

    <table border="1" style="width:100%">
    <thead>
      <tr>
        <th>NAME</th>
        <th>PRICE</th>
        <th>VAT</th>
      </tr>
    </thead>
    <tbody>
      {foreach from=$items item=item}
        <tr>
        <td>{$item.NAME}</td>
        <td>{$item.PRICE}</td>
        <td>{$item.VAT}</td>
        </tr>
        {/foreach}
    </tbody>
  </table>
</body>
</html>