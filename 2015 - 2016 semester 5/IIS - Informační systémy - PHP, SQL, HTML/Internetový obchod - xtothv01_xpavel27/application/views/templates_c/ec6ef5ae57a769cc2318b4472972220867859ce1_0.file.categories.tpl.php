<?php /* Smarty version 3.1.27, created on 2015-11-14 08:51:29
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/categories.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:205235810056476691b44221_10862773%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ec6ef5ae57a769cc2318b4472972220867859ce1' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/categories.tpl',
      1 => 1447519887,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205235810056476691b44221_10862773',
  'variables' => 
  array (
    'basepath' => 0,
    'errors' => 0,
    'indexpath' => 0,
    'SearchByName' => 0,
    'SearchByType' => 0,
    'categories' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56476691bcb749_72620791',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56476691bcb749_72620791')) {
function content_56476691bcb749_72620791 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '205235810056476691b44221_10862773';
?>
<!-- FEEDBACKS HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>

</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">
  	 
<!-- IMAGE -->
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/feedbacks.jpg" alt="..." class="img-rounded" style="width:100%">
<br>

<!-- ERRORS -->
<br>
<div class="alert alert-danger" role="alert" style="display:<?php if ($_smarty_tpl->tpl_vars['errors']->value == '') {?>none<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
 
</div>

<!-- SEARCH FEEDBACK -->
<h2>Search for the category</h2>  

<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/categories/show">
<div class="form-group">
<input type="text" class="form-control" id="seach_category_name" placeholder="Category name" name="seach_category_name" value="<?php echo $_smarty_tpl->tpl_vars['SearchByName']->value;?>
"> 

<select class="form‐control" id="seach_category_type" name="seach_category_type">
          <option value="0">Select type</option>
          <option value="S" <?php if ($_smarty_tpl->tpl_vars['SearchByType']->value == "S") {?>selected<?php }?>>Software</option>
          <option value="H" <?php if ($_smarty_tpl->tpl_vars['SearchByType']->value == "H") {?>selected<?php }?>>Hardware</option>
</select>

</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
<button type="submit" class="btn btn-warning" name="Create" value="1">Create</button>
</form>
  
<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>Name</th>
<th>Type</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$foreach_category_Sav = $_smarty_tpl->tpl_vars['category'];
?>
<?php if ($_smarty_tpl->tpl_vars['category']->value['TYPE'] == "S") {?>
<tr class="info">
<?php } else { ?>
<tr class="warning">
<?php }?>

<td><?php echo $_smarty_tpl->tpl_vars['category']->value['ID'];?>
</td>     
<td><?php echo $_smarty_tpl->tpl_vars['category']->value['NAME'];?>
</td>          
<td><?php echo $_smarty_tpl->tpl_vars['category']->value['TYPE'];?>
</td>      
  
<!-- OPTIONS -->
<td width="5%">
<a class="glyphicon glyphicon-trash" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/categories/deleteCategory/<?php echo $_smarty_tpl->tpl_vars['category']->value['ID'];?>
" ></a>
</td>
</tr>
<?php
$_smarty_tpl->tpl_vars['category'] = $foreach_category_Sav;
}
?>
</tbody>
</table>

<!-- FINNISH -->
</div>
</div>
</div>
</div>
</body>
</html><?php }
}
?>