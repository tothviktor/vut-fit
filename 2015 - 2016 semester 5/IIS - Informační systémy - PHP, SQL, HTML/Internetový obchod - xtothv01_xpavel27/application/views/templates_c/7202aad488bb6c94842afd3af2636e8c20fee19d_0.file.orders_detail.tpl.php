<?php /* Smarty version 3.1.27, created on 2015-11-14 09:18:14
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/orders_detail.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:81688484256476cd6ba26a4_20266805%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7202aad488bb6c94842afd3af2636e8c20fee19d' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/orders_detail.tpl',
      1 => 1445883809,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '81688484256476cd6ba26a4_20266805',
  'variables' => 
  array (
    'basepath' => 0,
    'order_id' => 0,
    'order_info' => 0,
    'indexpath' => 0,
    'items' => 0,
    'merchant' => 0,
    'item' => 0,
    'isadm' => 0,
    'ismerch' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56476cd6c48c64_81124480',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56476cd6c48c64_81124480')) {
function content_56476cd6c48c64_81124480 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '81688484256476cd6ba26a4_20266805';
?>
<!-- ORDER DETAILS HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- HEADER -->
<h2>Order details: <?php echo $_smarty_tpl->tpl_vars['order_id']->value;?>
 (User: <?php echo $_smarty_tpl->tpl_vars['order_info']->value['LOGIN'];?>
)</h2>
<div style="width:100%">

<!-- LEFT INFO -->
<div class="col-xs-6 col-md-3" style="float: left; width:23%">

	<!-- IMAGE -->
	<a href="#" class="thumbnail">
	<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/products/1.jpeg" alt="...">
	</a>

	<!-- EXTRACT POST -->
	<div class="col-md-6">
		<form method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/orders/orderDetail/<?php echo $_smarty_tpl->tpl_vars['order_id']->value;?>
">
		<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
		</form>
	</div>

	<!-- STATUS -->
	<div class="col-md-6">
		<?php if ($_smarty_tpl->tpl_vars['order_info']->value['EXPANDED'] == "Y") {?>
		<button class="btn btn-success">Expanded</button>
		<?php } elseif ($_smarty_tpl->tpl_vars['order_info']->value['PAYED'] == "Y") {?>
		<button class="btn btn-primary">Payed</button>
		<?php } else { ?>
		<button class="btn btn-danger">Waiting</button>
		<?php }?>
	</div>
</div>
   
<!-- SECOND INFO PANEL 1 -->
<div class="panel panel-default" style="float: right; width:77%;">
	
	<!-- CUSTOMER INFO -->
	<div class="panel-heading"><b>Customer</b></div>
	<div class="panel-body">
	<b>Name:&nbsp; </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['NAME'];?>
<br>
	<b>Surname:&nbsp; </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['SURNAME'];?>
<br>
	<b>Email:&nbsp; </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['MAIL'];?>
<br>
	<b>Telephone:&nbsp; </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['TELEPHONE'];?>
<br>
	<b>Address:&nbsp; </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['ADDRESS'];?>
<br>
	</div>

	<!-- SECOND INFO PANEL 2 -->
	<div class="panel-heading"><b>Status</b></div>
	<div class="panel-body">
	<b>Expanded:&nbsp; </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['EXPANDED'];?>
<br>
	<b>Payed:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['PAYED'];?>
<br>
	<b>Employee:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['EMPLOYEE_ID'];?>
<br>
	</div>

	<!-- SECOND INFO PANEL 3 -->
	<div class="panel-heading"><b>Price</b></div>
	<div class="panel-body">
	<b>Price:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['PRICE'];?>
 <br>
	</div>
</div>
</div>

<!-- INFO TABLE HEADER -->
<div class="span3">
	<table class="table">
	<thead>
	<tr>
	<th>NAME</th>
	<th>PRICE</th>
	<th>VAT</th>
	<th>COMPANY</th>
	<th>DETAILS</th>
	</tr>
</thead>

<!-- INFO TABLE BODY -->
<tbody>
	<?php
$_from = $_smarty_tpl->tpl_vars['items']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
	<?php if ($_smarty_tpl->tpl_vars['merchant']->value['COMPANY'] == $_smarty_tpl->tpl_vars['item']->value['COMPANY']) {?>
	<tr class="success">
	<?php } else { ?>
	<tr class="danger">
	<?php }?>
	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['NAME'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['PRICE'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['VAT'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['COMPANY'];?>
</td>

	<!-- OPTIONS -->
	<td width="5%">
	<a class="glyphicon glyphicon-search" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/productDetail/<?php echo $_smarty_tpl->tpl_vars['item']->value['PRODUCT_ID'];?>
"></a>
	<?php if ($_smarty_tpl->tpl_vars['isadm']->value || $_smarty_tpl->tpl_vars['ismerch']->value) {?>
	&nbsp;&nbsp;
	<a class="glyphicon glyphicon-trash" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/orders/deleteItem/<?php echo $_smarty_tpl->tpl_vars['order_id']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['item']->value['ITEM_ID'];?>
" >
	<?php }?>
	</a>
	</td>
	</tr>
	<?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
</tbody>

</table>
</div>
</div>
</div>
</body>
</html><?php }
}
?>