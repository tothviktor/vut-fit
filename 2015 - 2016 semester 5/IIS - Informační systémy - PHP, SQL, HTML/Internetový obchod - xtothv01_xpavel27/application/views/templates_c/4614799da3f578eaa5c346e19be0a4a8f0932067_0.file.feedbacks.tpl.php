<?php /* Smarty version 3.1.27, created on 2015-11-14 03:45:53
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/feedbacks.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:89199870256471ef1462964_91005123%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4614799da3f578eaa5c346e19be0a4a8f0932067' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/feedbacks.tpl',
      1 => 1447449745,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '89199870256471ef1462964_91005123',
  'variables' => 
  array (
    'basepath' => 0,
    'errors' => 0,
    'indexpath' => 0,
    'SearchByProduct' => 0,
    'isadm' => 0,
    'ismerch' => 0,
    'SearchByUser' => 0,
    'feedbacks' => 0,
    'feedback' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56471ef151d932_55049456',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56471ef151d932_55049456')) {
function content_56471ef151d932_55049456 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '89199870256471ef1462964_91005123';
?>
<!-- FEEDBACKS HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>

</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">
  	 
<!-- IMAGE -->
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/feedbacks.jpg" alt="..." class="img-rounded" style="width:100%">
<br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:<?php if ($_smarty_tpl->tpl_vars['errors']->value == '') {?>none<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
 
</div>

<!-- SEARCH FEEDBACK -->
<h2>Search for the feedback</h2>  

<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/feedbacks/show">
<div class="form-group">
<input type="text" class="form-control" id="seach_feedback_product" placeholder="Product name" name="seach_feedback_product" value="<?php echo $_smarty_tpl->tpl_vars['SearchByProduct']->value;?>
"> 
<?php if ($_smarty_tpl->tpl_vars['isadm']->value || $_smarty_tpl->tpl_vars['ismerch']->value) {?>
<input type="text" class="form-control" id="seach_feedback_user" placeholder="User name" name="seach_feedback_user" value="<?php echo $_smarty_tpl->tpl_vars['SearchByUser']->value;?>
">
<?php }?>
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>
  
<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>Product</th>
<th>User</th>
<th>Mark</th>
<th>Company</th>
<?php if (!$_smarty_tpl->tpl_vars['ismerch']->value) {?>
	<th>DETAILS</th>
<?php }?>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['feedbacks']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['feedback'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['feedback']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['feedback']->value) {
$_smarty_tpl->tpl_vars['feedback']->_loop = true;
$foreach_feedback_Sav = $_smarty_tpl->tpl_vars['feedback'];
?>
<?php if ($_smarty_tpl->tpl_vars['feedback']->value['MARK'] == "1" || $_smarty_tpl->tpl_vars['feedback']->value['MARK'] == "2") {?>
<tr class="danger">
<?php } elseif ($_smarty_tpl->tpl_vars['feedback']->value['MARK'] == "3") {?>
<tr class="info">
<?php } elseif ($_smarty_tpl->tpl_vars['feedback']->value['MARK'] > "4") {?>
<tr class="success">
<?php }?>

<td><?php echo $_smarty_tpl->tpl_vars['feedback']->value['ID'];?>
</td>     
<td><?php echo $_smarty_tpl->tpl_vars['feedback']->value['PRODUCT'];?>
</td>          
<td><?php echo $_smarty_tpl->tpl_vars['feedback']->value['USER'];?>
</td>      
<td><?php echo $_smarty_tpl->tpl_vars['feedback']->value['MARK'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['feedback']->value['COMPANY'];?>
</td>
  
<!-- OPTIONS -->
<?php if (!$_smarty_tpl->tpl_vars['ismerch']->value) {?>
	<td width="5%">
	<a class="glyphicon glyphicon-trash" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/feedbacks/deleteFeedback/<?php echo $_smarty_tpl->tpl_vars['feedback']->value['ID'];?>
" ></a>
	</td>
<?php }?>
</tr>
<?php
$_smarty_tpl->tpl_vars['feedback'] = $foreach_feedback_Sav;
}
?>
</tbody>
</table>

<!-- FINNISH -->
</div>
</div>
</div>
</div>
</body>
</html><?php }
}
?>