<?php /* Smarty version 3.1.27, created on 2015-11-14 02:56:11
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/products.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2635082995647134bf1e5f8_59110909%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d8b383b83a6e54743f2889b1e82cf5743ce9eaf' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/products.tpl',
      1 => 1445105618,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2635082995647134bf1e5f8_59110909',
  'variables' => 
  array (
    'basepath' => 0,
    'errors' => 0,
    'isadm' => 0,
    'ismerch' => 0,
    'pageid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5647134c04d1c8_15228422',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5647134c04d1c8_15228422')) {
function content_5647134c04d1c8_15228422 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2635082995647134bf1e5f8_59110909';
?>
<!-- PRODUCTS HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/products.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/myjs/products.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
</head>
<body>

<!-- SIDEBAR -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/products.jpg" alt="..." class="img-rounded" style="width:100%">
<br><br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:<?php if ($_smarty_tpl->tpl_vars['errors']->value == '') {?>none<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
 
</div>

<!-- NAVIGATION BAR -->  
<ul class="nav nav-tabs">
<li><a data-toggle="pill" href="#search">Find</a></li>
<?php if ($_smarty_tpl->tpl_vars['isadm']->value == true || $_smarty_tpl->tpl_vars['ismerch']->value == true) {?>
<li><a data-toggle="pill" href="#create">Create</a></li>
<?php }?>
</ul>

<!-- CONTENT -->
<div class="tab-content">
<?php echo $_smarty_tpl->getSubTemplate ('resources/products_search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php if ($_smarty_tpl->tpl_vars['isadm']->value == true || $_smarty_tpl->tpl_vars['ismerch']->value == true) {?>
<?php echo $_smarty_tpl->getSubTemplate ('resources/products_create.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
</div>
</div>
</div>

<!-- CONTENT SELECTOR + TYPE SELECTOR -->

<?php echo '<script'; ?>
>
 $("#product_type").change(function(){changeboxes();});
 $(document).ready(function(){
    
    <?php if ($_smarty_tpl->tpl_vars['pageid']->value == "1") {?> activaTab('search');
    <?php } else { ?> activaTab('create');
    <?php }?>
    
});
<?php echo '</script'; ?>
>


</body>
</html><?php }
}
?>