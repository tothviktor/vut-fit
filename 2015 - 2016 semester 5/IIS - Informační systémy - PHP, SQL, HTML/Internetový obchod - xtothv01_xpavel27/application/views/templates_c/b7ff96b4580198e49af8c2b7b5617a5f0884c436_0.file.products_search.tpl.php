<?php /* Smarty version 3.1.27, created on 2015-11-14 02:56:12
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/products_search.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:12951374595647134c0565a8_54938247%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b7ff96b4580198e49af8c2b7b5617a5f0884c436' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/products_search.tpl',
      1 => 1447449621,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12951374595647134c0565a8_54938247',
  'variables' => 
  array (
    'indexpath' => 0,
    'SearchByID' => 0,
    'SearchByName' => 0,
    'products' => 0,
    'product' => 0,
    'ismerch' => 0,
    'isadm' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5647134c0a1437_75814959',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5647134c0a1437_75814959')) {
function content_5647134c0a1437_75814959 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '12951374595647134c0565a8_54938247';
?>
<!-- PRODUCT SEARCH PANEL -->
<div id="search" class="tab-pane fade in active"> 
<h2>Search for the product</h2>
  
<!-- SEARCH FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/show/1">
<div class="form-group">
<input type="text" class="form-control" id="seach_product_id" placeholder="Product id" name="seach_product_id" value="<?php echo $_smarty_tpl->tpl_vars['SearchByID']->value;?>
"> 
<input type="text" class="form-control" id="seach_product_name" placeholder="Product name" name="seach_product_name" value="<?php echo $_smarty_tpl->tpl_vars['SearchByName']->value;?>
">
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>

<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>Name</th>
<th>Brand</th>
<th>Count</th>
<th>Price</th>
<th>VAT</th>
<th>CATEGORY</th>
<th>COMPANY</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['products']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['product'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['product']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
$foreach_product_Sav = $_smarty_tpl->tpl_vars['product'];
?>
<?php if ($_smarty_tpl->tpl_vars['product']->value['STOCK'] == "Y") {?>
 	<tr class="success">
<?php } else { ?>
  <tr class="danger">
<?php }?>
  
<td><?php echo $_smarty_tpl->tpl_vars['product']->value['ID'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['product']->value['NAME'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['product']->value['BRAND'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['product']->value['COUNT'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['product']->value['PRICE'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['product']->value['VAT'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['product']->value['CATEGORY'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['product']->value['COMPANY'];?>
</td>

<!-- SEARCH TABLE OPTIONS -->
<td width="10%">
<?php if (!$_smarty_tpl->tpl_vars['ismerch']->value && !$_smarty_tpl->tpl_vars['isadm']->value) {?>
	<a class="glyphicon glyphicon-download-alt" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/trashes/addItem/<?php echo $_smarty_tpl->tpl_vars['product']->value['ID'];?>
" ></a>&nbsp;&nbsp;
<?php }?>
<a class="glyphicon glyphicon-search" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/productDetail/<?php echo $_smarty_tpl->tpl_vars['product']->value['ID'];?>
"></a>&nbsp;&nbsp;
<?php if ($_smarty_tpl->tpl_vars['ismerch']->value || $_smarty_tpl->tpl_vars['isadm']->value) {?>
	<a class="glyphicon glyphicon-trash" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/deleteProduct/<?php echo $_smarty_tpl->tpl_vars['product']->value['ID'];?>
" ></a>
<?php }?>
</td>
</tr>
<?php
$_smarty_tpl->tpl_vars['product'] = $foreach_product_Sav;
}
?>

</tbody>
</table>
</div>
</div>
<?php }
}
?>