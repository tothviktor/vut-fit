<?php /* Smarty version 3.1.27, created on 2015-11-14 09:12:49
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/trashes.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:123066998956476b91b263e7_64168647%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '55ad9db9e0cfb93474a1702fb83ab387dfe3e275' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/trashes.tpl',
      1 => 1445628093,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '123066998956476b91b263e7_64168647',
  'variables' => 
  array (
    'basepath' => 0,
    'errors' => 0,
    'items' => 0,
    'indexpath' => 0,
    'mykey' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56476b91bc93e5_70073497',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56476b91bc93e5_70073497')) {
function content_56476b91bc93e5_70073497 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '123066998956476b91b263e7_64168647';
?>
<!-- FEEDBACKS HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/trashes.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>

</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">
  	 
<!-- IMAGE -->
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/trashes.jpg" alt="..." class="img-rounded" style="width:100%">
<br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:<?php if ($_smarty_tpl->tpl_vars['errors']->value == '') {?>none<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
 
</div>

<?php if ($_smarty_tpl->tpl_vars['items']->value == false || count($_smarty_tpl->tpl_vars['items']->value) < 5) {?>
<br>
<p class="bg-primary" align="center"><b>
<span style="font-size:large">Oh no you have an empty trash :(</span><br>
<span style="font-size:middle">Please add some products!</span><br><br>
<a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/show/1"><span class="glyphicon glyphicon-list-alt" style="color:white"></span></a><br><br>


<?php } else { ?>  
<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>Name</th>
<th>Price</th>
<th>Vat</th>
<th>Count</th>
<th>Delivery</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
	<?php
$_from = $_smarty_tpl->tpl_vars['items']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
$_smarty_tpl->tpl_vars['mykey'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['mykey']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>

	<?php if (is_int($_smarty_tpl->tpl_vars['mykey']->value) == false) {?>
	<?php continue 1;?>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['item']->value['STOCK'] == "Y") {?>
	<tr class="success">
	<?php } else { ?>
	<tr class="danger">
	<?php }?>

	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['NAME'];?>
</td>     
	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['PRICE'];?>
</td>          
	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['VAT'];?>
</td>      
	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['COUNT'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['DELIVERY'];?>
</td>

	  
	<!-- OPTIONS -->
	<td width="5%" align="center">
	<a class="glyphicon glyphicon-trash" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/trashes/deleteItem/<?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
" style="font-size:20px"></a>
	</td>
	</tr>
	<?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>

<tr class="info">
<td style="vertical-align: middle;font-size:large" valign="bottom"><b>TOTAL:</b></td>
<td style="vertical-align: middle;font-size:large"><b><?php echo $_smarty_tpl->tpl_vars['items']->value['TOTAL'];?>
</b></td>
<td style="vertical-align: middle;font-size:large"><b><?php echo $_smarty_tpl->tpl_vars['items']->value['TOTALV'];?>
</b></td>
<td style="vertical-align: middle;font-size:large"><b><?php echo $_smarty_tpl->tpl_vars['items']->value['TOTALC'];?>
</b></td>
<td style="vertical-align: middle;font-size:large"><b><?php echo $_smarty_tpl->tpl_vars['items']->value['TOTALD'];?>
</b></td>
<form>
<input type="hidden" name="finishOrder" id="finishOrder" value="1">
<td><button type="submit" class="btn btn-success" formaction="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/trashes/processOrder/">Complete!</button></td>
</form>
</tr>
</tbody>
</table>

<?php }?>


<!-- FINNISH -->
</div>
</div>
</div>
</div>
</body>
</html><?php }
}
?>