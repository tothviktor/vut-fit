<?php /* Smarty version 3.1.27, created on 2015-11-14 05:15:29
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/users_search.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1374390107564733f18c2278_24825063%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd9f0863acb0411999a65c672e393eb608107f425' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/users_search.tpl',
      1 => 1447449947,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1374390107564733f18c2278_24825063',
  'variables' => 
  array (
    'indexpath' => 0,
    'SearchByID' => 0,
    'SearchByLogin' => 0,
    'users' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564733f18ede36_73223502',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564733f18ede36_73223502')) {
function content_564733f18ede36_73223502 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1374390107564733f18c2278_24825063';
?>
<!-- SEARCH USER PANEL -->
<div id="search" class="tab-pane fade in active"> 
<h2>Search for the user</h2>

<!-- SEARCH USER FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/show/1">
<div class="form-group">
<input type="text" class="form-control" id="seach_user_id" placeholder="User id" name="seach_user_id" value="<?php echo $_smarty_tpl->tpl_vars['SearchByID']->value;?>
"> 
<input type="text" class="form-control" id="seach_user_login" placeholder="User login" name="seach_user_login" value="<?php echo $_smarty_tpl->tpl_vars['SearchByLogin']->value;?>
">
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>

<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>LOGIN</th>
<th>NAME</th>
<th>SURNAME</th>
<th>PERMISSIONS</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['users']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['user']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['user']->_loop = true;
$foreach_user_Sav = $_smarty_tpl->tpl_vars['user'];
?>
<tr class="success">
<td><?php echo $_smarty_tpl->tpl_vars['user']->value['ID'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['user']->value['LOGIN'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['user']->value['PERS_NAME'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['user']->value['SURNAME'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['user']->value['PERM_NAME'];?>
</td>

<!-- OPTIONS -->
<td width="5%">
<a class="glyphicon glyphicon-search" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/userDetail/<?php echo $_smarty_tpl->tpl_vars['user']->value['ID'];?>
"></a>&nbsp;&nbsp;
<a class="glyphicon glyphicon-trash" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/deleteUser/<?php echo $_smarty_tpl->tpl_vars['user']->value['ID'];?>
" ></a>
</td>

</tr>
<?php
$_smarty_tpl->tpl_vars['user'] = $foreach_user_Sav;
}
?>
</tbody>
</table>
</div>
</div><?php }
}
?>