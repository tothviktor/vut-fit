<?php /* Smarty version 3.1.27, created on 2015-11-14 02:56:08
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/login.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:70721283456471348c6f326_28257925%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5483eb605289a5c3cf8d293dcc6831b14fbc09db' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/login.tpl',
      1 => 1446050504,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '70721283456471348c6f326_28257925',
  'variables' => 
  array (
    'basepath' => 0,
    'error' => 0,
    'indexpath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56471348cf71a3_67040876',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56471348cf71a3_67040876')) {
function content_56471348cf71a3_67040876 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '70721283456471348c6f326_28257925';
?>
<!-- LOGIN HEADER -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/login.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
</head>

<body>

<!-- LOGIN FORM -->
<div class="container">

<div class="row">
<div class="col-sm-6 col-md-4 col-md-offset-4">

    <div class="account-wall">
    <img class="profile-img" src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login.png"alt="">

    <?php if ($_smarty_tpl->tpl_vars['error']->value == "1") {?>
        <p class="bg-danger" style="text-align:center">User not found!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "2") {?>
        <p class="bg-warning" style="text-align:center">User already exists!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "3") {?>
        <p class="bg-danger" style="text-align:center">Wrong informations entered!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "4") {?>
        <p class="bg-primary" style="text-align:center">User successfully created!</p>
    <?php }?>
    <form class="form-signin" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/login/index/0">
    <input type="text" class="form-control" placeholder="Login" id="login_user" name="login_user" required autofocus>
    <input type="password" class="form-control" placeholder="Password" id="login_password" name="login_password"required>
    <button class="btn btn-lg btn-success btn-block" type="submit" name="btn_login">Sign in</button>
    <button class="btn btn-lg btn-primary btn-block" type="submit" name="btn_create" value="Create">Create account</button>
    </form>
    </div>

</div>

</div>
</body>
</html><?php }
}
?>