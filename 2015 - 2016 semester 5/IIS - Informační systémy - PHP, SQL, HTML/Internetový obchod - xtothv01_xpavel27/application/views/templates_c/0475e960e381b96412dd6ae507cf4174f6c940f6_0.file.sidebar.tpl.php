<?php /* Smarty version 3.1.27, created on 2015-11-14 03:38:40
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/sidebar.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:24867800556471d405d5d49_06226366%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0475e960e381b96412dd6ae507cf4174f6c940f6' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/sidebar.tpl',
      1 => 1447501050,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '24867800556471d405d5d49_06226366',
  'variables' => 
  array (
    'isadm' => 0,
    'ismerch' => 0,
    'indexpath' => 0,
    'trashcount' => 0,
    'userID' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56471d40653c82_92053978',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56471d40653c82_92053978')) {
function content_56471d40653c82_92053978 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '24867800556471d405d5d49_06226366';
?>
<!-- SIDEBAR MENU -->
<div id="wrapper">
<div id="sidebar-wrapper">
<ul class="sidebar-nav">
  
<!-- SIDEBAR HEADER -->
<li class="sidebar-brand"><a href="#">

<!-- SIDEBAR NAME -->
<?php if ($_smarty_tpl->tpl_vars['isadm']->value) {?><b><font color="orange">Admin's</font></b>
<?php } elseif ($_smarty_tpl->tpl_vars['ismerch']->value) {?><b><font color="orange">Merchant's</font></b>
<?php } else { ?><b><font color="orange">Customer's</font></b>
<?php }?>

<font color="green">BackOffice</font></a></li>



<!-- ADMIN PANEL -->
<?php if ($_smarty_tpl->tpl_vars['isadm']->value) {?>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/dashboard/show">Dashboard</a></li>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/show/1">Users</a></li>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/merchants/show/1">Merchants</a></li>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/categories/show/">Categories</a></li>
<?php }?>

<!-- CUSTOMER PANNEL -->
<?php if ($_smarty_tpl->tpl_vars['isadm']->value == false && $_smarty_tpl->tpl_vars['ismerch']->value == false) {?>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/trashes/show">Trash <font color="#3399FF"><b>(<?php echo $_smarty_tpl->tpl_vars['trashcount']->value;?>
)</b></font></a></li>
<?php }?>

<!-- PROFILE -->
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/userDetail/<?php echo $_smarty_tpl->tpl_vars['userID']->value;?>
">
Profile
</a></li>

<!-- PRODUCTS -->  
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/show/1">
List Products
</a></li>
  
<!-- FEEDBACKS -->
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/feedbacks/show">
<?php if ($_smarty_tpl->tpl_vars['isadm']->value == true) {?>Feedbacks
<?php } else { ?> My Feedbacks <?php }?>
</a></li>

<!-- ORDERS -->
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/orders/show">
<?php if ($_smarty_tpl->tpl_vars['isadm']->value == true) {?>Orders
<?php } else { ?> My Orders <?php }?>
</a></li>

<!-- LOGOUT -->
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/login/outdex">
Logout
</a></li>

</ul>
</div><?php }
}
?>