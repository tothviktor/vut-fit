<?php /* Smarty version 3.1.27, created on 2015-11-14 09:21:18
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/orders.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:5124885356476d8e5cbdb1_27819287%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '26b36979e6d97680b6d511f48857a26d8a31a49f' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/orders.tpl',
      1 => 1447521675,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5124885356476d8e5cbdb1_27819287',
  'variables' => 
  array (
    'basepath' => 0,
    'errors' => 0,
    'indexpath' => 0,
    'SearchByID' => 0,
    'SearchByLogin' => 0,
    'orders' => 0,
    'order' => 0,
    'users' => 0,
    'user' => 0,
    'isadm' => 0,
    'ismerch' => 0,
    'userID' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56476d8e671444_28639493',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56476d8e671444_28639493')) {
function content_56476d8e671444_28639493 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '5124885356476d8e5cbdb1_27819287';
?>
<!-- ORDERS HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/orders.jpg" alt="..." class="img-rounded" style="width:100%">
<br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:<?php if ($_smarty_tpl->tpl_vars['errors']->value == '') {?>none<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
 
</div>
  
<!-- SEARCH PANEL -->
<div class="tab-content">
<h2>Search for the order</h2>

<!-- SEARCH FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/orders/show">
	<div class="form-group">
<input type="text" class="form-control" id="search_order_id" placeholder="Order id" name="search_order_id" value="<?php echo $_smarty_tpl->tpl_vars['SearchByID']->value;?>
"> 
<input type="text" class="form-control" id="search_order_login" placeholder="Customer login" name="search_order_login" value="<?php echo $_smarty_tpl->tpl_vars['SearchByLogin']->value;?>
">
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>

<!-- SEARCH TABLE HEADER-->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>LOGIN</th>
<th>ASIGNEE</th>
<th>PRICE</th>
<th>PAYED</th>
<th>EXPANDED</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY-->
<tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['orders']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['order'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['order']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
$foreach_order_Sav = $_smarty_tpl->tpl_vars['order'];
?>
<?php if ($_smarty_tpl->tpl_vars['order']->value['EXPANDED'] == "Y") {?>
<tr class="success">
<?php } elseif ($_smarty_tpl->tpl_vars['order']->value['PAYED'] == "Y") {?>
<tr class="info">
<?php } else { ?>
<tr class="danger">
<?php }?>
<td><?php echo $_smarty_tpl->tpl_vars['order']->value['ID'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['order']->value['LOGIN'];?>
</td>

<?php if ($_smarty_tpl->tpl_vars['order']->value['EMPLOYEE_ID']) {?>
	<?php
$_from = $_smarty_tpl->tpl_vars['users']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['user']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['user']->_loop = true;
$foreach_user_Sav = $_smarty_tpl->tpl_vars['user'];
?>
	<?php if ($_smarty_tpl->tpl_vars['user']->value['ID'] == $_smarty_tpl->tpl_vars['order']->value['EMPLOYEE_ID']) {?>
		<td><?php echo $_smarty_tpl->tpl_vars['user']->value['LOGIN'];?>
</td>
	<?php }?>
	<?php
$_smarty_tpl->tpl_vars['user'] = $foreach_user_Sav;
}
?>
<?php } else { ?>
<td>Not assigned</td>
<?php }?>

<td><?php echo $_smarty_tpl->tpl_vars['order']->value['PRICE'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['order']->value['PAYED'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['order']->value['EXPANDED'];?>
</td>

<!-- OPTIONS -->
<td width="5%">
<a class="glyphicon glyphicon-search" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/orders/orderDetail/<?php echo $_smarty_tpl->tpl_vars['order']->value['ID'];?>
"></a>
<?php if ($_smarty_tpl->tpl_vars['isadm']->value || $_smarty_tpl->tpl_vars['ismerch']->value) {?>
<a class="glyphicon glyphicon-eur" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/orders/orderPay/<?php echo $_smarty_tpl->tpl_vars['order']->value['ID'];?>
/<?php echo $_smarty_tpl->tpl_vars['userID']->value;?>
"></a>
<a class="glyphicon glyphicon-road" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/orders/orderShip/<?php echo $_smarty_tpl->tpl_vars['order']->value['ID'];?>
/<?php echo $_smarty_tpl->tpl_vars['userID']->value;?>
"></a>
<?php }?>
</td>
</tr>
<?php
$_smarty_tpl->tpl_vars['order'] = $foreach_order_Sav;
}
?>
</tbody>
</table>
</div>
</div>

<!-- FINISH -->
</div>
</div>
</div>
</div>
</body>
</html><?php }
}
?>