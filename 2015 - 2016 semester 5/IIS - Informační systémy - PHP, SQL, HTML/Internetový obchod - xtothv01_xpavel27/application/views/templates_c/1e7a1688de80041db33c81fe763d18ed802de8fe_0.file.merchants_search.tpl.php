<?php /* Smarty version 3.1.27, created on 2015-11-14 05:15:13
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/merchants_search.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1540156483564733e1d629f2_09133615%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1e7a1688de80041db33c81fe763d18ed802de8fe' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/merchants_search.tpl',
      1 => 1447449825,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1540156483564733e1d629f2_09133615',
  'variables' => 
  array (
    'indexpath' => 0,
    'SearchByID' => 0,
    'SearchByName' => 0,
    'merchants' => 0,
    'merchant' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564733e1d907f7_86454799',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564733e1d907f7_86454799')) {
function content_564733e1d907f7_86454799 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1540156483564733e1d629f2_09133615';
?>
<!-- SEARCH PANEL -->
<div id="search" class="tab-pane fade in active"> 
<h2>Search for the user</h2>

<!-- SEARCH FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/merchants/show/1">
<div class="form-group">
<input type="text" class="form-control" id="seach_merchant_id" placeholder="Merchant id" name="seach_merchant_id" value="<?php echo $_smarty_tpl->tpl_vars['SearchByID']->value;?>
"> 
<input type="text" class="form-control" id="seach_merchant_company" placeholder="Company name" name="seach_merchant_company" value="<?php echo $_smarty_tpl->tpl_vars['SearchByName']->value;?>
">
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>

<!-- SEARCH TABLE HEAD -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>ACM</th>
<th>COMPANY</th>
<th>DELIVERY</th>
<th>WEBSITE</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['merchants']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['merchant'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['merchant']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['merchant']->value) {
$_smarty_tpl->tpl_vars['merchant']->_loop = true;
$foreach_merchant_Sav = $_smarty_tpl->tpl_vars['merchant'];
?>
<tr class="success">
<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value['ID'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value['NAME'];?>
 <?php echo $_smarty_tpl->tpl_vars['merchant']->value['SURNAME'];?>
 (<?php echo $_smarty_tpl->tpl_vars['merchant']->value['LOGIN'];?>
)</td>
<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value['COMPANY'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value['DELIVERY'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value['WEBSITE'];?>
</td>

<!-- OPTIONS -->
<td width="5%">
<a class="glyphicon glyphicon-search" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/userDetail/<?php echo $_smarty_tpl->tpl_vars['merchant']->value['USER_ID'];?>
"></a>&nbsp;&nbsp;
<a class="glyphicon glyphicon-trash" aria-hidden="true" href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/merchants/deleteMerchant/<?php echo $_smarty_tpl->tpl_vars['merchant']->value['ID'];?>
" ></a>
</td>
</tr>
<?php
$_smarty_tpl->tpl_vars['merchant'] = $foreach_merchant_Sav;
}
?>

</tbody>
</table>
</div>
</div><?php }
}
?>