<?php /* Smarty version 3.1.27, created on 2015-11-14 05:15:33
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/users_detail.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1719637971564733f51f4c53_39742661%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '442c15301cdbd488b376b9b020a62ff92c4ef138' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/users_detail.tpl',
      1 => 1447448350,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1719637971564733f51f4c53_39742661',
  'variables' => 
  array (
    'basepath' => 0,
    'user_info' => 0,
    'user_id' => 0,
    'indexpath' => 0,
    'isadm' => 0,
    'permissions' => 0,
    'permission' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564733f5290d81_13720341',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564733f5290d81_13720341')) {
function content_564733f5290d81_13720341 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1719637971564733f51f4c53_39742661';
?>
<!-- USER DETAIL HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/myjs/users_detail.js"><?php echo '</script'; ?>
>
</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- LEFT INFO -->
<h2>User details: <?php echo $_smarty_tpl->tpl_vars['user_info']->value['LOGIN'];?>
 (ID: <?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
)</h2> <br>
<div style="width:100%">
<div class="col-xs-6 col-md-3" style="float: left; width:25%">
<a href="#" class="thumbnail">
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/products/1.jpeg" alt="...">
</a>
  
<!-- EXTRACT POST -->
<div class="col-md-6">
<form method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/userDetail/<?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
">
<button type="submit" class="btn btn-info" name="Search" value="1">Extract info</button>
</form>
</div>

<!-- MODIFY -->
  <div class="col-md-6">
    
    <button class="btn btn-warning" name="Edit" id="toogle">Modify!</button>

  
  </div>
</div>

<!-- RIGHT PANEL -->  
<div id="getInfoShow"> 
<div class="panel panel-default" style="float: right; width:75%;">
<div class="panel-heading"><b>Informations</b></div>
<div class="panel-body">
<b>Name:&nbsp; </b><?php echo $_smarty_tpl->tpl_vars['user_info']->value['PERS_NAME'];?>
<br>
<b>Surname:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['user_info']->value['SURNAME'];?>
<br>
<b>Mail:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['user_info']->value['MAIL'];?>
<br>
<b>Telephone:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['user_info']->value['TELEPHONE'];?>
<br>
<b>Address:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['user_info']->value['ADDRESS'];?>
<br>
<b>Permissions:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['user_info']->value['PERM_NAME'];?>
<br>
</div>
   
</div>
</div>

<div id="editInfoShow" style="display:none;">
<div style="width:75%;float:right" >
  
  <!-- HEAD -->
  <form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/editUser/<?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['user_info']->value['PERS_ID'];?>
">
  <div class="form-group">

  <!-- BASIC INFO -->
  <p class="bg-danger" style="padding: 10px 10px 10px 10px">

  <label for="edit_name">Name:</label>
  <input type="text" class="form-control" id="edit_name" placeholder="Name" name="edit_name" value="<?php echo $_smarty_tpl->tpl_vars['user_info']->value['PERS_NAME'];?>
" > 
  
  <label for="edit_surname">Surname:</label>
  <input type="text" class="form-control" id="edit_surname" placeholder="Surname" name="edit_surname" value="<?php echo $_smarty_tpl->tpl_vars['user_info']->value['SURNAME'];?>
"> 
  <br><br>

  <label for="edit_surname">Mail:</label>
  <input type="text" class="form-control" id="edit_mail" placeholder="Mail" name="edit_mail" value="<?php echo $_smarty_tpl->tpl_vars['user_info']->value['MAIL'];?>
"> 

  <label for="edit_surname">Telephone:</label>
  <input type="text" class="form-control" id="edit_telephone" placeholder="Telephone" name="edit_telephone" value="<?php echo $_smarty_tpl->tpl_vars['user_info']->value['TELEPHONE'];?>
">
  <br><br>

  <label for="edit_surname">Address:</label>
  <input type="text" class="form-control" id="edit_address" placeholder="Address" name="edit_address" value="<?php echo $_smarty_tpl->tpl_vars['user_info']->value['ADDRESS'];?>
">
  </p>

  <?php if ($_smarty_tpl->tpl_vars['isadm']->value) {?>
    <p class="bg-info" style="padding: 10px 10px 10px 10px">
    <label for="edit_permission">Select permissions:</label>
    <select class="form‐control" id="edit_permission" name="edit_permission">
    <option value="0">Select permissions</option>
    <?php
$_from = $_smarty_tpl->tpl_vars['permissions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['permission'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['permission']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['permission']->value) {
$_smarty_tpl->tpl_vars['permission']->_loop = true;
$foreach_permission_Sav = $_smarty_tpl->tpl_vars['permission'];
?>
      <option value="<?php echo $_smarty_tpl->tpl_vars['permission']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['user_info']->value['PERM_NAME'] == $_smarty_tpl->tpl_vars['permission']->value['NAME']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['permission']->value['NAME'];?>
</option>
    <?php
$_smarty_tpl->tpl_vars['permission'] = $foreach_permission_Sav;
}
?>
    </select>
    </p>
  <?php }?>
  
  <button type="submit" class="btn btn-primary">Submit</button>

  </div>
  </form>
 
</div>
</div>

</div>
</div>
</div>
</div>
</body>
</html><?php }
}
?>