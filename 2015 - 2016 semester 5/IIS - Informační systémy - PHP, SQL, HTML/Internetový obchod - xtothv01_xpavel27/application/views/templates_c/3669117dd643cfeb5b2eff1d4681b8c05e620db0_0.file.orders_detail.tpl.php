<?php /* Smarty version 3.1.27, created on 2015-11-18 02:13:45
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/export/orders_detail.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1592800579564c4f590a9a31_21871093%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3669117dd643cfeb5b2eff1d4681b8c05e620db0' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/export/orders_detail.tpl',
      1 => 1446056739,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1592800579564c4f590a9a31_21871093',
  'variables' => 
  array (
    'order_id' => 0,
    'order_info' => 0,
    'items' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564c4f59160f26_95351085',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564c4f59160f26_95351085')) {
function content_564c4f59160f26_95351085 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1592800579564c4f590a9a31_21871093';
?>
<html>
<head>
</head>
<body>

  <h2> Webshop Name Order </h2>
  <h2>Order details: <?php echo $_smarty_tpl->tpl_vars['order_id']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['order_info']->value['LOGIN'];?>
)
  <?php if ($_smarty_tpl->tpl_vars['order_info']->value['EXPANDED'] == "Y") {?>
    <font color="green">Expanded</font>
  <?php } elseif ($_smarty_tpl->tpl_vars['order_info']->value['PAYED'] == "Y") {?>
    <font color="orange">Payed</font>
  <?php } else { ?>
    <font color="red">Waiting</font>
    <?php }?>
  </h2>
 
  <font color="red"><b>Customer</b></font><br>
  <b>Name: </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['NAME'];?>
<br>
  <b>Surname: </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['SURNAME'];?>
<br>
  <b>Email: </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['MAIL'];?>
<br>
  <b>Telephone: </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['TELEPHONE'];?>
<br>
  <b>Address: </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['ADDRESS'];?>
<br><br>

  <font color="red"><b>Status</b></font><br>
  <b>Expanded: </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['EXPANDED'];?>
<br>
  <b>Payed: </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['PAYED'];?>
<br>
  <b>Employee: </b> <?php echo $_smarty_tpl->tpl_vars['order_info']->value['EMPLOYEE_ID'];?>
<br><br>
  
  <font color="red"><b>Price</b></font><br>
  <b>Price: </b><?php echo $_smarty_tpl->tpl_vars['order_info']->value['PRICE'];?>
<br><br>

    <table border="1" style="width:100%">
    <thead>
      <tr>
        <th>NAME</th>
        <th>PRICE</th>
        <th>VAT</th>
      </tr>
    </thead>
    <tbody>
      <?php
$_from = $_smarty_tpl->tpl_vars['items']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
        <tr>
        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['NAME'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['PRICE'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['VAT'];?>
</td>
        </tr>
        <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
    </tbody>
  </table>
</body>
</html><?php }
}
?>