<?php /* Smarty version 3.1.27, created on 2015-11-14 05:15:29
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/users_create.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1713424402564733f18f2d20_85951215%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '95314efed79482f909aa77b7a4c4b8f0a0bc0270' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/users_create.tpl',
      1 => 1446056348,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1713424402564733f18f2d20_85951215',
  'variables' => 
  array (
    'indexpath' => 0,
    'inputdata' => 0,
    'permissions' => 0,
    'permission' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564733f191ff61_79629096',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564733f191ff61_79629096')) {
function content_564733f191ff61_79629096 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1713424402564733f18f2d20_85951215';
?>
  <!-- CREATE USER PANEL -->
  <div id="create" class="tab-pane fade">
  <h3>Create new user</h3>
    
  <!-- CREATE USER FORM -->
  <form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/createNewUser">
  <div class="form-group">
  <div class="has-error">
    <p class="bg-danger" style="padding: 10px 10px 10px 10px">
      <label for="user_login">Login:</label>
      <input type="text" class="form-control" id="user_login" placeholder="Login" name="user_login" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['login'];?>
"> 
      <label for="user_password">Password:</label>
      <input type="text" class="form-control" id="user_password" placeholder="Password" name="user_password" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['password'];?>
">
    </p>
  </div>

  <p class="bg-warning" style="padding: 10px 10px 10px 10px">
  <label for="user_name">Name:</label>
  <input type="text" class="form-control" id="user_name" placeholder="Name" name="user_name" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['name'];?>
"> 
  <label for="user_surname">Surname:</label>
  <input type="text" class="form-control" id="user_surname" placeholder="Surname" name="user_surname" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['surname'];?>
"> 
  <br><br>
  <label for="user_mail">Mail:</label>
  <input type="text" class="form-control" id="user_mail" placeholder="Mail" name="user_mail" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['mail'];?>
">
  <label for="user_telephone">Telephone:</label>
  <input type="text" class="form-control" id="user_telephone" placeholder="Telephone" name="user_telephone" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['telephone'];?>
">
  <br><br>
  <label for="user_address">Address:</label>
  <input type="text" class="form-control" id="user_address" placeholder="Address" name="user_address" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['address'];?>
"> 
  </p>
  
  <!-- PERMISSIONS SELECTBOX -->
  <p class="bg-info" style="padding: 10px 10px 10px 10px">
  <label for="user_permission">Select permissions:</label>
  <select class="form‐control" id="user_permission" name="user_permission">
  <option value="0">Select permissions</option>
  <?php
$_from = $_smarty_tpl->tpl_vars['permissions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['permission'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['permission']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['permission']->value) {
$_smarty_tpl->tpl_vars['permission']->_loop = true;
$foreach_permission_Sav = $_smarty_tpl->tpl_vars['permission'];
?>
    <option value="<?php echo $_smarty_tpl->tpl_vars['permission']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['permission_id'] == $_smarty_tpl->tpl_vars['permission']->value['ID']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['permission']->value['NAME'];?>
</option>
  <?php
$_smarty_tpl->tpl_vars['permission'] = $foreach_permission_Sav;
}
?>
  </select>
  </p>
  
  <!-- SUBMIT BUTTON -->
  <button type="submit" class="btn btn-primary">Submit</button>
  </div>
  </form>
  </div><?php }
}
?>