<?php /* Smarty version 3.1.27, created on 2015-11-14 09:13:02
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/products_detail.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:113189501756476b9e5cb303_79947104%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e092c2a910188c37ffbda3313ecff9478fbcac12' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/products_detail.tpl',
      1 => 1446058851,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '113189501756476b9e5cb303_79947104',
  'variables' => 
  array (
    'basepath' => 0,
    'product_info' => 0,
    'product_id' => 0,
    'indexpath' => 0,
    'ismerch' => 0,
    'merchant' => 0,
    'isadm' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56476b9e692c03_26365524',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56476b9e692c03_26365524')) {
function content_56476b9e692c03_26365524 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '113189501756476b9e5cb303_79947104';
?>
<!-- PRODUCTS DETAIL HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/myjs/products_detail.js"><?php echo '</script'; ?>
>
</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- HEADER -->
<h2>
  Product details: <?php echo $_smarty_tpl->tpl_vars['product_info']->value['NAME'];?>
 (<?php echo $_smarty_tpl->tpl_vars['product_id']->value;?>
)
</h2> 
<br>

<!-- CONTENT -->
<div style="width:100%">

<!-- LEFT INFO -->
<div class="col-xs-6 col-md-3" style="float: left; width:25%;">
  <a href="#" class="thumbnail"><img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/products/1.jpeg" alt="..."></a>
  
  <!-- EXTRACT -->
  <div class="col-md-6">
    <form method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/productDetail/<?php echo $_smarty_tpl->tpl_vars['product_id']->value;?>
">
    <button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
    </form>
  </div>

  <!-- STOCK -->
  <div class="col-md-6">
    <?php if ($_smarty_tpl->tpl_vars['product_info']->value['STOCK'] == "Y") {?>
    <button class="btn btn-success">On Stock</button>
    <?php } else { ?>
    <button class="btn btn-danger">Inavaiable</button>
    <?php }?> 
  </div>

  <!-- MODIFY -->
  <div class="col-md-12">
    <?php if ($_smarty_tpl->tpl_vars['ismerch']->value && $_smarty_tpl->tpl_vars['product_info']->value['COMPANY'] == $_smarty_tpl->tpl_vars['merchant']->value['COMPANY']) {?>
    <button class="btn btn-warning" name="Edit" id="toogle">Modify this as merchant !</button>
    <?php } elseif ($_smarty_tpl->tpl_vars['isadm']->value) {?>
    <button class="btn btn-warning" name="Edit" id="toogle">Modify this as admin !</button>
    <?php } else { ?>
    <button class="btn btn-warning" name="Edit" id="tooglefeed">Give customer feedback !</button>
    <?php }?>
  </div>

  <?php if (!$_smarty_tpl->tpl_vars['ismerch']->value && !$_smarty_tpl->tpl_vars['isadm']->value) {?>
  <div class="col-md-12"><br>
  <div id="editInfoShowFeed" style="display:none;">
    <form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/feedbacks/addFeedback/<?php echo $_smarty_tpl->tpl_vars['product_id']->value;?>
">
    <div class="form-group">
    <p class="bg-danger" style="padding: 10px 10px 10px 10px">
    <b>Please select the mark</b><br>
    <select class="form‐control" id="mark" name="mark">
      <option value="0">Select the mark</option>
      <option value="1">Excelent</option>
      <option value="2">Super</option>
      <option value="3">Good</option>
      <option value="4">Bad</option>
      <option value="5">Worst</option>
    </select>
    <br><br>
    <button type="submit" class="btn btn-primary">Submit</button>
    </p>
    </div>
    </form>
  </div>
  </div>
  <?php }?>
</div>
  
<!-- INFO SHOW RIGHT --> 
<div id="getInfoShow">
<div class="panel panel-default" style="float: right; width:75%;">

  <!-- RIGHT PANNEL 1 -->
  <div class="panel-heading"><b>Description</b></div>
  <div class="panel-body"><?php echo $_smarty_tpl->tpl_vars['product_info']->value['DESCRIPTION'];?>
</div>
  <div class="panel-heading"><b>Pricing</b></div>
  <div class="panel-body">
  <b>Price:&nbsp; </b><?php echo $_smarty_tpl->tpl_vars['product_info']->value['PRICE'];?>
<br>
  <b>VAT:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['product_info']->value['VAT'];?>
<br>
  <b>Count:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['product_info']->value['COUNT'];?>
<br>
  </div>

  <!-- RIGHT PANNEL 2 -->
  <div class="panel-heading"><b>More infos</b></div>
  <div class="panel-body">
  <b>Brand:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['product_info']->value['BRAND'];?>
 <br>
  <b>Category:&nbsp; </b> <td><?php echo $_smarty_tpl->tpl_vars['product_info']->value['CATEGORY'];?>
</td> <br>
  <b>Company:&nbsp; </b> <td><?php echo $_smarty_tpl->tpl_vars['product_info']->value['COMPANY'];?>
</td><br>
  <b>Delivery:&nbsp; </b> <td><font color="red"><?php echo $_smarty_tpl->tpl_vars['product_info']->value['DELIVERY'];?>
</font> days</td>
  <br>
  </div>

  <!-- RIGHT PANNEL 3 -->
  <div class="panel-heading"><b>More infos</b></div>
  <div class="panel-body">
  <?php if ($_smarty_tpl->tpl_vars['product_info']->value['SOFTWARE']) {?>
  <b>Licence type:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['product_info']->value['LICENCE_TYPE'];?>
 <br>
  <b>Configuration:&nbsp; </b> <td><?php echo $_smarty_tpl->tpl_vars['product_info']->value['CONFIGURATION'];?>
</td> <br>
  <?php } else { ?>
  <b>Size:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['product_info']->value['SIZE'];?>
 <br>
  <b>Weight:&nbsp; </b> <?php echo $_smarty_tpl->tpl_vars['product_info']->value['WEIGHT'];?>
 <br>
  <?php }?>
  </div>

</div>
</div>

<!-- EDIT INFO RIGHT -->
<?php if ($_smarty_tpl->tpl_vars['ismerch']->value || $_smarty_tpl->tpl_vars['isadm']->value) {?>
<div id="editInfoShow" style="display:none;">
<div style="width:75%;float:right" >
  
  <!-- HEAD -->
  <form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/editProduct/<?php echo $_smarty_tpl->tpl_vars['product_id']->value;?>
">
  <div class="form-group">

  <!-- BASIC INFO -->
  <p class="bg-danger" style="padding: 10px 10px 10px 10px">
  <b>Basic Info</b><br>
  <input type="text" class="form-control" id="edit_name" placeholder="Name" name="edit_name" value="<?php echo $_smarty_tpl->tpl_vars['product_info']->value['NAME'];?>
"> 
  <input type="text" class="form-control" id="edit_count" placeholder="Count" name="edit_count" value="<?php echo $_smarty_tpl->tpl_vars['product_info']->value['COUNT'];?>
"> 
  <br><br>
  <input type="text" class="form-control" id="edit_brand" placeholder="Brand" name="edit_brand" value="<?php echo $_smarty_tpl->tpl_vars['product_info']->value['BRAND'];?>
"> 
  <input type="text" class="form-control" id="edit_price" placeholder="Price" name="edit_price" value="<?php echo $_smarty_tpl->tpl_vars['product_info']->value['PRICE'];?>
">
  <br><br>
  </p>

  <!-- HARDWARE / SOFTWARE -->
  <p class="bg-warning" style="padding: 10px 10px 10px 10px">
  <b>Specify Info</b><br>
  <?php if ($_smarty_tpl->tpl_vars['product_info']->value['SOFTWARE']) {?>
  	<input type="text" class="form-control" id="edit_licence_type" placeholder="Licence" name="edit_licence_type" value="<?php echo $_smarty_tpl->tpl_vars['product_info']->value['LICENCE_TYPE'];?>
">
  	<input type="text" class="form-control" id="edit_configuration" placeholder="Configuration" name="edit_configuration" value="<?php echo $_smarty_tpl->tpl_vars['product_info']->value['CONFIGURATION'];?>
">
  	<input type="hidden" name="edit_type" value="Software">
  <?php } else { ?>
  	<input type="text" class="form-control" id="edit_size" placeholder="Size" name="edit_size" value="<?php echo $_smarty_tpl->tpl_vars['product_info']->value['SIZE'];?>
">
  	<input type="text" class="form-control" id="edit_weight" placeholder="Weight" name="edit_weight" value="<?php echo $_smarty_tpl->tpl_vars['product_info']->value['WEIGHT'];?>
">
  	<input type="hidden" name="edit_type" value="Hardware">
  <?php }?>
  <br><br>
  </p>

  <!-- EDIT FORM STOCK -->
  <p class="bg-info" style="padding: 10px 10px 10px 10px">
  <b>Stock Info</b><br>
  <select class="form‐control" id="edit_stock" name="edit_stock">
    <option value="0">Select stock</option>
    <option value="Y" <?php if ($_smarty_tpl->tpl_vars['product_info']->value['STOCK'] == "Y") {?>selected<?php }?>>YES</option>
    <option value="N" <?php if ($_smarty_tpl->tpl_vars['product_info']->value['STOCK'] == "N") {?>selected<?php }?>>NO</option>
  </select>
  <br><br>
  </p>

  <!-- DESCRIPTION -->
  <p class="bg-success" style="padding: 10px 10px 10px 10px">
  <b>Description</b><br>
  <textarea class="form-control" id="edit_description" rows="4" placeholder="Description" name="edit_description" style="width:100%"><?php echo $_smarty_tpl->tpl_vars['product_info']->value['DESCRIPTION'];?>

  </textarea>
  <br><br>
  <button type="submit" class="btn btn-primary">Submit</button>
  </p>
  </div>
  </form>
  </p>
</div>
</div>
<?php }?>

<!-- FINISH -->
</div>
</div>
</div>
</body>
</html><?php }
}
?>