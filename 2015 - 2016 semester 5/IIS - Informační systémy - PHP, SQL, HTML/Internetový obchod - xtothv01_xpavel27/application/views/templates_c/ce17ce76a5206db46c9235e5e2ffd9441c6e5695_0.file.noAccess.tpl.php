<?php /* Smarty version 3.1.27, created on 2015-11-14 04:13:32
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/errors/noAccess.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:11712737755647256c99b268_70187425%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ce17ce76a5206db46c9235e5e2ffd9441c6e5695' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/errors/noAccess.tpl',
      1 => 1447503211,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11712737755647256c99b268_70187425',
  'variables' => 
  array (
    'basepath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5647256c9fefd0_39871150',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5647256c9fefd0_39871150')) {
function content_5647256c9fefd0_39871150 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '11712737755647256c99b268_70187425';
?>
<!-- LOGIN HEADER -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/jquery.backstretch.min.js"><?php echo '</script'; ?>
>
     <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/noAccess.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/noAccess.css">
</head>

<div class="top-content">
<div class="container">
<div class="row" id="aftermenuarray">
    
    <!-- Right head text -->
    <div class="col-md-12 text">   
        
        <!-- Head header -->
        <h1><strong>Access</strong> denied!</h1>
        
        <!-- Head description -->
        <div class="description">
            <p> Something went wrong! Try to log in again... Thanks</p>
        </div>

    </div>
   
</div>
</div>
</div>

<?php echo '<script'; ?>
>

jQuery(document).ready(function() {
    
  $.backstretch([
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/1.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/2.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/3.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/4.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/5.jpg"     
  ], {duration: 10000, fade:1000});
    
});

<?php echo '</script'; ?>
>
</body>
</html><?php }
}
?>