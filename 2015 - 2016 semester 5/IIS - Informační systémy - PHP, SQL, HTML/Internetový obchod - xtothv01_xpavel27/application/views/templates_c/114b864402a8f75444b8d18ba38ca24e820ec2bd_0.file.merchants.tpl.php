<?php /* Smarty version 3.1.27, created on 2015-11-14 05:15:13
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/merchants.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:230290776564733e1ce8f09_89954416%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '114b864402a8f75444b8d18ba38ca24e820ec2bd' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/merchants.tpl',
      1 => 1445105311,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '230290776564733e1ce8f09_89954416',
  'variables' => 
  array (
    'basepath' => 0,
    'errors' => 0,
    'pageid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564733e1d5b053_84946398',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564733e1d5b053_84946398')) {
function content_564733e1d5b053_84946398 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '230290776564733e1ce8f09_89954416';
?>
<!-- MERCHANT HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/myjs/merchants.js"><?php echo '</script'; ?>
>
</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/orders.jpg" alt="..." class="img-rounded" style="width:100%">
<br><br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:<?php if ($_smarty_tpl->tpl_vars['errors']->value == '') {?>none<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
 
</div>

<!-- NAVIGATION BAR -->  
<ul class="nav nav-tabs">
<li><a data-toggle="pill" href="#search">Find</a></li>
<li><a data-toggle="pill" href="#create">Create</a></li>
</ul>

<!-- SEARCH PRODUCT -->
<div class="tab-content">
<?php echo $_smarty_tpl->getSubTemplate ('resources/merchants_search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php echo $_smarty_tpl->getSubTemplate ('resources/merchants_create.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

</div>

<!-- FINISH -->
</div>
</div>
</div>

<!-- CONTENT SELECTOR -->

<?php echo '<script'; ?>
>
$(document).ready(function(){
  
  <?php if ($_smarty_tpl->tpl_vars['pageid']->value == "1") {?> activaTab('search');
  <?php } else { ?> activaTab('create');
  <?php }?>
  
});
<?php echo '</script'; ?>
>


</body>
</html><?php }
}
?>