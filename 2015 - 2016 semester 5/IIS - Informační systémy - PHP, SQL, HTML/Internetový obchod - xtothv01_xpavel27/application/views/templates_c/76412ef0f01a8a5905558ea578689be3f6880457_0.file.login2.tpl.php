<?php /* Smarty version 3.1.27, created on 2015-11-14 04:55:40
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/login2.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:144955975256472f4c278136_87562063%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '76412ef0f01a8a5905558ea578689be3f6880457' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/login2.tpl',
      1 => 1447505738,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '144955975256472f4c278136_87562063',
  'variables' => 
  array (
    'basepath' => 0,
    'error' => 0,
    'indexpath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56472f4c2fc4c1_52127089',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56472f4c2fc4c1_52127089')) {
function content_56472f4c2fc4c1_52127089 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '144955975256472f4c278136_87562063';
?>
<!-- LOGIN HEADER -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/login.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/jquery.backstretch.min.js"><?php echo '</script'; ?>
>
     <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/noAccess.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/noAccess.css">
</head>

<div class="top-content">
<div class="container">
<div class="row" id="aftermenuarray">
    
    <!-- Right head text -->
    <div class="col-md-6 text" id="rightheadtext">   
        
        <!-- Head header -->
        <h1><strong>Webstore</strong> login</h1>
        
        <!-- Head description -->
        <div class="description">
            <p> Please login to our web store!</p>
        </div>

    </div>
    
    <!-- Left login elements -->
    <div class="col-md-6 form-box" id="leftloginelems">
        
   <div class="account-wall">
    <img class="profile-img" src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login.png"alt="">

    <?php if ($_smarty_tpl->tpl_vars['error']->value == "1") {?>
        <p class="bg-danger" style="text-align:center">User not found!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "2") {?>
        <p class="bg-warning" style="text-align:center">User already exists!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "3") {?>
        <p class="bg-danger" style="text-align:center">Wrong informations entered!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "4") {?>
        <p class="bg-primary" style="text-align:center">User successfully created!</p>
    <?php }?>
    <form class="form-signin" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/login/index/0">
    <input type="text" class="form-control" placeholder="Login" id="login_user" name="login_user" required autofocus>
    <input type="password" class="form-control" placeholder="Password" id="login_password" name="login_password"required>
    <button class="btn btn-lg btn-success btn-block" type="submit" name="btn_login">Sign in</button>
    <button class="btn btn-lg btn-primary btn-block" type="submit" name="btn_create" value="Create">Create account</button>
    </form>
    </div>

    </div>
</div>
</div>
</div>

<?php echo '<script'; ?>
>

jQuery(document).ready(function() {
    
  $.backstretch([
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/1.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/2.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/3.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/4.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/5.jpg"     
  ], {duration: 10000, fade:1000});
    
});

<?php echo '</script'; ?>
>
</body>
</html><?php }
}
?>