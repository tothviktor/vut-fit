<?php /* Smarty version 3.1.27, created on 2015-11-18 17:25:09
         compiled from "/www/sites/1/site23091/public_html/application/views/templates/login2.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1237988844564ca665043771_64775050%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b2cd3982bf697445ef842c4edef033cd5d75d600' => 
    array (
      0 => '/www/sites/1/site23091/public_html/application/views/templates/login2.tpl',
      1 => 1447863793,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1237988844564ca665043771_64775050',
  'variables' => 
  array (
    'basepath' => 0,
    'error' => 0,
    'indexpath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564ca6650a30b0_30870994',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564ca6650a30b0_30870994')) {
function content_564ca6650a30b0_30870994 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1237988844564ca665043771_64775050';
?>
<!-- LOGIN HEADER -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/login.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/jquery.backstretch.min.js"><?php echo '</script'; ?>
>
     <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/noAccess.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/noAccess.css">
</head>

<div class="top-content">
<div class="container">
<div class="row" id="aftermenuarray">
    
    <!-- Right head text -->
    <div class="col-md-6 text" id="rightheadtext">   
        
        <!-- Head header -->
        <h1><strong>Webstore</strong> login</h1>
        
        <!-- Head description -->
        <div class="description">
            <p> Please login to our web store!</p>
        </div>

    </div>
    
    <!-- Left login elements -->
    <div class="col-md-6 form-box" id="leftloginelems">
        
   <div class="account-wall">
    <img class="profile-img" src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login.png"alt="">

    <?php if ($_smarty_tpl->tpl_vars['error']->value == "1") {?>
        <p class="bg-danger" style="text-align:center">User not found!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "2") {?>
        <p class="bg-warning" style="text-align:center">User already exists!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "3") {?>
        <p class="bg-danger" style="text-align:center">Wrong informations entered!</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value == "4") {?>
        <p class="bg-primary" style="text-align:center">User successfully created!</p>
    <?php }?>
    <form class="form-signin" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/login/index/0">
    <input type="text" class="form-control" placeholder="Login" id="login_user" name="login_user" required autofocus>
    <input type="password" class="form-control" placeholder="Password" id="login_password" name="login_password"required>
    <button class="btn btn-lg btn-success btn-block" type="submit" name="btn_login">Sign in</button>
    <button class="btn btn-lg btn-primary btn-block" type="submit" name="btn_create" value="Create">Create account</button>
    </form>
    </div>

    </div>
</div>
</div>
</div>

<?php echo '<script'; ?>
>

jQuery(document).ready(function() {
    
  $.backstretch([
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/1.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/2.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/3.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/4.jpg",
    "<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/login/5.jpg"     
  ], {duration: 10000, fade:1000});
    
});

<?php echo '</script'; ?>
>
</body>
</html><?php }
}
?>