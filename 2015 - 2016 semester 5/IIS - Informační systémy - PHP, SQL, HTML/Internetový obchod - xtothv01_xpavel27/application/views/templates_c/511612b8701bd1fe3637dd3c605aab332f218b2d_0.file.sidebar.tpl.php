<?php /* Smarty version 3.1.27, created on 2015-11-18 17:25:16
         compiled from "/www/sites/1/site23091/public_html/application/views/templates/resources/sidebar.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:957598162564ca66cb22cc4_67428964%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '511612b8701bd1fe3637dd3c605aab332f218b2d' => 
    array (
      0 => '/www/sites/1/site23091/public_html/application/views/templates/resources/sidebar.tpl',
      1 => 1447863800,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '957598162564ca66cb22cc4_67428964',
  'variables' => 
  array (
    'isadm' => 0,
    'ismerch' => 0,
    'indexpath' => 0,
    'trashcount' => 0,
    'userID' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564ca66cb44434_07571516',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564ca66cb44434_07571516')) {
function content_564ca66cb44434_07571516 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '957598162564ca66cb22cc4_67428964';
?>
<!-- SIDEBAR MENU -->
<div id="wrapper">
<div id="sidebar-wrapper">
<ul class="sidebar-nav">
  
<!-- SIDEBAR HEADER -->
<li class="sidebar-brand"><a href="#">

<!-- SIDEBAR NAME -->
<?php if ($_smarty_tpl->tpl_vars['isadm']->value) {?><b><font color="orange">Admin's</font></b>
<?php } elseif ($_smarty_tpl->tpl_vars['ismerch']->value) {?><b><font color="orange">Merchant's</font></b>
<?php } else { ?><b><font color="orange">Customer's</font></b>
<?php }?>

<font color="green">BackOffice</font></a></li>



<!-- ADMIN PANEL -->
<?php if ($_smarty_tpl->tpl_vars['isadm']->value) {?>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/dashboard/show">Dashboard</a></li>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/show/1">Users</a></li>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/merchants/show/1">Merchants</a></li>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/categories/show/">Categories</a></li>
<?php }?>

<!-- CUSTOMER PANNEL -->
<?php if ($_smarty_tpl->tpl_vars['isadm']->value == false && $_smarty_tpl->tpl_vars['ismerch']->value == false) {?>
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/trashes/show">Trash <font color="#3399FF"><b>(<?php echo $_smarty_tpl->tpl_vars['trashcount']->value;?>
)</b></font></a></li>
<?php }?>

<!-- PROFILE -->
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/users/userDetail/<?php echo $_smarty_tpl->tpl_vars['userID']->value;?>
">
Profile
</a></li>

<!-- PRODUCTS -->  
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/show/1">
List Products
</a></li>
  
<!-- FEEDBACKS -->
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/feedbacks/show">
<?php if ($_smarty_tpl->tpl_vars['isadm']->value == true) {?>Feedbacks
<?php } else { ?> My Feedbacks <?php }?>
</a></li>

<!-- ORDERS -->
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/orders/show">
<?php if ($_smarty_tpl->tpl_vars['isadm']->value == true) {?>Orders
<?php } else { ?> My Orders <?php }?>
</a></li>

<!-- LOGOUT -->
<li><a href="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/login/outdex">
Logout
</a></li>

</ul>
</div><?php }
}
?>