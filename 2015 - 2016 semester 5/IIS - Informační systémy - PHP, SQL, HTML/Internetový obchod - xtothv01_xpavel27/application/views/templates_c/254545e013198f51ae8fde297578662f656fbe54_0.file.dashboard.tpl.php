<?php /* Smarty version 3.1.27, created on 2015-11-14 09:22:45
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/dashboard.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:22625721756476de5bf7b81_91016242%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '254545e013198f51ae8fde297578662f656fbe54' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/dashboard.tpl',
      1 => 1445105046,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22625721756476de5bf7b81_91016242',
  'variables' => 
  array (
    'basepath' => 0,
    'errors' => 0,
    'monthPayed' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56476de5c5add9_54983157',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56476de5c5add9_54983157')) {
function content_56476de5c5add9_54983157 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '22625721756476de5bf7b81_91016242';
?>
<!-- DASHBOARD HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/dashboard.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
</head>
<body>

<!-- SIDEBAR MENU -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/dashboard.jpg" alt="..." class="img-rounded" style="width:100%">
<br><br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:<?php if ($_smarty_tpl->tpl_vars['errors']->value == '') {?>none<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
 
</div>

<!-- PAYED MONTHLY -->
<div class="panel panel-red" style="width:20%">
<div class="panel-heading">
<div class="row">
<div class="col-xs-3">
</div>
<div class="col-xs-9 text-right">
<div class="huge"><?php echo $_smarty_tpl->tpl_vars['monthPayed']->value;?>
 </div> CZK
</div>
</div>
</div> 
<div class="panel-footer">
<span class="pull-left">
  Payed this month!
</span>
<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
<div class="clearfix"></div>
</div>
</div>

<!-- FINISH -->
</div>
</div>
</div>
</body>
</html><?php }
}
?>