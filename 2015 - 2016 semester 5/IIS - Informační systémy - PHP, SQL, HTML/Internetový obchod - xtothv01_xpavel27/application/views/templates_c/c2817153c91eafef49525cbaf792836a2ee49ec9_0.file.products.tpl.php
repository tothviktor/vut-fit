<?php /* Smarty version 3.1.27, created on 2015-11-18 17:25:16
         compiled from "/www/sites/1/site23091/public_html/application/views/templates/products.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:30635052564ca66cab8432_40647467%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c2817153c91eafef49525cbaf792836a2ee49ec9' => 
    array (
      0 => '/www/sites/1/site23091/public_html/application/views/templates/products.tpl',
      1 => 1447863794,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30635052564ca66cab8432_40647467',
  'variables' => 
  array (
    'basepath' => 0,
    'errors' => 0,
    'isadm' => 0,
    'ismerch' => 0,
    'pageid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564ca66cb1b5f1_49146895',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564ca66cb1b5f1_49146895')) {
function content_564ca66cb1b5f1_49146895 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '30635052564ca66cab8432_40647467';
?>
<!-- PRODUCTS HEADER -->
<html>
<head>
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/mycss/products.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/jquery.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/myjs/products.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
</head>
<body>

<!-- SIDEBAR -->
<?php echo $_smarty_tpl->getSubTemplate ('resources/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="<?php echo $_smarty_tpl->tpl_vars['basepath']->value;?>
/images/products.jpg" alt="..." class="img-rounded" style="width:100%">
<br><br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:<?php if ($_smarty_tpl->tpl_vars['errors']->value == '') {?>none<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
 
</div>

<!-- NAVIGATION BAR -->  
<ul class="nav nav-tabs">
<li><a data-toggle="pill" href="#search">Find</a></li>
<?php if ($_smarty_tpl->tpl_vars['isadm']->value == true || $_smarty_tpl->tpl_vars['ismerch']->value == true) {?>
<li><a data-toggle="pill" href="#create">Create</a></li>
<?php }?>
</ul>

<!-- CONTENT -->
<div class="tab-content">
<?php echo $_smarty_tpl->getSubTemplate ('resources/products_search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php if ($_smarty_tpl->tpl_vars['isadm']->value == true || $_smarty_tpl->tpl_vars['ismerch']->value == true) {?>
<?php echo $_smarty_tpl->getSubTemplate ('resources/products_create.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
</div>
</div>
</div>

<!-- CONTENT SELECTOR + TYPE SELECTOR -->

<?php echo '<script'; ?>
>
 $("#product_type").change(function(){changeboxes();});
 $(document).ready(function(){
    
    <?php if ($_smarty_tpl->tpl_vars['pageid']->value == "1") {?> activaTab('search');
    <?php } else { ?> activaTab('create');
    <?php }?>
    
});
<?php echo '</script'; ?>
>


</body>
</html><?php }
}
?>