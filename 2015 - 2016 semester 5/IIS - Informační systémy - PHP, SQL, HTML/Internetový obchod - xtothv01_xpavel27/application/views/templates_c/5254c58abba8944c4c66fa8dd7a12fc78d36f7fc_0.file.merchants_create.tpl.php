<?php /* Smarty version 3.1.27, created on 2015-11-14 05:15:13
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/merchants_create.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1936065319564733e1d95835_27951235%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5254c58abba8944c4c66fa8dd7a12fc78d36f7fc' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/merchants_create.tpl',
      1 => 1446055649,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1936065319564733e1d95835_27951235',
  'variables' => 
  array (
    'indexpath' => 0,
    'inputdata' => 0,
    'acms' => 0,
    'acm' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564733e1db82c6_72412426',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564733e1db82c6_72412426')) {
function content_564733e1db82c6_72412426 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1936065319564733e1d95835_27951235';
?>
<!-- CREATE PANEL -->
<div id="create" class="tab-pane fade">
<h3>Create new merchant</h3>

<!-- CREATE FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/merchants/createNewMerchant">
<div class="form-group">
<div class="has-error">

	<p class="bg-danger" style="padding: 10px 10px 10px 10px">
	<label for="merchant_company">Company:</label>
	<input type="text" class="form-control" id="merchant_company" placeholder="Company name" name="merchant_company" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['company'];?>
"> 
	<br><br>
	<label for="merchant_delivery">Delivery:</label>
	<input type="text" class="form-control" id="merchant_delivery" placeholder="Delivery days" name="merchant_delivery" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['delivery'];?>
">
	<br><br>
	<label for="merchant_website">Website:</label>
	<input type="text" class="form-control" id="merchant_website" placeholder="Website" name="merchant_website" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['website'];?>
"> 
	</p>

	<!-- ACM SELECTION -->
	<p class="bg-warning" style="padding: 10px 10px 10px 10px">
	<label for="merchant_acm">Merchant ACM:</label>
	<select class="form‐control" id="merchant_acm" name="merchant_acm">
		<option value="0">Select ACM</option>
		<?php
$_from = $_smarty_tpl->tpl_vars['acms']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['acm'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['acm']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['acm']->value) {
$_smarty_tpl->tpl_vars['acm']->_loop = true;
$foreach_acm_Sav = $_smarty_tpl->tpl_vars['acm'];
?>
	  		<option value="<?php echo $_smarty_tpl->tpl_vars['acm']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['user_id'] == $_smarty_tpl->tpl_vars['acm']->value['ID']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['acm']->value['LOGIN'];?>
</option>
		<?php
$_smarty_tpl->tpl_vars['acm'] = $foreach_acm_Sav;
}
?>
	</select>
	</p>

<!-- SUBMIT BUTTON -->
<button type="submit" class="btn btn-primary">Submit</button>
</div>
</div>
</form>
</div><?php }
}
?>