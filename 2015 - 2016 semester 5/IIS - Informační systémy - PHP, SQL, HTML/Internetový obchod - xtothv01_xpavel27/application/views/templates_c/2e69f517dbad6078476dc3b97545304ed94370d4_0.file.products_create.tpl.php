<?php /* Smarty version 3.1.27, created on 2015-11-14 09:12:03
         compiled from "/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/products_create.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:64290546056476b637b1a80_90287533%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e69f517dbad6078476dc3b97545304ed94370d4' => 
    array (
      0 => '/Applications/mampstack-5.5.29-1/apache2/htdocs/iis/application/views/templates/resources/products_create.tpl',
      1 => 1447521108,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '64290546056476b637b1a80_90287533',
  'variables' => 
  array (
    'ismerch' => 0,
    'merchant' => 0,
    'indexpath' => 0,
    'inputdata' => 0,
    'categories' => 0,
    'category' => 0,
    'merchants' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56476b63878d00_14448384',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56476b63878d00_14448384')) {
function content_56476b63878d00_14448384 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '64290546056476b637b1a80_90287533';
?>
<!-- CREATE PRODUCT -->
<div id="create" class="tab-pane fade">
<h3>Create new product 
<?php if ($_smarty_tpl->tpl_vars['ismerch']->value) {?> as <?php echo $_smarty_tpl->tpl_vars['merchant']->value['COMPANY'];?>
 <?php }?>
</h3>
<br>

  
<!-- CREATE FORM BASIC -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="<?php echo $_smarty_tpl->tpl_vars['indexpath']->value;?>
/products/createNewProduct">
<div class="form-group">
<div class="has-error">
	
	<!-- NAME -->
	<p class="bg-danger" style="padding: 10px 10px 10px 10px">
	<label for="product_name">Name:</label>
	<input type="text" class="form-control" id="product_name" placeholder="Name" name="product_name" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Name'];?>
"> 
	
	<!-- COUNT -->
	<label for="product_count">Count:</label>
	<input type="text" class="form-control" id="product_count" placeholder="Count" name="product_count" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Count'];?>
">  
	<br><br>

	<!-- BRAND -->
	<label for="product_brand">Brand:</label>
	<input type="text" class="form-control" id="product_brand" placeholder="Brand" name="product_brand" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Brand'];?>
">
	
	<!-- PRICE -->
	<label for="product_price">Price:</label>
	<input type="text" class="form-control" id="product_price" placeholder="Price" name="product_price" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Price'];?>
"> 
	</p> 

	<!-- TYPE -->
	<p class="bg-warning" style="padding: 10px 10px 10px 10px">
	<label for="product_type">Type:</label>
	<select class="form‐control" id="product_type" name="product_type">
  		<option value="0">Select type</option>
  		<option value="1" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['ProductType'] == "1") {?>selected<?php }?>>Hardware</option>
  		<option value="2" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['ProductType'] == "2") {?>selected<?php }?>>Software</option>
	</select>

	<!-- CATEGORY -->
	<span id="softwareC"  <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['ProductType'] != "2") {?> style="display:none" <?php }?>>
		<label for="product_category">Category:</label>
		<select class="form‐control" id="product_category" name="product_category">
		  <option value="0">Select category</option>
		  <?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$foreach_category_Sav = $_smarty_tpl->tpl_vars['category'];
?>
		  <?php if ($_smarty_tpl->tpl_vars['category']->value['TYPE'] == "S") {?>
		  	<option value="<?php echo $_smarty_tpl->tpl_vars['category']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['Category'] == $_smarty_tpl->tpl_vars['category']->value['ID']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['category']->value['NAME'];?>
</option>
		  <?php }?>
		  <?php
$_smarty_tpl->tpl_vars['category'] = $foreach_category_Sav;
}
?>
		</select>
	</span>
	<span id="hardwareC" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['ProductType'] != "1") {?> style="display:none" <?php }?>>
		<label for="product_category">Category:</label>
		<select class="form‐control" id="product_category" name="product_category">
		  <option value="0">Select category</option>
		  <?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$foreach_category_Sav = $_smarty_tpl->tpl_vars['category'];
?>
		  <?php if ($_smarty_tpl->tpl_vars['category']->value['TYPE'] == "H") {?>
		  	<option value="<?php echo $_smarty_tpl->tpl_vars['category']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['Category'] == $_smarty_tpl->tpl_vars['category']->value['ID']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['category']->value['NAME'];?>
</option>
		  <?php }?>
		  <?php
$_smarty_tpl->tpl_vars['category'] = $foreach_category_Sav;
}
?>
		</select>
	</span>

	<!-- MERCHANT -->
	<?php if ($_smarty_tpl->tpl_vars['ismerch']->value) {?>
		<input type="hidden" name="product_merchant" id="product_merchant" value="<?php echo $_smarty_tpl->tpl_vars['merchant']->value['ID'];?>
">
	<?php } else { ?>
		<label for="product_merchant">Merchant:</label>
		<select class="form‐control" id="product_merchant" name="product_merchant">
		<option value="0">Select merchant</option>
		<?php
$_from = $_smarty_tpl->tpl_vars['merchants']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['merchant'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['merchant']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['merchant']->value) {
$_smarty_tpl->tpl_vars['merchant']->_loop = true;
$foreach_merchant_Sav = $_smarty_tpl->tpl_vars['merchant'];
?>
  			<option value="<?php echo $_smarty_tpl->tpl_vars['merchant']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['Merchant'] == $_smarty_tpl->tpl_vars['merchant']->value['ID']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['merchant']->value['COMPANY'];?>
</option>
		<?php
$_smarty_tpl->tpl_vars['merchant'] = $foreach_merchant_Sav;
}
?>
		</select>
	<?php }?>
	</p>

	<!-- WEIGHT -->
	<div id="hardware" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['ProductType'] != "1") {?> style="display:none" <?php }?>>
	<p class="bg-info" style="padding: 10px 10px 10px 10px">
	<label for="product_weight">Weight:</label>
	<input type="text" class="form-control" id="product_weight" placeholder="Weight" name="product_weight" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Weight'];?>
"> 
	
	<!-- SIZE -->
	<label for="product_size">Size:</label>
	<input type="text" class="form-control" id="product_size" placeholder="Size" name="product_size" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Size'];?>
"> 
	</p>
	</div>

	<!-- LICENCE -->
	<div id="software" <?php if ($_smarty_tpl->tpl_vars['inputdata']->value['ProductType'] != "2") {?> style="display:none" <?php }?>>
	<p class="bg-info" style="padding: 10px 10px 10px 10px">
	<label for="product_licence">Licence:</label>
	<input type="text" class="form-control" id="product_licence" placeholder="Licence" name="product_licence" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Licence'];?>
"> 

	<!-- CONFIGURATION -->
	<label for="product_config">Configuration:</label>
	<input type="text" class="form-control" id="product_config" placeholder="Config" name="product_config" value="<?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Config'];?>
"> 
	</p>
	</div>

	<!-- DESCRIPTION -->
	<p class="bg-success" style="padding: 10px 10px 10px 10px">
	<label for="product_description">Description:</label>
	<textarea class="form-control" id="product_description" rows="4" placeholder="Description" name="product_description"><?php echo $_smarty_tpl->tpl_vars['inputdata']->value['Description'];?>

	</textarea>
	</p>

	<button type="submit" class="btn btn-primary">Submit</button>

</div>
</form>

</div>
</div><?php }
}
?>