<?php

class categories extends CI_Controller
{
	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/categories_model');
        $this->load->model('database/products_model');
        $this->load->model('checks/categories_check_model');

        // Check if logged
		if ($this->session->userdata('login') == false)
			redirect("login/index/0");

		// No access for any if not ADM
		if (!$this->isADM())
			redirect("login/noAccess");
    }
	
	/* The main function to show users 
	 * @parameter - Page ID (show/create)
	 * @return - boolean with success */
	public function show()
	{
		// If create mode use another function
		if (!empty($this->input->post("Create")))
			$this->createNewCategory();

		// Load previous input datas
		$InputData = $this->session->userdata('categoryInput');
		$this->session->unset_userdata('categoryInput');

		// Process errors 
		$DisErrors = $this->session->userdata('errors');
		$this->session->unset_userdata('errors');

		// Process input datas
		$Export = $this->input->post('Search');
    	$SearchByType = $this->input->post('seach_category_type');
    	$SearchByName = $this->input->post('seach_category_name');

		// Load models
		$Categories = $this->categories_model->getAllCategories($SearchByName, $SearchByType);

		// Process Smarty
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('SearchByType', $SearchByType);
		$this->mysmarty->assign('SearchByName', $SearchByName);
		$this->mysmarty->assign('inputdata', $InputData);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('errors', $DisErrors);
		$this->mysmarty->assign('categories',$Categories);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process extraction
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/categories.tpl');
			CreatePDF($ExportHTML, 'Categories.pdf', true);
		}
		else $this->mysmarty->display('categories.tpl');
	}

	/* Function to create the new user 
	 * @parameter - void
	 * @return - boolean with success */
	public function createNewCategory()
	{
		// Process input datas for person
		$CategoryInfo = array 
    	(
    		'name' => $this->input->post('seach_category_name'), 
            'type' => $this->input->post('seach_category_type')
    	);

    	// Check input datas in model
		$Errors = $this->categories_check_model->checkCreateData($CategoryInfo);

		// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/categories/show', 'location');
  			return false;
    	}

    	// Create new product
		$this->categories_model->createNewCategory($CategoryInfo['name'], $CategoryInfo['type']);
    	redirect('/categories/show', 'location');
	}

	/* Function to delete the user 
	 * @parameter - User ID to delete 
	 * @return - void */
	public function deleteCategory($CategoryID)
	{
		// FK PRODUCTS
		if (!$this->products_model->fkCategory($CategoryID))
		{
			$this->session->set_userdata('errors', "Category used in product record!");
			redirect('/categories/show', 'location');
		}

		$this->categories_model->deleteCategory($CategoryID);
        redirect('/categories/show', 'location');
	}
}

?>
