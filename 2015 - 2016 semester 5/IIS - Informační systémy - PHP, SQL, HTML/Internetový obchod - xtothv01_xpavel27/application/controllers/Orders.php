<?php

class orders extends CI_Controller
{

	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/users_model');
        $this->load->model('database/orders_model');
        $this->load->model('database/trashes_model');

        // Check if logged
		if ($this->session->userdata('login') == false)
			redirect("login/index/0");
    }
	
	/* The main function to show orders 
	 * @parameter - void 
	 * @return - boolean with success */
	public function show()
	{
		// Process errors 
		$DisErrors = $this->session->userdata('errors');
		$this->session->unset_userdata('errors');

		// Process posts 
		$Export = $this->input->post('Search');
    	$SearchByID = $this->input->post('search_order_id');
    	$SearchByLogin = $this->input->post('search_orders_login');

    	// Check post datas
    	$Errors = "";
    	if (!empty($SearchByID) && !is_numeric($SearchByID))
    		$Errors .= "Error in search order id!<br>";
    	
    	// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/orders/show', 'location');
    		return false;
    	}

		// Get datas from models depending to user roles
		$Users = $this->users_model->getRequestedUsers($SearchByID, $SearchByLogin);
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		if ($this->isADM())
			$Orders = $this->orders_model->getRequestedOrders($SearchByID, $SearchByLogin);
		else if ($this->isMERCH())
		{
			$Merchant = $this->getMERCH();
			$Orders = $this->orders_model->getRequestedOrders($SearchByID, $SearchByLogin, "", $Merchant['ID']);
		}
		else 
			$Orders = $this->orders_model->getRequestedOrders($SearchByID, $SearchByLogin, $this->getUSR());

		// Process Smarty
		$this->mysmarty->assign('users', $Users);
		$this->mysmarty->assign('SearchByID', $SearchByID);
		$this->mysmarty->assign('SearchByLogin', $SearchByLogin);
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('errors', $DisErrors);
		$this->mysmarty->assign('orders',$Orders);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process export
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/orders.tpl');
			CreatePDF($ExportHTML, 'Orders.pdf', true);
		}
		else $this->mysmarty->display('orders.tpl');
	}

	/* Function to show order details 
	 * @parameter - Order ID to show details
	 * @return - void */
	public function orderDetail($OrderId)
	{
		// Get input datas
		$Merchant = $this->session->userdata('merchant');
		$Export = $this->input->post('Search');

		// Get model datas
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		$Order = $this->orders_model->getRequestedOrder($OrderId);
		$Items = $this->orders_model->getRequestedTrashItems($OrderId);

		// Send datas to template
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('merchant', $Merchant);
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('order_info',$Order);
		$this->mysmarty->assign('items',$Items);
		$this->mysmarty->assign('order_id',$OrderId);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process export
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/orders_detail.tpl');
			CreatePDF($ExportHTML, 'OrdersDetail.pdf', true);
		}
		else $this->mysmarty->display('orders_detail.tpl');
	}

	public function deleteItem($OrderID, $ItemID)
	{
		if (!$this->isMERCH() && !$this->isADM()) redirect('/login/noAccess', 'location');

		$Result = $this->orders_model->deleteItem($ItemID);
        redirect('/orders/orderDetail/'.$OrderID, 'location');
	}

	public function orderPay($OrderID, $UserID)
	{
		if (!$this->isMERCH() && !$this->isADM()) redirect('/login/noAccess', 'location');

		$Result = $this->orders_model->orderPay($OrderID, $UserID);
        redirect('/orders/show', 'location');
	}

	public function orderShip($OrderID, $UserID)
	{
		if (!$this->isMERCH() && !$this->isADM()) redirect('/login/noAccess', 'location');
		
		$Result = $this->orders_model->orderShip($OrderID, $UserID);
        redirect('/orders/show', 'location');
	}
}

?>
