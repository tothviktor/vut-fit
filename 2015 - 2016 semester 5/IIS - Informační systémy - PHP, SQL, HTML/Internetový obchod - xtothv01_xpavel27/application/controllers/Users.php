<?php

class users extends CI_Controller
{
	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/users_model');
        $this->load->model('checks/users_check_model');
        $this->load->model('database/trashes_model');
        $this->load->model('database/orders_model');
        $this->load->model('database/merchants_model');
        $this->load->model('database/feedbacks_model');
        // Check if logged
		if ($this->session->userdata('login') == false)
			redirect("login/index/0");
    }
	
	/* The main function to show users 
	 * @parameter - Page ID (show/create)
	 * @return - boolean with success */
	public function show($PageID)
	{
		if (!$this->isADM()) redirect('/login/noAccess', 'location');

		// Load previous input datas
		$InputData = $this->session->userdata('userInput');
		$this->session->unset_userdata('userInput');

		// Process errors 
		$DisErrors = $this->session->userdata('errors');
		$this->session->unset_userdata('errors');

		// Process input datas
		$Export = $this->input->post('Search');
    	$SearchByID = $this->input->post('seach_user_id');
    	$SearchByLogin = $this->input->post('seach_user_login');

    	// Check post datas
    	$Errors = "";
    	if (!empty($SearchByID) && !is_numeric($SearchByID))
    		$Errors .= "Error in search user id!<br>";
    	
    	// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/users/show/1', 'location');
    		return false;
    	}

		// Load models
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		$Permissions = $this->users_model->getAllPermissions();
		$Users = $this->users_model->getRequestedUsers($SearchByID, $SearchByLogin);

		// Process Smarty
		$this->mysmarty->assign('SearchByID', $SearchByID);
		$this->mysmarty->assign('SearchByLogin', $SearchByLogin);
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('inputdata', $InputData);
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('pageid', $PageID);
		$this->mysmarty->assign('errors', $DisErrors);
		$this->mysmarty->assign('permissions',$Permissions);
		$this->mysmarty->assign('users',$Users);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process extraction
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/users.tpl');
			CreatePDF($ExportHTML, 'Users.pdf', true);
		}
		else $this->mysmarty->display('users.tpl');
	}

	/* Function to create the new user 
	 * @parameter - void
	 * @return - boolean with success */
	public function createNewUser()
	{
		if (!$this->isADM()) redirect('/login/noAccess', 'location');

		// Process input datas for person
		$PersonInfo = array 
    	(
    		'name' => $this->input->post('user_name'), 
            'surname' => $this->input->post('user_surname'),
            'mail' => $this->input->post('user_mail'),
            'telephone' => $this->input->post('user_telephone'),
            'address' => $this->input->post('user_address')
    	);

    	// Process input datas for user
    	$UserInfo = array
    	(
    		'login' => $this->input->post('user_login'), 
    		'password' => $this->input->post('user_password'), 
    		'permission_id' => $this->input->post('user_permission')
    	);

    	// Save to session actual input
		$this->session->set_userdata(array('userInput' => array_merge($PersonInfo, $UserInfo)));

		// Modify password
		$UserInfo['password'] = md5($UserInfo['password']);

    	// Check input datas in model
		$Errors = $this->users_check_model->checkCreateData($PersonInfo, $UserInfo);

		// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/users/show/2', 'location');
  			return false;
    	}

    	// Create new product
		$Result = $this->users_model->createNewUser($PersonInfo, $UserInfo);
    	redirect('/users/show/1', 'location');
	}

	/* Function to delete the user 
	 * @parameter - User ID to delete 
	 * @return - void */
	public function deleteUser($UserId)
	{
		if (!$this->isADM()) redirect('/login/noAccess', 'location');

		// FK FEEDBACKS
		if (!$this->feedbacks_model->fkUser($UserId))
		{
			$this->session->set_userdata('errors', "User used in feedback record!");
			redirect('/users/show/1', 'location');
		}

		// FK MERCHANTS
		if (!$this->merchants_model->fkUser($UserId))
		{
			$this->session->set_userdata('errors', "User used in merchant record!");
			redirect('/users/show/1', 'location');
		}

		// FK ORDERS 2X
		if (!$this->orders_model->fkUser($UserId))
		{
			$this->session->set_userdata('errors', "User used in order record!");
			redirect('/users/show/1', 'location');
		}

		// FK TRASHES
		if (!$this->trashes_model->fkUser($UserId))
		{
			$this->session->set_userdata('errors', "User used in trash record!");
			redirect('/users/show/1', 'location');
		}

		$Result = $this->users_model->deleteUser($UserId);
        redirect('/users/show/1', 'location');
	}

	/* Function to show user details 
	 * @parameter - User ID to show details 
	 * @return - void */
	public function userDetail($UserId)
	{
		// Process input datas
		$Export = $this->input->post('Search');

		// Call model functions
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		$Users = $this->users_model->getRequestedUser($UserId);
		$Permissions = $this->users_model->getAllPermissions();

		// Process smarty
		$this->mysmarty->assign('permissions',$Permissions);
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('user_info',$Users);
		$this->mysmarty->assign('user_id',$UserId);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process export
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/users_detail.tpl');
			CreatePDF($ExportHTML, 'UsersDetail.pdf', true);
		}
		else $this->mysmarty->display('users_detail.tpl');
	}

	public function editUser($UserId, $PersonID)
	{
		// Process input datas
		$PersonInfo = array
		(
			'Name' 	  	 => trim($this->input->post('edit_name')),
			'Surname'  	 => trim($this->input->post('edit_surname')),
			'Mail' 	  	 => trim($this->input->post('edit_mail')),
			'Telephone'  => trim($this->input->post('edit_telephone')),
			'Address' 	 => trim($this->input->post('edit_address')),
			'Permission' => trim($this->input->post('edit_permission'))
    	);

    	$UserInfo = array
    	(
    		'Permission' => trim($this->input->post('edit_permission'))
    	);

		// Check for errors
    	$Errors = $this->users_check_model->checkEditData($PersonInfo, $UserInfo);

    	// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/products/show/1', 'location');
  			return false;
    	}

    	// Edit the selected product
    	$Result = $this->users_model->editUser($PersonInfo, $UserInfo, $UserId, $PersonID);
    	redirect('/products/show/1', 'location');
	}
}

?>
