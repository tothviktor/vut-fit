<?php

class trashes extends CI_Controller
{
	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/trashes_model');

        // Check if logged
		if ($this->session->userdata('login') == false)
			redirect("login/index/0");

		if ($this->isMERCH()) 
			redirect('/login/noAccess', 'location');
    }
	
	/* The main function to show feedbacks
	 * @parameter - void
	 * @return - void  */
	public function show()
	{ 
		// Process errors 
		$DisErrors = $this->session->userdata('errors');
		$this->session->unset_userdata('errors');
    	
		// Call model functions 
		$ActiveTrash = $this->trashes_model->getActiveTrash($this->getUSR());
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());	
		$Items = ($ActiveTrash < 1 ? false : $this->trashes_model->getRequestedItems($this->getUSR(), $ActiveTrash));
		
		// Process Smarty
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('errors', $DisErrors);
		$this->mysmarty->assign('items',$Items);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));
		$this->mysmarty->display('trashes.tpl'); 
	}

	public function addItem($ProductID) 
	{
		// Try to get active trash
		$ActiveTrash = $this->trashes_model->getActiveTrash($this->getUSR()); 

		// If not any create new one
		if ($ActiveTrash == false)
			$ActiveTrash = $this->trashes_model->newActiveTrash($this->getUSR());

		// Add product to this trash in trash_items
		$this->trashes_model->addItemToTrash($ActiveTrash, $ProductID);

		redirect("products/show/1");
	}

	public function processOrder()
	{
		$ActiveTrash = $this->trashes_model->getActiveTrash($this->getUSR()); 
		$TrashPrice = $this->trashes_model->getTrashPrice($ActiveTrash);
		$this->trashes_model->createNewOrder($ActiveTrash, $this->getUSR(), $TrashPrice);
		$this->trashes_model->closeTrash($ActiveTrash);

		$ProductsIDs = $this->trashes_model->getProductsIDsFromTrash($ActiveTrash);
		$this->trashes_model->removeFromCount($ProductsIDs);
		redirect("products/show/1");
	}

	public function deleteItem($ItemID)
	{
		$this->trashes_model->deleteItemFromTrash($ItemID);
		redirect("trashes/show");
	}

	public function fkProduct($ProductID)
    {
        $query = $this->db->query('SELECT * FROM trash_items WHERE product_id =' . $ProductID);
        return ($query->num_rows() > 0 ? false : true);
    }
}

?>
