<?php

class feedbacks extends CI_Controller
{
	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/feedbacks_model');
        $this->load->model('database/products_model');
        $this->load->model('database/trashes_model');

        // Check if logged
		if ($this->session->userdata('login') == false)
			redirect("login/index/0");
    }
	
	/* The main function to show feedbacks
	 * @parameter - void
	 * @return - void  */
	public function show()
	{ 
		// Process errors 
		$DisErrors = $this->session->userdata('errors');
		$this->session->unset_userdata('errors');
		$Merchant = $this->getMERCH();

		// Process post data
		$Export = $this->input->post('Search');
		$SearchByProduct = $this->input->post('seach_feedback_product');
    	$SearchByUser = $this->input->post('seach_feedback_user');
    	
		// Call model functions 
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		$Products = $this->products_model->getRequestedProducts();
		$Users = $this->feedbacks_model->getAllUsers();

		// Get datas depending on role
		if ($this->isADM()) 
			$Feedbacks = $this->feedbacks_model->getRequestedFeedbacks($SearchByProduct, $SearchByUser, "", "");
		else if ($this->isMERCH())			
			$Feedbacks = $this->feedbacks_model->getRequestedFeedbacks($SearchByProduct, $SearchByUser, $Merchant['ID'], "");
		else 
			$Feedbacks = $this->feedbacks_model->getRequestedFeedbacks($SearchByProduct, "", "", $this->getUSR());
		
		// Process Smarty
		$this->mysmarty->assign('SearchByProduct', $SearchByProduct);
		$this->mysmarty->assign('SearchByUser', $SearchByUser);
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('errors', $DisErrors);
		$this->mysmarty->assign('products',$Products);
		$this->mysmarty->assign('feedbacks',$Feedbacks);
		$this->mysmarty->assign('users',$Users);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process export
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/feedbacks.tpl');
			CreatePDF($ExportHTML, 'Feedbacks.pdf', true);
		}
		else $this->mysmarty->display('feedbacks.tpl');
	}

	/* Function to delete feedback 
	 * parameter - Feedback ID
	 * return - void */
	public function deleteFeedback($FeedbackId)
	{
		if ($this->isMERCH()) redirect('/login/noAccess', 'location');

		if ($this->isADM())
			$Result = $this->feedbacks_model->deleteFeedback($FeedbackId);
		else
			$Result = $this->feedbacks_model->deleteFeedback($FeedbackId, $this->getUSR());
        redirect('/feedbacks/show', 'location');
	}

	public function addFeedback($ProductID)
	{
		if ($this->isMERCH()) redirect('/login/noAccess', 'location');

		$Mark = $this->input->post('mark');

		if ($Mark < 1)
		{
			$this->session->set_userdata('errors', "Error in create setting feedback!<br>");
    		redirect('/products/show/1', 'location');
  			return false;
  		}

  		$Result = $this->feedbacks_model->addFeedback($ProductID, $Mark, $this->getUSR());
  		redirect('/products/productDetail/' . $ProductID, 'location');
  		return true;
	}
}

?>
