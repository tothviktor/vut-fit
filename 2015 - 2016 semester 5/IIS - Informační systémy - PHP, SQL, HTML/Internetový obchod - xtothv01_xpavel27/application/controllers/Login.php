<?php

class login extends CI_Controller
{
	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/login_model');
        $this->load->model('database/users_model');
        $this->load->model('checks/users_check_model');
    }
	
	/* The main loggin page 
	 * @parameter - void 
	 * @return - void */
	public function index($Error = 0)
	{	
		// If create mode use another function
		if (!empty($this->input->post("btn_create")))
			$this->create();

		// Process post data
		$User = $this->input->post("login_user");
        $Pass = $this->input->post("login_password");

		// If correct info passed
		if (!empty($User) && !empty($Pass)) 
		{
			// Get user
			$Login = $this->login_model->getUser($User, $Pass);

			// Get merchant
			if ($Login['PERMISSION'] == '2')
				$Merchant = $this->login_model->getMerchant($Login['ID']);

			// If user found 
			if ($Login != false) 
            {
            	// Fill the session
                $SessionData = array
                (
                	'username' => $Login['LOGIN'],
                	'permission' => $Login['PERMISSION'],
                	'person'	=> $Login['PERSON'],
                	'user_id'	=> $Login['ID'],
                    'login'    => TRUE,
                    'merchant' => array ('ID' => $Merchant['ID'], 'COMPANY' => $Merchant['COMPANY']),
                );
                
                // Save session and redirect
                $this->session->set_userdata($SessionData);
                redirect("products/show/1");
            }

            // If not found ask again
            else redirect('login/index/1');
		}

		// Process Smarty
		$this->mysmarty->assign('error',$Error);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));
		$this->mysmarty->display('login2.tpl');
	}

	/* Function to logout user
	 * @parameter - void
	 * @return - void  */
	public function outdex()
	{
		$this->session->sess_destroy();
		redirect('login/index/0');
	}

	private function create()
	{
		$User = $this->input->post("login_user");
        $Pass = $this->input->post("login_password");

		$GetUser = $this->login_model->getUsername($User);
		if ($GetUser != false) redirect('login/index/2');

		$UserInfo = array
    	(
    		'login' => $User, 
    		'password' => md5($Pass), 
    		'permission_id' => 3
    	);

    	$PersonInfo = array 
    	(
    		'name' => '', 
            'surname' => '',
            'mail' => '',
            'telephone' => '',
            'address' => ''
    	);

    	$Errors = $this->users_check_model->checkCreateData(false, $UserInfo);
    	if (!empty($Errors)) redirect('login/index/3');

		$Result = $this->users_model->createNewUser($PersonInfo, $UserInfo);
		redirect('login/index/4');
	}

	public function noAccess($Error = 0)
	{	
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));
		$this->mysmarty->display('errors/noAccess.tpl');
	}
}

?>
