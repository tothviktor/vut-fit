<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH.'helpers/Dompdf/dompdf_config.inc.php' );

function CreatePDF($html, $filename='', $stream=TRUE) 
{
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    if ($stream) {
        $dompdf->stream($filename.".pdf");
    } else {
        return $dompdf->output();
    }
}
?>