<?php

class Users_check_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* Function to check datas for user creation 
     * @parameter - Informations about person table
     * @parameter - Informations about user table 
     * @return - String with errors */
    public function checkCreateData($PersonInfo, $UserInfo)
    {
        $Errors = "";

        if ($PersonInfo != false)
        {
            if (!empty($PersonInfo['name']) && strlen($PersonInfo['name']) < 3)
                $Errors .= "Error in create person name!<br>";
            if (!empty($PersonInfo['surname']) && strlen($PersonInfo['surname']) < 3)
                $Errors .= "Error in create person surname!<br>";
            if(!empty($PersonInfo['mail']) && preg_match('/\S+@\S+.[a-zA-Z]+/', $PersonInfo['mail']) !== 1)
                $Errors .= "Error in create person mail!<br>";
             if(!empty($PersonInfo['telephone']) && preg_match('/\+?[0-9]/', $PersonInfo['telephone']) !== 1)
                $Errors .= "Error in create peron telephone!<br>";
            if (!empty($PersonInfo['address']) && strlen($PersonInfo['address']) < 5)
                $Errors .= "Error in create person address!<br>";
        }

        if (empty($UserInfo['login']) || strlen($UserInfo['login']) < 5)
            $Errors .= "Error in create user name!<br>";
        if (empty($UserInfo['password']) || strlen($UserInfo['password']) < 5)
            $Errors .= "Error in create user password!<br>";
        if (empty($UserInfo['permission_id']) || $UserInfo['permission_id'] == "0" || !is_numeric($UserInfo['permission_id']))
            $Errors .= "Error in create user permissions!<br>";
        return $Errors;
    }

    public function checkEditData($PersonInfo, $UserInfo)
    {
        $Errors = "";

        if (!empty($PersonInfo['Name']) && strlen($PersonInfo['Name']) < 5)
            $Errors .= "Error in create person name!<br>";
        if (!empty($PersonInfo['Surname']) && strlen($PersonInfo['Surname']) < 5)
            $Errors .= "Error in create person surname!<br>";
        if(!empty($PersonInfo['Mail']) && preg_match('/\S+@\S+.[a-zA-Z]+/', $PersonInfo['Mail']) !== 1)
            $Errors .= "Error in create person mail!<br>";     
        if(!empty($PersonInfo['Telephone']) && preg_match('/\+?[0-9]/', $PersonInfo['Telephone']) !== 1)
            $Errors .= "Error in create peron telephone!<br>";
        if (!empty($PersonInfo['Address']) && strlen($PersonInfo['Address']) < 5)
            $Errors .= "Error in create person address!<br>";

        if (!empty($UserInfo['Permission']) && ($UserInfo['Permission'] == "0" || !is_numeric($UserInfo['Permission'])))
            $Errors .= "Error in create user permissions!<br>";

        return $Errors;
    }
}