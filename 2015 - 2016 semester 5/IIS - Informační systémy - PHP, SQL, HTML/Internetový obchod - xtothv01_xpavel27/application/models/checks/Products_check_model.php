<?php

class Products_check_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* Function to check datas to edit product
     * @param - Array of input datas for edit
     * @return - Error messages */
    public function checkEditData($NewProductInfo)
    {
        // Check basic infos
        $Errors = "";
        if (!empty($NewProductInfo['Name']) && strlen($NewProductInfo['Name']) < 5)
            $Errors .= "Error in create product name!<br>";
        if (!empty($NewProductInfo['Count']) && (intval($NewProductInfo['Count']) < 1 || !is_numeric($NewProductInfo['Count'])))
            $Errors .= "Error in create product count!<br>";
        if (!empty($NewProductInfo['Brand']) && strlen($NewProductInfo['Brand']) < 5)
            $Errors .= "Error in create product brand!<br>";
        if (!empty($NewProductInfo['Price']) && (intval($NewProductInfo['Price']) < 1 || !is_numeric($NewProductInfo['Price'])))
            $Errors .= "Error in create product price!<br>";
        if (!empty($NewProductInfo['ProductType']) && $NewProductInfo['ProductType'] == "0")
            $Errors .= "Error in create product type!<br>";
        if (!empty($NewProductInfo['Category']) && $NewProductInfo['Category'] == "0")
            $Errors .= "Error in create product category!<br>";
        if (!empty($NewProductInfo['Merchant']) && $NewProductInfo['Merchant'] == "0")
            $Errors .= "Error in create product merchant!<br>";
        if (!empty($NewProductInfo['Description']) && strlen($NewProductInfo['Description']) < 5)
            $Errors .= "Error in create product description!<br>";
        if(!empty($NewProductInfo['Weight']) && (intval($NewProductInfo['Weight']) < 1 || !is_numeric($NewProductInfo['Weight'])))
            $Errors .= "Error in create product weight!<br>";
        if(!empty($NewProductInfo['Size']) && strlen($NewProductInfo['Size']) < 5)
            $Errors .= "Error in create product size!<br>";   
        if(!empty($NewProductInfo['Licence']) && strlen($NewProductInfo['Licence']) < 5)
            $Errors .= "Error in create product licence!<br>";
        if(!empty($NewProductInfo['Config']) && strlen($NewProductInfo['Config']) < 5)
            $Errors .= "Error in create product config!<br>";
        return $Errors;
    }

    /* Function to check datas to create product
     * @param - Array of input datas for creation
     * @return - Error messages */
    public function checkCreateData($NewProductInfo)
    {
        // Check basic infos
        $Errors = "";
        if (empty($NewProductInfo['Name']) || strlen($NewProductInfo['Name']) < 5)
            $Errors .= "Error in create product name!<br>";
        if (empty($NewProductInfo['Count']) || intval($NewProductInfo['Count']) < 1 || !is_numeric($NewProductInfo['Count']))
            $Errors .= "Error in create product count!<br>";
        if (empty($NewProductInfo['Brand']) || strlen($NewProductInfo['Brand']) < 5)
            $Errors .= "Error in create product brand!<br>";
        if (empty($NewProductInfo['Price']) || intval($NewProductInfo['Price']) < 1 || !is_numeric($NewProductInfo['Price']))
            $Errors .= "Error in create product price!<br>";
        if (empty($NewProductInfo['ProductType']) || $NewProductInfo['ProductType'] == "0")
            $Errors .= "Error in create product type!<br>";
        if (empty($NewProductInfo['Category']) || $NewProductInfo['Category'] == "0")
            $Errors .= "Error in create product category!<br>";
        if (empty($NewProductInfo['Merchant']) || $NewProductInfo['Merchant'] == "0")
            $Errors .= "Error in create product merchant!<br>";
        if (empty($NewProductInfo['Description']) || strlen($NewProductInfo['Description']) < 5)
            $Errors .= "Error in create product description!<br>";

        // Check hardware infos
        if ($NewProductInfo['ProductType'] == "1") 
        {
            if(empty($NewProductInfo['Weight']) || intval($NewProductInfo['Weight']) < 1 || !is_numeric($NewProductInfo['Weight']))
                $Errors .= "Error in create product weight!<br>";
            if(empty($NewProductInfo['Size']) || strlen($NewProductInfo['Size']) < 5)
                $Errors .= "Error in create product size!<br>";
        }

        // Check software infos
        else if ($NewProductInfo['ProductType'] == "2")
        {
            if(empty($NewProductInfo['Licence']) || strlen($NewProductInfo['Licence']) < 5)
                $Errors .= "Error in create product licence!<br>";
            if(empty($NewProductInfo['Config']) || strlen($NewProductInfo['Config']) < 5)
                $Errors .= "Error in create product config!<br>";
        }
        return $Errors;
    }
}