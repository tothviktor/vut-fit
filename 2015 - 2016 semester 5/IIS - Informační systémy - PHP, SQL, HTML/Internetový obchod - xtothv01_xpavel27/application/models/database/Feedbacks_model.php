<?php

class Feedbacks_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* The main core function to get datas about feedbacks 
     * @parameter1 - Product name
     * @parameter2 - User login
     * @parameter3 - Product company ID 
     * @parameter4 - Feedback commenter user ID 
     * @return - array with result datas*/
    public function getRequestedFeedbacks($SearchByProduct = "", $SearchByUser = "", $SearchByCompanyID = "", $SearchByUserID = "") 
    {
        // Create base SQL query 
        $this->db->select('fe.id, us.login, pr.name, fe.mark, me.company');
        $this->db->from('feedbacks fe');
        $this->db->join('users us', 'fe.user_id = us.id');
        $this->db->join('products pr', 'fe.product_id = pr.id');
        $this->db->join('merchants me', 'pr.merchant_id = me.id');

        // Append where conditions
    	if (!empty($SearchByProduct))
            $this->db->like('pr.name', $SearchByProduct);
    	if (!empty($SearchByUser))
            $this->db->where('us.login', $SearchByUser);
        if (!empty($SearchByCompanyID))
            $this->db->where('me.id', $SearchByCompanyID);
        if (!empty($SearchByUserID))
            $this->db->where('fe.user_id', $SearchByUserID);
    	
    	// Process the query and create result
    	$query = $this->db->get();
		$Result = array();

		// Fill result with output datas
		foreach ($query->result() as $row)
		{
		    array_push($Result, array
		   	(
		   		'ID' => $row->id,
		   		'USER' => $row->login,
		   		'PRODUCT' => $row->name,
		   		'MARK' => $row->mark,
                'COMPANY' => $row->company
		   	));
       	}
       	return $Result;
    }

    /* Function to get all users data without conditions
     * @parameter - void 
     * @return - array with result datas */
    public function getAllUsers()
    {
        // Process query and create array
    	$query = $this->db->get('users'); 
		$Result = array();

        // Fill output datas
		foreach ($query->result() as $row)
		{
		    array_push($Result, array
			(
		   		'ID' => $row->id,
		   		'LOGIN' => $row->login,
		   	));
       	}
       	return $Result;
    }

    /* Delete the selected feedback 
     * @parameter - Feedback ID to delete 
     * @return - void */
    public function deleteFeedback($FeedbackID, $UserID = 0) 
    {
        if ($UserID != 0)
            $this->db->delete('feedbacks', array('id' => $FeedbackID, 'user_id' => $UserID));
        else
            $this->db->delete('feedbacks', array('id' => $FeedbackID));
    }

    public function addFeedback($ProductID, $Mark, $User)
    {
        $this->db->insert('feedbacks', array('mark' => $Mark, 'product_id' => $ProductID, 'user_id' => $User));
    }

    public function fkProduct($ProductID)
    {
        $query = $this->db->query('SELECT * FROM feedbacks WHERE product_id =' . $ProductID);
        return ($query->num_rows() > 0 ? false : true);
    }

    public function fkUser($UserID)
    {
        $query = $this->db->query('SELECT * FROM feedbacks WHERE user_id =' . $UserID);
        return ($query->num_rows() > 0 ? false : true);
    }
}

?>