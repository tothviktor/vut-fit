<?php

class Merchants_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* The main function to get all merchant with selected conditions
     * @parameter - Merchant ID to search
     * @parameter - Merchant Company name to search 
     * @return - result with datas */
    public function getRequestedMerchants($SearchByID = '', $SearchByName = '') 
    {	
        // Create SQL query
        $this->db->select('me.id, us.login, pers.name, pers.surname, me.company, me.delivery, me.website, us.id user_id');
        $this->db->from('merchants me');
        $this->db->join('users us', 'me.user_id = us.id');
        $this->db->join('persons pers', 'us.person_id = pers.id');

    	// Process where conditions
    	if (!empty($SearchByID))
            $this->db->where('me.id', $SearchByID);
    	if (!empty($SearchByName))
            $this->db->where('me.company', $SearchByName);
    	
    	// Process the query and create result
    	$query = $this->db->get();
		$Result = array();

		// Fill result with output datas
		foreach ($query->result() as $row)
		{
		    array_push($Result, array
		   	(
		   		'ID' => $row->id,
		   		'LOGIN' => $row->login,
                'NAME' => $row->name,
                'SURNAME' => $row->surname,
		   		'COMPANY' => $row->company,
                'DELIVERY' => $row->delivery,
                'WEBSITE' => $row->website,
                'USER_ID' => $row->user_id
		   	));
       	}
       	return $Result;
    }

    /* Get all avivable merchant users 
     * @parameter - void
     * @return - void  */
    public function getAllAcms() 
    {   
        // Prepare SQL query
        $this->db->select('us.id, us.login');
        $this->db->from('users us');
        $this->db->join('permissions pe', 'us.permission_id = pe.id');
        $this->db->where('pe.name', 'Merchant');
       
        // Process the query and create result
        $query = $this->db->get();
        $Result = array();

        // Fill result with output datas
        foreach ($query->result() as $row)
        {
            if ($this->fkUser($row->id))
            {
                array_push($Result, array
                (
                    'ID' => $row->id,
                    'LOGIN' => $row->login
                ));
            }
        }
        return $Result;
    }

    /* Function to create new merchant users
     * @parameter - Array with merchant input datas
     * @return - void */
    public function createNewMerchant($MerchantInfo) 
    {
    	$ResultUser = $this->db->insert('merchants', $MerchantInfo);
    }

    /* Function to delete selected merchant
     * @parameter - Merchant ID to delete
     * @return - void */
    public function deleteMerchant($MerchantID) 
    {
        $this->db->delete('merchants', array('id' => $MerchantID));
    }

    /* Function to check input datas to merchant create
     * @parameter - Array with merchant info datas to create
     * @return - Errors string */
    public function checkCreateData($MerchantInfo)
    {
        $Errors = "";
        if (empty($MerchantInfo['company']) || strlen($MerchantInfo['company']) < 5)
            $Errors .= "Error in create merchant company!<br>";
        if (empty($MerchantInfo['delivery']) || !is_numeric($MerchantInfo['delivery']))
            $Errors .= "Error in create merchant delivery!<br>";
        if (empty($MerchantInfo['website']) || strlen($MerchantInfo['website']) < 5)
            $Errors .= "Error in create merchant website!<br>";
        if (empty($MerchantInfo['user_id']) || $MerchantInfo['user_id'] == "0" || !is_numeric($MerchantInfo['user_id']))
            $Errors .= "Error in create merchant user id !<br>";
        return $Errors;
    }

    public function fkUser($UserID)
    {
        $query = $this->db->query('SELECT * FROM merchants WHERE user_id =' . $UserID);
        return ($query->num_rows() > 0 ? false : true);
    }
}

?>