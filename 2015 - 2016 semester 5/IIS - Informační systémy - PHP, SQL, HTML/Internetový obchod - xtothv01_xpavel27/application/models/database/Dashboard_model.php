<?php

class Dashboard_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /* Get payments for the month 
     * @parameter - void 
     * @return - array with price */
    public function getMonthPayed() 
    {
        // Create the query
        $this->db->select('SUM(price) payed_month', false);
        $this->db->where("timestamp > '" . date('Y-m') . "-01'");
        $query = $this->db->get('orders');

        // Get and return result
        $ResultQ = $query->result();
        $Result = array();
        return $ResultQ[0]->payed_month;
    }
}

?>