<?php

class Categories_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* Function to get all categories 
     * @param - void
     * @return - result of all categories */
    public function getAllCategories($Name = "", $Type = "")
    {
        // Process query 
        $this->db->from('categories');

        if (!empty($Name))
            $this->db->like('name', $Name);
        if (!empty($Type))
            $this->db->where('type', $Type);

        $query = $this->db->get();
        $Result = array();

        // Get results 
        foreach ($query->result() as $row)
        {
            array_push($Result, array
            (
                'ID' => $row->id,
                'NAME' => $row->name,
                'TYPE' => $row->type
            ));
        }
        return $Result;
    }

    /* Delete the selected feedback 
     * @parameter - Feedback ID to delete 
     * @return - void */
    public function deleteCategory($CategoryID) 
    {
        $this->db->delete('categories', array('id' => $CategoryID));
    }

    public function createNewCategory($Name, $Type)
    {
        $this->db->insert('categories', array('name' => $Name, 'type' => $Type));
    }
}

?>