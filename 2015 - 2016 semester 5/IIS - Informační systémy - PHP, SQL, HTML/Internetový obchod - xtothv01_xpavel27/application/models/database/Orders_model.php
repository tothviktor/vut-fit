<?php

class Orders_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /* Function to get info about requisted order detail
     * @parameter - Order ID to show details
     * @return - The array with result array */
    public function getRequestedOrder($OrderID) 
    {
        // Create SQL query
        $this->db->select('od.employee_id,us.login,od.id,od.payed,od.expanded,od.price, pe.name, pe.surname, pe.mail, pe.telephone, pe.address');
        $this->db->from('orders od');
        $this->db->join('users us', 'od.user_id = us.id');
        $this->db->join('persons pe', 'us.person_id = pe.id');

        // Process database request
        $query = $this->db->get();
        $ResultQ = $query->result();
        $Result = array();

        // Create result array
        $Result['ID'] = $ResultQ[0]->id;
        $Result['LOGIN'] = $ResultQ[0]->login;
        $Result['EMPLOYEE_ID'] = $ResultQ[0]->employee_id;
        $Result['PRICE'] = $ResultQ[0]->price;
        $Result['PAYED'] = $ResultQ[0]->payed;
        $Result['EXPANDED'] = $ResultQ[0]->expanded;
        $Result['NAME'] = $ResultQ[0]->name;
        $Result['SURNAME'] = $ResultQ[0]->surname;
        $Result['MAIL'] = $ResultQ[0]->mail;
        $Result['TELEPHONE'] = $ResultQ[0]->telephone;
        $Result['ADDRESS'] = $ResultQ[0]->address;
        return $Result;
    }

    /* The main function to get all orders depending on contidions
     * @parameter - Order ID to search by
     * @parameter - Login name to search by
     * @parameter - Customer filtration by User ID 
     * @parameter - Merchant filtration by Merchant ID 
     * @return - Array with order datas */
    public function getRequestedOrders($SearchByID = "", $SearchByLogin = "", $UseUserID = "", $UseMerchantID = "") 
    {
        // Prepare SQL query
        $this->db->select('od.trash_id, od.employee_id,us.login,od.id,od.payed,od.expanded,od.price');
        $this->db->from('orders od');
        $this->db->join('users us', 'od.user_id = us.id');

        // Process where conditions
    	if (!empty($SearchByID))
    	   $this->db->where('od.id', $SearchByID);
    	if (!empty($SearchByLogin))
            $this->db->like('us.login', $SearchByLogin);
        if (!empty($UseUserID))
            $this->db->where('od.user_id', $UseUserID);
    	
    	// Process the query and create result
    	$query = $this->db->get();
		$Result = array();

		// Fill result with output datas
		foreach ($query->result() as $row)
		{
            // Get all items from the trash
            $Found = false;
            if ($this->isMERCH())
            {
                // Prepare SQL query and process it
                $this->db->select('prd.merchant_id'); 
                $this->db->from('trash_items tri');
                $this->db->join('products prd', 'tri.product_id = prd.id');
                $this->db->where('tri.trash_id', $row->trash_id);
                $query = $this->db->get();
            
                // Try to find merchant's product in trash
                foreach ($query->result() as $item)
                {
                    if ($item->merchant_id == $UseMerchantID)
                        $Found = true;
                }
            }
            else $Found = true;

            // Add informations into output array
            if ($Found)
            {
		      array_push($Result, array
	   		  (
	   	   		'ID' => $row->id,
	   			'LOGIN' => $row->login,
	   			'EMPLOYEE_ID' => $row->employee_id,
	   			'PRICE' => $row->price,
	   			'PAYED' => $row->payed,
	   			'EXPANDED' => $row->expanded
	   		  ));
            }
       	}
       	return $Result;
    }

    /* Function to get required items from the trash 
     * @parameter - ID of the order 
     * @return - Array with the order details */
    public function getRequestedTrashItems($OrderID) 
    {
        // Prepare SQL query 
        $this->db->select('merch.company, trit.id, prd.id prod_id, prd.name, prd.price, prd.vat'); 
        $this->db->from('orders od');
        $this->db->join('trashes tr', 'od.trash_id = tr.id');
        $this->db->join('trash_items trit', 'tr.id = trit.trash_id');
        $this->db->join('products prd', 'trit.product_id = prd.id');
        $this->db->join('merchants merch', 'prd.merchant_id = merch.id');
        $this->db->where('od.id', $OrderID);
        
        // Process the query and create result
        $query = $this->db->get();
        $Result = array();

        // Fill result with output datas
        foreach ($query->result() as $row)
        {
            array_push($Result, array
            (
                'COMPANY' => $row->company,
                'NAME' => $row->name,
                'PRICE' => $row->price,
                'VAT' => $row->vat,
                'ITEM_ID' => $row->id,
                'PRODUCT_ID' => $row->prod_id
            ));
        }
        return $Result;
    }

    public function deleteItem($ItemID)
    {
        $this->db->delete('trash_items', array('id' => $ItemID));
    }


    public function orderPay($OrderID, $UserID)
    {
        $this->db->where('id', $OrderID);
        $this->db->update('orders', array('payed' => 'Y', 'employee_id' => $UserID));
    }

    public function orderShip($OrderID, $UserID)
    {
        $this->db->where('id', $OrderID);
        $this->db->update('orders', array('expanded' => 'Y', 'employee_id' => $UserID));
    }

    public function fkUser($UserID)
    {
        $query = $this->db->query('SELECT * FROM orders WHERE user_id =' . $UserID . ' OR employee_id=' . $UserID);
        return ($query->num_rows() > 0 ? false : true);
    }
}

?>