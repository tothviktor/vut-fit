<?php

class Products_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* The main function to get product details
     * @parameter - Product ID for details
     * @return - Array of infos for product */
    public function getRequestedProduct($ProductID) 
    {
        // Check software
        $this->db->select('software_id')->from('products')->where('id', $ProductID);
        $query = $this->db->get();
        $ResultQ = $query->result();
        $ifSoftware = $ResultQ[0]->software_id;

        // Check hardware
        $this->db->select('hardware_id')->from('products')->where('id', $ProductID);
        $query = $this->db->get();
        $ResultQ = $query->result();
        $ifHardware = $ResultQ[0]->hardware_id;

        // Get query 
        $this->db->select('pr.software_id, pr.hardware_id, pr.id, pr.name, pr.description, pr.count');
        $this->db->select('pr.brand, pr.price, pr.vat, pr.stock, ca.name ca_name, me.company, me.delivery');
        $this->db->from('products pr');
        $this->db->join('categories ca', 'pr.category_id = ca.id');
        $this->db->join('merchants me', 'pr.merchant_id = me.id');
        $this->db->where('pr.id', $ProductID);

        // Join hardware and software
        if ($ifHardware) 
        {
            $this->db->join('hardware ha', 'pr.hardware_id = ha.id');
            $this->db->select('ha.size, ha.weight');
        }
        else if ($ifSoftware)
        {
            $this->db->join('software so', 'pr.software_id = so.id');
            $this->db->select('so.licence_type, so.configuration');
        }

        // Process query
        $query = $this->db->get();
        $ResultQ = $query->result();
        $Result = array();

        // Create result array
        $Result['ID'] = $ResultQ[0]->id;
        $Result['NAME'] = $ResultQ[0]->name;
        $Result['COUNT'] = $ResultQ[0]->count;
        $Result['BRAND'] = $ResultQ[0]->brand;
        $Result['DESCRIPTION'] = $ResultQ[0]->description;
        $Result['PRICE'] = $ResultQ[0]->price;
        $Result['VAT'] = $ResultQ[0]->vat;
        $Result['STOCK'] = $ResultQ[0]->stock;
        $Result['CATEGORY'] = $ResultQ[0]->ca_name;
        $Result['COMPANY'] = $ResultQ[0]->company;
        $Result['DELIVERY'] = $ResultQ[0]->delivery;
        $Result['SOFTWARE'] = ($ifSoftware ? true : false);
        $Result['HARDWARE'] = ($ifHardware ? true : false);

        // Append to the result specified informations 
        if ($ifHardware) 
        {
            $Result['SIZE'] = $ResultQ[0]->size;
            $Result['WEIGHT'] = $ResultQ[0]->weight;
        }
        else if ($ifSoftware)
        {
            $Result['LICENCE_TYPE'] = $ResultQ[0]->licence_type;
            $Result['CONFIGURATION'] = $ResultQ[0]->configuration;
        }

        return $Result;
    }

    /* The main function to show products depending on conditions
     * @parameter - Product ID to search by
     * @parameter - Product Name to search by 
     * @return - Result of all required products */
    public function getRequestedProducts($SearchByID = "", $SearchByName = "", $Customer = false, $Merchant = false) 
    {
        // Get query
        $this->db->select('pr.id, pr.name, pr.count, pr.brand, pr.price, pr.vat');
        $this->db->select('pr.stock, ca.name ca_name, ca.id ca_id, me.company');
        $this->db->from('products pr');
        $this->db->join('categories ca', 'pr.category_id = ca.id');
        $this->db->join('merchants me', 'pr.merchant_id = me.id');

        // Get where conditions
    	if (!empty($SearchByID))
            $this->db->where('pr.id', $SearchByID);
    	if (!empty($SearchByName))
            $this->db->like('pr.name', $SearchByName);
        if ($Customer)
            $this->db->where('pr.count >', 0);
        if (is_array($Merchant))
            $this->db->where('me.company', $Merchant['COMPANY']);
    	
    	// Process the query and create result
    	$query = $this->db->get();
		$Result = array();

		// Fill result with output datas
		foreach ($query->result() as $row)
		{
		    array_push($Result, array
		   	(
		   		'ID' => $row->id,
		   		'NAME' => $row->name,
		   		'COUNT' => $row->count,
		   		'BRAND' => $row->brand,
		   		'PRICE' => $row->price,
		   		'VAT' => $row->vat,
		   		'STOCK' => $row->stock,
		   		'CATEGORY' => $row->ca_name,
                'CATEGORY_ID' => $row->ca_id,
                'COMPANY' => $row->company
		   	));
       	}
       	return $Result;
    }

    /* Function to create new product
     * @parameter - Array with informations for create
     * @return - void */
    public function createNewProduct($NewProductInfo) 
    {
    	// Initialize variables
    	$ProductType = $NewProductInfo['ProductType'];
    	$SpecifyInfo = array();
    	$InsertTable = "";
    	$SoftwareID = 0;
    	$HardwareID = 0;

    	// If type is hardware
    	if ($ProductType == "1")
    	{
    		$InsertTable = "hardware";
    		$SpecifyInfo = array 
    		(
    			'weight' => $NewProductInfo['Weight'],
    			'size' => $NewProductInfo['Size']
    		);
    	}

    	// If type is software
    	else if ($ProductType == "2")
    	{
    		$InsertTable = "software";
    		$SpecifyInfo = array 
    		(
    			'licence_type' => $NewProductInfo['Licence'],
    			'configuration' => $NewProductInfo['Config']
    		);
    	}

    	// Proces specified query
    	$ResultSpecify = $this->db->insert($InsertTable, $SpecifyInfo);
    	$SpecifyID = $this->db->insert_id();
    	
    	// Create datas array for product
    	$ProductInfo = array
    	(
    		'name'        => $NewProductInfo['Name'],
    		'description' => $NewProductInfo['Description'],
    		'count'       => $NewProductInfo['Count'],
    		'brand'       => $NewProductInfo['Brand'],
    		'price'       => $NewProductInfo['Price'],
    		'stock'       => ($NewProductInfo['Count'] > 0 ? 'Y' : 'N'),
    		'category_id' => $NewProductInfo['Category'],
            'merchant_id' => $NewProductInfo['Merchant']
    	);

    	// Append specify info ID
    	if ($ProductType == "1") 
    		$ProductInfo['hardware_id'] = $SpecifyID;
    	else if ($ProductType == "2") 
    		$ProductInfo['software_id'] = $SpecifyID;
    	
    	// Process product query
    	$ResultProduct = $this->db->insert('products', $ProductInfo);
    }

    /* Function to delete product 
     * @param - Product id to delete
     * @return - void */
    public function deleteProduct($ProductID) 
    {
        $Product = $this->getRequestedProduct($ProductID);
        $Software = $Product['SOFTWARE'];
        $Hardware = $Product['HARDWARE'];

        $this->db->delete('products', array('id' => $ProductID));
        
        if (!empty($Software)) $this->db->delete('software', array('id' => $Software));
        if (!empty($Hardware)) $this->db->delete('hardware', array('id' => $Hardware));
    }

    /* Function to perform product editations
     * @parameter - Array with product update info 
     * @parameter - Product id to edit
     * @return boolean */
    public function editProduct($ProductInfo, $ProductID)
    {
        // If not empty update info update it in database
        if(!empty($ProductInfo['Name'])) $this->db->update('products', array('name' => $ProductInfo['Name']), "id = " . $ProductID);
        if(!empty($ProductInfo['Brand'])) $this->db->update('products', array('brand' => $ProductInfo['Brand']), "id = " . $ProductID);
        if(!empty($ProductInfo['Price'])) $this->db->update('products', array('price' => $ProductInfo['Price']), "id = " . $ProductID);
        if(!empty($ProductInfo['Description'])) $this->db->update('products', array('description' => $ProductInfo['Description']), "id = " . $ProductID);
        if(!empty($ProductInfo['Size'])) $this->db->update('hardware', array('size' => $ProductInfo['Size']), "id = " . $ProductID);
        if(!empty($ProductInfo['Weight'])) $this->db->update('hardware', array('weight' => $ProductInfo['Weight']), "id = " . $ProductID);
        if(!empty($ProductInfo['Licence'])) $this->db->update('software', array('licence_type' => $ProductInfo['Licence']), "id = " . $ProductID);
        if(!empty($ProductInfo['Config'])) $this->db->update('software', array('configuration' => $ProductInfo['Config']), "id = " . $ProductID);
        if(!empty($ProductInfo['Count'])) $this->db->update('products', array('count' => $ProductInfo['Count']), "id = " . $ProductID);
        return true;
    }

    public function fkMerchant($MerchantID)
    {
        $query = $this->db->query('SELECT * FROM products WHERE merchant_id =' . $MerchantID);
        return ($query->num_rows() > 0 ? false : true);
    }

    public function fkCategory($CategoryID)
    {
        $query = $this->db->query('SELECT * FROM products WHERE category_id =' . $CategoryID);
        return ($query->num_rows() > 0 ? false : true);
    }
}

?>