<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH.'libraries/Smarty/libs/Smarty.class.php' );

class MySmarty extends Smarty {

public function __construct()
    {
        parent::__construct();

        $this->setTemplateDir(APPPATH . "views/templates");
        $this->setCompileDir(APPPATH . "views/templates_c");
        $this->setConfigDir(APPPATH . "config");
        $this->setCacheDir(APPPATH . "chache");
        log_message('debug', "Smarty Class Initialized");
    }
}