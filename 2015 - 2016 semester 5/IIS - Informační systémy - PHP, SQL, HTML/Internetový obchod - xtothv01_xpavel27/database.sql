-- phpMyAdmin SQL Dump
-- version 4.4.6.1
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost:3306
-- Čas generovania: St 18.Nov 2015, 18:42
-- Verzia serveru: 5.6.27-log
-- Verzia PHP: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `czxpavel27`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL COMMENT 'Id of category',
  `name` varchar(20) NOT NULL COMMENT 'Name of the category',
  `type` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `categories`
--

INSERT INTO `categories` (`id`, `name`, `type`) VALUES
(6, 'Notebook', 'H'),
(7, 'System', 'S');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('b99275ab22d704252d3a5caddc336a954865c902', '147.229.221.212', 1447863954, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434373836333739353b757365726e616d657c733a31303a22637a78706176656c3237223b7065726d697373696f6e7c733a313a2233223b706572736f6e7c733a323a223139223b757365725f69647c733a323a223133223b6c6f67696e7c623a313b6d65726368616e747c613a323a7b733a323a224944223b4e3b733a373a22434f4d50414e59223b4e3b7d),
('54aad73b88f4a26b7da7b8ad0078b0c34cc0ffa7', '147.229.221.212', 1447865599, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434373836353434363b),
('83eadd5dc2e92f83cb4731bedd67d93d5b144861', '147.229.221.212', 1447868372, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434373836383337323b);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id of the feedback',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK user id',
  `product_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK product id',
  `mark` int(10) NOT NULL COMMENT 'Mark for the feedback'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `user_id`, `product_id`, `mark`) VALUES
(8, 16, 21, 3),
(9, 16, 22, 1),
(10, 16, 23, 5);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `hardware`
--

CREATE TABLE IF NOT EXISTS `hardware` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id of hardware',
  `weight` float NOT NULL COMMENT 'Weight of hardware',
  `size` varchar(20) NOT NULL COMMENT 'Size of hardware'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `hardware`
--

INSERT INTO `hardware` (`id`, `weight`, `size`) VALUES
(29, 300, '10x20x30'),
(30, 10, '10x20x25');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `merchants`
--

CREATE TABLE IF NOT EXISTS `merchants` (
  `id` bigint(20) unsigned NOT NULL,
  `company` text NOT NULL,
  `website` text NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `delivery` int(3) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `merchants`
--

INSERT INTO `merchants` (`id`, `company`, `website`, `user_id`, `delivery`) VALUES
(6, 'FIT VUT', 'www.vutbr.cz', 15, 2);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) unsigned NOT NULL,
  `payed` char(1) DEFAULT NULL,
  `expanded` char(1) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `employee_id` bigint(20) unsigned DEFAULT NULL,
  `trash_id` bigint(20) unsigned DEFAULT NULL,
  `price` float DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `orders`
--

INSERT INTO `orders` (`id`, `payed`, `expanded`, `user_id`, `employee_id`, `trash_id`, `price`, `timestamp`) VALUES
(18, 'Y', 'N', 16, 15, 20, 330, '2015-11-18 17:36:42'),
(19, 'N', 'N', 16, NULL, 21, 1213.2, '2015-11-18 17:36:49');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) NOT NULL COMMENT 'Id of permission',
  `name` varchar(20) NOT NULL COMMENT 'Name of permission'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `permissions`
--

INSERT INTO `permissions` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Merchant'),
(3, 'Customer');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id of the merchant',
  `name` varchar(20) DEFAULT NULL COMMENT 'Name of the merchant',
  `surname` varchar(20) DEFAULT NULL COMMENT 'Surname of the merchant',
  `address` varchar(50) DEFAULT NULL COMMENT 'Address of the merchant',
  `telephone` varchar(20) DEFAULT NULL COMMENT 'Telephone of the merchant',
  `mail` varchar(20) DEFAULT NULL COMMENT 'Mail of the merchant'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `persons`
--

INSERT INTO `persons` (`id`, `name`, `surname`, `address`, `telephone`, `mail`) VALUES
(19, '', '', '', '', ''),
(20, 'Viktor', 'Tóthv', 'Kolejní 2', '+42022444455', 'tothviktor@xxx.cz'),
(21, 'Martin', 'Pavelka', 'Kolejni 2 A0X', '+421911321987', 'martinpavelka@xxx.cz'),
(22, 'Filip', 'Nagyx', 'Kolejni 2 01', '+421111222333', 'nagyfilip@xxx.cz');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id of the product',
  `name` varchar(50) NOT NULL COMMENT 'Name of the product',
  `description` varchar(50) NOT NULL COMMENT 'Description of the product',
  `count` int(10) NOT NULL COMMENT 'Count of the products inside',
  `brand` varchar(20) NOT NULL COMMENT 'Brand of the product',
  `price` float NOT NULL COMMENT 'Price of the product',
  `vat` float NOT NULL COMMENT 'Vat of the product',
  `stock` varchar(1) NOT NULL COMMENT 'Stock availability',
  `software_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK software id ',
  `hardware_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK hardware id ',
  `category_id` int(10) unsigned DEFAULT NULL COMMENT 'FK category id ',
  `merchant_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `count`, `brand`, `price`, `vat`, `stock`, `software_id`, `hardware_id`, `category_id`, `merchant_id`) VALUES
(21, 'Galaxy S5', 'Chceme všetky body :)', 4, 'Samsung', 200, 40, 'Y', NULL, 29, 6, 6),
(22, 'E530x', 'Máme super produkt, chceme dobre ohodnotenie ;)\r\n	', 9, 'Lenovo', 1011, 202.2, 'Y', NULL, 30, 6, 6),
(23, 'Windows 10 education', 'Máme super produkt, chceme dobre ohodnotenie ;)\r\n	', 34, 'Microsoft', 75, 15, 'Y', 9, NULL, 6, 6);

--
-- Spúšťače `products`
--
DELIMITER $$
CREATE TRIGGER `productInsert` BEFORE INSERT ON `products`
 FOR EACH ROW BEGIN
IF NEW.count < 1 THEN
SET NEW.stock = 'N';
ELSE
SET NEW.stock = 'Y';
END IF;
SET NEW.vat = 0.2 * NEW.price;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `productUpdate` BEFORE UPDATE ON `products`
 FOR EACH ROW BEGIN
IF NEW.count < 1 THEN
SET NEW.stock = 'N';
ELSE
SET NEW.stock = 'Y';
END IF;
SET NEW.vat = 0.2 * NEW.price;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `software`
--

CREATE TABLE IF NOT EXISTS `software` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id of software',
  `licence_type` varchar(20) NOT NULL COMMENT 'Type of license',
  `configuration` varchar(100) NOT NULL COMMENT 'Configuration description'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `software`
--

INSERT INTO `software` (`id`, `licence_type`, `configuration`) VALUES
(9, 'GPU Ver 2', 'Windows 10 NPC');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `trashes`
--

CREATE TABLE IF NOT EXISTS `trashes` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id of the trash',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK user id',
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `trashes`
--

INSERT INTO `trashes` (`id`, `user_id`, `status`) VALUES
(20, 16, 'CRE'),
(21, 16, 'CRE');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `trash_items`
--

CREATE TABLE IF NOT EXISTS `trash_items` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id of item from trash',
  `trash_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK id of trash',
  `product_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK id of product'
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `trash_items`
--

INSERT INTO `trash_items` (`id`, `trash_id`, `product_id`) VALUES
(47, 20, 23),
(48, 20, 21),
(49, 21, 22);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id of user',
  `person_id` bigint(20) unsigned DEFAULT NULL COMMENT 'FK person id',
  `permission_id` int(10) DEFAULT NULL COMMENT 'FK persmission id',
  `login` varchar(20) NOT NULL COMMENT 'User login',
  `password` varchar(100) NOT NULL COMMENT 'User password'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `users`
--

INSERT INTO `users` (`id`, `person_id`, `permission_id`, `login`, `password`) VALUES
(13, 19, 3, 'czxpavel27', '0c805ac720fcc47e89a4e2c9215314b0'),
(14, 20, 1, 'administrator', '200ceb26807d6bf99fd6f4f0d1ca54d4'),
(15, 21, 2, 'merchant', '4c94e3115c1eeb9bd1e5e4bcb0bcab4e'),
(16, 22, 3, 'customer', '91ec1f9324753048c0096d036a694f86');

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexy pre tabuľku `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexy pre tabuľku `hardware`
--
ALTER TABLE `hardware`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `merchants`
--
ALTER TABLE `merchants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_merchants_user_id` (`user_id`);

--
-- Indexy pre tabuľku `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `trash_id` (`trash_id`);

--
-- Indexy pre tabuľku `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `software_id` (`software_id`),
  ADD KEY `hardware_id` (`hardware_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `fk_products_merchant_id` (`merchant_id`);

--
-- Indexy pre tabuľku `software`
--
ALTER TABLE `software`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `trashes`
--
ALTER TABLE `trashes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexy pre tabuľku `trash_items`
--
ALTER TABLE `trash_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trash_id` (`trash_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexy pre tabuľku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_person_id` (`person_id`),
  ADD KEY `fk_permission_id` (`permission_id`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of category',AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pre tabuľku `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of the feedback',AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pre tabuľku `hardware`
--
ALTER TABLE `hardware`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of hardware',AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pre tabuľku `merchants`
--
ALTER TABLE `merchants`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pre tabuľku `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pre tabuľku `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Id of permission',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pre tabuľku `persons`
--
ALTER TABLE `persons`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of the merchant',AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT pre tabuľku `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of the product',AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pre tabuľku `software`
--
ALTER TABLE `software`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of software',AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pre tabuľku `trashes`
--
ALTER TABLE `trashes`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of the trash',AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pre tabuľku `trash_items`
--
ALTER TABLE `trash_items`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of item from trash',AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT pre tabuľku `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id of user',AUTO_INCREMENT=17;
--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD CONSTRAINT `feedbacks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `feedbacks_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Obmedzenie pre tabuľku `merchants`
--
ALTER TABLE `merchants`
  ADD CONSTRAINT `fk_merchants_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Obmedzenie pre tabuľku `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`trash_id`) REFERENCES `trashes` (`id`);

--
-- Obmedzenie pre tabuľku `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`),
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`software_id`) REFERENCES `software` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`hardware_id`) REFERENCES `hardware` (`id`),
  ADD CONSTRAINT `products_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Obmedzenie pre tabuľku `trashes`
--
ALTER TABLE `trashes`
  ADD CONSTRAINT `fk_trashes_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Obmedzenie pre tabuľku `trash_items`
--
ALTER TABLE `trash_items`
  ADD CONSTRAINT `trash_items_ibfk_1` FOREIGN KEY (`trash_id`) REFERENCES `trashes` (`id`),
  ADD CONSTRAINT `trash_items_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Obmedzenie pre tabuľku `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `fk_person_id` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
