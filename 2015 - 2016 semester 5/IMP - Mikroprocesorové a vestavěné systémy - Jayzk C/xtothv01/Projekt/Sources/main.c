/*
 *  Meno:       Viktor
 *  Priezvisko: T�th
 *  Login:      xtothv01
 *  Trieda:     3BIT
 *  
 *  Orginal
 */

#include <hidef.h>
#include "derivative.h"

/* definicia booleovskych typov 
 *---------------------------*/
#define TRUE 1
#define FALSE 0

/* priradenie hodnot v pameti, na ktore su namapovany jednotlive prvky k premenym
 * -------------------------------------------------------------------------------- */
#define lavySeg7 (*(volatile char*)(0x200))       /* Lava cislica displaja */
#define pravySeg7 (*(volatile char*)(0x201))      /* Prava cislica displaja */

/* 8 prepinacov -> bity 7 az 0
 * 7b - vypnutie(0), zapnutie(1) 
 * 6b - rezim nastavenie(0) / citanie(1)
 * 5b - citanie dole(0) / hore(1)
 * 4b - lavy segment(0), pravy segment(1) displeje
 * 3b - 0b - nastavenie hodnoty */
#define DILSwitch (*(volatile char*)(0x202))      

/* switch so dvoma polohami
 * posledny bit 0 - dolna poloha
 * posledny bit 1 - horna poloha */
#define prepinac (*(volatile char*)(0x203))

/* Definice vyznamu cislic na 7-seg displeji
 * ----------------------------------------- */
#define NULA 0x3F
#define JEDNA 0x06
#define DVA 0x5B
#define TRI 0x4F
#define STYRI 0x66
#define PAT 0x6D
#define SEST 0x7D
#define SEDEM 0x07
#define OSEM 0x7F
#define DEVAT 0x6F
#define A 0x77
#define B 0x7C
#define C 0x39
#define D 0x5E
#define E 0x79
#define F 0x71

/* Globalne premenne
 * ----------------- */
byte index = 0;
byte hodnoty[4] = {0x00,0x00,0x00,0x00};
byte pocitadlo = 3;
byte koniecCitania = FALSE;

/* Prototypy pouzitych funkcii
 * -------------------------- */ 
void rtc_init(void);
void pociatocnyStav(void);
void start(void);
void set(void);
	// Prototypy funkcii k intuitivnejsie manipulacii
byte zapnuty(void);
byte nastavenie(void);
byte citanieDoleHore(void);
byte displejLavyPravy(void);
void hodnotaIndexu(byte lavy, byte pravy);
void zistiIndex(void);

/* Funkcie na prevod medzi binarnou hodnotou a hodnotou na 7-seg displeji */
byte binNaDisplej(byte hodnota);
byte displejNaBin (byte hodnota);

/* ------------------------
 *          MAIN
 * ------------------------*/
void main(void) 
{
	rtc_init();
	EnableInterrupts; 				//povolenie prerusenia

	for(;;) 
	{
		__RESET_WATCHDOG();				//krmenie watchdogu
	}
}

/* Inicializacia RTC modulu
 * -----------------------*/
void rtc_init(void) 
{
	/* Nastavenie pociatocneho stavu na ovladanie prvkov a displej */
	prepinac = 0x00;
	DILSwitch = 0x00;
	pociatocnyStav();

	/* Konfiguracia prerusenia od RTC z 1-kHz zdroja hodin
	 * zvolenie casu 0.1s - najvhodnejsi hodnota vzhladom k simulacie */
	RTCMOD = 0x00;
	RTCSC = 0x1D;
}
                    
/* Pociatocni stav
 * --------------
 * vynulovanie stavu citaca
 * prepinace 6 az 0 sa nastavi do spodnej polohy 
 * obnovenie stavu premennych na pociatocny*/
void pociatocnyStav () 
{
	// Tri zmeny vo vnutorneho stavu po dosiahnuti konecnej hodnoty 
	if(koniecCitania != 0 && pocitadlo != 0)
	{
		lavySeg7 = 0x00;
		pravySeg7 = 0x00;
		index = 0;
		pocitadlo -= 1;
	}
	// Pociatocny stav
	else
	{ 
	    lavySeg7 = NULA;
		pravySeg7 = NULA;
		DILSwitch &= 0x80;
		index = 0;
		hodnoty[0] = 0x00;
		hodnoty[1] = 0x00;
		hodnoty[2] = 0x00;
		hodnoty[3] = 0x00;
		pocitadlo = 3;
		koniecCitania = 0;
	}	
}

/* -----------------------------------
 * ISR = interrupt server routine 
 * Obsluzna rutina pre preruseni z RTC
 * -----------------------------------*/
interrupt VectorNumber_Vrtc void ISR_rtc(void)
{
	RTCSC = RTCSC | 0x80;

	if (zapnuty()) 
	{
		if (nastavenie())
			set(); 		// REZIM SET
		else
			start();	// REZIM START
	} 
	else 
		pociatocnyStav();	// REZIM STOP
}

/* Funkcia kontroluje rezim citania na hore/dole
 * ------------------------------------------ */
byte citanieDoleHore (void) {
  /* prepinac 5 
   * v dolnom polohe = citanie dole
   * v hornom polohe = citenie hore */
	return ((DILSwitch & 0x20) == 0x20) ? TRUE : FALSE;

}

/* REZIM START
 * -----------
 * Funkcia zvysuje/znizuje hodnoty na displeji podla daneho nastavenia */
void start(void) 
{
	if(koniecCitania == FALSE)  	//Kontrola koniecneho stavu
	{	
		if(citanieDoleHore())		//Citanie hore
		{
			if((hodnoty[0]) == 0x0F)				// Najmenej vyznamna hodnota (NULTY polbajt)
			{
				hodnoty[0] = 0x00;
				if(index == 0x00)
					index = 0x01;
				
				if((hodnoty[1]) == 0x0F)			// (PRVY polbajt)
				{
					hodnoty[1] = 0x00;
					if(index == 0x01)
						index = 0x02;
					
					if((hodnoty[2]) == 0x0F)		// (DRUHY polbajt)
					{
						hodnoty[2] = 0x00;
						if(index == 0x02)
							index = 0x03;
						
						if((hodnoty[3]) == 0x0F) 	// Najviac vyznamna hodnota (TRETI polbajt)
						{
							hodnoty[3] = 0x00;
							koniecCitania = TRUE;
							pociatocnyStav();	
						}
						else 
							hodnoty[3] += 0x01;	
					}
					else 
						hodnoty[2] += 0x01;	
				}
				else 
					hodnoty[1] += 0x01;	
			}
			else 
				hodnoty[0] += 0x01;	
		}
		else 				//Citanie dole
		{
			if((hodnoty[0]) == 0x00 )					// Najmenej vyznamna hodnota (NULTY polbajt)
			{
				if((hodnoty[1]) == 0x00)				// (PRVY polbajt)
				{
					if((hodnoty[2]) == 0x00)			// (DRUHY polbajt)
					{
						if((hodnoty[3]) == 0x00)		// Najviac vyznamna hodnota (TRETI polbajt)
						{
							koniecCitania = TRUE;
							pociatocnyStav();
						}
						else 
						{
							hodnoty[3] -= 0x01;
							hodnoty[2] = 0x0F;	
						}
					}
					else 
					{
						hodnoty[2] -= 0x01;
						hodnoty[1] = 0x0F;	
					}
				}
				else 
				{
					hodnoty[1] -= 0x01;
					hodnoty[0] = 0x0F;	
				}
			}
			else 
				hodnoty[0] -= 0x01;	
			zistiIndex();		
		}
	}
	else		// Dosiahnutie koniecneho stavu, extra 3 zmeny stavu (blika 7-seg displej)
		pociatocnyStav ();
	if(pocitadlo == 3 || pocitadlo == 1)			// Zaistenie "blikania"
	{
		// Vypis aktualnych cisle na dispej
		pravySeg7 = binNaDisplej(hodnoty[index]);
		lavySeg7 = binNaDisplej(index);
	}	
}

/* Funkcia kontroluje zapnutie/vypnutie aplikacie
 * ------------------------------------------ */
byte zapnuty (void)
{
	/* prepinac 7
	* v dolnom polohe = vypnuty
	* v hornom polohe = zapnuty */
	return ((DILSwitch & 0x80) == 0x80) ? TRUE : FALSE;
}

/* Funkcia kontroluje rezim nastavenia/citania
 * ---------------------------------------- */
byte nastavenie (void)
{
	// prepinac 6 
	// v dolnom polohe = nastavenie
	// v hornom polohe = citanie
	return ((DILSwitch & 0x40) == 0x00) ? TRUE : FALSE;
}

/* REZIM SET
 * ---------
 * pomocou prepinaca 3 az 0 je mozne nastavit vybrany displej */
void set(void)  
{
	if (displejLavyPravy()) 
		lavySeg7 = binNaDisplej(DILSwitch & 0x03);
	else 
	{
		pravySeg7 = binNaDisplej(DILSwitch & 0x0F);
		hodnotaIndexu(lavySeg7, pravySeg7);
	}
	zistiIndex();
}

/* Funkcia kontroluje volbu medzi lavym/pravym displejom
 * --------------------------------------------------- */
byte displejLavyPravy (void) 
{
	/* prepinac 4 
	* v dolnom polohe = lavy
	* v hornom polohe = pravy */
	return ((DILSwitch & 0x10) == 0x00) ? TRUE : FALSE;	
}

/* Prevod binarnej hodnoty na hodnotu displeje
 * ------------------------------------------ */
byte binNaDisplej(byte hodnota) 
{
	switch(hodnota)
	{
		case 0x01: return JEDNA;
		case 0x02: return DVA;
		case 0x03: return TRI;
		case 0x04: return STYRI;
		case 0x05: return PAT;
		case 0x06: return SEST;
		case 0x07: return SEDEM;
		case 0x08: return OSEM;
		case 0x09: return DEVAT;
		case 0x0A: return A;
		case 0x0B: return B;
		case 0x0C: return C;
		case 0x0D: return D;
		case 0x0E: return E;
		case 0x0F: return F;
		default: return NULA; 
	}   
}

/* Prevod hodnoty displeje na binarnu hodnotu
 * ------------------------------------------ */
byte displejNaBin (byte hodnota) {
	switch(hodnota) {
		case JEDNA: return 0x01;
		case DVA: return 0x02;
		case TRI: return 0x03;
		case STYRI: return 0x04;
		case PAT: return 0x05;
		case SEST: return 0x06;
		case SEDEM: return 0x07;
		case OSEM: return 0x08;
		case DEVAT: return 0x09;
		case A: return 0x0A;
		case B: return 0x0B;
		case C: return 0x0C;
		case D: return 0x0D;
		case E: return 0x0E;
		case F: return 0x0F;
		default: return 0x00; 
	}  
}

/* Funkcia vrati hodnoty podla indexu
 * ------------------------------------------ */	
void hodnotaIndexu(byte lavy, byte pravy)
{
	switch(lavy) 
	{
		case JEDNA:	hodnoty[1] = displejNaBin(pravy); break;			    	
		case DVA:	hodnoty[2] = displejNaBin(pravy); break;
		case TRI:	hodnoty[3] = displejNaBin(pravy); break;			    
		default:	hodnoty[0] = displejNaBin(pravy);
	}
}

/* Funkcia vrati najvyssi index podla hodnoty
 * ------------------------------------------ */
void zistiIndex()
{
	if(hodnoty[3] != 0x00)
		index = 3;
	else if(hodnoty[2] != 0x00)
		index = 2;
	else if(hodnoty[1] != 0x00)
		index = 1;
	else if(hodnoty[0] != 0x00)
		index = 0;
}