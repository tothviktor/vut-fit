﻿namespace Kalkulacka
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.jedna = new System.Windows.Forms.Button();
            this.dva = new System.Windows.Forms.Button();
            this.tri = new System.Windows.Forms.Button();
            this.styri = new System.Windows.Forms.Button();
            this.pat = new System.Windows.Forms.Button();
            this.sest = new System.Windows.Forms.Button();
            this.sedem = new System.Windows.Forms.Button();
            this.osem = new System.Windows.Forms.Button();
            this.devat = new System.Windows.Forms.Button();
            this.nula = new System.Windows.Forms.Button();
            this.vysledok = new System.Windows.Forms.Button();
            this.ciarka = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.nasobenie = new System.Windows.Forms.Button();
            this.delenie = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.zaporne = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.vedeckáToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vedeckáToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.klassickáToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odmocnina = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 27);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(268, 20);
            this.textBox1.TabIndex = 0;
            // 
            // jedna
            // 
            this.jedna.Location = new System.Drawing.Point(12, 50);
            this.jedna.Name = "jedna";
            this.jedna.Size = new System.Drawing.Size(40, 30);
            this.jedna.TabIndex = 1;
            this.jedna.Text = "1";
            this.jedna.UseVisualStyleBackColor = true;
            this.jedna.Click += new System.EventHandler(this.jedna_Click);
            // 
            // dva
            // 
            this.dva.Location = new System.Drawing.Point(58, 51);
            this.dva.Name = "dva";
            this.dva.Size = new System.Drawing.Size(40, 29);
            this.dva.TabIndex = 2;
            this.dva.Text = "2";
            this.dva.UseVisualStyleBackColor = true;
            this.dva.Click += new System.EventHandler(this.dva_Click);
            // 
            // tri
            // 
            this.tri.Location = new System.Drawing.Point(104, 51);
            this.tri.Name = "tri";
            this.tri.Size = new System.Drawing.Size(40, 30);
            this.tri.TabIndex = 3;
            this.tri.Text = "3";
            this.tri.UseVisualStyleBackColor = true;
            this.tri.Click += new System.EventHandler(this.tri_Click);
            // 
            // styri
            // 
            this.styri.Location = new System.Drawing.Point(12, 86);
            this.styri.Name = "styri";
            this.styri.Size = new System.Drawing.Size(40, 30);
            this.styri.TabIndex = 4;
            this.styri.Text = "4";
            this.styri.UseVisualStyleBackColor = true;
            this.styri.Click += new System.EventHandler(this.styri_Click);
            // 
            // pat
            // 
            this.pat.Location = new System.Drawing.Point(58, 86);
            this.pat.Name = "pat";
            this.pat.Size = new System.Drawing.Size(40, 30);
            this.pat.TabIndex = 5;
            this.pat.Text = "5";
            this.pat.UseVisualStyleBackColor = true;
            this.pat.Click += new System.EventHandler(this.pat_Click);
            // 
            // sest
            // 
            this.sest.Location = new System.Drawing.Point(104, 86);
            this.sest.Name = "sest";
            this.sest.Size = new System.Drawing.Size(40, 30);
            this.sest.TabIndex = 6;
            this.sest.Text = "6";
            this.sest.UseVisualStyleBackColor = true;
            this.sest.Click += new System.EventHandler(this.sest_Click);
            // 
            // sedem
            // 
            this.sedem.Location = new System.Drawing.Point(12, 122);
            this.sedem.Name = "sedem";
            this.sedem.Size = new System.Drawing.Size(40, 30);
            this.sedem.TabIndex = 7;
            this.sedem.Text = "7";
            this.sedem.UseVisualStyleBackColor = true;
            this.sedem.Click += new System.EventHandler(this.sedem_Click);
            // 
            // osem
            // 
            this.osem.Location = new System.Drawing.Point(58, 122);
            this.osem.Name = "osem";
            this.osem.Size = new System.Drawing.Size(40, 30);
            this.osem.TabIndex = 8;
            this.osem.Text = "8";
            this.osem.UseVisualStyleBackColor = true;
            this.osem.Click += new System.EventHandler(this.osem_Click);
            // 
            // devat
            // 
            this.devat.Location = new System.Drawing.Point(104, 122);
            this.devat.Name = "devat";
            this.devat.Size = new System.Drawing.Size(40, 30);
            this.devat.TabIndex = 9;
            this.devat.Text = "9";
            this.devat.UseVisualStyleBackColor = true;
            this.devat.Click += new System.EventHandler(this.devat_Click);
            // 
            // nula
            // 
            this.nula.Location = new System.Drawing.Point(58, 158);
            this.nula.Name = "nula";
            this.nula.Size = new System.Drawing.Size(40, 30);
            this.nula.TabIndex = 10;
            this.nula.Text = "0";
            this.nula.UseVisualStyleBackColor = true;
            this.nula.Click += new System.EventHandler(this.nula_Click);
            // 
            // vysledok
            // 
            this.vysledok.Location = new System.Drawing.Point(12, 158);
            this.vysledok.Name = "vysledok";
            this.vysledok.Size = new System.Drawing.Size(40, 30);
            this.vysledok.TabIndex = 11;
            this.vysledok.Text = "=";
            this.vysledok.UseVisualStyleBackColor = true;
            this.vysledok.Click += new System.EventHandler(this.vysledok_Click);
            // 
            // ciarka
            // 
            this.ciarka.Location = new System.Drawing.Point(104, 158);
            this.ciarka.Name = "ciarka";
            this.ciarka.Size = new System.Drawing.Size(40, 30);
            this.ciarka.TabIndex = 12;
            this.ciarka.Text = ",";
            this.ciarka.UseVisualStyleBackColor = true;
            this.ciarka.Click += new System.EventHandler(this.ciarka_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(150, 52);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(40, 29);
            this.plus.TabIndex = 13;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(150, 86);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(40, 30);
            this.minus.TabIndex = 14;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // nasobenie
            // 
            this.nasobenie.Location = new System.Drawing.Point(150, 122);
            this.nasobenie.Name = "nasobenie";
            this.nasobenie.Size = new System.Drawing.Size(40, 30);
            this.nasobenie.TabIndex = 15;
            this.nasobenie.Text = "*";
            this.nasobenie.UseVisualStyleBackColor = true;
            this.nasobenie.Click += new System.EventHandler(this.nasobenie_Click);
            // 
            // delenie
            // 
            this.delenie.Location = new System.Drawing.Point(150, 158);
            this.delenie.Name = "delenie";
            this.delenie.Size = new System.Drawing.Size(40, 30);
            this.delenie.TabIndex = 16;
            this.delenie.Text = "/";
            this.delenie.UseVisualStyleBackColor = true;
            this.delenie.Click += new System.EventHandler(this.delenie_Click);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(196, 52);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(40, 30);
            this.clear.TabIndex = 17;
            this.clear.Text = "C";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(196, 86);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(40, 30);
            this.delete.TabIndex = 18;
            this.delete.Text = "Del";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // zaporne
            // 
            this.zaporne.Location = new System.Drawing.Point(196, 122);
            this.zaporne.Name = "zaporne";
            this.zaporne.Size = new System.Drawing.Size(40, 30);
            this.zaporne.TabIndex = 19;
            this.zaporne.Text = "+/-";
            this.zaporne.UseVisualStyleBackColor = true;
            this.zaporne.Click += new System.EventHandler(this.zaporne_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vedeckáToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(292, 24);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // vedeckáToolStripMenuItem
            // 
            this.vedeckáToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vedeckáToolStripMenuItem1,
            this.klassickáToolStripMenuItem});
            this.vedeckáToolStripMenuItem.Name = "vedeckáToolStripMenuItem";
            this.vedeckáToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.vedeckáToolStripMenuItem.Text = "Volby";
            this.vedeckáToolStripMenuItem.Click += new System.EventHandler(this.vedeckáToolStripMenuItem_Click);
            // 
            // vedeckáToolStripMenuItem1
            // 
            this.vedeckáToolStripMenuItem1.Name = "vedeckáToolStripMenuItem1";
            this.vedeckáToolStripMenuItem1.Size = new System.Drawing.Size(127, 22);
            this.vedeckáToolStripMenuItem1.Text = "Vedecká";
            // 
            // klassickáToolStripMenuItem
            // 
            this.klassickáToolStripMenuItem.Name = "klassickáToolStripMenuItem";
            this.klassickáToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.klassickáToolStripMenuItem.Text = "Klassická";
            this.klassickáToolStripMenuItem.Click += new System.EventHandler(this.klassickáToolStripMenuItem_Click);
            // 
            // odmocnina
            // 
            this.odmocnina.Location = new System.Drawing.Point(196, 158);
            this.odmocnina.Name = "odmocnina";
            this.odmocnina.Size = new System.Drawing.Size(40, 30);
            this.odmocnina.TabIndex = 21;
            this.odmocnina.Text = "sqrt";
            this.odmocnina.UseVisualStyleBackColor = true;
            this.odmocnina.Click += new System.EventHandler(this.odmocnina_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.odmocnina);
            this.Controls.Add(this.zaporne);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.delenie);
            this.Controls.Add(this.nasobenie);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.ciarka);
            this.Controls.Add(this.vysledok);
            this.Controls.Add(this.nula);
            this.Controls.Add(this.devat);
            this.Controls.Add(this.osem);
            this.Controls.Add(this.sedem);
            this.Controls.Add(this.sest);
            this.Controls.Add(this.pat);
            this.Controls.Add(this.styri);
            this.Controls.Add(this.tri);
            this.Controls.Add(this.dva);
            this.Controls.Add(this.jedna);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button jedna;
        private System.Windows.Forms.Button dva;
        private System.Windows.Forms.Button tri;
        private System.Windows.Forms.Button styri;
        private System.Windows.Forms.Button pat;
        private System.Windows.Forms.Button sest;
        private System.Windows.Forms.Button sedem;
        private System.Windows.Forms.Button osem;
        private System.Windows.Forms.Button devat;
        private System.Windows.Forms.Button nula;
        private System.Windows.Forms.Button vysledok;
        private System.Windows.Forms.Button ciarka;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button nasobenie;
        private System.Windows.Forms.Button delenie;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button zaporne;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem vedeckáToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vedeckáToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem klassickáToolStripMenuItem;
        private System.Windows.Forms.Button odmocnina;
    }
}

