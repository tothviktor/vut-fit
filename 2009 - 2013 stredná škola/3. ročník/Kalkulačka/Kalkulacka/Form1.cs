﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kalkulacka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void jedna_Click(object sender, EventArgs e)
        {
            textBox1.Text += 1;
        }

        private void dva_Click(object sender, EventArgs e)
        {
            textBox1.Text += 2;
        }

        private void tri_Click(object sender, EventArgs e)
        {
            textBox1.Text += 3;
        }

        private void styri_Click(object sender, EventArgs e)
        {
            textBox1.Text += 4;
        }

        private void pat_Click(object sender, EventArgs e)
        {
            textBox1.Text += 5;
        }

        private void sest_Click(object sender, EventArgs e)
        {
            textBox1.Text += 6;
        }

        private void sedem_Click(object sender, EventArgs e)
        {
            textBox1.Text += 7;
        }

        private void osem_Click(object sender, EventArgs e)
        {
            textBox1.Text += 8;
        }

        private void devat_Click(object sender, EventArgs e)
        {
            textBox1.Text += 9;
        }

        private void nula_Click(object sender, EventArgs e)
        {
            textBox1.Text += 0;
        }

        double cislo = 0;
        int operacia = 0;
        bool desCiarka = false;

        private void plus_Click(object sender, EventArgs e)
        {
            cislo = Convert.ToDouble(textBox1.Text);
            operacia = 0;
            textBox1.Text = "";
            desCiarka = false;

        }

        private void minus_Click(object sender, EventArgs e)
        {
            cislo = Convert.ToDouble(textBox1.Text);
            operacia = 1;
            textBox1.Text = "";
            desCiarka = false;
        }

        private void nasobenie_Click(object sender, EventArgs e)
        {
            cislo = Convert.ToDouble(textBox1.Text);
            operacia = 2;
            textBox1.Text = "";
            desCiarka = false;
        }

        private void delenie_Click(object sender, EventArgs e)
        {
            cislo = Convert.ToDouble(textBox1.Text);
            operacia = 3;
            textBox1.Text = "";
            desCiarka = false;
        }

        private void vysledok_Click(object sender, EventArgs e)
        {
            double vysledok = 0;
            switch (operacia)
            {
                case 0: vysledok = cislo + Convert.ToDouble(textBox1.Text); break;
                case 1: vysledok = cislo - Convert.ToDouble(textBox1.Text); break;
                case 2: vysledok = cislo * Convert.ToDouble(textBox1.Text); break;
                case 3: if (Convert.ToDouble(textBox1.Text) != 0.0)
                    {
                        vysledok = cislo / Convert.ToDouble(textBox1.Text);
                    }
                    else
                    {
                        MessageBox.Show("Nepovolene delenie !!!");
                    }
                    break;     
            }
            textBox1.Text = Convert.ToString(vysledok);
            desCiarka = false;

        }

        private void clear_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void ciarka_Click(object sender, EventArgs e)
        {
           
            if (!desCiarka)

                if(textBox1 .Text == "")
                {
                    textBox1.Text += "0,";
                }
                else
                {
                    textBox1.Text += ',';
                }
                desCiarka = true;
        }

        private void delete_Click(object sender, EventArgs e)
        {
            string ret = textBox1.Text;
            ret = ret.Substring(0, ret.Length);
            textBox1.Text = ret;
        }

        private void zaporne_Click(object sender, EventArgs e)
        {
            cislo = Convert.ToDouble(textBox1.Text);
            cislo = 0 - cislo;
            textBox1.Text = Convert.ToString(cislo);
        }

        private void vedeckáToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Vybral si vedecku klavesnicu");
        }

        private void klassickáToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Vybral si kalssicku klavesnicu");
        }

        private void odmocnina_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Nezadali ste ziadne cislo");
            }
            else
            {
                cislo = Convert.ToDouble(textBox1.Text);

                if (cislo > 0)
                {
                    cislo = Math.Sqrt(cislo);
                }
                else
                {
                    MessageBox.Show("Zadane cislo je mensie ako 0, NIE JE ODMOCNITELNE!!!");
                }
            }
            
            textBox1.Text = cislo.ToString();
            desCiarka = false;
        }

        

    }
}
