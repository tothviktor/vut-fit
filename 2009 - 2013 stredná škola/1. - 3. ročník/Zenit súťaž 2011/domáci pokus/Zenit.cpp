#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

int main()
{
 	ofstream vystup ("Skola.OUT");
 	ifstream vstup ("vstup.in");
 	
 	int pocetK=0,najPlocha=0,najKancelaria=0,nezarKancelaria=0,pocZarKancelarie=0,j=0,
	 	najPoschodie=0,najPracovisk1=0,najPracovisk2=0,k=0,preSefa=0,pocUradnikov=0,
	    pocSefov=0,naklady=0,celPlocha=0,pocUpratovaciek=0;
 	
 	double najPriestor=9999999;
 	
 	vstup>>pocetK;
 	cout<<"Pocet kancelarie je: "<<pocetK<<endl;
 	
 	int*cislo=new int [pocetK-1];
 	int*plocha=new int [pocetK-1];
 	int*pracoviska=new int [pocetK-1];
 	int*zarKancelarie=new int [pocetK-1];
 	int*najPriestorov=new int [pocetK-1];
 	
 	for(int i=0;i<pocetK;i++)
 	{
	 		cislo[i]=0;
	 		plocha[i]=0;
	 		pracoviska[i]=0;
	 		zarKancelarie[i]=0;
	 		najPriestorov[i]=0;
    }
    
    for(int i=0;i<pocetK;i++)
    {
	  		vstup>>cislo[i];
	  		vstup>>plocha[i];
	  		vstup>>pracoviska[i];
	  		
	  		cout<<cislo[i]<<" ";
	  		cout<<plocha[i]<<" ";
	  		cout<<pracoviska[i]<<endl;
	  		
	  		if(plocha[i]>najPlocha)
	  		{
			 					   najPlocha=plocha[i];
			 					   najKancelaria=cislo[i];
		    }
		    if(pracoviska[i]==0)
		    {
			 					nezarKancelaria=cislo[i];
		    }
		    else
		    {
			 	pocZarKancelarie++;
			 	zarKancelarie[j]=cislo[i];
			 	j++;
		    }
		    if(cislo[i]>najPoschodie)
		    {
			 						najPoschodie=cislo[i];
		    }
		    if(pracoviska[i]>najPracovisk1)
		    {
			 							  najPracovisk1=pracoviska[i];
			 							  najPracovisk2=cislo[i];
		    }
		    if((double(plocha[i])/double(pracoviska[i]))<=najPriestor)
		    {
			 						  najPriestor=1.00*(double(plocha[i])/double(pracoviska[i]));
			 						  najPriestorov[k]=cislo[i];
			 						  k++;
		    }
		    if(pracoviska[i]==1)
		    {
			 					preSefa=cislo[i];
			 					pocSefov++;
		    }
		    pocUradnikov+=pracoviska[i];
		    
		    naklady=13*(((pocUradnikov-pocSefov)*1000)+pocSefov*1500);
		    
		    celPlocha+=plocha[i];
		    
		    if((celPlocha%200)==0)
		    {
			 					  pocUpratovaciek=celPlocha/200;
		    }
		    else
		    {
			 	pocUpratovaciek=(celPlocha/200)+1;
		    }		    
		}
//------------------------------------------------------------------------------
		int pocetC=0,sucet=0,zmena1=0,pocZmena=0,zmena2=0,maxZmena=0,pocHodnot=1,
		maxPocHodnot=0,l=0,pocNajHodnot=0,f=0;
		
		double priemer=0;
		
		vstup>>pocetC;
		cout<<endl<<"Pocet cisel je: "<<pocetC<<endl;
		
		int*postupnost=new int[pocetC];
				
		for(int i=0;i<pocetC;i++)
		{
		 		postupnost[i]=0;
		}
		for(int i=0;i<pocetC;i++)
		{
		 		vstup>>postupnost[i];
		 		
		 		cout<<postupnost[i]<<" ";
		 		
		 		sucet+=postupnost[i];
		 		priemer=double(sucet)/double(pocetC);
		 		
		 		if(zmena1!=postupnost[i])
		 		{
				 						zmena1=postupnost[i];
				 						pocZmena++;
		        }
		}
		
		for(int i=0;i<pocetC-1;i++)
		{
		 		if((postupnost[i]-postupnost[i+1])<0)
  		      	{
				 								zmena2=(postupnost[i]-postupnost[i+1])*(-1);
				 								if(zmena2>maxZmena)
				 								{
												 				  maxZmena=zmena2;
 										        }
		        }
				else
				{
				 		 						zmena2=(postupnost[i]-postupnost[i+1]);
		        		 						if(zmena2>maxZmena)
						 						{
							 				  	 				  maxZmena=zmena2;
 										        }
				}
				
				if(postupnost[i]==postupnost[i+1])
				{
				 								  pocHodnot++;
				 								  if(pocHodnot>maxPocHodnot)
				 								  {
												   							maxPocHodnot=pocHodnot;
				                                  }
		        }
		        else
		        {
				 	pocHodnot=1;
				}
		}
		
		int*rovnakeHodnoty=new int[pocZmena];
		
		for(int i=0;i<pocetC-1;i++)
		{
		 		if(postupnost[i]==postupnost[i+1])
				{
		                        pocHodnot++;
				 				if(pocHodnot==maxPocHodnot)
				 				{
												rovnakeHodnoty[l]=postupnost[i];
												l++;
				                }
		        }
		        else
		        {
				 	pocHodnot=1;
				}
		}
				
		for(int i=0;i<pocetC;i++)
		{
		 		for(int j=0+i,k=0;j<pocetC;j++)
				{
				 	if(postupnost[i]==postupnost[j])
				 	{
					 			k++;
					 			if(k>pocNajHodnot)
					 			{
								 				  pocNajHodnot=k;
							    }
				    }
				}			  
		}
		
		int*najVysCisla=new int [pocNajHodnot-1];
		
		for(int j=0,i=0;i<pocetC;i++)
		{		 		
		 		for(int j=0+i,k=0;j<pocetC;j++)
				{
				 	if(postupnost[i]==postupnost[j])
				 	{
					 			k++;
					 			if(k==pocNajHodnot && najVysCisla[f]!=postupnost[i])
					 			{
								 				  najVysCisla[f]=postupnost[i];
								 				  f++;
							    }
				    }
				}			  
		}
		
		int pocPlaticov=0,vsePlatice=0,nePlatic=0;
		vstup>>pocPlaticov;
		
		int*platice=new int [99];
		
		for(int i=0;i<pocPlaticov-1;i++)
		{
		 		vstup>>platice[i];
		 		
		 		nePlatic+=platice[i];
		 		vsePlatice+=i+1;
		}
		vsePlatice+=pocPlaticov;
		
		
		
			
//------------------------------------------------------------------------------		
 	cout<<endl<<endl<<"MENO: Toth V. 3IT SPSE"<<endl;
 	vystup<<"MENO: Toth V. 3IT SPSE"<<endl;
 	
 	cout<<"a) Urad a stolicka to je moje."<<endl;
 	vystup<<"a) Urad a stolicka to je moje."<<endl;
 	
 	cout<<"b) Najvacsia plocha: "<<najPlocha<<endl;
 	vystup<<"b) Najvacsia plocha: "<<najPlocha<<endl;
 	
 	cout<<"b) Najvacsia kancelaria: "<<najKancelaria<<endl;
 	vystup<<"b) Najvacsia kancelaria: "<<najKancelaria<<endl;
 	
 	cout<<"b) Nezariadena kancelaria: "<<nezarKancelaria<<endl;
 	vystup<<"b) Nezariadena kancelaria: "<<nezarKancelaria<<endl;
 	
 	cout<<"b) Zariadene kancelarie: "<<pocZarKancelarie<<" v:";
 	vystup<<"b) Zariadene kancelarie: "<<pocZarKancelarie<<" v:";
 	
 	qsort (zarKancelarie,pocZarKancelarie, sizeof(int), compare);
 	for(int i=0;i<pocZarKancelarie;i++)
 	{
	 		cout<<" "<<zarKancelarie[i];
	 		vystup<<" "<<zarKancelarie[i];
    }
    cout<<endl;
    vystup<<endl;
    
    cout<<"b) Najvyssie poschodie: "<<najPoschodie/100<<endl;
 	vystup<<"b) Najvyssie poschodie: "<<najPoschodie/100<<endl;
 	
 	cout<<"b) Najviac pracovisk: "<<najPracovisk1<<" v: "<<najPracovisk2<<endl;
 	vystup<<"b) Najviac pracovisk: "<<najPracovisk1<<" v: "<<najPracovisk2<<endl;
 	
 	cout<<"b) Najmenej priestoru: "<<setprecision(2)<<fixed<<najPriestor<<" v:";
 	vystup<<"b) Najmenej priestoru: "<<setprecision(2)<<fixed<<najPriestor<<" v:";
 	
 	qsort (najPriestorov,k, sizeof(int), compare);
 	for(int i=0;i<k;i++)
 	{
	 		cout<<" "<<najPriestorov[i];
	 		vystup<<" "<<najPriestorov[i];
    }
    cout<<endl;
    vystup<<endl;
    
   	cout<<"c) Kancelaria pre sefa: "<<preSefa<<endl;
    vystup<<"c) Kancelaria pre sefa: "<<preSefa<<endl;
    
    cout<<"c) Pocet uradnikov: "<<pocUradnikov<<endl;
    vystup<<"c) Pocet uradnikov: "<<pocUradnikov<<endl;
    
    cout<<"d) Mzdove naklady: "<<naklady<<" eur"<<endl;
    vystup<<"d) Mzdove naklady: "<<naklady<<" eur"<<endl;
    
    cout<<"e) Celkova plocha: "<<celPlocha<<" pocet upratovaciek: "<<pocUpratovaciek<<endl;
    vystup<<"e) Celkova plocha: "<<celPlocha<<" pocet upratovaciek: "<<pocUpratovaciek<<endl;
    
    cout<<"f) Pocet: "<<pocetC<<" postupnost:";
    vystup<<"f) Pocet: "<<pocetC<<" postupnost:";
    for(int i=0;i<pocetC;i++)
    {
	 		cout<<" "<<postupnost[i];
	 		vystup<<" "<<postupnost[i];
    }
    cout<<endl;
    vystup<<endl;
    
    cout<<"f) Priemer: "<<setprecision(2)<<fixed<<priemer<<endl;
    vystup<<"f) Priemer: "<<setprecision(2)<<fixed<<priemer<<endl;
    
    cout<<"f) Zmeny: "<<pocZmena-1<<endl;
    vystup<<"f) Zmeny: "<<pocZmena-1<<endl;
    
    cout<<"f) Max. zmena o: "<<maxZmena<<endl;
    vystup<<"f) Max. zmena o: "<<maxZmena<<endl;
    
    cout<<"f) Rovnake hodnoty za sebou: "<<maxPocHodnot<<" a to: ";
    vystup<<"f) Rovnake hodnoty za sebou: "<<maxPocHodnot<<" a to: ";
    
    qsort (rovnakeHodnoty,l, sizeof(int), compare);
 	for(int i=0;i<l;i++)
 	{
	 		cout<<" "<<rovnakeHodnoty[i];
	 		vystup<<" "<<rovnakeHodnoty[i];
    }
    cout<<endl;
    vystup<<endl;
    
    cout<<"f) Najcastejsie: "<<pocNajHodnot<<" krat sa vyskytuje:";
    vystup<<"f) Najcastejsie: "<<pocNajHodnot<<" krat sa vyskytuje:";
    
    qsort (najVysCisla,f, sizeof(int), compare);
 	for(int i=0;i<f;i++)
 	{
	 		cout<<" "<<najVysCisla[i];
	 		vystup<<" "<<najVysCisla[i];
    }
    cout<<endl;
    vystup<<endl;
    
    cout<<"g) Zoznam N: "<<pocPlaticov<<" zaplatili:";
    vystup<<"g) Zoznam N: "<<pocPlaticov<<" zaplatili:";
    for(int i=0;i<pocPlaticov-1;i++)
	{		
		 		cout<<" "<<platice[i];	
		 		vystup<<" "<<platice[i];
	}
	cout<<endl;
    vystup<<endl;
    
    cout<<"g) Nezaplatil: "<<vsePlatice-nePlatic<<endl;
    vystup<<"g) Nezaplatil: "<<vsePlatice-nePlatic<<endl;
    
    
 	system("pause");
 	return 0;
}
