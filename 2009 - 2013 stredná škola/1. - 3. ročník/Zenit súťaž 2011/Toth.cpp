//Toth V. 3IT SPSE
#include<iostream>
#include<fstream>
#include<iomanip>
using namespace std;

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

int main()
{
    ofstream vystup("Skola.OUT");
    ifstream vstup("vstup.in");
    
    int pocetK,najvacsiaPlocha=0,najvacsiaKancelaria=0,neZaKancelaria=0,ZaKancelaria=0,j=0,poschodie=0,pracovisko1=0,pracovisko2=0,k=0,
    preSefa=0,uradnici=0,pocSefov=0,mzda=0,pracovnici=0;
    double priestor=9999;   

    vstup>>pocetK;
    cout<<"Pocet kancelarie je: "<<pocetK<<endl;
    
    int *cislo=new int[pocetK-1];
    int *plocha=new int[pocetK-1];
    int *pracoviska=new int[pocetK-1];
    int *cisloZaKancelarie=new int[pocetK-1];
    int *plochaPracoviska=new int[pocetK-1];
    
    for(int i=0;i<pocetK;i++)
    {
            cislo[i]=0;
            plocha[i]=0;
            pracoviska[i]=0;
            cisloZaKancelarie[i]=0;
            plochaPracoviska[i]=0;
    }
            
        
    for(int i=0;i<pocetK;i++)
    {
            vstup>>cislo[i];
            vstup>>plocha[i];
            vstup>>pracoviska[i];
            
            cout<<cislo[i]<<" ";
            cout<<plocha[i]<<" ";
            cout<<pracoviska[i]<<" "<<endl;
            
            if(najvacsiaPlocha<plocha[i])
            {
                                   najvacsiaPlocha=plocha[i];
                                   najvacsiaKancelaria=cislo[i];                                   
            }
            if(pracoviska[i]==0)
            {
                                neZaKancelaria=cislo[i];
                                
            }
            if(pracoviska[i]!=0)
            {
                                ZaKancelaria++;
                                cisloZaKancelarie[j]=cislo[i];
                                j++;
            }
            if(poschodie<cislo[i])
            {
                                  poschodie=cislo[i];
            }
            if(pracovisko1<pracoviska[i])
            {
                                        pracovisko1=pracoviska[i];
                                        pracovisko2=cislo[i];
            }
            if(priestor>=double(plocha[i])/double(pracoviska[i]))
            {
                                             priestor=double(plocha[i])/double(pracoviska[i]);
                                             plochaPracoviska[k]=cislo[i];
                                             k++;
            }
            if(pracoviska[i]==1)
            {
                        preSefa=cislo[i];
                        pocSefov++;
            }
            uradnici=uradnici+pracoviska[i];
            pracovnici=uradnici-pocSefov;
            mzda=(((pocSefov*1500)+(pracovnici*1000))*13);
            
            
                                  
    }
    
    cout<<"MENO: Toth V. 3IT SPSENZ"<<endl;
    vystup<<"MENO: Toth V. 3IT SPSENZ"<<endl;
    
    cout<<"a) Urad a stolicka to je moje."<<endl;
    vystup<<"a) Urad a stolicka to je moje."<<endl;
    
    cout<<"b) Najvacsia plocha: "<<najvacsiaPlocha<<endl;
    vystup<<"b) Najvacsia plocha: "<<najvacsiaPlocha<<endl;
    cout<<"b) Najvacsia kancelaria: "<<najvacsiaKancelaria<<endl;
    vystup<<"b) Najvacsia kancelaria: "<<najvacsiaKancelaria<<endl;
    cout<<"b) Nezariadena kancelaria: "<<neZaKancelaria<<endl;
    vystup<<"b) Nezariadena kancelaria: "<<neZaKancelaria<<endl;
    cout<<"b) Zariadene kancelarie: "<<ZaKancelaria<<" v: ";
    vystup<<"b) Zariadene kancelarie: "<<ZaKancelaria<<" v: ";
    
    qsort (cisloZaKancelarie,ZaKancelaria, sizeof(int), compare);
    for(int i=0; i<ZaKancelaria; i++)
    {
            cout<<cisloZaKancelarie[i]<<" ";
            vystup<<cisloZaKancelarie[i]<<" ";
    }
    cout<<endl;
    vystup<<endl;
    poschodie=poschodie/100;
    cout<<"b) Najvyssie poschodie: "<<poschodie<<endl;
    vystup<<"b) Najvyssie poschodie: "<<poschodie<<endl;
    cout<<"b) Najviac pracovisk: "<<pracovisko1<<" v: "<<pracovisko2<<endl;
    vystup<<"b) Najviac pracovisk: "<<pracovisko1<<" v: "<<pracovisko2<<endl;
    cout<<"b) Najmenej priestoru: "<<setprecision(2)<<fixed<<priestor<<" v: ";
    vystup<<"b) Najmenej priestoru: "<<setprecision(2)<<fixed<<priestor<<" v: ";
    
    qsort (plochaPracoviska,k, sizeof(int), compare);
    
    for(int i=0;i<k;i++)
    {
            cout<<plochaPracoviska[i]<<" ";
            vystup<<plochaPracoviska[i]<<" ";
    }
    cout<<endl;
    vystup<<endl;
    cout<<"c) Kancelaria pre sefa: "<<preSefa<<endl;
    vystup<<"c) Kancelaria pre sefa: "<<preSefa<<endl;
    cout<<"c) Pocet uradnikov: "<<uradnici<<endl;
    vystup<<"c) Pocet uradnikov: "<<uradnici<<endl;
    cout<<"d) Mzdove naklady: "<<mzda<<" eur"<<endl;
    vystup<<"d) Mzdove naklady: "<<mzda<<" eur"<<endl;
    
    system("pause");
    return 0;
}
