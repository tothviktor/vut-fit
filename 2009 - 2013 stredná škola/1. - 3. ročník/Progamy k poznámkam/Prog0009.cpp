//Prog0009 - Prikaz switch

#include <iostream>
using namespace std;

int main()
{
    int cis1, cis2;
    int volba;
    
    cout << "Zadaj dve ciselne hodnoty\n\n";
    cin >> cis1 >> cis2;
    
    //Jednoduche menu
    cout << "Zvol jednu z moznosti\n";
    cout << "1 - Sucet\n";
    cout << "2 - Rozdiel\n";
    cout << "3 - Sucin\n";
    cout << "4 - Podiel\n\n";
    
    cin >> volba;
    
    switch (volba)
    {
        case 1: 	cout << "Sucet je " << cis1 + cis2 << endl;
    				break;
        case 2: 	cout << "Rozdiel je " << cis1 - cis2 << endl;
    			    break;    
        case 3: 	cout << "Sucin je " << cis1 * cis2 << endl;
    				break;
        case 4: 	cout << "Podiel je " << (float)cis1 / cis2 << endl;
    				break;			
        default:	cout << "Nezadal si spravnu hodnotu" << endl; 
    }    
    system("pause");
    return 0;
}
