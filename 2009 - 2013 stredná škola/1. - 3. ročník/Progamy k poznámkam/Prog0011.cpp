//Prog0011 - Prikaz for

#include <iostream>
using namespace std;

int main()
{
    //********Priklad 1********
    cout << "***Priklad 1***" << "\n\n";
    for (int i=1; i<11; i = i + 1)	//Hlavicka cyklu
    	cout << i << "  ";			//Telo cyklu
    
    
    //********Priklad 2********
    cout << "\n\n" << "***Priklad 2***" << "\n\n";
    int a=1;
    cout << '\n';
    for (int i=1; i<11; i++)		//Premenna i deklarovana v hlavicke 
    {								//cyklu, po vystupe z cyklu zanika
      	a = a + 2*i;
       	cout << a << "  ";  
   	}    
    
    //********Priklad 3********
    cout << "\n\n" << "***Priklad 3***" << "\n\n";
    int i=1;
    cout << '\n';
    for (; i<11; i++)				//V hlavicke je vynechana inicializacia
    {						
      	cout << "Ahoj" << "\n";
       	cout << i*2 << '\n';  
   	}    
    
    //********Priklad 4********
    cout << "\n\n" << "***Priklad 4***" << "\n\n";
    for (int i=0, j=0; i<5; i++, j++)	
    {						
       	cout << i*2 << "  " << j*3 << '\n';  
   	}   
    
    
    system("pause");
    return 0;
}    
