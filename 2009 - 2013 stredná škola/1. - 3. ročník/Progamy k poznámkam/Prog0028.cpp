//Prog0028 - Vystup textu do suboru
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ofstream vystup("test.txt");                            //otvorenie suboru test.txt
    vystup << "Tato veta sa zapise priamo do suboru\n";     //vystup textu do suboru
    vystup << "Aj tato\n";
    vystup.close();                                         //zatvorenie suboru
    
    system("pause");
    return 0;
}
