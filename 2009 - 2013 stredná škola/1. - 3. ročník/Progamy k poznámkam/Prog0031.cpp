//Prog0031 - Nacitanie textu zo suboru
//Najskor je potrebne spustit program Prog0022.cpp, aby sa vytvoril subor test.txt!!!
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    string veta;                      //Retazec, do ktoreho sa bude ukladat text
    
    ifstream vstup("test.txt");       //Otvorenie suboru test.text na citanie                            
    getline(vstup,veta);              //Nacitanie jedneho riadku textu zo suboru    
    vstup.close();                    //Uzatvorenie suboru test.txt
    
    cout << "V subore test.txt sa nachadza nasledovny text" << endl;                                        
    cout << veta << endl;
    
    system("pause");
    return 0;
}
