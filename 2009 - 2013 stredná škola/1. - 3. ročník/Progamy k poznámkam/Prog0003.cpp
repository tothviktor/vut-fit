//Prog0002 -- zobrazuje hodnotu promennej

#include <iostream>
using namespace std;

int main()
{
	int vek=20;				// vytvori celociselnu premennu a inicializuje ju na hodnotu 20
	
	cout << "Tento rok oslavim ";
	cout << vek;			// zobraz� hodnotu promennej vek
	cout << " rokov.\n";
	system("pause");
	return 0;
}
