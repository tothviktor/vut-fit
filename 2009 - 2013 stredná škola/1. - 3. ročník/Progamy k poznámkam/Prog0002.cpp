//Prog0002 -- pr�klady v�pisu liter�lov

#include <iostream>
using namespace std;
int main()
{
	cout << 1589 << '\n';				      //vyp�e celo��seln� liter�l 1589
    cout << 256 << 324 << '\n';			      //vyp�e dva celo��seln� liter�ly za sebou 256324
    cout << 128.56 << '\n';				      //vyp�e re�lny liter�l 128.56
    cout << 'T' << '\n';				      //vyp�e znakov� liter�l T
    cout << "Programujeme v C++" << '\n';	  //vyp�e re�azcov� liter�l Programujeme v C++
    cout << "Programujeme v C++\n" ;		  //to ist�, �o v prech�dzaj�com pr�klade
    cout << "Programujeme\n v C++" << '\n' ;  //na jeden riadok vyp�e Programujeme
							                  //na druh� riadok vyp�e v C++
    
    system("pause");
	return 0;
}
