//Prog0030 - Vypis 10 nahodnych cisel z intervalu <1,100>  do suboru cisla.dat
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    
    int nahCislo;
    ofstream nahodneCisla("cisla.dat");                            
    
    for (int i=1;i<=10;i++)
    {
        nahCislo=1+rand()%100;
        nahodneCisla << nahCislo << endl;
    }
    
    nahodneCisla.close();                                         
    
    system("pause");
    return 0;
}
