//Prog0005 -- prikaz priradenia

#include <iostream>
using namespace std;

int main()
{
	int cislo;                         //nahodna hodnota v premennej cislo
	short int vekZiaka;                //nahodna hodnota v premennej vekZiaka
	
	cislo=15;                          //v premennej cislo je teraz hodnota 15
	vekZiaka=cislo;                    //v premennej vekZiaka je teraz hodnota 15
	cout << "cislo:" << cislo << endl << "vekZiaka:" << vekZiaka << endl;
	
	cislo=cislo+1;                     //v premennej cislo je teraz hodnota 16
	                                   //hodnota v premennej vekZiaka ostava nezmenena
	cout << endl << "cislo:" << cislo << endl << "vekZiaka:" << vekZiaka << endl;
	
	cislo=14.8;                        //do premennej cislo sa ulozi hodnota 14!!!
	                                   //vsimnite si varovanie kompilatora
    cout << endl << "cislo:"  << cislo << endl;
    
    /* Zamyslite sa nad hodnotou v premennej vekZiaka; skuste zistit preco je
    v nej hodnota -31072 a nie hodnota 100000 */ 
    cislo=100000;
    vekZiaka=cislo;
    cout << endl << "cislo:" << cislo << endl << "vekZiaka:" << vekZiaka << endl;
    

	system("pause");
	return 0;
}
