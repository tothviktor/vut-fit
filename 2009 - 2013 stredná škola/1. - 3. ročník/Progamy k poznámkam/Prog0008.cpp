//Prog0008 - Prikaz if else

#include <iostream>
using namespace std;

int main()
{
    
    int cislo;
    cout << "Zadaj lubovolne cislo" << endl;
    cin >> cislo;
    
    if (cislo>=1 && cislo<=20)
        cout << "Cislo je z intervalu <1,20>" << endl;
    else
        cout << "Cislo nie je z intervalu <1,20>" << endl;
    
    system("pause");
    return 0;
}
