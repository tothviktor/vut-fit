//Prog0032 - Nacitanie textu zo suboru znak po znaku
//Najskor je potrebne spustit program Prog0023.cpp, aby sa vytvoril subor zapis.txt!!!
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    string veta;                      //Retazec, do ktoreho sa bude ukladat text
                                      //znak po znaku
    char znak;                        //premenna, do ktorej sa bude nacitavat kazdy
                                      //znak zo suboru
    
    ifstream vstup("zapis.txt");       //Otvorenie suboru zapis.text na citanie                            
    
    while (vstup.get(znak))            //cyklus bezi dovtedy, kym sa neprecita 
    {                                  //posledny znak zo suboru
          veta=veta+znak;              //retazec veta sa postupne doplna znakmi
    }
    
    cout << "V subore zapis.txt sa nachadza nasledovny text" << endl;                                        
    cout << veta << endl;
    
    system("pause");
    return 0;
}
