//Prog0006 -- aritmeticke vyrazy

#include <iostream>
using namespace std;

int main()
{
	int cislo1,cislo2,vysledok;        //deklaracia troch premennych
	
	/* Sucet celych cisel */
	cislo1=15;
	cislo2=8;
	vysledok=cislo1+cislo2;
    cout << "Sucet cisel " << cislo1 << " a " << cislo2 << " je " << vysledok << endl;
    
    /* Sucin celych cisel */
    cislo1=24;
	cislo2=6;
	vysledok=cislo1*cislo2;
    cout << "Sucin cisel " << cislo1 << " a " << cislo2 << " je " << vysledok << endl;

    /* Podiel celych cisel */
    cislo1=24;
	cislo2=6;
	vysledok=cislo1/cislo2;
    cout << "Podiel cisel " << cislo1 << " a " << cislo2 << " je " << vysledok << endl;
    
    /* Ukazka celociselneho delenia */
    cislo1=31;
	cislo2=7;
	vysledok=cislo1/cislo2;
    cout << "Podiel cisel " << cislo1 << " a " << cislo2 << " je " << vysledok << endl;
    
    /* Zvysok po celociselnom deleni */
    cislo1=31;
	cislo2=7;
	vysledok=cislo1%cislo2;
    cout << "Zvysok po celociselnom deleni cisel " << cislo1 << " a " << cislo2 << " je " << vysledok << endl;
    
    /* Zlozitejsi aritmeticky vyraz s tromi premennymi */
    cislo1=18;
    cislo2=14;
    int cislo3=10;
    vysledok=(cislo1+cislo2)*cislo3;
    cout << '(' << cislo1 << '+' << cislo2 << ")*" << cislo3 << '=' << vysledok << endl;
    
    /* Vsimnite si vysledok teraz, ked sme z vyrazu odstranili zatvorky */
    vysledok=cislo1+cislo2*cislo3;
    cout << cislo1 << '+' << cislo2 << "*" << cislo3 << '=' << vysledok << endl;
    
    
	system("pause");
	return 0;
}
