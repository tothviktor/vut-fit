//Prog0010 - Prikaz cyklu while

#include <iostream>
using namespace std;

int main()
{
    
    /******************** Priklad 1 ********************/
    cout << "Priklad 1" << endl;
    
    int i,N;
    cin >> i >> N;
    
    while (i<=N)
    {
          cout << i << " " << i*i << endl;
          i=i+1;
    }
    
    cout << endl << endl;
    
    /******************** Koniec ********************/
    
    
    
     
    /******************** Priklad 2 ********************/
    cout << "Priklad 2" << endl;
    
    int cislo;
    int HRANICNA_HODNOTA=50;
    
    while ((cin >> cislo), cislo<=HRANICNA_HODNOTA)
    {
          cout << "Zadali ste hodnotu " << cislo << endl;
    }
    
    cout << "Zadane cislo prekrocilo hranicnu hodnotu" << endl << endl;
    cin.get();
    
    /******************** Koniec ********************/
    
    
    
    
    /******************** Priklad 3 ********************/
    cout << "Priklad 3" << endl;
    
    string slovo="koniec";
    string vstup;
    
    while (getline(cin,vstup), !(vstup==slovo))
    {
          cout << "Zadali ste slovo " << vstup << endl;
    }
    
    cout << "Koniec cyklu" << endl << endl;
    
    /******************** Koniec ********************/
    
    
    system("pause");
    return 0;
}
