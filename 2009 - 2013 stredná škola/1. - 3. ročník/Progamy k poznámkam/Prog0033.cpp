//Prog0033 - Nacitanie cisel zo suboru cisla.dat
//Najskor je potrebne spustit program Prog0024.cpp, aby sa vytvoril subor cisla.dat!!!
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    
    int nahCislo[10];                  //Pole, ktore sa naplni cislami zo suboru
    ifstream vstupNC("cisla.dat");     //Otvorenie suboru cisla.dat na citanie                          
    
    for (int i=0;i<=9;i++)
    {
        vstupNC >> nahCislo[i];        //Precitanie jedneho cisla zo suboru a
    }                                  //Zapis do daneho prvku pola
    
    for (int i=0;i<=9;i++)
    {
        cout << nahCislo[i] << endl;   //Vypis hodnot pola
    }
    
    vstupNC.close();                   //Zatvorenie suboru cisla.dat                                         
    
    system("pause");
    return 0;
}
