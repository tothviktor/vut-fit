//Prog0029 - Zapisovanie na koniec existujuceho suboru
//Pri prvom spusteni programu sa najskor vytvori subor zapis.txt a zapise sa text
//Pri dalsich sputeniach programu sa vzdy zapisuje na koniec suboru zapis.txt
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ofstream vypis("zapis.txt", ios::app);                            
    vypis << "Tento text bude zapisany tolkokrat, kolkokrat program spustis\n";     
    
    vypis.close();                                         
    
    system("pause");
    return 0;
}
