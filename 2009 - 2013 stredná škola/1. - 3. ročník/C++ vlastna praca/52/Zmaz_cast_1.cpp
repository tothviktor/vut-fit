/*52. Zmaz_cast_1
Vytvorte program, ktor� vyma�e �ubovo�n� �as� re�azca. Program si najsk�r vy�iada
zadanie re�azca a n�sledne dve ��seln� hodnoty: poz�ciu, od ktorej m� za�a� maza�
a po�et znakov, ktor� m� zmaza�. Program potom vyp�e re�azec po zmazan� aj s jeho
aktu�lnou ve�kos�ou*/

#include<iostream>
using namespace std;
int main()
{
    string retazec;
    int cast,pocet;
    
    cout<<"Zapiste jedneho retazca: ";
    getline(cin,retazec);
    
    cout<<"Zadajte cislo od ktoreho znaku chcete zmazat retazec: ";
    cin>>cast;
    cout<<"Zadajte cislo kolko znakov chcete zmazat z retazca: ";
    cin>>pocet;
        
    retazec.erase(cast,pocet);
    
    cout<<retazec<<endl;
    
    system ("pause");
    return 0;
}
