/*42. Sinus
Vytvorte program na v�po�et s�nusu uhla zadan�ho z kl�vesnice v stup�och.
Pozor! Funkcia sin v C++ pou��va ako argument uhol v radi�noch*/

#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    const double PI=3.14159;
    double stupen;
    double sinus;
    
    cout<<"Zadajte uhol v stupnoch: ";
    cin>>stupen;
    
    sinus=sin((stupen*PI)/180);
    
    cout<<"Sinusovy uhol je: "<<sinus<<endl;
    
    system("pause");
    return 0;
}
