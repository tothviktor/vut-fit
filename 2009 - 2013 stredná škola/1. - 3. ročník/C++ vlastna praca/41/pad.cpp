/*41. Pad
Vytvorte program, ktor� vypo��ta za ak� �as dopadne na Zem teleso s v��ky h. Gravita�n�
zr�chlenie g bude zadan� ako kon�tanta rovn� 9.81ms-2.*/

#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    const double g=9.81;
    double h,t;
    
    cout<<"Zadajte vysku v metroch: ";
    cin>>h;
    
    t=sqrt(h/g);
    
    cout<<"Cas za ktory teleso dopadne na Zem je: "<<t<<" s\n";
    
    system ("pause");
    return 0;
}
