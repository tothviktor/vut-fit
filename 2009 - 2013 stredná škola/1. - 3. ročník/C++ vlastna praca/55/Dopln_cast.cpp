/*55. Dopln_cast
Vytvorte program, ktor� do zadan�ho re�azca dopln� in� re�azec. Program si najsk�r
vy�iada zadanie prv�ho re�azca. Potom bude zadan� poz�cia, od ktorej m� doplni�
p�vodn� re�azec a n�sledne bude zadan� re�azec, ktor� sa m� do p�vodn�ho doplni�.
Program potom vyp�e doplnen� re�azec.*/

#include<iostream>
using namespace std;
int main()
{
    string retazec1,retazec2;
    int pozicia;
    
    cout<<"Zadajte prvy retazec: ";
    cin>>retazec1;
    cout<<"Zadajte poziciu druheho retazca: ";
    cin>>pozicia;
    cout<<"Zadajte druhy retazec: ";
    cin>>retazec2;
    retazec1.insert(pozicia,retazec2);
    
    cout<<retazec1<<endl;
    
    system("pause");
    return 0;
}
