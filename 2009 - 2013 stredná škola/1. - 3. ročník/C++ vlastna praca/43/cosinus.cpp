/*43. Cosinus
Vytvorte program na v�po�et kos�nusu uhla zadan�ho z kl�vesnice v stup�och.
Pozor! Funkcia cos v C++ pou��va ako argument uhol v radi�noch*/

#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    double stupen;
    double cosinus;
    const double PI=3.14159;
    
    cout<<"Zadajte uhol v stupnoch: ";
    cin>>stupen;
    
    cosinus=cos((stupen*PI)/180);
    
    cout<<"Cosinusovy uhol sa rovna: "<<cosinus<<endl;
    
    system("pause");
    return 0;
}
