/*51. Automobil
Vytvorte program, ktor� umo�n� na��ta� nasledovn� �daje:
n�zvy, farby karos�ri�, po�et najazden�ch km, spotrebu �tyroch automobilov. Program
potom vyp�e zadan� �daje v preh�adnej tabu�ke, napr:
Nazov Farba Najazdene Spotreba na 100km
BMW          cierna            120000           8.5
OPEL         cervena           89000            6.3
MAZDA        modra             110000           7.6
PEUGEOT      zelena            100000           8.7
             PRIEMER           104750           7.775*/

#include <iostream>
using namespace std;
int main()
{
    string nazov1,nazov2,nazov3,nazov4,farba1,farba2,farba3,farba4;
    
    double najazdene1,najazdene2,najazdene3,najazdene4,spotreba1,spotreba2,
    spotreba3,spotreba4,pNajazdene,pSpotreba;
    
    cout<<"Zadajte nazov prveho auta: ";
    cin>>nazov1;
    cout<<"Zadajte nazov druheho auta: ";
    cin>>nazov2;
    cout<<"Zadajte nazov tretieho auta: ";
    cin>>nazov3;
    cout<<"Zadajte nazov stvrteho auta: ";
    cin>>nazov4;
    
    cout<<"Zadajte farbu prveho auta: ";
    cin>>farba1;
    cout<<"Zadajte farbu druheho auta: ";
    cin>>farba2;
    cout<<"Zadajte farbu tretieho auta: ";
    cin>>farba3;
    cout<<"Zadajte farbu stvrteho auta: ";
    cin>>farba4;
    
    cout<<"Zadajte pocet najazdenych kilometrov na prvom aute: ";
    cin>>najazdene1;
    cout<<"Zadajte pocet najazdenych kilometrov na druhom aute: ";
    cin>>najazdene2;
    cout<<"Zadajte pocet najazdenych kilometrov na tretom aute: ";
    cin>>najazdene3;
    cout<<"Zadajte pocet najazdenych kilometrov na stvrtom aute: ";
    cin>>najazdene4;
    
    cout<<"Zadajte spotrebu prveho auta na 100km: ";
    cin>>spotreba1;
    cout<<"Zadajte spotrebu druheho auta na 100km: ";
    cin>>spotreba2;
    cout<<"Zadajte spotrebu tretieho auta na 100km: ";
    cin>>spotreba3;
    cout<<"Zadajte spotrebu stvrteho auta na 100km: ";
    cin>>spotreba4;
    
    pNajazdene=(najazdene1+najazdene2+najazdene3+najazdene4)/4;
    pSpotreba=(spotreba1+spotreba2+spotreba3+spotreba4)/4;
    
    cout<<"Nazov\t"<<"Farba\t"<<"Najazdene\t"<<"Spotreba na 100km\n\n";
    cout<<nazov1<<"\t"<<farba1<<"\t"<<najazdene1<<"\t\t"<<spotreba1<<"\n";
    cout<<nazov2<<"\t"<<farba2<<"\t"<<najazdene2<<"\t\t"<<spotreba2<<"\n";
    cout<<nazov3<<"\t"<<farba3<<"\t"<<najazdene3<<"\t\t"<<spotreba3<<"\n";
    cout<<nazov4<<"\t"<<farba4<<"\t"<<najazdene4<<"\t\t"<<spotreba4<<"\n";
    cout<<"\tPRIEMER\t"<<pNajazdene<<"\t\t"<<pSpotreba<<"\n";
    
    system ("pause");
    return 0;
}
