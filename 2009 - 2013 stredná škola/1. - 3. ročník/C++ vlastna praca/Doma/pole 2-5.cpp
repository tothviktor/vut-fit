/*Vytvorte pole do ktoreho sa vygeneruju nahodne hodnoty z intervalu <a,b> a potom 
vytvorte dalsie 2 polia do jedneho sa ulozia parne cisla delitelne piatimi a do 
druheho neparne cisla delitelne piatimi na konci treba dat vypisat vsetky 3 polia*/

#include <iostream>
#include <ctime>
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    
    int N,a,b,p=0,n=0;
    cout<<"Zadaj zaciatok intervalu: ";
    cin>>a;
    cout<<"Zadaj koniec intervalu: ";
    cin>>b;
    cout<<"Zadaj pocet vygenerovanych cisel : ";
    cin>>N;
    
    int *pole=new int [N];
    int *poleParne=new int [N];
    int *poleNeparne=new int [N];
    
    for(int i=0,j=0;i<N;i++)
    {
            j=a+rand()%(b-a+1);
            pole[i]=j;
            
            if(pole[i]%2==0 && pole[i]%5==0)
            {
                            poleParne[p]=j;
                            p++;
            }
            if(pole[i]%2==1 && pole[i]%5==0)
            {
                            poleNeparne[n]=j;
                            n++;
            }  
    }
    
    cout<<"Vsetky vygenerovane cisla: \n";
    for(int i=0;i<N;i++)
    {
            cout<<pole[i]<<",";
    }
    cout<<"\nVsetky parne vygenerovane cisla: \n";
    for(int i=0;i<p;i++)
    {
            cout<<poleParne[i]<<",";
    }
    cout<<"\nVsetky neparne vygenerovane cisla: \n";
    for(int i=0;i<n;i++)
    {
            cout<<poleNeparne[i]<<",";
    }
    
    system("pause");
    return 0;
}
