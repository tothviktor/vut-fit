/*Vytvorte pole do ktoreho sa vygeneruju nahodne hodnoty z intervalu 
<a,b> a potom vytvorte dalsie 2 polia do jedneho sa ulozia parne cisla
delitelne piatimi a do druheho neparne cisla delitelne piatimi na konci
treba dat vypisat vsetky 3 polia */

#include <iostream>
#include <ctime>
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    
    int N,a,b,p=0,n=0,;
    cout<<"Zadaj zaciatok intervalu: ";
    cin>>a;
    cout<<"Zadaj koniec intervalu: ";
    cin>>b;
    cout<<"Zadaj pocet vygenerovanych cisel : ";
    cin>>N;
    
    int *poleCisel=new int [N];
    int *poleParnychCisel=new int [N];
    int *poleNeparnychCisel=new int [N];
    
    for(int i=0,j=0;i<N;i++)
    {
            j=a+rand()%(b-a+1);
            poleCisel[i]=j;
            if(j%2==0 && j%5==0)
            {
                poleParnychCisel[p]=j;
                p++;
            }
            if(j%2==1 && j%5==0)
            {
                poleNeparnychCisel[n]=j;      
                n++;
            }
    }
    cout << "Pole nahodnych cisel:" << endl;
    for(int i=0;i<N;i++)
    {
         cout << poleCisel[i] << endl;
    }
    cout << "Pole parnych cisel delitelny 5:" << endl;
    for(int i=0;i<p;i++)
    {
         cout << poleParnychCisel[i] << endl;
    }
    cout << "Pole neparnych cisel delitelny 5:" << endl;
    for(int i=0;i<n;i++)
    {
         cout << poleNeparnychCisel[i] << endl;
    }
    
    system("pause");
    return 0;
}
