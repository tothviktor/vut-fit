/*Vytvorte program, ktor� pole vo volnom �lo�i�ti 
 - zapln� celoc�seln�mi hodnotami generovan�mi z <a,b>.     nahodneHodnoty
 - n�sledne dajte pole vyp�sat.                             vypisPola
 - zadan� hodnota sa vyskytla, alebo nie.                   vyskytHodnoty
 - zadan� hodnota kolko kr�t sa vyskytla.                   pocetnostVyskytu
 - maxim�lna prvok pola.                                    maximalnaPrvokPola
 - vzdialenost medzi maxim�lnej a minim�lnej hodnoty.            vzdialenostMaxMinPola
 Na zaplnenie pola a na v�pis pou�ite funkciu !!!*/

#include <iostream>
using namespace std;

void nahodneHodnoty(int*pole,int a,int b,int N);
void vypisPola(int*pole,int N);
int vyskytHodnoty(int*pole,int N,int cislo);
void pocetnostVyskytu(int*pole,int N,int cislo,int v);
int maximalnaPrvokPola(int*pole,int N,int max);
int vzdialenostMaxMinPola(int*pole,int N,int max,int min);

int main()
{
	int zaciatokIntervalu, koniecIntervalu, pocetCisel, vyskytCislo,vzdialenostPrvkov,hodnota,
    maxPrvok= -2147483640,minPrvok= 2147483640;

	cout<<"Zadajte zaciatok intervalu: ";
	cin>>zaciatokIntervalu;
	cout<<"Zadajte koniec intervalu: ";
	cin>>koniecIntervalu;
	cout<<"Zadajte pocet cisel v intervalu: ";
	cin>>pocetCisel;
	cout<<"Zadajte cislo na porovnanie z intervalu <"<<zaciatokIntervalu<<","<<koniecIntervalu<<"> : ";
	cin>>vyskytCislo;

    int*poleCelychCisel= new int[pocetCisel];
    
    nahodneHodnoty       (poleCelychCisel,zaciatokIntervalu,koniecIntervalu,pocetCisel);
    vypisPola            (poleCelychCisel,pocetCisel); 
    hodnota=vyskytHodnoty                   (poleCelychCisel,pocetCisel,vyskytCislo);
    pocetnostVyskytu     (poleCelychCisel,pocetCisel,vyskytCislo,hodnota);
    maxPrvok=maximalnaPrvokPola             (poleCelychCisel,pocetCisel,maxPrvok);
    vzdialenostPrvkov=vzdialenostMaxMinPola (poleCelychCisel,pocetCisel,minPrvok,maxPrvok);
    
    cout<<"Maximalna hodnota pola je: "<<maxPrvok<<endl;
    cout<<"Vzdialenost medzi maximalnej a minimalnej hodnoty je: "<<vzdialenostPrvkov<<endl;
    
	system ("pause");
	return 0;
}
//------------------------------------------------------------------------------
void nahodneHodnoty(int*pole,int a,int b,int N)
{
     for(int i=0;i<N;i++)
     {
            pole[i]=a+rand()%(b-a+1);
     }                  
}
//------------------------------------------------------------------------------
void vypisPola(int*pole,int N)
{
     for(int i=0;i<N;i++)
     {
            cout<<"pole["<<i<<"] ma hodnotu :"<<pole[i]<<endl;
     }                  
}
//------------------------------------------------------------------------------
int vyskytHodnoty(int*pole,int N,int cislo)
{
     int v=0;
     for(int i=0;i<N;i++)
     {
            if(cislo==pole[i])
            {
                v++;
		    }
     }
     if(v>0)
     {
            cout<<"Vyskytla sa hodnota\n";
     }
     else
     {
            cout<<"Nevyskytla sa hodnota\n";
     }
     return v;
}
//------------------------------------------------------------------------------
void pocetnostVyskytu(int*pole,int N,int cislo,int v)
{
            cout<<"Vyskytla sa hodnota "<<v<<" krat\n";
}
//------------------------------------------------------------------------------
int maximalnaPrvokPola(int*pole,int N,int max)
{
     for(int i=0;i<N;i++)
     {
          if(max<pole[i])
          {
              max=pole[i];
          }  
     }
     return max;          
}
//------------------------------------------------------------------------------
int vzdialenostMaxMinPola(int*pole,int N,int min,int max )
{
     for(int i=0;i<N;i++)
     {
          if(min>pole[i])
          {
              min=pole[i];
          }  
     }
     return max-min;    
}
