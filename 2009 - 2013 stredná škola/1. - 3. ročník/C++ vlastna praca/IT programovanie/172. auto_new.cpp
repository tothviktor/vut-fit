/* Auto_new
Vytvorte program, ktor� umo�n� na��ta� typ (osobn�, n�kladn�), v�robn� zna�ku, rok 
v�roby, po�et najazden�ch kilometrov a spotrebu N automobilov. Program si potom 
vy�iada zadanie roku v�roby a n�sledne vyp�e s��et po�tu najazden�ch kilometrov 
a priemern� spotrebu v�etk�ch automobilov, vyroben�ch v danom roku. V�etky premenn� 
nech s� ulo�en� v z�sobn�ku, �trukt�ra nech je vo vo�nom �lo�i�ti*/

#include<iostream>
#include<string>
using namespace std;
struct doprava
{
	string typ, vyrobnaZnacka;
	int rokVyroby, pocetKm;
	double spotreba;
};

int main()
{

	int N, sucetKm=0, sucetSpotreby=0, rokVyrobyM, pocetAut=0;
	cout << "Vlozte N " << endl;
	cin >> N;
	cin.ignore();
	doprava *vozidlo=new doprava[N];
	for(int i=0;i<N;++i)
    {
		cout << endl << "Udajte typ vozidla " << endl;
		getline(cin,(vozidlo+i)->typ);
		cout << "Vlozte vyrobnu znacku " << endl;
		cin.ignore();
		getline(cin,(vozidlo+i)->vyrobnaZnacka);
		cout << "Zadajte rok vyroby vozidla " << endl;
		cin >> (vozidlo+i)->rokVyroby;
		cout << "Zadajte spotrebu vozidla " << endl;
		cin >> (vozidlo+i)->spotreba;
		cout << "Vlozte pocet najazdenych kilometrov vozidla " << endl;
		cin >> (vozidlo+i)->pocetKm;
		cin.ignore();
	}

	cout << endl << "Zadajte rok vyroby v ktorom chcete zobrazit vozidla " << endl;
	cin >> rokVyrobyM;
	for(int i=0;i<N;++i)
    {
		if((vozidlo+i)->rokVyroby==rokVyrobyM)
        {
			sucetKm=(vozidlo+i)->pocetKm+sucetKm;
			sucetSpotreby=(vozidlo+i)->spotreba+sucetSpotreby;
			pocetAut++;
		}
	}
	cout << "Sucet poctu najazdenych kilometrov je " << sucetKm << endl
		 << "Priemerna spotreba aut vyrobenych v " << rokVyrobyM << " je " 
		 << double(sucetSpotreby)/pocetAut << endl;

	system("pause");
	return 0;
}
