/*58. Podiel_pt
Vytvorte program, ktor� vypo��ta necelo��seln� podiel dvoch cel�ch ��sel, zadan�ch
z kl�vesnice. V�etky premenn� nech s� ulo�en� v z�sobn�ku a nech sa s nimi pracuje cez
ukazovatele.*/

#include <iostream>
using namespace std;
int main()
{
    double cislo1,cislo2,vysledok,*ptCislo1,*ptCislo2,*ptVysledok;
    
    ptCislo1=&cislo1;
    ptCislo2=&cislo2;
    ptVysledok=&vysledok;
    
    cout<<"Zadajte prve cislo ktore chcete delit: ";
    cin>>*ptCislo1;
    cout<<"Zadajte druhe cislo cislo: ";
    cin>>*ptCislo2;
    
    *ptVysledok=(*ptCislo1)/(*ptCislo2);
    
    cout<<"Podiel dvoch cisel je: "<<*ptVysledok<<endl;
       
    system ("pause");
    return 0;
}
