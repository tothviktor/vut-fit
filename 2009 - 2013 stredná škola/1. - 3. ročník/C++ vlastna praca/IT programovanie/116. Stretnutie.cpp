/*116. Stretnutie *
Vytvorte program, ktor� by simuloval ch�dzu dvoch os�b, nazveme ich Bob a Alica
(kv�li lep�iemu pochopeniu zadania pozri zadanie �.100). Obaja vyrazia naraz (nie
z rovnak�ho miesta) a po ur�itom po�te krokov zastan�. Program m� zisti�, �i sa po�as
ch�dze stretli. Ak sa stretli, tak program vyp�e poz�ciu miesta stretnutia (ak sa stretli dva
a viackr�t, tak program vyp�e len poz�ciu posledn�ho stretnutia, nako�ko zatia� nevieme
pracova� s po�ami) a po�et krokov, ktor� dovtedy pre�li. Ak sa nestretli, tak program
vyp�e najmen�iu vzdialenos�, ak� bola medzi nimi po�as ch�dze.*/

#include <iostream>
#include <ctime>
#include <cmath>
using namespace std;
int main()
{
    srand((unsigned)time(NULL));
    int a=0,b=0,x1,y1,x2,y2,N=0;
    double cislo,vzdialenost=4294967294;
    
    cout<<"Zadajte x-ovu suradnicu, kde stoji Bob: ";
    cin>>x1;
    cout<<"Zadajte y-ovu suradnicu, kde stoji Bob: ";
    cin>>y1;
    cout<<"Zadajte x-ovu suradnicu, kde stoji Alica: ";
    cin>>x2;
    cout<<"Zadajte y-ovu suradnicu, kde stoji Alica: ";
    cin>>y2;
    
    while(N<=0)
    {
           cout<<"Zadajte pocet krokov, ktore Bob a Alica ma vykonat: ";
           cin>>N;
    }

    cout<<"\nVstup\t\tVystup\t\tOsoba\n";
    cout<<x1<<","<<y1<<"\t\t["<<x1<<","<<y1<<"]\t\tBob\n";
    cout<<x2<<","<<y2<<"\t\t["<<x2<<","<<y2<<"]\t\tAlica\n";
    cout<<"Pocet krokov: "<<N<<"\n\n";
    
    for(int i=1,s1=0,s2=0;i<=N;i++)
    {
            s1=1+rand()%4;
            switch(s1)
            {
                     case 1:y1++;    //hore 
                     break;
                     case 2:x1++;    //doprava
                     break;
                     case 3:y1--;    //dole
                     break;
                     case 4:x1--;    //dolava
                     break;
            }
                        
            s2=1+rand()%4;
            switch(s2)
            {
                      case 1:y2++;    //hore 
                      break;
                      case 2:x2++;    //doprava
                      break;
                      case 3:y2--;    //dole
                      break;
                      case 4:x2--;    //dolava
                      break;                
            }
            cout<<i<<". Bob  \tkrok - pozicia \t["<<x1<<","<<y1<<"]\n"; 
            cout<<i<<". Alica\tkrok - pozicia \t["<<x2<<","<<y2<<"]\n"; 
            
            if(x1==x2&&y1==y2)
            {
				a=x1;
				b=y1;
				vzdialenost=0;
            }
			else
			{
				cislo=sqrt(pow(double(x1-x2),2)+pow(double(y1-y2),2));
				
                if(vzdialenost>cislo)
                {
                        vzdialenost=cislo;
                }
			}
    }
    
    if(vzdialenost==0)
    {
        cout<<"Stretli sa na bode ["<<a<<","<<b<<"]\n";
    }
    else
    {
        cout<<"Najmensia vzdialenost bola: "<<vzdialenost<<endl;
    }
    
    system ("pause");
    return 0;
}
