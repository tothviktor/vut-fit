/*60. Priemer_pt
Vytvorte program, ktor� vypo��ta priemer 5 cel�ch ��sel zadan�ch z kl�vesnice. V�etky
premenn� nech s� ulo�en� v z�sobn�ku a nech sa s nimi pracuje cez ukazovatele.*/

#include<iostream>
using namespace std;
int main()
{
    double cislo1=0,cislo2=0,cislo3=0,cislo4=0,cislo5=0,priemer;
    double *ptCislo1,*ptCislo2,*ptCislo3,*ptCislo4,*ptCislo5,*ptPriemer;
    
    ptCislo1=&cislo1;
    ptCislo2=&cislo2;
    ptCislo3=&cislo3;
    ptCislo4=&cislo4;
    ptCislo5=&cislo5;
    ptPriemer=&priemer;
    
    cout<<"Zadajte si prve cislo: ";
    cin>>*ptCislo1;
    cout<<"Zadajte si druhe cislo: ";
    cin>>*ptCislo2;
    cout<<"Zadajte si tretie cislo: ";
    cin>>*ptCislo3;
    cout<<"Zadajte si stvrte cislo: ";
    cin>>*ptCislo4;
    cout<<"Zadajte si piate cislo: ";
    cin>>*ptCislo5;
    
    *ptPriemer=((*ptCislo1)+(*ptCislo2)+(*ptCislo3)+(*ptCislo4)+(*ptCislo5))/5;
    
    cout<<"Priemer piatich cisel je: "<<*ptPriemer<<endl;
    
    system("pause");
    return 0;
}
