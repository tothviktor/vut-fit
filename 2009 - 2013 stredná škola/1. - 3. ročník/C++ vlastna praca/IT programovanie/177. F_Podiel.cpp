/*177. F_Podiel
Vytvorte program na delenie dvoch re�lnych c�sel, zadan�ch z kl�vesnice. Definujte
pr�slu�n� funkciu. �lohou funkcie bude vydelit zadan� c�sla (obe jej bud� odovzdan� pri
volan�). V�sledok delenia bude vr�ten� hlavn�mu programu a n�sledne vyp�san�.*/

#include<iostream>
using namespace std;

double sucinCisel(double,double);

int main()
{
	double cislo1,cislo2,vysledok;
	cout<<"Zadajte cislo, ktore chcete delit: ";
	cin>>cislo1;
	cout<<"Zadajte cislo, ktorim chcete delit: ";
	cin>>cislo2;
	
	while(cislo2==0)
	{
                    cout<<"S nulou sa neda delit!!!\n";
                    cin>>cislo2;
    }
	
	vysledok=sucinCisel(cislo1,cislo2);

	cout<<"Vysledok je "<<vysledok<<endl;

	system ("pause");
	return 0;
}

double sucinCisel(double a,double b)
{
	return a/b;
}
