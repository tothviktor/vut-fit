/*186.Retazec_rf
Z kl�vesnice bude zadan� �ubovo�n� veta a jeden znak. Vytvorte funkciu Zisti() typu
void, ktorej bud� veta a znak odovzdan�. Funkcia zist� nasledovn�: po�et slov vo vete,
d�ku vety a tie� ko�kokr�t sa vo vete vyskytuje dan� znak. Z funkcie main() bud� potom
tieto hodnoty vyp�san�.*/

#include <iostream>
#include <string>
using namespace std;

void Zisti(string v,char z,int & dlzka,int & pocetSlov,int & danyZnak);

int main()
{
    int dlzka=0,pocetSlov=1,danyZnak=0;
    string veta;
    char znak;
    
    cout<<"Napis si jednu vetu:";
    getline(cin,veta);
    cout<<"Zadaj znak:";
    cin>>znak;
    
    Zisti(veta,znak,dlzka,pocetSlov,danyZnak);
    
    cout<<"Dlzka retazca je:"<<dlzka<<endl;
    cout<<"V retazce sa nachadza "<<pocetSlov<<" slovo/a"<<endl;
    cout<<"Znak "<<znak<<"sa nachadza "<<danyZnak<<" krat v retazci"<<endl;
       
    system ("pause");
    return 0;
}
void Zisti(string v,char z,int & dlzka,int & pocetSlov,int & danyZnak)
{
     char m=32;
     dlzka=v.length();  

     for(int i=0;i<dlzka;i++)
     {
            if(v[i]==m)
            {
                   pocetSlov++;
            }
     }    

     for(int i=0;i<dlzka;i++)
     {
            if(v[i]==z)
            {
                   danyZnak++;
            }                      
     }
}
