/*115. Nahodna_chodza *
Vytvorte program, ktor� by simuloval n�hodn� ch�dzu osoby, ktor� nazveme Bob. Bob
vyraz� z ur�it�ho miesta a po zadanom po�te krokov sa dostane na in� miesto. �lohou
programu je vyp�sa� jednotliv� prejden� poz�cie a nakoniec aj vzdialenos� od po�iatku.
Bob sa bude pohybova� v s�radnicovom syst�me, ktor� je dan� x-ovou a y-ovou
s�radnicou v tvare [x,y]. Bob sa m��e v jednom kroku pohn�� smerom vpravo, v�avo,
hore a dolu.
Program si najsk�r vy�iada poz�ciu (t.j. x-ov� a y-ov� s�radnicu) miesta, z ktor�ho vyraz�.
Potom si vy�iada zadanie ��sla N, t.j. po�tu krokov. Po zadan� N, Bob vykon� N
n�hodn�ch krokov a zastane.
Vstup V�stup
5,12 [5,12]
5 1. krok � pozicia [5,13]
2. krok � pozicia [5,14]
3. krok � pozicia [4,14]
4. krok � pozicia [4,15]
5. krok � pozicia [3,15]
Vzdialenost: 2,83*/

#include <iostream>
#include <cmath>
#include <ctime>
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    
    int x=0,y=0,a=0,b=0,N=0;
    double e=0;
    
    cout<<"Zadajte x-ovu suradnicu, kde stoji Bob: ";
    cin>>x;
    cout<<"Zadajte y-ovu suradnicu, kde stoji Bob: ";
    cin>>y;
    
    while(N<=0)
    {
           cout<<"Zadajte pocet krokov, ktore Bob ma vykonat: ";
           cin>>N;
    }
    a=x;
    b=y;
    
    cout<<"\nVstup\t\t\tVystup\n";
    cout<<a<<","<<b<<"\t\t\t["<<a<<","<<b<<"]\n";
    cout<<"Pocet krokov: "<<N<<"\n\n";
    
    for(int i=1,s=0;i<=N;i++)
    {
            s=1+rand()%4;
            switch(s)
            {
                     case 1:y++;    //hore 
                     break;
                     case 2:x++;    //doprava
                     break;
                     case 3:y--;    //dole
                     break;
                     case 4:x--;    //dolava
                     break;
            }
            cout<<i<<". krok - pozicia \t["<<x<<","<<y<<"]\n";       
    }
    e=sqrt(pow(double(a-x),2)+pow(double(b-y),2));
    cout<<"Vzdialenost: "<<e<<endl;
    
    system("pause");
    return 0;
}
