/*69. Interval *
Vytvorte program, ktor� zist�, �i cel� ��slo zadan� z kl�vesnice je z intervalu <a,b>.
Hranice intervalu bud� tie� cel� ��sla, zadan� z kl�vesnice. Ak zadan� ��slo bude
z intervalu <a,b>, tak program vyp�e, napr. �Zadane cislo 25 je z intervalu <5,35>�.
Ak zadan� ��slo nebude z intervalu, tak sa vyp�e, napr. �Zadane cislo 15 nie je z intervalu
<25,60>�.*/

#include<iostream>
using namespace std;
int main()
{
    int a,b,cislo;
    
    cout<<"Zadajte si jedno cele cislo: ";
    cin>>cislo;
    
    cout<<"Zadajte si zaciatok intervalu: ";
    cin>>a;
    
    cout<<"Zadajte si koniec intervalu: ";
    cin>>b;
    
    if(cislo>a and cislo<b)
    {
         cout<<"Zadane cislo "<<cislo<<" je z intervalu <"<<a<<","<<b<<">\n";
    }
    else
    {
         cout<<"Zadane cislo "<<cislo<<" nie je z intervalu <"<<a<<","<<b<<">\n";
    }
    system ("pause");
    return 0;
}
