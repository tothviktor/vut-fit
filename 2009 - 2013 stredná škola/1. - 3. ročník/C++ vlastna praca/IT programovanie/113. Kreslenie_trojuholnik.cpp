/*
Kreslenie_trojuholnik **
Vytvorte program, ktor� zo znaku, zadan�ho z kl�vesnice, vykresl� pravouhl�
rovnoramenn� trojuholn�k. Program si najsk�r vy�iada znak, ktor�m bude kresli�
a n�sledne d�ku ramien. Napr. zad�me # a 6, program vykresl� nasledovn�:
#
##
###
####
#####
######
*/

#include <iostream>
#include <ctime>
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    
    char z;
    int d;
    
    z=rand();
    cout<<"Zadajte dlzku rovnoramenneho trojuholnika: ";
    cin>>d;

    for(int i=1;i<=d;i++)
    {
         for(int j=1;j<=i;j++)
         {
              cout<<z;
         }
         cout<<endl;
    }
    
    system ("pause");
    return 0;
}
