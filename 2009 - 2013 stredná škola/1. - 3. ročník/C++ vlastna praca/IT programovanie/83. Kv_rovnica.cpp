/*83. Kv_rovnica
Vytvorte program na rie�enie kvadratickej rovnice ax2+bx+c=0 v obore re�lnych ��sel.*/
#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    double a,b,c,x1,x2,D;
    cout<<"Doplnte cisla do vzorca ax2+bx+c=0\n";
    cout<<"Zadajte a: ";
    cin>>a;
    cout<<"Zadajte b: ";
    cin>>b;
    cout<<"Zadajte c: ";
    cin>>c;
    
    D=b*b-(4*a*c);
    
    if(D<0)
    {
           cout<<"Kvadraticka funkcia nema riesenie.\n";
    }
    else
    {
           D=sqrt(D);
           x1=(-b-D)/(2*a);
           x2=(-b+D)/(2*a);
           
           if(D==0)
           {
                    cout<<"Kvadraticka rovnica ma len jedno riesenie x= "<<x1<<endl;
           }
           else
           {
                    cout<<"Vysledok kvadratickej funkcii je: \nx1= "<<x1<<"\tx2= "<<x2<<endl;
           }
    }
    system ("pause");
    return 0;
}
