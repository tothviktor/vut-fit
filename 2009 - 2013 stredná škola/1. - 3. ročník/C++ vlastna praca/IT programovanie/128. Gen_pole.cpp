/*128. Gen_pole
Vytvorte program, ktor� do N- prvkov�ho po�a vygeneruje �ubovo�n� celo��seln�
hodnoty z intervalu <a,b>. Po vygenerovan� poslednej hodnoty, program obsah cel�ho
po�a vyp�e.*/

#include <iostream>
#include <ctime>
using namespace std;
int main()
{
    srand((unsigned)time(NULL));
    int N=0,a,b;
    cout<<"Zadajte zaciatok intervalu: ";
    cin>>a;
    cout<<"Zadajte koniec intervalu: ";
    cin>>b;
    while(N<=0)
    {
               cout<<"Zadajte kolko cisel chcete vypisat z intervalu <"<<a<<","<<b<<"> : ";
               cin>>N;
    }
    int*pole=new int[N];
    
    for(int i=0;i<N;i++)
    {
            pole[i]=a+rand()%(b-a+1);
    }
    for(int i=0;i<N;i++)
    {
            cout<<"V "<<i+1<<". pole je hodnota: "<<pole[i]<<endl;
    }
    system ("pause");
    return 0;
}
