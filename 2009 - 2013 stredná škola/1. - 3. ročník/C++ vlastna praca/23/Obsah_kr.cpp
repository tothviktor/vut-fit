/*23. Obsah_kr
Vytvorte program na v�po�et obsahu kruhu. PI zadajte ako kon�tantu*/

#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    const double PI=3.14159;
    double polomer;
    double obsah;
    
    cout<<"Zadajte polomer kruhu v mm: ";
    cin>>polomer;
    
    obsah=pow(polomer,2)*PI;
    cout<<"Obsah kruhu je: "<<obsah<<" mm2\n";
    
    system ("pause");
    return 0;
}
