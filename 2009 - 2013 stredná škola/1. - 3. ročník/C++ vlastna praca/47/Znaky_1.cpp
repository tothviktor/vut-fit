/*47. Znaky_1
Vytvorte program, ktor� po zadan� �ubovo�n�ho znaku z kl�vesnice, vyp�e tento znak
trikr�t za sebou. Napr.: zad�me * a program vyp�e *** */

#include<iostream>
using namespace std;
int main()
{
    char znak;
    
    cout<<"Zadajte jeden znak: ";
    cin>>znak;
    
    string znaky(3,znak);
    
    cout<<"Zadany znak 3 krat za sebou: "<<znaky<<endl<<"\a";
    
    system("pause");
    return 0;
}
