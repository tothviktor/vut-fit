/*36. BMI
Vytvorte program, ktor� vypo��ta hodnotu BMI (Body Mass Index):
BMI = hmotnos� v kg / (v��ka v m)2
V�stup napr.: �Vase BMI je 19.58�*/

#include <iostream>
using namespace std;
int main()
{
    double hmotnost;
    double vyska;
    double BMI;
    
    cout<<"Zadajte vasu hmotnost v kg: ";
    cin>>hmotnost;
    cout<<"Zadajte vasu vysku v m: ";
    cin>>vyska;
    
    BMI=hmotnost/(vyska*vyska);
    cout.precision(4);
    cout<<endl<<"Vase BMI je: "<<BMI<<endl;
    
    system ("pause");
    return 0;
}
