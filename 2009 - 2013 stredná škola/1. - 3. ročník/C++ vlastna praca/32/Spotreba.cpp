/*32. Spotreba
Nap�te program, ktor� sa sp�ta, ko�ko kilometrov autom ste pre�li a ko�ko 
litrov benz�nu ste spotrebovali. Program potom vyp�e va�u spotrebu v litroch 
na 100 kilometrov*/

#include <iostream>
using namespace std;
int main()
{
    double draha;
    double spotreba;
    double spotrebaAuta;
    
    cout<<"Zadajte kolko kilometrov autom ste presli: ";
    cin>>draha;
    cout<<"Zadajte kolko litrov benzinu ste spotrebovali: ";
    cin>>spotreba;
    
    spotrebaAuta=(100*spotreba)/draha;
    
    cout<<"Vasa spotreba na 100 kilometrov je: "<<spotrebaAuta<<"liter benzin\n";
    
    system ("pause");
    return 0;
}
