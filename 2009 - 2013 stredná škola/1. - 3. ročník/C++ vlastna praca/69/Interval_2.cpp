/*69. Interval_2 *
Vytvorte program, ktor� zist� vzdialenos� cel�ho ��sla, zadan�ho z kl�vesnice, od
intervalu <a,b>, ktor�ho hranice bud� tie� zadan� z kl�vesnice. Ak je dan� ��slo
z intervalu, tak to program vyp�e, ak nie je tak vyp�e vzdialenos� od jednej z hran�c
intervalu. Pr�klady vstupov a v�stupov z programu:
Vstup               V�stup
25                  Zadane cislo 25 je z intervalu <17,33>
17
33
Vstup               V�stup
18                  Zadane cislo 18 nie je z intervalu <37,65>
37                  Jeho vzdialenost od intervalu je 19
65*/
#include <iostream>
using namespace std;
int main()
{
    int cislo,a,b,c,d;
    cout<<"Zadajte dve hranicne body intervalu: \n";
    cin>>a;
    cin>>b;
    cout<<"Zadajte jedno cislo: ";
    cin>>cislo;
    
    if(b<a)
    {
           c=a;
           a=b;                                               //a<b 
           b=c;
    }
    if(cislo>=a and cislo<=b)
    {
                cout<<"Vstup\tVystup\n"
                <<cislo<<"\t Zadane cislo "<<cislo<<" je z intervalu <"<<a<<","<<b<<">.\n"
                <<a<<"\n"
                <<b<<endl;
    }       
    if(cislo<a)
    {
                d=a-cislo;
                cout<<"Vstup\tVystup\n"
                <<cislo<<"\tZadane cislo "<<cislo<<" nie je z intervalu <"<<a<<","<<b<<">\n"
                <<a<<"\tJeho vzdialenost od intervalu je "<<d<<".\n"
                <<b<<endl;
    }
    if(cislo>b)
    {
                d=cislo-b;
                cout<<"Vstup\tVystup\n"
                <<cislo<<"\tZadane cislo "<<cislo<<" nie je z intervalu <"<<a<<","<<b<<">\n"
                <<a<<"\tJeho vzdialenost od intervalu je "<<d<<".\n"
                <<b<<endl;
    }
    system("pause");
    return 0;
}
