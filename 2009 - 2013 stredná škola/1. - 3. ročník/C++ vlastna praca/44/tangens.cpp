/*44. Tangens
Vytvorte program na v�po�et tangensu uhla zadan�ho z kl�vesnice v stup�och.*/

#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    const double PI=3.14159;
    double stupen,tangens;
    
    cout<<"Zadajte uhol v stupnoch: ";
    cin>>stupen;
    
    tangens=tan((stupen*PI)/180);
    
    cout<<"Tangensovi uhol sa rovna: "<<tangens<<endl;
    
    system("pause");
    return 0;
} 
