﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="10008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="EK-LM3S8962" Type="Embedded">
		<Property Name="alias.name" Type="Str">EK-LM3S8962</Property>
		<Property Name="alias.value" Type="Str">Embedded:RealView:EK-LM3S8962</Property>
		<Property Name="EIOPath" Type="Str">eio\eio.xml</Property>
		<Property Name="EmbeddedUrl" Type="Str">Embedded:RealView:EK-LM3S8962</Property>
		<Property Name="Type" Type="Str">EK-LM3S8962</Property>
		<Item Name="Analog Input" Type="Folder">
			<Item Name="AI0" Type="Elemental IO">
				<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
			</Item>
			<Item Name="AI1" Type="Elemental IO">
				<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
			</Item>
			<Item Name="AI2" Type="Elemental IO">
				<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
			</Item>
			<Item Name="AI3" Type="Elemental IO">
				<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
			</Item>
		</Item>
		<Item Name="Digital Output" Type="Folder">
			<Item Name="LED0" Type="Elemental IO">
				<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/LED0</Value>
   </Attribute>
</AttributeSet>
</Property>
			</Item>
		</Item>
		<Item Name="seriová linka na 4 vstupy.vi" Type="VI" URL="../seriová linka na 4 vstupy.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Bytes At Serial Port.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Bytes At Serial Port.vi"/>
				<Item Name="EKLM3S8962 Display Draw String.vi" Type="VI" URL="/&lt;vilib&gt;/Embedded/ARM/EK-LM3S8962/display/EKLM3S8962 Display Draw String.vi"/>
				<Item Name="EKLM3S8962 Display Init.vi" Type="VI" URL="/&lt;vilib&gt;/Embedded/ARM/EK-LM3S8962/display/EKLM3S8962 Display Init.vi"/>
				<Item Name="LM3Sxxxx GPIO Write Pin.vi" Type="VI" URL="/&lt;vilib&gt;/Embedded/ARM/LM3Sxxxx/GPIO/LM3Sxxxx GPIO Write Pin.vi"/>
				<Item Name="LM3Sxxxx_ADC_Read_VI.vi" Type="VI" URL="/&lt;vilib&gt;/Embedded/ARM/LM3Sxxxx/ADC/LM3Sxxxx_ADC_Read_VI.vi"/>
				<Item Name="Open Serial Driver.vi" Type="VI" URL="/&lt;vilib&gt;/instr/_sersup.llb/Open Serial Driver.vi"/>
				<Item Name="Serial Port Init.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Serial Port Init.vi"/>
				<Item Name="Serial Port Read.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Serial Port Read.vi"/>
				<Item Name="Serial Port Write.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Serial Port Write.vi"/>
				<Item Name="serpConfig.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/serpConfig.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
			</Item>
			<Item Name="visarc" Type="Document" URL="/C/Program Files (x86)/National Instruments/LabVIEW 2010/resource/visarc"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Application" Type="EmbeddedBuildSpec">
				<Property Name="BDTFlags" Type="Str">19D480B3-60B9-4747-8550-18B3B8EDA5DC</Property>
				<Property Name="CompilerFlags" Type="Str"></Property>
				<Property Name="Console output" Type="UInt">1</Property>
				<Property Name="ConstantAllocation" Type="UInt">0</Property>
				<Property Name="ConstantDeallocation" Type="UInt">0</Property>
				<Property Name="DebugMode" Type="UInt">2</Property>
				<Property Name="DebugOptions" Type="UInt">0</Property>
				<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
				<Property Name="EmbeddedUrl" Type="Str">Embedded:RealView:EK-LM3S8962:Application</Property>
				<Property Name="Enable cross-module optimization" Type="Bool">true</Property>
				<Property Name="Enable debugging" Type="Bool">false</Property>
				<Property Name="Enable profile information" Type="Bool">false</Property>
				<Property Name="ExpressionFolding" Type="Bool">false</Property>
				<Property Name="Gateway" Type="Str">0.0.0.0</Property>
				<Property Name="Generate C Function Calls" Type="Bool">false</Property>
				<Property Name="GenerateGuardCode" Type="Bool">false</Property>
				<Property Name="GenerateIntegerOnly" Type="Bool">false</Property>
				<Property Name="GenerateSerialOnly" Type="Bool">false</Property>
				<Property Name="GenerateSerialOnlyTEMP" Type="Bool">false</Property>
				<Property Name="Host name" Type="Str">board</Property>
				<Property Name="Include paths" Type="Bin">%!#!!!!!!!)!$E!S`````Q21982I!!!;1%!!!@````]!!!V*&lt;G.M&gt;72F)("B&gt;'BT!!%!!1!!!!!!!!!!</Property>
				<Property Name="Interrupt[0].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[0].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[0].Name" Type="Str">Timer 0</Property>
				<Property Name="Interrupt[0].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[0].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[0].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[0].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[1].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[1].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[1].Name" Type="Str">Timer 1</Property>
				<Property Name="Interrupt[1].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[1].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[1].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[1].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[10].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[10].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[10].Name" Type="Str">GPIO G</Property>
				<Property Name="Interrupt[10].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[10].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[10].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[10].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[11].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[11].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[11].Name" Type="Str">PWM Generator 0</Property>
				<Property Name="Interrupt[11].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[11].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[11].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[11].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[12].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[12].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[12].Name" Type="Str">PWM Generator 1</Property>
				<Property Name="Interrupt[12].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[12].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[12].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[12].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[13].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[13].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[13].Name" Type="Str">PWM Generator 2</Property>
				<Property Name="Interrupt[13].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[13].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[13].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[13].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[14].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[14].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[14].Name" Type="Str">PWM Fault</Property>
				<Property Name="Interrupt[14].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[14].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[14].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[14].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[15].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[15].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[15].Name" Type="Str">QEI 0</Property>
				<Property Name="Interrupt[15].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[15].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[15].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[15].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[16].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[16].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[16].Name" Type="Str">QEI 1</Property>
				<Property Name="Interrupt[16].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[16].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[16].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[16].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[17].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[17].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[17].Name" Type="Str">ADC Sequence 0</Property>
				<Property Name="Interrupt[17].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[17].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[17].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[17].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[18].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[18].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[18].Name" Type="Str">ADC Sequence 1</Property>
				<Property Name="Interrupt[18].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[18].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[18].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[18].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[19].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[19].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[19].Name" Type="Str">ADC Sequence 2</Property>
				<Property Name="Interrupt[19].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[19].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[19].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[19].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[2].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[2].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[2].Name" Type="Str">Timer 2</Property>
				<Property Name="Interrupt[2].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[2].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[2].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[2].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[20].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[20].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[20].Name" Type="Str">Analog Comparator</Property>
				<Property Name="Interrupt[20].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[20].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[20].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[20].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[21].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[21].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[21].Name" Type="Str">Brownout Detection</Property>
				<Property Name="Interrupt[21].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[21].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[21].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[21].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[22].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[22].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[22].Name" Type="Str">Watchdog Timer</Property>
				<Property Name="Interrupt[22].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[22].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[22].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[22].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[3].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[3].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[3].Name" Type="Str">Timer 3</Property>
				<Property Name="Interrupt[3].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[3].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[3].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[3].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[4].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[4].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[4].Name" Type="Str">GPIO A</Property>
				<Property Name="Interrupt[4].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[4].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[4].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[4].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[5].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[5].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[5].Name" Type="Str">GPIO B</Property>
				<Property Name="Interrupt[5].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[5].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[5].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[5].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[6].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[6].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[6].Name" Type="Str">GPIO C</Property>
				<Property Name="Interrupt[6].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[6].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[6].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[6].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[7].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[7].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[7].Name" Type="Str">GPIO D</Property>
				<Property Name="Interrupt[7].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[7].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[7].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[7].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[8].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[8].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[8].Name" Type="Str">GPIO E</Property>
				<Property Name="Interrupt[8].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[8].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[8].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[8].UseThread" Type="Bool">true</Property>
				<Property Name="Interrupt[9].Configuration" Type="Str"></Property>
				<Property Name="Interrupt[9].Handler" Type="UInt">0</Property>
				<Property Name="Interrupt[9].Name" Type="Str">GPIO F</Property>
				<Property Name="Interrupt[9].Priority" Type="Int">25</Property>
				<Property Name="Interrupt[9].StartupEnabled" Type="UInt">0</Property>
				<Property Name="Interrupt[9].Used" Type="Bool">false</Property>
				<Property Name="Interrupt[9].UseThread" Type="Bool">true</Property>
				<Property Name="InterruptCount" Type="Int">23</Property>
				<Property Name="IP address" Type="Str">0.0.0.0</Property>
				<Property Name="LinkerFlags" Type="Str"></Property>
				<Property Name="MAC address" Type="Str"></Property>
				<Property Name="MAC in target memory" Type="Bool">true</Property>
				<Property Name="Mode" Type="UInt">1</Property>
				<Property Name="Optimization" Type="UInt">2</Property>
				<Property Name="Optimize for time" Type="Bool">false</Property>
				<Property Name="Optimize heap size" Type="Bool">true</Property>
				<Property Name="Preprocessor symbols (define)" Type="Str"></Property>
				<Property Name="Primary DNS" Type="Str">0.0.0.0</Property>
				<Property Name="Secondary DNS" Type="Str">0.0.0.0</Property>
				<Property Name="SourceTopLevelVI" Type="Ref">/EK-LM3S8962/seriová linka na 4 vstupy.vi</Property>
				<Property Name="Subnet mask" Type="Str">0.0.0.0</Property>
				<Property Name="Target serial port" Type="UInt">1</Property>
				<Property Name="Use DHCP" Type="Bool">true</Property>
				<Property Name="Use MicroLib" Type="Bool">false</Property>
				<Property Name="UseStackVariables" Type="Bool">false</Property>
			</Item>
			<Item Name="Application 1" Type="EmbeddedBuildSpec">
				<Property Name="DefaultBuildSpec" Type="Bool">false</Property>
				<Property Name="EmbeddedUrl" Type="Str">Embedded:RealView:EK-LM3S8962:Application</Property>
				<Property Name="SourceTopLevelVI" Type="Ref"></Property>
			</Item>
			<Item Name="Application 2" Type="EmbeddedBuildSpec">
				<Property Name="BDTFlags" Type="Str">85254210-0979-44EC-B7B7-D137D3F21674</Property>
				<Property Name="ConstantAllocation" Type="UInt">0</Property>
				<Property Name="ConstantDeallocation" Type="UInt">0</Property>
				<Property Name="DebugOptions" Type="UInt">0</Property>
				<Property Name="DefaultBuildSpec" Type="Bool">false</Property>
				<Property Name="EmbeddedUrl" Type="Str">Embedded:RealView:EK-LM3S8962:Application</Property>
				<Property Name="Enable debugging" Type="Bool">true</Property>
				<Property Name="ExpressionFolding" Type="Bool">false</Property>
				<Property Name="Generate C Function Calls" Type="Bool">false</Property>
				<Property Name="GenerateGuardCode" Type="Bool">true</Property>
				<Property Name="GenerateIntegerOnly" Type="Bool">false</Property>
				<Property Name="GenerateSerialOnly" Type="Bool">false</Property>
				<Property Name="SourceTopLevelVI" Type="Ref"></Property>
				<Property Name="UseStackVariables" Type="Bool">false</Property>
			</Item>
		</Item>
	</Item>
</Project>
