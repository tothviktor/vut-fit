/*
 * Prvá domáca úloha
 * Meno balíka: pokus
 * Meno triedy: Uloha
 */
package pokus;

/**
 *
 * @author Viktor
 */
public class Uloha {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    System.out.println("!!! Robím prvú domácu úlohu !!! ");
    System.out.print("Literál celočíselný: ");
    System.out.println(156);
    System.out.print("Literál reálny: ");
    System.out.println(20.48);
    System.out.print("Literál znakový: ");
    System.out.println('W');
    System.out.print("Literál reťazcový: ");
    System.out.println("Je 20 hodín a 34 minút.");    
    System.out.print("Literál boolovské: ");
    System.out.println(true);
    
    }
}
