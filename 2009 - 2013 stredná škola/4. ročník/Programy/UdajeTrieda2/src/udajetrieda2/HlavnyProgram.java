/*
 * Viktor Tóth
 * 4ITB
 * 8.4.2
 */
package udajetrieda2;

import java.util.Locale;
import java.util.Scanner;

public class HlavnyProgram {

    public static void main(String[] args) {
        Scanner konzolovyVstup = new Scanner(System.in);
        konzolovyVstup.useLocale(Locale.FRENCH);
        
        System.out.print("Počet žiakov v triede je:");
        int pocetZiakov = konzolovyVstup.nextInt();
        System.out.print("Počet všetkých vymeškaných hodín je:");
        int pocetVymeskanychHodin =konzolovyVstup.nextInt();
        System.out.print("Počet žiakov, ktorý prospeli s vyznamenaním:");
        int vyznamenie = konzolovyVstup.nextInt();
        System.out.print("Počet žiakov, ktorý prospeli velmy dobre:");
        int velmyDobre = konzolovyVstup.nextInt();
        System.out.print("Počet žiakov, ktorý prospeli :");
        int prospeli = konzolovyVstup.nextInt();
        System.out.print("Počet žiakov, ktorý neprospeli:");
        int neprospeli = konzolovyVstup.nextInt();
        
        System.out.println("\n\n\t\t!!!Zhrnutie!!!\n");
        System.out.println("Počet žiakov v triede je:\t\t\t"+pocetZiakov);
        System.out.println("Počet všetkých vymeškaných hodín je:\t\t"
                +pocetVymeskanychHodin);
        System.out.println("Počet žiakov, ktorý prospeli s vyznamenaním:\t"
                +vyznamenie);
        System.out.println("Počet žiakov, ktorý prospeli velmy dobre:\t"
                +velmyDobre);
        System.out.println("Počet žiakov, ktorý prospeli :\t\t\t"
                +prospeli);
        System.out.println("Počet žiakov, ktorý neprospeli:\t\t\t"
                +neprospeli);    
    }
}

