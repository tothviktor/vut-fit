/*
 * vytvoriť N prvkové pole, zaplnit ho, zavolať metodu na zistenie max hodnotu
 * max hodnotu vypísať
 */
package pole;

import java.util.Locale;
import java.util.Scanner;

/**
 * @author Viktor
 */
public class Pole {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadajte pocet prvkov v poli: ");
        int N = conIN.nextInt();
        
        int [] pole = new int [N];
        
        for(int i=0;i<N;i++)
        {
            System.out.print("Zadaj "+(i+1)+". prvok: ");
            pole [i]=conIN.nextInt();    
        }
        int max = maxHodnota.maxHodnota(pole);
        System.out.print("Najvacsie cislo v poli je: "+max+"\n");
    }
}
