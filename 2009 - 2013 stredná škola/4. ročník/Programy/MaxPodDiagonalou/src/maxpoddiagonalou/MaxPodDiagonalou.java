/*
 * program vypíše najväčšie číslo pod diagonálou
 * 20.7.1
 */
package maxpoddiagonalou;

import java.util.Locale;
import java.util.Scanner;

public class MaxPodDiagonalou {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadajte velkost dvojrozmerneho pola: ");
        int N = conIN.nextInt();
        
        int [][] pole = new int [N][N];
                
        dvojrozmernePole.vypisPola(pole);
        dvojrozmernePole.doplneniePola(pole);
        dvojrozmernePole.vypisPola(pole);
        int cislo = dvojrozmernePole.najCisloPodDiagonalou(pole);
        
        System.out.println("Najvacie cislo pod diagonalov poli je: "
                                                            +cislo);
    }
}
