/*
 * Viktor Toth
 * 4ITB
 * Sinus uhla
 * 10.2
 */
package sinusuhla;

import java.util.Locale;
import java.util.Scanner;

public class HlavnyProgram {

    public static void main(String[] args) {
        Scanner conIn = new Scanner(System.in);
        conIn.useLocale(Locale.FRENCH);
   
        System.out.print("Zadajte uhol v stupnoch: ");
        double uholvSupnoch = conIn.nextDouble();
        
        double sinusUhla = Math.sin((uholvSupnoch*(Math.PI))/180);
        System.out.println("Uhol v stupnoch je "+uholvSupnoch+
                            " a z toho sinus je "+sinusUhla);
    }
}
