/*
 * vypísanie N celých čísel, vygenerované náhodne z intervalu <0,100>
 */
package cykluswhile;

import java.util.Locale;
import java.util.Scanner;

/**
 * @author Viktor
 */
public class CyklusWhile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    Scanner conIN = new Scanner(System.in);
    conIN.useLocale(Locale.FRENCH);
    
    System.out.print("Kolko cisel ma program vypisat, ZADAJ CISLO: ");
    int N = conIN.nextInt();
    
    System.out.print("Nahodne vygenerovane cisla su: ");
    while(N>0)
    {
        long NCislo = Math.round(Math.random()*100);
        System.out.print(NCislo+" ");
        N--;
    }  
    }
}
