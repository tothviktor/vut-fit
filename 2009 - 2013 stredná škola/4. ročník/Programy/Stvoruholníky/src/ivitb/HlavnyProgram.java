// 22.5

package ivitb;

import java.util.Locale;
import java.util.Scanner;

public class HlavnyProgram {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        Stvoruholník stvorec =new Stvoruholník();
        
        System.out.print("Zadaj dlzku strany a: ");
        int a = conIN.nextInt();
        System.out.print("Zadaj dlzku strany b: ");
        int b = conIN.nextInt();
        
        stvorec.setStrany(a,b);
        
        Stvoruholník obdlznik =new Stvoruholník();
        System.out.print("Zadaj dlzku strany a: ");
        a = conIN.nextInt();
        System.out.print("Zadaj dlzku strany b: ");
        b = conIN.nextInt();
        
        obdlznik.setStrany(a,b);
        
        stvorec.getStranaA();
        stvorec.getStranaB();
        stvorec.getObvod();
        
        obdlznik.getStranaA();
        obdlznik.getStranaB();
        obdlznik.getObvod();
    }
}
