
package ivitb;

public class Stvoruholník {
    private int stranaA;
    private int stranaB;
    
    public void setStrany(int a, int b)
    {
        stranaA = a;
        stranaB = b;
    }
    public void getStranaA()
    {
        System.out.println("\nDlzka strany \'a\' je: "+stranaA);
    }
    public void getStranaB()
    {
        System.out.println("Dlzka strany \'b\' je: "+stranaB);
    }
    public void getObvod()
    {
        System.out.println("Obvod stvoruholnika je: "
                +((2*stranaA)+(2*stranaB)));
    }
}
