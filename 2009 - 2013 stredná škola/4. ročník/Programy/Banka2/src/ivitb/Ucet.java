package ivitb;

public class Ucet {
    
    protected int cisloUctu;
    protected double stavNaUcte;

    public Ucet (int cisloUctu, double stavNaUcte)
    {
        this.cisloUctu=cisloUctu;
        this.stavNaUcte=stavNaUcte;
    }
    public void setCisloUctu(int cisloUctu)
    {
        this.cisloUctu=cisloUctu;
    }
    public int getCisloUctu()
    {
        return this.cisloUctu;
    }
    public double getStavnaUcte()
    {
        return this.stavNaUcte;
    }  
}
