package ivitb;

public class BeznyUcet extends Ucet
{
    int pocetVkladov=0;
    int pocetVyberov=0;
    public BeznyUcet(int cisloUctu,double stavNaUcte,int pocetVkladov,int pocetVyberov) 
    {
        super(cisloUctu, stavNaUcte);
        this.pocetVkladov=pocetVkladov;
        this.pocetVyberov=pocetVyberov;
    }
    public void vklad(double vklad)
    {
        if(vklad>0)
        {
            this.stavNaUcte+=vklad;
            this.pocetVkladov++;
        }
    }
    public void vyber(double vyber)
    {
        if(vyber<=this.stavNaUcte)
        {
           this.stavNaUcte-=vyber;
           this.pocetVyberov++; 
        }
        else
        {
            System.out.println("Vyber je vacci nez vase zasoby.");
        }
    }
}