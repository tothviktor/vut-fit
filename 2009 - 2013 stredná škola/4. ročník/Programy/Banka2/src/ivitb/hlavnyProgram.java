/*
 * 25.9.(1,2)
 */
package ivitb;

import java.util.Locale;
import java.util.Scanner;

public class hlavnyProgram {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
      
        BeznyUcet prvy = new BeznyUcet(1, 1000, 9, 5);
        prvy.vklad(1000);
        System.out.println("Stav na ucte je: "+ prvy.getStavnaUcte());
        prvy.vyber(1500);
        System.out.println("Stav na ucte je: "+ prvy.getStavnaUcte()+"\n");
                
        SporiaciUcet druhy = new SporiaciUcet(2, 1000, 9, 1);
        druhy.vyberPo10(100);
        System.out.println("Stav na ucte je: "+ druhy.getStavnaUcte());
        druhy.vklad(500);
        System.out.println("Stav na ucte je: "+ druhy.getStavnaUcte());
        druhy.vyberPo10(100);
        System.out.println("Stav na ucte je: "+ druhy.getStavnaUcte());
                
    }
}
