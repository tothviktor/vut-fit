package ivitb;

public class SporiaciUcet extends BeznyUcet 
{
    public SporiaciUcet(int cisloUctu,double stavNaUcte,int pocetVkladov,
            int pocetVyberov)
    {
        super(cisloUctu, stavNaUcte, pocetVkladov, pocetVyberov);     
    }
    public void vyberPo10 (int vyber)
    {
        if(this.pocetVkladov>=10)
        {
           if(this.stavNaUcte>=vyber)
           {
               this.stavNaUcte-=vyber;
           }
        }
        else
        {
            System.out.println("Nemate dostatok vkladov, "
                    + "potrebujete este vlozit "+ (10-this.pocetVkladov)
                    +" vkladov na ucet.");
        }
    }
}