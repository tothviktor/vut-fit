package ivitb;

public class Student {
    private String Meno;
    private String Priezvisko;
    private String Trieda;
    private int Znamka1;
    private int Znamka2;
    private int Znamka3;
    private double Priemer;
    private int PocetVymeskanychHodin;
    
    public void setZakladneUdaje (String meno,String priezvisko, String trieda)
    {       
        Meno = meno;
        Priezvisko = priezvisko;
        Trieda = trieda;
    }
    public void setZnamky (int znamka1,int znamka2,int znamka3)
    {
        Znamka1 = znamka1;
        Znamka2 = znamka2;
        Znamka3 = znamka3;
        Priemer = (((double)znamka1+znamka2+znamka3)/3);
    }
    public void setVymeskaneHodiny(int pocetVymeskanychHodin)
    {
        PocetVymeskanychHodin = pocetVymeskanychHodin;
    } 
    
    public void getMeno()
    {
        System.out.println("Meno: "+Meno);
    }
    public void getPriezvisko()
    {
        System.out.println("Priezvisko: "+Priezvisko);
    }
    public void getTrieda()
    {
        System.out.println("Trieda: "+Trieda);
    }
    public void getZnamka1()
    {
        System.out.println("Znamka z programovania: "+Znamka1);
    }
    public void getZnamka2()
    {
        System.out.println("Znamka z matematiky: "+Znamka2);
    }
    public void getZnamka3()
    {
        System.out.println("Znamka zo slovenciny: "+Znamka3);
    }
    public void getPriemer()
    {
        System.out.println("Priemer: "+Priemer);
    }
    public void getVymeskaneHodiny()
    {
        System.out.println("Pocet vymeskanych hodin: "+PocetVymeskanychHodin);
    }
}
