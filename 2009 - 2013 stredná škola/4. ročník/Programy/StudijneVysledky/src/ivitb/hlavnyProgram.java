//22.6.3
package ivitb;

import java.util.Locale;
import java.util.Scanner;

public class hlavnyProgram {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        conIN.reset();
        
        System.out.print("Zadaj meno: ");
        String meno = conIN.nextLine();
        System.out.print("Zadaj priezvisko: ");
        String priezvisko = conIN.nextLine();
        System.out.print("Zadaj triedu: ");
        String trieda = conIN.nextLine();
        System.out.print("Zadaj znamku z programovania: ");
        int znamka1 = conIN.nextInt();
        System.out.print("Zadaj znamku z matematiky: ");
        int znamka2 = conIN.nextInt();
        System.out.print("Zadaj znamku zo slovenciny: ");
        int znamka3 = conIN.nextInt();
        System.out.print("Zadaj pocet vymeskanych hodin: ");
        int pocetVymeskanychHodin = conIN.nextInt();
        
        Student ziak = new Student();
        ziak.setZakladneUdaje(meno, priezvisko, trieda);
        ziak.setZnamky(znamka1, znamka2, znamka3);
        ziak.setVymeskaneHodiny(pocetVymeskanychHodin);
        
        ziak.getMeno();
        ziak.getPriezvisko();
        ziak.getTrieda();
        ziak.getZnamka1();
        ziak.getZnamka2();
        ziak.getZnamka3();
        ziak.getPriemer();
        ziak.getVymeskaneHodiny();  
    }
}
