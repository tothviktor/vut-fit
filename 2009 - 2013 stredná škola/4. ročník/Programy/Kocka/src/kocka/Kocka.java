/*
 * Hadzanie kockou
 */
package kocka;

import java.util.Locale;
import java.util.Scanner;

public class Kocka {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadajte pocet hodov: ");
        int N = conIN.nextInt();
        int cislo = 0;
        int jedna = 0;
        int dva = 0;
        int tri = 0;
        int styri = 0;
        int pat = 0;
        int sest = 0;
        
        for(int i=0;i<N;i++)
        {
            cislo = (int)Math.round(1+Math.random()*(5));
            
            switch(cislo)
            {
                case 1 : jedna++;
                    break;
                case 2 : dva++;
                    break;
                case 3 : tri++;
                    break;
                case 4 : styri++;
                    break;
                case 5 : pat++;
                    break;
                case 6 : sest++;
                    break;
            }
            System.out.print(cislo+" ");
        }
        System.out.println();
        System.out.println("Jednotka padla \t"+jedna+" krat, percentualne zastupenie je: "+((double)jedna/N*100)+"%");
        System.out.println("Dvojka padla \t"+dva+" krat, percentualne zastupenie je: "+((double)dva/N*100)+"%");
        System.out.println("Trojka padla \t"+tri+" krat, percentualne zastupenie je: "+((double)tri/N*100)+"%");
        System.out.println("Stvorka padla \t"+styri+" krat, percentualne zastupenie je: "+((double)styri/N*100)+"%");
        System.out.println("Patka padla \t"+pat+" krat, percentualne zastupenie je: "+((double)pat/N*100)+"%");
        System.out.println("Sestka padla \t"+sest+" krat, percentualne zastupenie je: "+((double)sest/N*100)+"%");
        
    }
}
