/*
 * Viktor Tóth
 * 4ITB
 * 7.3.3
 */
package udajeautomobil;

/**
 * @author Viktor
 */
public class HlavnyProgram {

    public static void main(String[] args) {
        
        double priemernaSpotreba = 6.5;
        int najazdeneKilometre = 200000;
        int rokVyroby = 2005;
        int vykon = 70;
        
        System.out.println("Škoda Oktávia");
        System.out.println("Priemerná spotreba:\t"+priemernaSpotreba);
        System.out.println("Najazdené kilometre:\t"+najazdeneKilometre);
        System.out.println("Rok výroby:\t\t"+rokVyroby);
        System.out.println("Výkon v KW:\t\t"+vykon);
        
    }
}
