/*
 * vstup: znak, dlzka strany
 * vystup: pravouhli trojuholnik
 */
package kreslenietrojuholnika;

import java.util.Locale;
import java.util.Scanner;

/**
 * @author Viktor
 */
public class KreslenieTrojuholnika {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadaj znak:");
        char znak = conIN.nextLine().charAt(0);
        System.out.print("Zadaj dlzku strany trojuholnika: ");
        int dlzka = conIN.nextInt();
        
        for(int i=1;i<=dlzka;i++)
        {
            for(int j=1;j<=i;j++)
            {
                System.out.print(znak);
            }
            System.out.println();
        }
    }
}
