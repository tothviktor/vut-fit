//TRIEDA Cisla
package ivitb;

public class Cisla implements Vypocty{

    double cislo1;
    double cislo2;
    double vysledok =0;

    public Cisla(double cislo1, double cislo2) {
        this.cislo1 = cislo1;
        this.cislo2 = cislo2;
    }
    
     @Override
    public double sucet() {
        return this.cislo1+this.cislo2;
    }

    @Override
    public double rozdiel() {
        return this.cislo1-this.cislo2;
    }

    @Override
    public double sucin() {
        return this.cislo1*this.cislo2;
    }

    @Override
    public double podiel() {
        return this.cislo1/this.cislo2;
    }
}
