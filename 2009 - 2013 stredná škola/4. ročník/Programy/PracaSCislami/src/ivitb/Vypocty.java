// INTERFACE Vypocty
package ivitb;

public interface Vypocty {
    public double sucet();
    public double rozdiel();
    public double sucin();
    public double podiel();
}
