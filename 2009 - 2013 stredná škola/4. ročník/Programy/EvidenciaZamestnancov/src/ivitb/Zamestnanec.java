//trieda Zamestnanec 
//podtrieda PracovneUdaje
package ivitb;

public class Zamestnanec{
    private String meno;
    private int vek;

    public Zamestnanec() {
    }

    public Zamestnanec(String meno, int vek) {
        this.meno = meno;
        this.vek = vek;
    }

    public String getMeno() {
        return meno;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public int getVek() {
        return vek;
    }

    public void setVek(int vek) {
        this.vek = vek;
    }
    
    public class PracovneUdaje extends Zamestnanec{
        int pocetRokovVPraxe;
        double vyskaMzdy;

        public PracovneUdaje() {
        }

        public PracovneUdaje(int pocetRokovVPraxe, double vyskaMzdy) {
            this.pocetRokovVPraxe = pocetRokovVPraxe;
            this.vyskaMzdy = vyskaMzdy;
        }

        public int getPocetRokovVPraxe() {
            return pocetRokovVPraxe;
        }

        public void setPocetRokovVPraxe(int pocetRokovVPraxe) {
            this.pocetRokovVPraxe = pocetRokovVPraxe;
        }

        public double getVyskaMzdy() {
            return vyskaMzdy;
        }

        public void setVyskaMzdy(double vyskaMzdy) {
            this.vyskaMzdy = vyskaMzdy;
        }
    }
}
