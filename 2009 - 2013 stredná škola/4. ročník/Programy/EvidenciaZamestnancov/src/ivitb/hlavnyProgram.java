//27.3.2
package ivitb;

public class hlavnyProgram {

    public static void main(String[] args) {
        Zamestnanec prvy =new Zamestnanec();
        
        Zamestnanec.PracovneUdaje druhy = prvy.new PracovneUdaje();       
        druhy.setMeno("Laci");
        druhy.setVek(30);
        druhy.setPocetRokovVPraxe(5);
        druhy.setVyskaMzdy(1000);
        
        Zamestnanec.PracovneUdaje treti = prvy.new PracovneUdaje();
        treti.setMeno("Vladimir");
        treti.setVek(33);
        treti.setPocetRokovVPraxe(2);
        treti.setVyskaMzdy(800);
        
        System.out.println(druhy.getMeno());
        System.out.println(druhy.getVek());
        System.out.println(druhy.getPocetRokovVPraxe());
        System.out.println(druhy.getVyskaMzdy());
        
        System.out.println(treti.getMeno());
        System.out.println(treti.getVek());
        System.out.println(treti.getPocetRokovVPraxe());
        System.out.println(treti.getVyskaMzdy());
    }
}
