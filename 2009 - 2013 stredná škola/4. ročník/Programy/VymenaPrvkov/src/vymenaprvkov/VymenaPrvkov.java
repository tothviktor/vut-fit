/*
 * 19.10.5
 */
package vymenaprvkov;

import java.util.Scanner;

/**
 * @author Viktor
 */
public class VymenaPrvkov {

    public static void main(String[] args) {
        Scanner conIN=new Scanner(System.in);
        
        System.out.print("Zadajte pocet prvkov: ");
        int N = conIN.nextInt();
        System.out.print("Zadajte zaciatok intervalu: ");
        int a = conIN.nextInt();
        System.out.print("Zadajte koniec intervalu: ");
        int b = conIN.nextInt();
        
        int [] pole = new int [N];
        
        for(int i=0;i<N;i++)
        {
            pole[i] = (int)(a+Math.random()*(b-a+1));
            System.out.print(pole[i]+" ");
        }
        
        int [] newpole = new int [N];
        newpole = vymena.vymena(pole);
        
        System.out.println("\nToto je vypis pola: ");
        for(int i=0;i<N;i++)
        {
            System.out.print(newpole[i]+" ");
        }
        System.out.println();
    }
}
