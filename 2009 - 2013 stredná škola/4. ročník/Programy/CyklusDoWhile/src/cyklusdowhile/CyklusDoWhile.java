/*
 * bezi cyklus ky nezadame slovo konic
 */
package cyklusdowhile;

import java.util.Locale;
import java.util.Scanner;

/**
 * @author Viktor
 */
public class CyklusDoWhile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ConIN = new Scanner(System.in);
        ConIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadaj slovo:");
        String slovo;
        do
        {
            slovo = ConIN.nextLine();
        }
        while(!(slovo.equals("koniec"))&&!(slovo.equals("end")));
    }
}
