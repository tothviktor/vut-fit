//Hlavna trieda SlnecnaSustava
//Podtrieda Zem
package ivitb;

public class SlnecnaSustava {
    int pocetHviezd;
    int pocetPlanet;

    public SlnecnaSustava() {
    }

    public SlnecnaSustava(int pocetHviezd, int pocetPlanet) {
        this.pocetHviezd = pocetHviezd;
        this.pocetPlanet = pocetPlanet;
    }

    public int getPocetHviezd() {
        return pocetHviezd;
    }

    public void setPocetHviezd(int pocetHviezd) {
        this.pocetHviezd = pocetHviezd;
    }

    public int getPocetPlanet() {
        return pocetPlanet;
    }

    public void setPocetPlanet(int pocetPlanet) {
        this.pocetPlanet = pocetPlanet;
    }
    
    
    
    public class Zem{
        int pocetKontinentov;
        int pocetOceanov;

        public Zem() {
        }

        public Zem(int pocetKontinentov, int pocetOceanov) {
            this.pocetKontinentov = pocetKontinentov;
            this.pocetOceanov = pocetOceanov;
        }

        public int getPocetKontinentov() {
            return pocetKontinentov;
        }

        public void setPocetKontinentov(int pocetKontinentov) {
            this.pocetKontinentov = pocetKontinentov;
        }

        public int getPocetOceanov() {
            return pocetOceanov;
        }

        public void setPocetOceanov(int pocetOceanov) {
            this.pocetOceanov = pocetOceanov;
        }                
    }
}
