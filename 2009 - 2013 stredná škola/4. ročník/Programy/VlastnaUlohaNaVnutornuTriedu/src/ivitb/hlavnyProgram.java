// 27.3.3
package ivitb;

public class hlavnyProgram {
    
    public static void main(String[] args) {
        SlnecnaSustava slnecnaSustava = new SlnecnaSustava();
        slnecnaSustava.setPocetHviezd(1);
        slnecnaSustava.setPocetPlanet(8);
        SlnecnaSustava.Zem zem = slnecnaSustava.new Zem();
        zem.setPocetKontinentov(7);
        zem.setPocetOceanov(3);
        
        System.out.println(slnecnaSustava.getPocetHviezd());
        System.out.println(slnecnaSustava.getPocetPlanet());
        System.out.println(zem.getPocetKontinentov());
        System.out.println(zem.getPocetOceanov());
    }
}
