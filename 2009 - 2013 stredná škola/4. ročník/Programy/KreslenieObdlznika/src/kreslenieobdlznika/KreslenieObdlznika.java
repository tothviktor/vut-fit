/*
 * vstup: znak,sirka,vyska
 * vystup: obdlznik
 */
package kreslenieobdlznika;

import java.util.Locale;
import java.util.Scanner;

/**
 * @author Viktor
 */
public class KreslenieObdlznika {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadaj znak: ");
        char znak = conIN.nextLine().charAt(0);
        System.out.print("Zadaj sirku: ");
        int sirka = conIN.nextInt();
        System.out.print("Zadaj vysku: ");
        int vyska = conIN.nextInt();
        
        for(int i=0;i<vyska;i++)
        {
            for(int j=0;j<sirka;j++)
            {
                System.out.print(znak);
            }
            System.out.println();
        }        
    }
}
