/*
 * Faktorial
 */
package faktorial;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author Viktor
 */
public class Faktorial {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadaj faktorial: ");
        int N = conIN.nextInt();
        
        int cislo=N;
        for(int i=1;i<N;i++)
        {
            cislo=cislo*(N-i);
        }
        System.out.println(cislo);
    }
}
