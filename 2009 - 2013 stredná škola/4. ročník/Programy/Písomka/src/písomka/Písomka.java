/*
 * načítať 5 čísel, potom zistiť najväčšie, 
 * sraviť z nich priemer zaokruhlený na 2 des miesta a vypísať
 */
package písomka;

import java.util.Locale;
import java.util.Scanner;

/**
 * @author Viktor
 */
public class Písomka {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.US);
        
        System.out.print("Zadaj prve cislo: ");
        double a = conIN.nextDouble();
        System.out.print("Zadaj druhe cislo: ");
        double b = conIN.nextDouble();
        System.out.print("Zadaj tretie cislo: ");
        double c = conIN.nextDouble();
        System.out.print("Zadaj stvrte cislo: ");
        double d = conIN.nextDouble();
        System.out.print("Zadaj piate cislo: ");
        double e = conIN.nextDouble();
        
        double max = Math.max(a,Math.max(b,Math.max(c,Math.max(d,e ))));
        double priemer= Math.round(((a+b+c+d+e)/5)*100.00)/100.00;
        
        System.out.println("Najvacsie cislo je: "+max);
        System.out.println("Priemer zo vsetkych cisel je: "+priemer);
    }
}
