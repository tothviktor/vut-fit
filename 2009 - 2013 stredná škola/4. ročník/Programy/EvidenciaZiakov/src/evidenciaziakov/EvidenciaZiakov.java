/*
 * Evidencia Ziaka
 * 21.6.1
 */
package evidenciaziakov;

public class EvidenciaZiakov {

    public static void main(String[] args) {
        Osoba jedna = new Osoba();
        jedna.meno = "Viktor";
        jedna.priezvisko = "Toth";
        jedna.datumNarodenia = 1994;
        jedna.rok = 2012-(jedna.datumNarodenia);
        jedna.vyska = 189;
        
        Osoba dva = new Osoba();
        dva.meno = "Peter";
        dva.priezvisko = "Alaxa";
        dva.datumNarodenia = 1993;
        dva.rok = 2012-(jedna.datumNarodenia);
        dva.vyska = 180;
        
        Osoba tri = new Osoba();
        tri.meno = "Stefan";
        tri.priezvisko = "Mrkvicka";
        tri.datumNarodenia = 1990;
        tri.rok = 2012-(jedna.datumNarodenia);
        tri.vyska = 183;
        
        System.out.println("OSOBA JEDNA");
        System.out.println("Meno: "+jedna.meno);
        System.out.println("Priezvisko: "+jedna.priezvisko);
        System.out.println("Datum narodenia: "+jedna.datumNarodenia);
        System.out.println("Vek: "+jedna.rok);
        System.out.println("Vyska: "+jedna.vyska);
        
        System.out.println("\nOSOBA DVA");
        System.out.println("Meno: "+dva.meno);
        System.out.println("Priezvisko: "+dva.priezvisko);
        System.out.println("Datum narodenia: "+dva.datumNarodenia);
        System.out.println("Vek: "+dva.rok);
        System.out.println("Vyska: "+dva.vyska);
        
        System.out.println("\nOSOBA TRI");
        System.out.println("Meno: "+tri.meno);
        System.out.println("Priezvisko: "+tri.priezvisko);
        System.out.println("Datum narodenia: "+tri.datumNarodenia);
        System.out.println("Vek: "+tri.rok);
        System.out.println("Vyska: "+tri.vyska);
    }
}