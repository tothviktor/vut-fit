/*
 * Aplikácia nech zobrazí na aprvom riadku Vaše meno a priezvisko, na 
 * druhom riadku triedu na treťom riadku počet vymeškaných hodín a na 
 * ďalších riadkoch známky z jednotlivých predmetov.
 * (použivajte skratky predmetov - VYT, MAT atď.)
 */
package vysledkystudia;

/**
 * @author Viktor
 */
public class HlavnyProgram {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Meno Priezvisko:\tViktor Tóth");
        System.out.println("Pocet vymeškaných hodín:" +0);
        System.out.println("MAT\t"+1);
        System.out.println("SJL\t"+3);
        System.out.println("ANJ\t"+4);
        System.out.println("NEJ\t"+1);
        System.out.println("PRO\t"+1);
        System.out.println("SIT\t"+1);
        System.out.println("SXT\t"+2);
        System.out.println("ELE\t"+1);
        System.out.println("PCA\t"+1);
        System.out.println("TEV\t"+1);

    }
}
