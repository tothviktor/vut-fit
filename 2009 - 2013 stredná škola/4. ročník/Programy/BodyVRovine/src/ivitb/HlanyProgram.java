//22.6.1
package ivitb;

import java.util.Locale;
import java.util.Scanner;

public class HlanyProgram {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        int x;
        int y;
        
        System.out.print("Zadaj suradnicu x: ");
        x = conIN.nextInt();
        System.out.print("Zadaj suradnicu y: ");
        y = conIN.nextInt();
        
        Bod prvy = new Bod();
        prvy.serSuradnice(x, y);
        
        System.out.print("Zadaj suradnicu x: ");
        x = conIN.nextInt();
        System.out.print("Zadaj suradnicu y: ");
        y = conIN.nextInt();
        
        Bod druhy = new Bod();
        druhy.serSuradnice(x, y);
        
        System.out.print("Zadaj suradnicu x: ");
        x = conIN.nextInt();
        System.out.print("Zadaj suradnicu y: ");
        y = conIN.nextInt();
        
        Bod treti = new Bod();
        treti.serSuradnice(x, y);
        
        prvy.getRozdiel();
        druhy.getRozdiel();
        treti.getRozdiel();
    }
}
