/*
 * podprogram  - trojuholnik
 */
package ivitb;

public class Trojuholnik {
    double stranaA;
    double stranaB;
    double stranaC;

    public Trojuholnik(double stranaA,double stranaB,double stranaC)
    {
        if(stranaA+stranaB>stranaC&&stranaA+stranaC>stranaB&&stranaB+stranaC>stranaA)
        {
            this.stranaA=stranaA;
            this.stranaB=stranaB;
            this.stranaC=stranaC;
        }
        else
        {
            this.stranaA=0;
            this.stranaB=0;
            this.stranaC=0;
        }
    }
    void setStrany(double stranaA, double stranaB, double stranaC)
    {
        this.stranaA=stranaA;
        this.stranaB=stranaB;
        this.stranaC=stranaC;
    }
    double getStranaA()
    {
        return this.stranaA;
    }
    double getStranaB()
    {
        return this.stranaB;
    }
    double getStranaC()
    {
        return this.stranaC;
    }
    
    
}
