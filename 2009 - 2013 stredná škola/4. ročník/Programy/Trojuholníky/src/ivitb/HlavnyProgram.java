/*
 * 23.7.1
 */
package ivitb;

import java.util.Locale;
import java.util.Scanner;

public class HlavnyProgram {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadajte pocet trojuholnikov: ");
        int N = conIN.nextInt();
        
        Trojuholnik trojuholnik[] = new Trojuholnik[N];
        
        for(int i=0;i<N;i++)
        {
            System.out.print("Zadaj stranu A "+(i+1)+". trojuholnika: ");
            double A = conIN.nextDouble();
            System.out.print("Zadaj stranu B "+(i+1)+". trojuholnika: ");
            double B = conIN.nextDouble();
            System.out.print("Zadaj stranu C "+(i+1)+". trojuholnika: ");
            double C = conIN.nextDouble(); 
            
            trojuholnik[i]= new Trojuholnik(A, B, C);
        }

        for(int i=0;i<N;i++)
        {
            System.out.println("\n"+(i+1)+". trojuholnik");
            System.out.println("Dlzka strany A:"+trojuholnik[i].getStranaA());
            System.out.println("Dlzka strany B:"+trojuholnik[i].getStranaB());
            System.out.println("Dlzka strany C:"+trojuholnik[i].getStranaC());
        }        
    }
}
