//20.7.4

package dynamickepoleretezcov;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class DynamickePoleRetezcov {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        int i=0;
        System.out.print("Zadaj 1. retazec: ");
        String retazec = conIN.nextLine();
        ArrayList<Object> pole = new ArrayList<>();
        pole.add(retazec);
        
        do
        {
            System.out.print("Zadaj "+(i+2)+". retazec: ");
            retazec = conIN.nextLine();
            if(retazec.length()>(pole.get(i).toString().length()))
            {
                i++;
                pole.add(retazec);
            }
            else
            {
                break;
            }
        }
        while(true);
        
        for(int j=0;j<=i;j++)
        {
            System.out.println(pole.get(j).toString());
        }
        System.out.println("Pocet retazcov je: "+(i+1));
    }
}
