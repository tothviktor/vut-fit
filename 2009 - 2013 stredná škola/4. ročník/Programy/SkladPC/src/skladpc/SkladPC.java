/*
 * Sklad PC
 * 21.6.2
 */
package skladpc;

public class SkladPC {

    public static void main(String[] args) {
        Evidencia pc1 = new Evidencia();
        pc1.Znacka = "Lenovo";
        pc1.frekPro_Ghz = 2;
        pc1.kapacitaHDD_GB = 750;
        pc1.kapacitaRAM_GB = 4;
        pc1.rychlostSietKarty_Mbps = 100;
        
        Evidencia pc2 = new Evidencia();
        pc2.Znacka = "ASUS";
        pc2.frekPro_Ghz = 3;
        pc2.kapacitaHDD_GB = 500;
        pc2.kapacitaRAM_GB = 3;
        pc2.rychlostSietKarty_Mbps = 1000;
        
        Evidencia pc3 = new Evidencia();
        pc3.Znacka = "TOSHIBA";
        pc3.frekPro_Ghz = 3;
        pc3.kapacitaHDD_GB = 1000;
        pc3.kapacitaRAM_GB = 8;
        pc3.rychlostSietKarty_Mbps = 1000;
        
        System.out.println("PC1");
        System.out.println("Znacka: "+pc1.Znacka);
        System.out.println("Frekvencia procesora:"+pc1.frekPro_Ghz+" Ghz");
        System.out.println("Kapacita RAM pamete:"+pc1.kapacitaRAM_GB+"GB");
        System.out.println("Kapacita HDD"+pc1.kapacitaHDD_GB+"GB");
        System.out.println("Rychlost sietovej karty"+
                pc1.rychlostSietKarty_Mbps+"Mbps");
        
        System.out.println("PC2");
        System.out.println("Znacka: "+pc2.Znacka);
        System.out.println("Frekvencia procesora:"+pc2.frekPro_Ghz+" Ghz");
        System.out.println("Kapacita RAM pamete:"+pc2.kapacitaRAM_GB+"GB");
        System.out.println("Kapacita HDD"+pc2.kapacitaHDD_GB+"GB");
        System.out.println("Rychlost sietovej karty"+
                pc2.rychlostSietKarty_Mbps+"Mbps");
               
        System.out.println("PC3");
        System.out.println("Znacka: "+pc3.Znacka);
        System.out.println("Frekvencia procesora:"+pc3.frekPro_Ghz+" Ghz");
        System.out.println("Kapacita RAM pamete:"+pc3.kapacitaRAM_GB+"GB");
        System.out.println("Kapacita HDD"+pc3.kapacitaHDD_GB+"GB");
        System.out.println("Rychlost sietovej karty"+
                pc3.rychlostSietKarty_Mbps+"Mbps");
    }
}

