// 20.7.2
package polenahodnychhodnot;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class PoleNahodnychHodnot {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadaj zaciatok intervalu: ");
        int a = conIN.nextInt();
        System.out.print("Zadaj koniec intervalu: ");
        int b = conIN.nextInt();
        
        int P=0;
        do
        {
            System.out.print("Zadaj hladane cislo z intervalu: ");
            P = conIN.nextInt();
        }
        while(P<a || P>b);
        
        ArrayList<Integer> pole = new ArrayList<>();
        
        int i = 0;
        int cislo = 0;
        System.out.print("V dynamickom poli nachadzaju sa tieto cisla: ");
        do
        {
            cislo = (int)(a+Math.random()*(b-a+1));
            pole.add(cislo);
            System.out.print(cislo+" ");
            i++;
        }
        while(cislo!=P);
        
        System.out.println("\nPocet prvkov v poli je: "+pole.size());
    }
}
