/*
 * Pad telesa
 * Viktor Toth
 * 4ITB
 * 10.4.2
 */
package padtelesa;

import java.util.Locale;
import java.util.Scanner;

public class HlavnyProgram {

    public static void main(String[] args) {
        Scanner conIn = new Scanner(System.in);
        conIn.useLocale(Locale.FRENCH);
        
        final double zrychlenie = 9.81;
        System.out.print("Zadajte vysku: ");
        double vyska = conIn.nextDouble();
        double cas = Math.sqrt(vyska/zrychlenie);
        System.out.println("Z vysky "+vyska+"teleso spadne za "+cas+" sekund.");
      }
}
