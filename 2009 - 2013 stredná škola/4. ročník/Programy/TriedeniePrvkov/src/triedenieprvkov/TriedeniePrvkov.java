/*
 * 19.10.6
 */
package triedenieprvkov;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Viktor
 */
public class TriedeniePrvkov {

    public static void main(String[] args) {
        Scanner conIN=new Scanner(System.in);
        
        System.out.print("Zadajte pocet prvkov: ");
        int N = conIN.nextInt();
        System.out.print("Zadajte zaciatok intervalu: ");
        int a = conIN.nextInt();
        System.out.print("Zadajte koniec intervalu: ");
        int b = conIN.nextInt();
        
        int [] pole = new int [N];
        int [] pole1 = new int [N];
        //int [] pole1 = new int [N];
        
        for(int i=0;i<N;i++)
        {
            pole[i] = (int)(a+Math.random()*(b-a+1));
            pole1[i] = pole[i];
            System.out.print(pole[i]); 
        }
        
        Arrays.sort(pole);
        System.out.print("\n");
        for(int i=0;i<N;i++)
        {
            System.out.print(pole[i]); 
        }
  
        System.out.print("\n");
        for(int i=0;i<N;i++)
        {
            System.out.print(pole1[i]); 
        }
    }
}
