/*
 * Viktor Tóth
 * 4ITB
 * Objem Gule
 * 9.7.6
 */
package objemgule;

import java.util.Locale;
import java.util.Scanner;

public class HlavnyProgram {

    public static void main(String[] args) {
        Scanner conIn = new Scanner(System.in);
        conIn.useLocale(Locale.FRENCH);
        final double PI = 3.14159;
        System.out.print("Zadajte polomer gule: ");
        double polomer = conIn.nextDouble();
        double Objem = 4*PI*(Math.pow(polomer, 3) );
        System.out.println("Objem gule s polomerom "+polomer+" je: "+Objem);  
    }
}
