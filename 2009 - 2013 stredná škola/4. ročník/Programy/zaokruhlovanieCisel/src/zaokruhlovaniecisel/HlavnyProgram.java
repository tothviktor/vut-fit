/*
 * Zaokruhlovani cisel
 * Viktor Toth
 * 4ITB
 * 10.3
 */
package zaokruhlovaniecisel;

import java.util.Locale;
import java.util.Scanner;

public class HlavnyProgram {

    public static void main(String[] args) {
        Scanner conIn = new Scanner(System.in);
        conIn.useLocale(Locale.FRENCH);
        
        System.out.print("Zadajte jedno desatinne cislo: " );
        double cislo = conIn.nextDouble();
        System.out.print("Pocet desatinnych miest:");
        int zaokruhlenie = conIn.nextInt();
        
        double zaokruhleneCislo= 
        Math.round(cislo*Math.pow(10,zaokruhlenie))/
                         Math.pow(10,zaokruhlenie);
        
        System.out.println("Cislo "+cislo+" zaokruhlene na "+zaokruhlenie+
                " desatinnych miest je: "+zaokruhleneCislo);
    }
}
