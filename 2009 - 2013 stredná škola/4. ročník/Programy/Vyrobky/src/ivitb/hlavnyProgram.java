//22.6.2
package ivitb;

import java.util.Locale;
import java.util.Scanner;

public class hlavnyProgram {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        Vyrobok jedna = new Vyrobok();
        
        System.out.print("Zadaj nazov vyrobku: ");
        String nazov = conIN.nextLine();        
        System.out.print("Zadaj pocet kusov: ");
        int pocet = conIN.nextInt();
        System.out.print("Zadaj cenu vyrobku: ");
        double cena = conIN.nextDouble();
        
        jedna.setVyrobok(nazov, pocet, cena);
        
        Vyrobok dva = new Vyrobok();
        
        conIN.nextLine();
        System.out.print("Zadaj nazov vyrobku: ");
        nazov = conIN.nextLine();        
        System.out.print("Zadaj pocet kusov: ");
        pocet = conIN.nextInt();
        System.out.print("Zadaj cenu vyrobku: ");
        cena = conIN.nextDouble();
        
        dva.setVyrobok(nazov, pocet, cena);
        
        Vyrobok tri = new Vyrobok();

        System.out.print("Zadaj nazov vyrobku: ");
        conIN.nextLine();
        nazov = conIN.nextLine();        
        System.out.print("Zadaj pocet kusov: ");
        pocet = conIN.nextInt();
        System.out.print("Zadaj cenu vyrobku: ");
        cena = conIN.nextDouble();
        
        tri.setVyrobok(nazov, pocet, cena);
        
        Vyrobok styri = new Vyrobok();
        conIN.nextLine();
        System.out.print("Zadaj nazov vyrobku: ");
        nazov = conIN.nextLine();        
        System.out.print("Zadaj pocet kusov: ");
        pocet = conIN.nextInt();
        System.out.print("Zadaj cenu vyrobku: ");
        cena = conIN.nextDouble();
        
        styri.setVyrobok(nazov, pocet, cena);
        
        Vyrobok pat = new Vyrobok();
        conIN.nextLine();
        System.out.print("Zadaj nazov vyrobku: ");
        nazov = conIN.nextLine();        
        System.out.print("Zadaj pocet kusov: ");
        pocet = conIN.nextInt();
        System.out.print("Zadaj cenu vyrobku: ");
        cena = conIN.nextDouble();
        
        pat.setVyrobok(nazov, pocet, cena);
        
        System.out.print("Vyrobok jedna\n");
        jedna.getOProdokte();
        System.out.print("Vyrobok dva\n");
        dva.getOProdokte();
        System.out.print("Vyrobok tri\n");
        tri.getOProdokte();
        System.out.print("Vyrobok stiry\n");
        styri.getOProdokte();
        System.out.print("Vyrobok pat\n");
        pat.getOProdokte();
    }
}
  