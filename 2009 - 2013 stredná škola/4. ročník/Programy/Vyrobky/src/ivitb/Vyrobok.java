package ivitb;

public class Vyrobok {
    private String nazovVyrobku;
    private int pocetKusov;
    private double cenaVyrobku;
    
    public void setVyrobok (String nazov, int pocet, double cena)
    {
        nazovVyrobku= nazov;
        pocetKusov = pocet;
        cenaVyrobku = cena;    
    }
    
    public void getOProdokte()
    {
        System.out.println("Nazov:\t\t"+nazovVyrobku+"\n"
                + "Pocet kusov:\t"+pocetKusov+"\n"
                + "Cena:\t\t"+cenaVyrobku+"\n");
    }
}
