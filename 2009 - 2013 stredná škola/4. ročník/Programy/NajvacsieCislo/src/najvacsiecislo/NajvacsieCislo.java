/*
 * Aplikácia vypíše najvacsie cislo z piatich cisel
 */
package najvacsiecislo;

import java.util.Locale;
import java.util.Scanner;

/**
 * @author Viktor
 */
public class NajvacsieCislo {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        System.out.print("Zadajte prve cislo: ");
        int cislo1 = conIN.nextInt();
        System.out.print("Zadajte druhe cislo: ");
        int cislo2 = conIN.nextInt();
        System.out.print("Zadajte tretie cislo: ");
        int cislo3 = conIN.nextInt();
        System.out.print("Zadajte stvrte cislo: ");
        int cislo4 = conIN.nextInt();
        System.out.print("Zadajte piate cislo: ");
        int cislo5 = conIN.nextInt();
        
        int max = Math.max(cislo1,Math.max(cislo2,Math.max(cislo3,
                  Math.max(cislo4,cislo5))));
        
        System.out.println("Najvacsie cislo zo vsetkych cisel je cislo: "
                +max);  
    }
}
