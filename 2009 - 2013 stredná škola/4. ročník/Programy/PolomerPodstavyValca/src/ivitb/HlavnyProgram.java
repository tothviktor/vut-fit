//28.8.1
package ivitb;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class HlavnyProgram {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        //Valec_1
        try
        {
        double objemValca = conIN.nextDouble();
        double vyskaValca = conIN.nextDouble();
        
        RozmeryValca valec1 = new RozmeryValca(objemValca, vyskaValca);
        
        System.out.println("Objem valca: "+valec1.getObjemValca());
        System.out.println("Vyska valca: "+valec1.getVyskaValca());
        System.out.println("Polomer postavy valca: "+valec1.getPolomerPodstavyValca());
        }
        catch(InputMismatchException vynimkaRetazec)
        {
            System.out.println("Nebolo zadane cislo!");
        }
        
        //Valec_2
        try
        {
        double objemValca = conIN.nextDouble();
        double vyskaValca = conIN.nextDouble();
        
        RozmeryValca valec2 = new RozmeryValca(objemValca, vyskaValca);
        
        System.out.println("Objem valca: "+valec2.getObjemValca());
        System.out.println("Vyska valca: "+valec2.getVyskaValca());
        System.out.println("Polomer postavy valca: "+valec2.getPolomerPodstavyValca());
        }
        catch(InputMismatchException vynimkaRetazec)
        {
            System.out.println("Nebolo zadane cislo!");
        }  
    }
}
