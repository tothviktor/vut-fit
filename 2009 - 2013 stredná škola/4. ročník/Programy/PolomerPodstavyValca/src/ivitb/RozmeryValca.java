//Rozmery Valca
package ivitb;

public class RozmeryValca {
    double objemValca;
    double vyskaValca;

    public RozmeryValca() {
    }

    public RozmeryValca(double objemValca, double vyskaValca) {
        this.objemValca = objemValca;
        this.vyskaValca = vyskaValca;
    }

    public double getObjemValca() {
        return objemValca;
    }

    public void setObjemValca(double objemValca) {
        this.objemValca = objemValca;
    }

    public double getVyskaValca() {
        return vyskaValca;
    }

    public void setVyskaValca(double vyskaValca) {
        this.vyskaValca = vyskaValca;
    }
    
    public double getPolomerPodstavyValca(){
        return Math.sqrt((this.objemValca)/(this.vyskaValca*Math.PI));
    }
}
