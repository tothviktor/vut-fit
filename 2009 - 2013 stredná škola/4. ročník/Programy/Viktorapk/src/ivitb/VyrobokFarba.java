//Podtrieda
package ivitb;

public class VyrobokFarba extends VyrobokZnacka implements Cena{

    String farba;
    double cena;
    
    void setFarba(String farbaVyrobku)
    {
        this.farba=farbaVyrobku;
    }

    @Override
    public void cena(double cena) {
        this.cena = cena;
    }
    
    void getVypisVsetkychINFO()
    {
        System.out.println("Znacka vyrobku: "+this.Znacka);
        System.out.println("Farba vyrobku: "+this.farba);
        System.out.println("Cena vyrobku: "+this.cena);
    }
}
