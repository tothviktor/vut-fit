/*
 * Podprogram - Ucet
 */
package ivitb;

public class Ucet {
    int cisloUctu;
    double stavNaUcte;
    static int pocetUctov;
    static double celkovaHodnotaUctov;

    public Ucet(int cisloUctu, double pociatocnyVklad)
    {
        this.cisloUctu = cisloUctu;
        if(pociatocnyVklad>0)
        {
            this.stavNaUcte = pociatocnyVklad;
            pocetUctov++;
            celkovaHodnotaUctov+=pociatocnyVklad;
        }
        else
        {
            this.stavNaUcte = 0;
        }
    }
    void setVklad(double vklad)
    {
          if(vklad>0)
          {
              this.stavNaUcte=this.stavNaUcte+vklad;
              celkovaHodnotaUctov+=vklad;
              System.out.print("Na ucet ste pridali "+vklad+"€" );
          }
    }
    void setVyber(double vyber)
    {
        if(vyber>0&&vyber<=this.stavNaUcte)
        {
            this.stavNaUcte=this.stavNaUcte-vyber;
            celkovaHodnotaUctov-=vyber;
            System.out.print("Z uctu ste vybrali "+ vyber+ "€");
        }
    }
    double getStavNaUcte()
    {
        return this.stavNaUcte;
    }
    static private int getpocetUctov()
    {
        return pocetUctov;
    }
    static private double getcelkovaHodnotaUctov()
    {
        return celkovaHodnotaUctov;
    }
    void zrusenieUctu()
    {
        celkovaHodnotaUctov-= this.stavNaUcte;
        this.cisloUctu = 0;
        this.stavNaUcte =0;
        pocetUctov--;
        System.out.print("Vas ucet bol uspesne zmazany!");
    }
}
