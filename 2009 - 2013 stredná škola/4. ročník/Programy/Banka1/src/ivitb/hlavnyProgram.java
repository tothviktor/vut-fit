/*
 * 24.3 -doplnenie programu 24.2
 */
package ivitb;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class hlavnyProgram {

    public static void main(String[] args) {
        Scanner conIN = new Scanner(System.in);
        conIN.useLocale(Locale.FRENCH);
        
        int moznost=0;
        double suma;
        int cisloUctu = 1;
        int hladanyUcet;
        
        ArrayList<Ucet> ucet =new ArrayList<Ucet>();
        
        System.out.println("Novy ucet\t\t\t -> 1");
        System.out.println("Odstranenie konkretneho uctu\t -> 2");
        System.out.println("Vklad na konkretny ucet\t\t -> 3");
        System.out.println("Vyber z konkretneho uctu\t -> 4");
        System.out.println("Informacie o konkretnom ucte\t -> 5");
        System.out.println("Informacie o celkovy stav\t -> 6");
        System.out.println("Koniec\t\t\t\t -> 7");
        
        while(moznost!=7)
        {
            System.out.print("Zadaj jedno z uvedenych hodnot (1-7): ");
            moznost = conIN.nextInt();
            if(moznost<8&&moznost>0)
            {
                switch(moznost)
                {
                    case 1: 
                        System.out.println("Zadajte pociatocny vklad: ");
                        suma= conIN.nextDouble();

                        if(suma>=0)
                        {
                            ucet.add(new Ucet(cisloUctu, suma));
                            System.out.println("Vas ucet bol uspesne vytvoreny: "
                                    + "\nCislo uctu je: "+(cisloUctu)
                                    + "\nVklad na ucte je: "+suma);
                            cisloUctu++;
                        }

                        else
                        {
                            System.out.println("Zadali ste ZAPORNU hodnotu!");
                        }
                        break;

                    case 2: 
                        System.out.println("Zadajte cislo uctu: ");
                        hladanyUcet= conIN.nextInt();

                        if(hladanyUcet>0&&hladanyUcet<(cisloUctu-1))
                        {
                            ucet.remove(hladanyUcet);
                            System.out.println("Vas ucet s cislom "+hladanyUcet
                                    +" bol uspesne zmazany!");
                        }

                        else
                        {
                            System.out.println("Zadane cislo uctu NEEXISTUJE!");
                        }
                        break;

                    case 3:
                        System.out.println("Zadajte cislo uctu: ");
                        hladanyUcet = conIN.nextInt();
                        if(hladanyUcet>0&&hladanyUcet<(cisloUctu-1))
                        {
                            System.out.println("Zadajte pozadovany vklad v €: ");
                            suma=conIN.nextDouble();
                            if(suma>=0)
                            {
                                ucet.get(hladanyUcet).setVklad(suma);
                            }
                            else
                            {
                                System.out.println("Zadali ste ZAPORNU hodnotu!");
                            }
                        }

                        else
                        {
                            System.out.println("Zadane cislo uctu NEEXISTUJE!");
                        }
                        break;
                    case 4:
                        System.out.println("Zadajte cislo uctu: ");
                        hladanyUcet = conIN.nextInt();
                        if(hladanyUcet>0&&hladanyUcet<(cisloUctu-1))
                        {
                            System.out.println("Zadajte pozadovany vyber v €: ");
                            suma=conIN.nextDouble();
                            if(suma>=0&&suma<=(ucet.get(hladanyUcet).stavNaUcte))
                            {
                                ucet.get(hladanyUcet).setVyber(suma);
                            }
                            if(suma>(ucet.get(hladanyUcet).stavNaUcte))
                            {
                                System.out.println("Na ucte nemate dostatok penazi");
                            }
                            if(suma<0)
                            {
                                System.out.println("Zadali ste ZAPORNU hodnotu!");
                            }
                        }

                        else
                        {
                            System.out.println("Zadane cislo uctu NEEXISTUJE!");
                        }
                        break;
                    case 5:
                        System.out.println("Zadajte cislo uctu: ");
                        hladanyUcet = conIN.nextInt();
                        if(hladanyUcet>0&&hladanyUcet<(cisloUctu-1))
                        {
                            System.out.println("Cislo uctu je: "+ucet.get(hladanyUcet).cisloUctu);
                            System.out.println("Stav na ucte je: "+ucet.get(hladanyUcet).stavNaUcte +" €");
                        }

                        else
                        {
                            System.out.println("Zadane cislo uctu NEEXISTUJE!");
                        }    
                        break;
                    case 6:
                        System.out.println("Pocet uctov je: "+Ucet.pocetUctov);
                        System.out.println("Celkova hodnota vsetkych uctov je: "+Ucet.celkovaHodnotaUctov);
                        break;
                }
            }
            else
            {
                System.out.print("Zadali ste nespravne cislo.");
            }
        }
    }
}
