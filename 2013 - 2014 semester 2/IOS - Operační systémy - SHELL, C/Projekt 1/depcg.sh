#!/bin/bash

ARG_G=false
ARG_P=false
ARG_D=false
ARG_R=false

while getopts :gpd:r: ARG; do
	case "$ARG" in
	g) ARG_G=true;;
	p) ARG_P=true;;
	d) ARG_D=true;ARG_FUN_D=$OPTARG;;
	r) AEG_R=true;ARG_FUN_R=$OPTARG;;
	*) echo "Chyba!" >&2; exit 1;;
	esac
done

OBJDUMP=$(objdump -d -j .text ${!#})

VYSTUP=$(

while read -r LINE; do

	NACITANY_RIADOK=$(echo "$LINE")

	FUNKCIA=$(echo "$NACITANY_RIADOK" | sed -nr 's/^[a-z0-9]+ <([a-z.A-Z0-9_-]*)>:/\1/p')

	if [ "$FUNKCIA" != "" ]; then
		NAJDENA_FUNKCIA="$FUNKCIA"
		continue
	fi

	if [ "$ARG_P" = "true" ]; then
		VOLANIE=$(echo "$NACITANY_RIADOK" | sed -nr 's/[a-z0-9]+:.*callq .*<(.*)>/\1/p' )
	
	else 
		VOLANIE=$(echo "$NACITANY_RIADOK" | sed -nr 's/[a-z0-9]+:.*callq .*<([a-zA-Z0-9_-]*)>/\1/p')
	fi
	
	if [[ "$NAJDENA_FUNKCIA" = "$ARG_FUN_D" ]] && [[ "$VOLANIE" != "" ]] && [[ "$ARG_FUN_D" != "" ]]; then
		if [ "$ARG_G" = "true" ];then
			echo "$NAJDENA_FUNKCIA -> $VOLANIE;"
		else
			echo "$NAJDENA_FUNKCIA -> $VOLANIE"
		fi

	elif [[ "$VOLANIE" = "$ARG_FUN_R" ]] && [[ "$ARG_FUN_R" != "" ]]; then
		if [ "$ARG_G" = "true" ];then
			echo "$NAJDENA_FUNKCIA -> $VOLANIE;"
		else
			echo "$NAJDENA_FUNKCIA -> $VOLANIE"
		fi

	elif [[ "$ARG_FUN_D" = "" ]] && [[ "$ARG_FUN_R" = "" ]] && [[ "$VOLANIE" != "" ]]; then
		if [ "$ARG_G" = "true" ];then
			echo "$NAJDENA_FUNKCIA -> $VOLANIE;"
		else
			echo "$NAJDENA_FUNKCIA -> $VOLANIE"
		fi
	fi

done <<< "$OBJDUMP"

)
if [ "$ARG_G" = "true" ];then
	echo digraph CG {
	echo "$VYSTUP" | sed 's/'\@plt'/'_PLT'/g' | sort -u
	echo }
else
	echo "$VYSTUP" | sort -u
fi
