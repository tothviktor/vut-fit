#!/bin/bash

ARG_G=false
ARG_D=false
ARG_R=false

while getopts :gd:r: ARG; do
	case "$ARG" in
	g) ARG_G=true;;
	d) ARG_D=true;ARG_OBJ_D=$OPTARG;;
	r) ARG_R=true;ARG_OBJ_R=$OPTARG;;
	*) echo "Chyba!" >&2; exit 1;;
	esac
done

NM=$(nm *.o)

VYSTUP=$(

while read -r LINE; do
	NACITANY_RIADOK=$(echo "$LINE")
	

	FUNKCIA=$(echo "$NACITANY_RIADOK" | grep '.o:' |  sed -n 's/\([0-9a-zA-Z_]*\).*/\1/p')

	if [ "$FUNKCIA" != "" ]; then
		NAJDENA_FUNKCIA="$FUNKCIA"
		continue
	fi	
	
	if [ "$ARG_D" = "true" ]; then
		OBJ_T=$(echo "$NACITANY_RIADOK" | sed -n 's/^.* [TB] \([0-9a-zA-Z_]*\).*/\1/p')

		if [ "$OBJ_T" != "" ];then
			echo "$NAJDENA_FUNKCIA -> $OBJ_T"					
		fi
	elif [ "$ARG_R" = "true" ]; then 
		OBJ_U=$(echo "$NACITANY_RIADOK" | sed -n 's/^[U] \([0-9a-zA-Z_]*\).*/\1/p')
		
		if [ "$OBJ_U" != "" ];then
			echo "$NAJDENA_FUNKCIA -> $OBJ_U"
		fi
	fi

done <<< "$NM"
)
echo "$VYSTUP" | sort -u
