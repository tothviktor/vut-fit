#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/wait.h>

#define NEVYHOVUJUCE_CISLO -1

/*
makro k zavreti semaforov
Kompletne vycistenie pamati po semaforoch
Odmapujeme pamat
Zmazeme pamat a zavreme ju
Zavreme subor
  */
#define CLEAN_SEM \
    do {\
    sem_close(sem_na_molo);\
    sem_close(sem_finished);\
    sem_close(sem_molo_hackers);\
    sem_close(sem_molo_serfs);\
    sem_close(sem_boarding);\
    sem_close(sem_landing);\
    sem_close(sem_finish);\
    sem_close(sem_board_extra);\
    sem_close(sem_vystup);\
    \
    sem_unlink("/xtothv01_semafor1");\
    sem_unlink("/xtothv01_semafor2");\
    sem_unlink("/xtothv01_semafor3");\
    sem_unlink("/xtothv01_semafor4");\
    sem_unlink("/xtothv01_semafor5");\
    sem_unlink("/xtothv01_semafor6");\
    sem_unlink("/xtothv01_semafor7");\
    sem_unlink("/xtothv01_semafor8");\
    sem_unlink("/xtothv01_semafor9");\
    \
    munmap(sharedVar, sizeof(int));\
    munmap(sharedBoardHackers, sizeof(int));\
    munmap(sharedBoardSerfs, sizeof(int));\
    munmap(sharedOnShip, sizeof(int));\
    munmap(sharedFinished, sizeof(int));\
    \
    shm_unlink("/xtothv01_sharedMemory1");\
    shm_unlink("/xtothv01_sharedMemory2");\
    shm_unlink("/xtothv01_sharedMemory3");\
    shm_unlink("/xtothv01_sharedMemory4");\
    shm_unlink("/xtothv01_sharedMemory5");\
    \
    close(sharedMemory_fd1);\
    close(sharedMemory_fd2);\
    close(sharedMemory_fd3);\
    close(sharedMemory_fd4);\
    close(sharedMemory_fd5);\
    \
    fclose(subor);}\
    \
    while (0)

#define vystup(...) do {\
		sem_wait(sem_vystup);\
		fprintf(subor, __VA_ARGS__);\
		fflush(subor);\
		sem_post(sem_vystup);\
	} while(0)

// struktura pre parametry
typedef struct parametre{
    int pocetOsob;            //pocet HACKERS a SERFS
    int genHackers;           //max. doba po ktorom sa generuje novy HACKER
    int genSerfs;             //max. doba po ktorom sa generuje novy SERF
    int genDobaPlavby;        //max. doba PAVBY
}Tparam;

// soubor s vystupom procesov
FILE *subor = NULL;

//prototyp funkcii
void kontrolaParametrov(int argc, char *argv[], Tparam *parameter);
bool nacitajParametre(Tparam *parameter,char *argv []);
int string_to_number(char *retazec);
int nahodneCisloZIntervalu(int min, int max);
void help();
void otvorsubor();

//---------------------------------------------------Zaciatok MAIN-------------------------------------------------------

int main(int argc,char *argv[])
{
    /*
    stuktura do kotoreho neskor budu nacitane parametre
    */
    Tparam parameter;

    /*
    pri nevhodnych parametrov skonci program exit(1)
    pri vhodnych parametrov parametre sa zapisu do struktury parameter
    */
    kontrolaParametrov(argc,argv,&parameter);

    /*
    Vytvorenie semaforov
        param 1 - nazov semaforu zacinajuci /
        param 2 - flagy, O_CREAT - vytvori ho, O_EXCL - exkluzivne
        param 3 - prava pre subor citanie/zapis
        param 4 - pociatocna hodnota semaforu
    */
    sem_t *sem_na_molo = sem_open("/xtothv01_semafor1", O_CREAT | O_EXCL, 0644, 1);
    sem_t *sem_finished = sem_open("/xtothv01_semafor2", O_CREAT | O_EXCL, 0644, 1);
    sem_t *sem_molo_hackers = sem_open("/xtothv01_semafor3", O_CREAT | O_EXCL, 0644, 0);
    sem_t *sem_molo_serfs = sem_open("/xtothv01_semafor4", O_CREAT | O_EXCL, 0644, 0);
    sem_t *sem_boarding = sem_open("/xtothv01_semafor5", O_CREAT | O_EXCL, 0644, 0);
    sem_t *sem_landing = sem_open("/xtothv01_semafor6", O_CREAT | O_EXCL, 0644, 0);
    sem_t *sem_finish = sem_open("/xtothv01_semafor7", O_CREAT | O_EXCL, 0644, 0);
    sem_t *sem_board_extra = sem_open("/xtothv01_semafor8", O_CREAT | O_EXCL, 0644, 1);
    sem_t *sem_vystup = sem_open("/xtothv01_semafor9", O_CREAT | O_EXCL, 0644, 1);

    /*
    Vytvorime zdielanu pamat
        param 1 - nazov zdielanej pamate zacinajuci /
        param 2 - flagy, ako semafory, pribudol O_RDWR, ktory explicitne povoli citanie/zapis
        param 3 - prava pre citanie/zapis, bez O_RDWR to nema ziadny vyznam
    */
    int sharedMemory_fd1 = shm_open("/xtothv01_sharedMemory1", O_CREAT | O_EXCL | O_RDWR, 0644);
    int sharedMemory_fd2 = shm_open("/xtothv01_sharedMemory2", O_CREAT | O_EXCL | O_RDWR, 0644);
    int sharedMemory_fd3 = shm_open("/xtothv01_sharedMemory3", O_CREAT | O_EXCL | O_RDWR, 0644);
    int sharedMemory_fd4 = shm_open("/xtothv01_sharedMemory4", O_CREAT | O_EXCL | O_RDWR, 0644);
    int sharedMemory_fd5 = shm_open("/xtothv01_sharedMemory5", O_CREAT | O_EXCL | O_RDWR, 0644);

    /*
    Vytvorime si v nasej zdielanej pamati miesto pre 5*1 integer
    */
    ftruncate(sharedMemory_fd1, (sizeof(int)));
    ftruncate(sharedMemory_fd2, (sizeof(int)));
    ftruncate(sharedMemory_fd3, (sizeof(int)));
    ftruncate(sharedMemory_fd4, (sizeof(int)));
    ftruncate(sharedMemory_fd5, (sizeof(int)));
    /*
    Namapujeme zdielanu pamat do adresneho priestoru procesu
        param 1 - najlepsie NULL viz man mmap
        param 2 - velkost pamate
        param 3 - prava na tejto pamati
        param 4 - MAP_SHARED specifikuje ze namapovana pamat bude zdielana medzi childmi procesu
        param 5 - subor descriptor co vratil shm_open
        param 6 - offset, nechat 0, viz man mmap
    */
    int *sharedVar = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, sharedMemory_fd1, 0);
    int *sharedBoardHackers = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, sharedMemory_fd2, 0);
    int *sharedBoardSerfs = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, sharedMemory_fd3, 0);
    int *sharedOnShip = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, sharedMemory_fd4, 0);
    int *sharedFinished = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, sharedMemory_fd5, 0);

    // Nevalidne ukoncenie + O_EXCL => unlink aby dalsie spustenie ok
    if ((sharedVar == NULL) || (sharedVar == (void *) - 1))
    {
        sem_unlink("/xtothv01_semafor1");
        sem_unlink("/xtothv01_semafor2");
        sem_unlink("/xtothv01_semafor3");
        sem_unlink("/xtothv01_semafor4");
        sem_unlink("/xtothv01_semafor5");
        sem_unlink("/xtothv01_semafor6");
        sem_unlink("/xtothv01_semafor7");
        sem_unlink("/xtothv01_semafor8");
        sem_unlink("/xtothv01_semafor9");

        shm_unlink("/xtothv01_sharedMemory1");
        shm_unlink("/xtothv01_sharedMemory2");
        shm_unlink("/xtothv01_sharedMemory3");
        shm_unlink("/xtothv01_sharedMemory4");
        shm_unlink("/xtothv01_sharedMemory5");

        exit(2);
    }

    (*sharedVar)++;
    *sharedFinished = 2 * parameter.pocetOsob;

    otvorsubor();


    pid_t pidHackers = fork();

    if(pidHackers == -1)
    {
        CLEAN_SEM;              // zle nacitanie proccess ID  - dokoncit
        exit(2);
    }

    if(pidHackers == 0)               //CHILD - vytvorenie procesu, ktor� bude vytv�rat procesy HACKERS
    {
        //child process HACKERS

        // stackoverflow 8623131
        srand(time(NULL) ^ (getpid()<<16));

        //----------------------------------
        for (int iH = 1;  iH <= parameter.pocetOsob; ++iH)
        {
            usleep(nahodneCisloZIntervalu(0,parameter.genHackers) * 1000);

            pid_t pidChild = fork();
            if (pidChild == -1)
            {
                CLEAN_SEM;              // zle nacitanie proccess ID  - dokoncit
                exit(2);
            }
            if (pidChild > 0)          //PARENT - vytvaranie dalsich procesov
                continue;

            // stackoverflow 8623131
            srand(time(NULL) ^ (getpid()<<16));

            //CHILD
            vystup("%d: hacker: %d: started\n", (*sharedVar)++, iH);

            sem_wait(sem_na_molo);
            (*sharedBoardHackers)++;

            vystup("%d: hacker: %d: waiting for boarding: %d: %d\n", (*sharedVar)++, iH, (*sharedBoardHackers), (*sharedBoardSerfs));

            sem_wait(sem_board_extra);
            if ( (*sharedBoardSerfs >=4 ) || (*sharedBoardHackers >=4 ) || ( (*sharedBoardSerfs >=2) && (*sharedBoardHackers >=2) ) )
            {
                if (*sharedBoardHackers == 4)
                {
                    *sharedBoardHackers -= 4;
                    sem_post(sem_molo_hackers);
                    sem_post(sem_molo_hackers);
                    sem_post(sem_molo_hackers);
                    sem_post(sem_molo_hackers);
                }
                else if (*sharedBoardSerfs == 4)
                {
                    *sharedBoardSerfs -= 4;
                    sem_post(sem_molo_serfs);
                    sem_post(sem_molo_serfs);
                    sem_post(sem_molo_serfs);
                    sem_post(sem_molo_serfs);
                }
                else
                {
                    *sharedBoardSerfs -= 2;
                    sem_post(sem_molo_serfs);
                    sem_post(sem_molo_serfs);
                    *sharedBoardHackers -= 2;
                    sem_post(sem_molo_hackers);
                    sem_post(sem_molo_hackers);
                }

                *sharedOnShip = 0;
                sem_post(sem_boarding);
            }
            else
                sem_post(sem_na_molo);
            sem_post(sem_board_extra);

            sem_wait(sem_molo_hackers);

            vystup("%d: hacker: %d: boarding: %d: %d\n", (*sharedVar)++, iH, (*sharedBoardHackers), (*sharedBoardSerfs));

            sem_wait(sem_boarding);
            (*sharedOnShip)++;
            if (*sharedOnShip == 4)
            {
                vystup("%d: hacker: %d: captain\n", (*sharedVar)++, iH);

                usleep(nahodneCisloZIntervalu(0,parameter.genDobaPlavby) * 1000);

                sem_post(sem_landing);
                sem_post(sem_landing);
                sem_post(sem_landing);
            }
            else
            {
                vystup("%d: hacker: %d: member\n", (*sharedVar)++, iH);

                sem_post(sem_boarding);

                sem_wait(sem_landing);
            }

            vystup("%d: hacker: %d: landing: %d: %d\n", (*sharedVar)++, iH, (*sharedBoardHackers), (*sharedBoardSerfs));

            if (*sharedOnShip == 4)
                sem_post(sem_na_molo);

            sem_wait(sem_finished);
            (*sharedFinished)--;
            if (*sharedFinished == 0)
                sem_post(sem_finish);
            sem_post(sem_finished);

            sem_wait(sem_finish);

            vystup("%d: hacker: %d: finished\n", (*sharedVar)++, iH);

            sem_post(sem_finish);

            CLEAN_SEM;
            exit(0);
        }
        // Cakame na skoncenie vsetkych child procesov
        for (int iH = 1;  iH <= parameter.pocetOsob; ++iH)
            wait(NULL);

        CLEAN_SEM;
        exit(0);

        //----------------------------------
    }
    else if (pidHackers > 0)          //PARENT - vytvorenie procesu, ktor� bude vytv�rat procesy SERFS
    {
        pid_t pidSerfs = fork();

        if(pidSerfs == -1)
        {
            CLEAN_SEM;              // zle nacitanie proccess ID  - dokoncit
            exit(2);
        }
        else if(pidSerfs == 0)
        {
            //child process SERFS

            // stackoverflow 8623131
            srand(time(NULL) ^ (getpid()<<16));

            for ( int iS = 1; iS <= parameter.pocetOsob; ++iS)
            {
                usleep(nahodneCisloZIntervalu(0,parameter.genSerfs) * 1000);

                pid_t pidChild = fork();
                if (pidChild == -1)
                {
                    CLEAN_SEM;              // zle nacitanie proccess ID  - dokoncit
                    exit(2);
                }
                if (pidChild > 0)          //PARENT - vytvaranie dalsich procesov
                    continue;

                // stackoverflow 8623131
                srand(time(NULL) ^ (getpid()<<16));

                //CHILD
                vystup("%d: serf: %d: started\n", (*sharedVar)++, iS);

                sem_wait(sem_na_molo);
                (*sharedBoardSerfs)++;

                vystup("%d: serf: %d: waiting for boarding: %d: %d\n", (*sharedVar)++, iS, (*sharedBoardHackers), (*sharedBoardSerfs));

                sem_wait(sem_board_extra);
                if ( (*sharedBoardSerfs >=4 ) || (*sharedBoardHackers >=4 ) || ( (*sharedBoardSerfs >=2) && (*sharedBoardHackers >=2) ) )
                {
                    if (*sharedBoardHackers == 4)
                    {
                        *sharedBoardHackers -= 4;
                        sem_post(sem_molo_hackers);
                        sem_post(sem_molo_hackers);
                        sem_post(sem_molo_hackers);
                        sem_post(sem_molo_hackers);
                    }
                    else if (*sharedBoardSerfs == 4)
                    {
                        *sharedBoardSerfs -= 4;
                        sem_post(sem_molo_serfs);
                        sem_post(sem_molo_serfs);
                        sem_post(sem_molo_serfs);
                        sem_post(sem_molo_serfs);
                    }
                    else
                    {
                        *sharedBoardSerfs -= 2;
                        sem_post(sem_molo_serfs);
                        sem_post(sem_molo_serfs);
                        *sharedBoardHackers -= 2;
                        sem_post(sem_molo_hackers);
                        sem_post(sem_molo_hackers);
                    }

                    *sharedOnShip = 0;
                    sem_post(sem_boarding);
                }
                else
                    sem_post(sem_na_molo);
                sem_post(sem_board_extra);

                sem_wait(sem_molo_serfs);

                vystup("%d: serf: %d: boarding: %d: %d\n", (*sharedVar)++, iS, (*sharedBoardHackers), (*sharedBoardSerfs));

                sem_wait(sem_boarding);
                (*sharedOnShip)++;
                if (*sharedOnShip == 4)
                {
                    vystup("%d: serf: %d: captain\n", (*sharedVar)++, iS);

                    usleep(nahodneCisloZIntervalu(0,parameter.genDobaPlavby) * 1000);

                    sem_post(sem_landing);
                    sem_post(sem_landing);
                    sem_post(sem_landing);
                }
                else
                {
                    vystup("%d: serf: %d: member\n", (*sharedVar)++, iS);

                    sem_post(sem_boarding);

                    sem_wait(sem_landing);
                }

                vystup("%d: serf: %d: landing: %d: %d\n", (*sharedVar)++, iS, (*sharedBoardHackers), (*sharedBoardSerfs));

                if (*sharedOnShip == 4)
                    sem_post(sem_na_molo);

                sem_wait(sem_finished);
                (*sharedFinished)--;
                if (*sharedFinished == 0)
                    sem_post(sem_finish);
                sem_post(sem_finished);

                sem_wait(sem_finish);

                vystup("%d: serf: %d: finished\n", (*sharedVar)++, iS);

                sem_post(sem_finish);

                CLEAN_SEM;
                exit(0);
            }
            // Cakame na skoncenie vsetkych child procesov
            for ( int iS = 1; iS <= parameter.pocetOsob; ++iS)
                wait(NULL);

            CLEAN_SEM;
            exit(0);
        }

    }

    //-------------------------------------------------CISTENIE ZATVARANIE ATD. -------------------------------------------------

    // Cakame na skoncenie child procesov - pidHackers aj pidSerfs
    wait(NULL);
    wait(NULL);

    CLEAN_SEM;

    return 0;
}
//---------------------------------------------------KONIEC MAIN-------------------------------------------------------

void otvorsubor()
{
    subor=fopen("rivercrossing.out", "w+");
    if (subor == NULL)
    {
        exit(2);
    }
    setbuf(subor, NULL);
}

void kontrolaParametrov (int argc, char *argv[], Tparam *parameter)
{
    if(argc == 5)
    {
        if(true == nacitajParametre(parameter,argv))
        {
            //printf("parametre sa nacitali uspesne \n");
        }
        else
        {
            help();
            exit(1);
        }
    }
    else
    {
        help();
        exit(1);
    }
}

bool nacitajParametre(Tparam *parameter,char *argv [])
{
    int nacitanyArgument[4];

    for(int i=1; i<=4; i++ )
    {
        nacitanyArgument[i-1]=string_to_number(argv[i]);
        if(nacitanyArgument[i-1] == -1)
        {
            return false;
        }
    }

    //int arg= nacitanyArgument[0];

    if((nacitanyArgument[0])%2 == 0 && nacitanyArgument[0] > 0 &&
       nacitanyArgument[1] < 5001 &&
       nacitanyArgument[2] < 5001 &&
       nacitanyArgument[3] < 5001)
    {
        parameter -> pocetOsob = nacitanyArgument[0];
        parameter -> genHackers = nacitanyArgument[1];
        parameter -> genSerfs = nacitanyArgument[2];
        parameter -> genDobaPlavby = nacitanyArgument[3];
        return true;
    }
    else
    {
        return false;
    }

}

//retazec na cele c�slo, ktor� je v intervale 0 <= x <= 5000
int string_to_number(char *retazec)
{
    char *a;
    a=NULL;
    double cislo = strtod(retazec,&a);
    if(*a == 0 && cislo >= 0 && cislo < 5001)
    {
        int celeCislo = cislo;
        if( 1.0*(cislo-celeCislo) == 0.0)
        {
            return celeCislo;
        }
        else
        {
            return NEVYHOVUJUCE_CISLO;
        }
    }
    else
    {
        return NEVYHOVUJUCE_CISLO;
    }
}

int nahodneCisloZIntervalu(int min, int max)
{
    return ( rand() % ( max - min + 1 ) ) + min;
}

void help()
{
    printf( "IOS: Projekt 2 - River Crossing Problem\n");
    printf( "AUTOR:     Viktor Toth, xtothv01@stud.fit.vutbr.cz\n");
    printf( "POPIS:     Implementace modifikovaneho synchronizacniho problemu\n");
    printf( "POUZITIE:   ./rivercrossing [P] [H] [S] [R]\n");
    printf( "           - Vystup programu se zapise do souboru ./rivercrossing.out\n\n");
    printf( "POPIS PARAMETRU:\n\n");
    printf( " - Vsechny parametry jsou celociselne hodnoty\n");
    printf( " - Casove udaje parametru [H] a [S] jsou dany v miliseknudach\n\n");
    printf( " [P]  - pocet osob generovanych v kazdom kategorii\n");
    printf( " [H]  - maximalna hodnota doby, po ktorom je generovany novy proces hacker\n");
    printf( " [S]  - maximalna hodnota doby, po ktorom je generovany novy proces serf\n");
    printf( " [R]  - maximalna hodnota doby plavby\n\n");
    printf( " [P]  >  0 ||\t P  2  ==  0\n");
    printf( " [H]  >= 0 ||\t P < 5001\n");
    printf( " [S]  >= 0 ||\t P < 5001\n\n");
    printf( " [R]  >= 0 ||\t P < 5001\n\n\n");
    printf( "PRIKLAD:   ./rivercrossing 2 2 5 5\n");
    printf( "           ./rivercrossing 10 15 7 7\n");
}
