/****************************************
 *                                      *
 *  Projekt do predmetu IMS 2017/2018   *
 *  									*
 *        Celulárne automaty            *
 *     Simulovanie šírenia požiaru      *
 *                                      *
 *  autori: Viktor Toth (xtothv01)      *
 *          Adam Zivcak (xzivca03)      *
 *                                      *
 ****************************************/

#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <vector>
#include <math.h>

using namespace std;

#define PALIVO			0
#define HORIACA_PLOCHA	1
#define	DOBA_HORENIA	2
#define	SMER_VETRA		3

#define VVP	99			//Velmi   Vysoka Pravdepodobnost
#define VYP	75			//VYsoka         Pravdepodobnost
#define SVP	30			//Stredne Vysoka Pravdepodobnost
#define NIP 3			//NIzka          Pravdepodobnost
#define VNP	1			//Velmi   Nizka  Pravdepodobnost

const int Plocha_X=30;
const int Plocha_Y=30;
const int Plocha_Z=4;

// Funkcia skuma, ci okolo kontrolovanej plochy, ci hori/horia plochy
bool HoriOkolo (int les[Plocha_X][Plocha_Y][Plocha_Z],int i,int j);

// Osetrenie krajnych pozicii
bool KrajLavyHorny (int les[Plocha_X][Plocha_Y][Plocha_Z],int i,int j);
bool KrajPravyHorny (int les[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool KrajHorny (int les[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool KrajLavyDolny (int les[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool KrajLavy (int les[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool KrajPravyDolny (int les[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool KrajDolny (int les[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool KrajPravy (int les[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool KrajZiadny (int les[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);

// Pristup okolitym ploch
bool OkoloHornaLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool OkoloHorna (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool OkoloHornaPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool OkoloLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool OkoloPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool OkoloDolnaLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool OkoloDolna (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool OkoloDolnaPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);

// Plyv vietoru
bool Vietor (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j, int HL, int H, int HP, int L, int Stred, int P, int DL, int D, int DP);

// Identifikovanie smeru vetra
bool VietorHornaLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool VietorHorna (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool VietorHornaPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool VietorLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool VietorNefuka (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool VietorPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool VietorDolnaLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool VietorDolna (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);
bool VietorDolnaPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j);

bool Pravdepodobnost(int pravdepodobnost);

int main(int argc, char *argv[])
{
	
	if(argc < 2)
	{
		cout<< "Program je potrebne spustit s parametrom "<<"\033[1m"<<"nazvu testovacieho suboru"<<"\033[0m"<<".\nVolitelny parameter je "<<"\033[1m"<<"-g "<<"\033[0m"<<",ktory zapne graficke znazornenie simulacie"<<endl;
		cout<< "Priklad spustenia: ./IMS_Celularni_automaty testy/test4b -g"<<endl;
		exit(0);
	}
	
	srand((unsigned) time(NULL));   // zabezpečí, že po každom spustení programu sa bude generovať vždy iná postupnosť čísel
	
	double statiskikaDobaHorenia = 0;
	double statiskikaZhorenaPlocha = 0;
	double statiskikaVelkostSkody = 0;
	double statiskikaNedotknutaPlocha = 0;
	
	int nehorlavaPlocha = 0;
	int cenaPlochy = 0;
	
	for (int cisloSimulacie=1; cisloSimulacie <= 10; cisloSimulacie++)
	{
		/*
		Parametre lesNovy
			[x][y][z]
			[x][y][0] - Palivo ( -1, 0 - 9 )     
			[x][y][1] - horiaca plocha ( 0 - 1 )
			[x][y][2] - doba horenia ( 0 - 9 )
			[x][y][3] - smer vetra ( 1 - 9 ) ( 5 nefuka )
		*/
		
		int lesNovy [Plocha_X][Plocha_Y][Plocha_Z];
		int les [Plocha_X][Plocha_Y][Plocha_Z];
		
		bool rozdielne = true;
		
		int dobaHorenia = 0;
		int zhorenaPlocha = 0;
		int velkostSkody = 0;
		
		std::vector<int> cisla;
		
		ifstream inputFile(argv[1]);        // Nacitavanie subor-u
		
		if (inputFile.good())
		{
			int aktualneCislo = 0;
			while (inputFile >> aktualneCislo)
			{
				cisla.push_back(aktualneCislo);
			}
			inputFile.close();
		
			for (int x = 0, i = 0; x < Plocha_Y; x++)
			{
				for (int y  = 0; y < Plocha_X; y++)
				{
					for (int z = 0; z < Plocha_Z; z++)
					{
						lesNovy[x][y][z] = cisla[i];
						i++;
					}
				}
			}
		}
		else 
		{
			cout << "Chyba pri nacitani suboru-u! --> Program konci!\n";
			exit(0);
		}
		
		copy(&lesNovy[0][0][0], &lesNovy[0][0][0]+Plocha_X*Plocha_Y*Plocha_Z, &les[0][0][0]); 		// kopirovanie mapy
		
		for(int i = 0; i < Plocha_X; i++)
		{
			for(int j = 0; j < Plocha_Y; j++)
			{
				if (les[i][j][PALIVO] != -1)
				{
					cenaPlochy += les[i][j][PALIVO];
				}
				else
				{
					nehorlavaPlocha++;
				}
			}	
		}
		
		while(rozdielne)
		{
			for (int i = 0; i < Plocha_X; i++)
			{
				for (int j = 0; j < Plocha_Y; j++)
				{
					if ( les[i][j][PALIVO] > 0)					// HORLAVY ?
					{
						if( les[i][j][HORIACA_PLOCHA] == 0)		// Nehori -> Okolo hori ?
						{
							if (HoriOkolo(les,i,j))
							{
								lesNovy[i][j][HORIACA_PLOCHA] = 1; 
							}
						}                                                           
						else if ( les[i][j][HORIACA_PLOCHA] == 1)							// Hori       
						{                                                           
							lesNovy[i][j][PALIVO] = les[i][j][PALIVO] - 1;  				// horlave palivo sa mina      
							lesNovy[i][j][DOBA_HORENIA] = les[i][j][DOBA_HORENIA] + 1;		// doba horenia narasta            
																					
							if ( lesNovy[i][j][PALIVO] == 0)								// Ak Dohorel   
							{                                                       
								lesNovy[i][j][HORIACA_PLOCHA] = 0;                               
							}                                                       
						}                                                           
					}                                                                    
				}                                                                   
			}                                                                       
		
			// -------------------- KONTROLA / POROVNANIE NOVE vs. STARTE POLE/LES --------------------------------------
			rozdielne = false;                                                      
			dobaHorenia++;                                                          
																				
			for(int i = 0; i < Plocha_X; i++)                                       
			{                                                                       
				for(int j = 0; j < Plocha_Y; j++)                                        
				{                                                                   																
					if ( memcmp((void*)les[i][j],(void*)lesNovy[i][j], Plocha_Z * sizeof(int)) != 0)
					{                                                               
						rozdielne = true;                                           
						break;                                                      
					}                                                               
				}                                                                   
				if(rozdielne == true)
				{
					break;
				}
			}
			
			// ------------------------ VYPIS / STATISTIKA ------------------------------------------------------------------
			if (argc == 3 && (strcmp(argv[2],"-g")) == 0)
			{
			cout<<endl<< "\033[1;30;47m"<<dobaHorenia<<". Stav horenia:"<<"\033[0m"<<"\n\n";
			for(int i = 0; i < Plocha_X; i++)
			{
				for(int j = 0; j < Plocha_Y; j++)		// Zostavajuce palivo
				{				
					if (les[i][j][PALIVO] == -1)
					{
						cout << "\033[34m"<< "\u25A0"<<"\033[0m ";
					}
					else if (les[i][j][PALIVO] == 0)
					{
						cout << "\033[37m"<< "\u25A0"<<"\033[0m ";	// http://www.theasciicode.com.ar/extended-ascii-code/black-square-ascii-code-254.html
					}
					else if(les[i][j][PALIVO] >= 1 && les[i][j][HORIACA_PLOCHA] == 1)
					{
						cout << "\033[31m"<< "\u25A0"<<"\033[0m ";   // https://stackoverflow.com/questions/2616906/how-do-i-output-coloured-text-to-a-linux-terminal
					}
					else 
					{
						cout << "\033[32m"<< "\u25A0"<<"\033[0m ";
					}
				}
				cout<<endl;
			}
			}			
			copy(&lesNovy[0][0][0], &lesNovy[0][0][0]+Plocha_X*Plocha_Y*Plocha_Z, &les[0][0][0]);   // Stary les/pole nahradim novym 
		}

		for(int i = 0; i < Plocha_X; i++)
		{
			for(int j = 0; j < Plocha_Y; j++)
			{
				if (les[i][j][DOBA_HORENIA] > 0)
				{
					zhorenaPlocha++;
					velkostSkody +=  les[i][j][DOBA_HORENIA];
				}
			}	
		}
		
		statiskikaDobaHorenia += dobaHorenia;
		statiskikaNedotknutaPlocha += Plocha_X*Plocha_Y-zhorenaPlocha;
		statiskikaZhorenaPlocha += zhorenaPlocha;
		statiskikaVelkostSkody += velkostSkody;
		
		cout<<"_______________________________________________________________\n\n";
		cout<<"\033[1;30;47m\tVYSLEDOK "<< cisloSimulacie <<". SIMULACIE:\n";
		cout<<"Doba horenia je:\t\t\t"<<dobaHorenia<<endl;
		cout<<"Nedotknuta plocha je:\t\t\t"<<Plocha_X*Plocha_Y-zhorenaPlocha<<endl;
		cout<<"Zhorena plocha je:\t\t\t"<<zhorenaPlocha<<endl;
		cout<<"Velkost skody je:\t\t\t"<<velkostSkody<<"\033[0m"<<endl;

	}
	
	cout<<"_______________________________________________________________\n\n"<< "\033[1;30;47m\tZHRNUTIE:\n\n";
	cout<<"Velkost plochy je:\t\t\t"<<Plocha_X<<"x"<<Plocha_Y<<" -> "<<Plocha_X*Plocha_Y<<endl;
	cout<<"Z toho:"<<endl;
	cout<<"\tPocet nehorlavych ploch:\t"<<nehorlavaPlocha/10<<endl;
	cout<<"\tPocet horlavych ploch:\t\t"<<(Plocha_X*Plocha_Y)-(nehorlavaPlocha/10)<<endl;
	cout<<"\t\"Cena\" plochy je:\t\t"<<cenaPlochy/10<<endl<<endl;
	
	cout<<"Priemerna doba horenia je:\t\t"<<(statiskikaDobaHorenia/10)<<endl;
	cout<<"Priemere nedotknuta plocha je:\t\t"<<(statiskikaNedotknutaPlocha/10)<<"\t-> "<< round(10*((100*statiskikaNedotknutaPlocha)/10)/(Plocha_X*Plocha_Y))/10 <<" %"<<endl;
	cout<<"Priemere zhorena plocha je:\t\t"<<(statiskikaZhorenaPlocha/10)<<"\t-> "<< round(10*((100*statiskikaZhorenaPlocha)/10)/(Plocha_X*Plocha_Y))/10 <<" %"<<endl;
	cout<<"Priemere velkost skody je:\t\t"<<(statiskikaVelkostSkody/10)<<"\t-> "<< round(10*((100*statiskikaVelkostSkody)/10)/(cenaPlochy/10))/10 <<" %"<<"\033[0m"<<endl;
	
	return 0;
}

bool HoriOkolo(int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{
	if (i == 0 && j == 0)									// + 0 0
	{                                                       // 0 0 0
		return KrajLavyHorny(les,i,j);  	                // 0 0 0                                                                                               
	}                                                       
	else if (i == 0 && j == Plocha_Y-1)                     // 0 0 +                                                        
	{                                                       // 0 0 0                                                        															 
		return KrajPravyHorny(les,i,j);						// 0 0 0                           
	}                                                                                                                     
	else if (i == 0)                                        // 0 + 0                                                              
	{                                                       // 0 0 0                                                              
        return KrajHorny(les,i,j);		                    // 0 0 0	                         
	}                                                                                                                      
	else if (i == Plocha_X-1 && j == 0)                     // 0 0 0                                                               
	{                                                       // 0 0 0                                                               
        return KrajLavyDolny(les,i,j); 		                // + 0 0                          
	}                                                                                                                       
	else if (j == 0)                                        // 0 0 0                                                                   
	{                                                       // + 0 0     	                                                                
        return KrajLavy(les,i,j);		                    // 0 0 0                           
	}                                                                                                                       
	else if (i == Plocha_X-1 && j == Plocha_Y-1)            // 0 0 0                                                                
	{                                                       // 0 0 0                                                                
        return KrajPravyDolny(les,i,j);		                // 0 0 +	                           
	}						                                                                                                
	else if (i == Plocha_X-1)                               // 0 0 0                                                                
	{                                                       // 0 0 0                                                                
        return KrajDolny(les,i,j);        		            // 0 + 0                         
	}						                                                                                                
	else if (j == Plocha_Y-1)                               // 0 0 0                                                                
	{                                                       // 0 0 +                                                                
	    return KrajPravy(les,i,j);                          // 0 0 0	                           
	}				                                                                                                                
	else                                                    // 0 0 0                                                                
	{                                                       // 0 + 0                                                                
	     return KrajZiadny(les,i,j);                        // 0 0 0			                         
	}
}

bool KrajLavyHorny (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{                                                                                        
	return (OkoloPrava(les,i,j) || OkoloDolna(les,i,j) || OkoloDolnaPrava(les,i,j));      
}
bool KrajPravyHorny (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{	
	return (OkoloLava(les,i,j) || OkoloDolnaLava(les,i,j) || OkoloDolna(les,i,j));		
}
bool KrajHorny (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{	
	return (OkoloLava(les,i,j) || OkoloPrava(les,i,j) || OkoloDolnaLava(les,i,j) || OkoloDolna(les,i,j) || OkoloDolnaPrava(les,i,j));		
}
bool KrajLavyDolny (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{
	return (OkoloHorna(les,i,j) || OkoloHornaPrava(les,i,j) || OkoloPrava(les,i,j));		
}
bool KrajLavy (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{
	return (OkoloHorna(les,i,j) || OkoloHornaPrava(les,i,j) || OkoloPrava(les,i,j) || OkoloDolna(les,i,j) || OkoloDolnaPrava(les,i,j));
}
bool KrajPravyDolny (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{                             	
	return (OkoloHornaLava(les,i,j) || OkoloHorna(les,i,j) || OkoloLava(les,i,j));                                	  
}
bool KrajDolny (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{      	
	return (OkoloHornaLava(les,i,j) || OkoloHorna(les,i,j) || OkoloHornaPrava(les,i,j) || OkoloLava(les,i,j) || OkoloPrava(les,i,j));        	
}
bool KrajPravy (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{ 	   
	return (OkoloHornaLava(les,i,j) || OkoloHorna(les,i,j) || OkoloLava(les,i,j) || OkoloDolnaLava(les,i,j) || OkoloDolna(les,i,j));    	                  
}
bool KrajZiadny (int les[Plocha_X][Plocha_Y][Plocha_Z],int i, int j)
{	
	return (OkoloHornaLava(les,i,j) || OkoloHorna(les,i,j) || OkoloHornaPrava(les,i,j) || OkoloLava(les,i,j) || OkoloPrava(les,i,j) || OkoloDolnaLava(les,i,j) || OkoloDolna(les,i,j) || OkoloDolnaPrava(les,i,j));		
}

// ---------------- Skumanie ploch okolo kontrolovaneho bodu --------------------------------
bool OkoloHornaLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	if (bod[i-1][j-1][HORIACA_PLOCHA] != 0)									// VVP VYP SVP			// + 0 0
	{                                                                       // VYP VYP NIP          // 0 0 0
		return Vietor(bod,i-1,j-1,VVP,VYP,SVP,VYP,VYP,NIP,SVP,NIP,VNP);     // SVP NIP VNP          // 0 0 0
	}
	return false;
}                                                                                                   
bool OkoloHorna (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{                                                                                                   
	if (bod[i-1][j][HORIACA_PLOCHA] != 0)									// VYP VVP VYP          // 0 + 0
	{                                                                       // SVP VYP SVP          // 0 0 0
		return Vietor(bod,i-1,j,VYP,VVP,VYP,SVP,VYP,SVP,NIP,VNP,NIP);       // NIP VNP NIP          // 0 0 0
	}                                                                                                       
	return false;                                                                                   
}                                                                                                   
bool OkoloHornaPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)                                  
{                                                                                                  
	if (bod[i-1][j+1][HORIACA_PLOCHA] != 0)									// SVP VYP VVP          // 0 0 +
	{                                                                       // NIP VYP VYP          // 0 0 0
		return Vietor(bod,i-1,j+1,SVP,VYP,VVP,NIP,VYP,VYP,VNP,NIP,SVP);     // VNP NIP SVP          // 0 0 0         
	}                                                                                               
	return false;                                                                                   
}                                                                                                   
bool OkoloLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)                                
{                                                                                                   
	if (bod[i][j-1][HORIACA_PLOCHA] != 0)									// VYP SVP NIP         	// 0 0 0 
	{                                                                       // VVP VYP VNP         	// + 0 0 
		return Vietor (bod,i,j-1,VYP,SVP,NIP,VVP,VYP,VNP,VYP,SVP,NIP);      // VYP SVP NIP         	// 0 0 0 
	}                                                                                               
	return false;                                                                                   
}                                                                                                   
bool OkoloPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)                               
{                                                                                                   
	if (bod[i][j+1][HORIACA_PLOCHA] != 0)									// NIP SVP VYP       	// 0 0 0 
	{                                                                       // VNP VYP VVP       	// 0 0 + 
		return Vietor (bod,i,j+1,NIP,SVP,VYP,VNP,VYP,VVP,NIP,SVP,VYP);      // NIP SVP VYP       	// 0 0 0 
	}                                                                                            	 
	return false;                                                                                	 
}                                                                                                	 
bool OkoloDolnaLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)                        	 
{                                                                                                	 
	if (bod[i+1][j-1][HORIACA_PLOCHA] != 0)									// SVP NIP VNP       	 // 0 0 0
	{                                                                       // VYP VYP NIP       	 // 0 0 0
		return Vietor (bod,i+1,i-1,SVP,NIP,VNP,VYP,VYP,NIP,VVP,VYP,SVP);    // VVP VYP SVP       	 // + 0 0
	}                                                                                            	 
	return false;                                                                                	 
}                                                                                                	 
bool OkoloDolna (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)                            	 
{                                                                                                	         
	if (bod[i+1][j][HORIACA_PLOCHA] != 0)									// NIP VNP NIP       	 // 0 0 0
	{                                                                       // SVP VYP SVP       	 // 0 0 0
		return Vietor (bod,i+1,j, NIP,VNP,NIP,SVP,VYP,SVP,VYP,VVP,VYP);     // VYP VVP VYP       	 // 0 + 0
	}                                                                                            	         
	return false;                                                                                	 
}                                                                                                	 
bool OkoloDolnaPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)                       	 
{                                                                                                	         
	if (bod[i+1][j+1][HORIACA_PLOCHA] != 0)									// VNP NIP SVP       	 // 0 0 0
	{                                                                       // NIP VYP VYP       	 // 0 0 0
		return Vietor (bod,i+1,j+1, VNP,NIP,SVP,NIP,VYP,VYP,SVP,VYP,VVP);   // SVP VYP VVP       	 // 0 0 +
	}                                                                                                       
	return false;                                                                                   
}                                                                                                   
                                                                                                    
bool Vietor (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j, int HL, int H, int HP, int L, int Stred, int P, int DL, int D, int DP)
{
	if (VietorHornaLava (bod,i,j) && Pravdepodobnost(HL))
	{
		return true;
	}
	else if (VietorHorna (bod,i,j) && Pravdepodobnost(H))
	{
		return true;
	}
	else if (VietorHornaPrava (bod,i,j) && Pravdepodobnost(HP))
	{
		return true;
	}
	else if (VietorLava (bod,i,j) && Pravdepodobnost(L))
	{
		return true;
	}
	else if (VietorNefuka (bod,i,j) && Pravdepodobnost(Stred))
	{
		return true;
	}
	else if (VietorPrava (bod,i,j) && Pravdepodobnost(P))
	{
		return true;
	}
	else if (VietorDolnaLava (bod,i,j) && Pravdepodobnost(DL))
	{
		return true;
	}
	else if (VietorDolna (bod,i,j) && Pravdepodobnost(D))
	{
		return true;
	}
	else if (VietorDolnaPrava (bod,i,j) && Pravdepodobnost(DP))
	{
		return true;
	}
	return false;
}

bool VietorHornaLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 1);
}
bool VietorHorna (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 2);
}
bool VietorHornaPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 3);
}
bool VietorLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 4);
}
bool VietorNefuka (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 5);
}
bool VietorPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 6);
}
bool VietorDolnaLava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 7);
}
bool VietorDolna (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 8);
}
bool VietorDolnaPrava (int bod[Plocha_X][Plocha_Y][Plocha_Z], int i, int j)
{
	return (bod[i][j][SMER_VETRA] == 9);
}

bool Pravdepodobnost (int pravdepod) 
{
	if((rand() % 100) < pravdepod)
	{
		return true;
	}
	else
	{
		return false;
	}
}














