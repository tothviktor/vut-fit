#include <iostream>
#include <string>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <time.h>
#include <ctime>
#include <algorithm>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <errno.h>

#define SEKUNDA 1000000

using namespace std;

// kontrola, ci IPv4 adresa je validna
bool je_ipv4_adresa(const string& str)
{
    struct sockaddr_in sa;
    return inet_pton(AF_INET, str.c_str(), &(sa.sin_addr))!=0;
}
// kontrola, ci IPv6 adresa je validna
bool je_ipv6_adresa(const string& str)
{
    struct sockaddr_in6 sa;
    return inet_pton(AF_INET6, str.c_str(), &(sa.sin6_addr))!=0;

}

// kontrola na spravnost argumentov
bool kontrolaParametrov2(char const *argv[])
{
	if((std::string(argv[1]) == "-s" && std::string(argv[3]) == "-i" && std::string(argv[5]) == "-f" ) ||
	   (std::string(argv[1]) == "-s" && std::string(argv[5]) == "-i" && std::string(argv[3]) == "-f" ) ||
	   (std::string(argv[3]) == "-s" && std::string(argv[1]) == "-i" && std::string(argv[5]) == "-f" ) ||
	   (std::string(argv[3]) == "-s" && std::string(argv[5]) == "-i" && std::string(argv[1]) == "-f" ) ||
	   (std::string(argv[5]) == "-s" && std::string(argv[1]) == "-i" && std::string(argv[3]) == "-f" ) ||
	   (std::string(argv[5]) == "-s" && std::string(argv[3]) == "-i" && std::string(argv[1]) == "-f" ))
	{
	   return true;
	}
	else
		return false;
}		

// kontola na pocet argumentov a volanie funkcie kontrolaParametrov2
bool kontrolaParametrov(int argc, char const *argv[])
{
	if (argc == 7) 	
	{
		if (kontrolaParametrov2 (argv) == true)
			return true;
		return false;
	}
	else
		return false;
}

// nacita parameter podla argumentu
string priradParameter(char const *argv[], const char *param)
{
	for (int i = 1; i<=5; i=i+2)
		if (std::string(argv[i]) == param)
			return argv[i+1];
}

// z retazca nacita vsetky podretazce (aplikácie/filtry) ktore su rozdelene zo znakom ","
void rozsekajFilter(string retazec, string Aplikacie[], int pocetAplikacii)
{
	std::size_t pozicia;
	
	for(int i=0; i<=pocetAplikacii;i++)
	{
		pozicia = retazec.find(",");
		Aplikacie[i] = retazec.substr (0,pozicia);
		retazec.erase(0,pozicia+1);
	}
}

// pomocna funkcia na posielanie dat
int resolvehelper(const char* hostname, int family, const char* service, sockaddr_storage* pAddr)
{
    int result;
    addrinfo* result_list = NULL;
    addrinfo hints = {};
    hints.ai_family = family;
    hints.ai_socktype = SOCK_DGRAM;
    result = getaddrinfo(hostname, service, &hints, &result_list);
    if (result == 0)
    {
        memcpy(pAddr, result_list->ai_addr, result_list->ai_addrlen);
        freeaddrinfo(result_list);
    }

    return result;
}

//  -------------------- HLAVNY PROGRAM -------------------------
int main(int argc, char const *argv[])
{
	std::string IPadresa;		// IP ADRESA SYSLOG SERVERA, na ktorý sa budú odosielať správy. UDP port bude vždy 514.
	int interval; 				// hodnota v SEKUNDACH, ako často sa kontroluje zoznam bežiacich aplikácií/služieb (min. hodnota 1)
	std::string filter; 		// ZOZNAM APLIKACII, ktoré chceme detegovať. napr.: "vlc,firefox,filezilla". (bez uvodzoviek)
	
	if (kontrolaParametrov(argc, argv) != true)
	{		
		fprintf(stderr, "Zle parametre\n");
		return -1;
	}	
	
	IPadresa = priradParameter(argv,"-s");
	interval = atoi(priradParameter(argv,"-i").c_str());
	filter = priradParameter(argv,"-f");
	
	if ( false  == (je_ipv4_adresa(IPadresa) || je_ipv6_adresa(IPadresa)))
	{		
		fprintf(stderr, "Zle parametre - nespravny format IP adresy \n");
		return -1;
	}		

	int pocetAplikacii = std::count(filter.begin(), filter.end(), ',');  //pocet aplikacii v parametre napr.: vlc,bropbox - > 2
	std::string filtri[pocetAplikacii+1];
	rozsekajFilter(filter, filtri, pocetAplikacii);	// nacitam do pola filtri aplikacie
	
	std::string nacitaneSpojenia="";	// aktualne nacitane spojenie ( jeden riadok )
	std::string ulozeneSpojenia = "";	// vsetky nacitane spojenia pocas behu programu, kazde spojenie je ulozene IBA RAZ
	std::string kontrolaAplikacie = "";	// aplikacie nacitana zo spojenia
	
	FILE *in;
	int result = 0;
    int sock = socket(AF_INET, SOCK_DGRAM, 0);

    sockaddr_in addrListen = {};
    addrListen.sin_family = AF_INET;
    result = bind(sock, (sockaddr*)&addrListen, sizeof(addrListen));
    if (result == -1)
    {
       int lasterror = errno;
       std::cout << "error: " << lasterror;
       exit(1);
    }

    sockaddr_storage addrDest = {};
    result = resolvehelper(IPadresa.c_str(), AF_INET, "514", &addrDest);
	
    if (result != 0)
    {
       int lasterror = errno;
       std::cout << "error: " << lasterror;
       exit(1);
    }
	
	// nekonecny cyklus programu
	while(true) 
	{
		// [sluzba], v zatvorke [pocet znakov] --> protokol (3) + IPv6 adresa (40) + port (6) + IP adresav6 (40) + port (6) + meno aplikacie (55) -> 150    
		char buff[150]; // obsahuje jeden riadok nacitaneho spojenia
		// if( !(in = popen("netstat -atunp 2>/dev/null | grep ESTABLISHED | awk '{print $1,$4,$5,$7}'| sed 's/:\([0-9]* \)/ \1/g' | sed 's/\//\/ /' | awk '{print $1,$2,$3,$4,$5,$7}' | sed 's/^\(...\)./\1 /g' | sed 's/  / /'", "r")))
		
		// nacitanie a vyfiltrovanie spojenia podla kriterii
		if( !(in = popen("netstat -atunp 2>/dev/null | grep ESTABLISHED | awk '{print $1,\t$4,\t$5,\t$7}'| sed 's/:\\([0-9]* \\)/ \\1/g' | sed 's/\\//\\/ /' | awk '{print $1,\t$2,\t$3,\t$4,\t$5,\t$7}' | sed 's/^\\(...\\)./\\1 /g' | sed 's/  / /'", "r")))
		{
			cout<<"CHYBA Koniec programu\n";
			return 0;
		}
		while(fgets(buff,sizeof(buff), in) != NULL)
		{
			nacitaneSpojenia = buff;
			if (ulozeneSpojenia.find(nacitaneSpojenia) == std::string::npos)
			{
				kontrolaAplikacie = nacitaneSpojenia;
				kontrolaAplikacie.erase (0,nacitaneSpojenia.find_last_of( " " )+1);
				
				for (int i=0; i<=pocetAplikacii;i++)
				{
					if (kontrolaAplikacie.find(filtri[i]) != std::string::npos)
					{
						cout<< nacitaneSpojenia;	// vypis na stdout
						sendto(sock, nacitaneSpojenia.c_str(), strlen(nacitaneSpojenia.c_str()), 0, (sockaddr*)&addrDest, sizeof(addrDest));	// posielanie na syslog server
						break;
					}
				}
				ulozeneSpojenia = ulozeneSpojenia+nacitaneSpojenia; // ukladanie spojeni do retazca (vysledok je zoznam vsetkych spojeni)
			}
		}
		pclose(in);		
		usleep(interval * SEKUNDA); // opakovanie cyklu kazdych (interval) sekund
	}
	cout<<"Koniec programu\n";
	return 0;
}